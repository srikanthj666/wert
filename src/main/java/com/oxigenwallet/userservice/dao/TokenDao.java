/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.oxigenwallet.userservice.dao;

import com.oxigenwallet.userservice.common.json.request.DeviceInfo;
import com.oxigenwallet.userservice.model.Token;
import java.sql.Timestamp;
import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Disjunction;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author sagarsharma
 */
@Repository
@Transactional
public class TokenDao {
    
    @Autowired
    private SessionFactory _sessionFactory;
    
    private Session getSession() {
        return _sessionFactory.getCurrentSession();
    }

    /**
     *
     * @param token
     */
    public void save(Token token) {
        getSession().save(token);
    }

    /**
     *
     * @param token
     */
    public void update(Token token) {
        getSession().update(token);
    }
    
    public List<Token> getAllActiveToken() {
        Criteria cr = getSession().createCriteria(Token.class);
        
        cr.add(Restrictions.ge("expired_at", new Timestamp(System.currentTimeMillis())));
        
        List<Token> resultList = cr.list();
        if (resultList.size() == 0) {
            return null;
        }
        return resultList;
    }

    public Token getToken(String token) {
        Criteria cr = getSession().createCriteria(Token.class);
        
        cr.add(Restrictions.eq("auth_code", token));
        
        List<Token> resultList = cr.list();
        if (resultList.size() == 0) {
            return null;
        }
        return resultList.get(0);
    }

    public Token getDeviceToken(String token, DeviceInfo deviceInfo){
        Criteria cr = getSession().createCriteria(Token.class);

        cr.add(Restrictions.eq("auth_code", token));
        if(deviceInfo.getDeviceId()!=null && !deviceInfo.getDeviceId().isEmpty()){
            cr.add(Restrictions.eq("device_id", deviceInfo.getDeviceId()));
        }

        List<Token> resultList = cr.list();
        if (resultList.size() == 0) {
            return null;
        }
        return resultList.get(0);
    }

    public void updateOnDelete(String token) {
        
        Query q = getSession().createQuery("update Token set is_active = :newVal where token = :token");
        q.setBoolean("newVal", false);
        q.setString("token", token);
        
        q.executeUpdate();
        
    }

    public boolean isDeviceAccessTokenExist(String token, DeviceInfo deviceInfo) {
        Criteria cr = getSession().createCriteria(Token.class);

        Long currentTime = System.currentTimeMillis();
        if(deviceInfo.getDeviceId()!=null && !deviceInfo.getDeviceId().trim().isEmpty()){
            cr.add(
                    Restrictions.and(
                            Restrictions.eq("token", token),
                            Restrictions.eqOrIsNull("device_id", deviceInfo.getDeviceId().trim()),
                            Restrictions.gt("expired_at", new Timestamp(currentTime))
                    )
            );
        }else{
            cr.add(Restrictions.eq("token", token));
        }

        List<Token> resultList = cr.list();
        if (resultList.size() == 0) {
            return false;
        }
        return true;
    }
}
