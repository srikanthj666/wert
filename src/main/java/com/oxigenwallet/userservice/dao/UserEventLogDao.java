package com.oxigenwallet.userservice.dao;

import com.oxigenwallet.userservice.model.User;
import com.oxigenwallet.userservice.model.UserEventLogs;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
@Transactional
public class UserEventLogDao {

    @Autowired
    private SessionFactory _sessionFactory;

    private Session getSession() {
        return _sessionFactory.getCurrentSession();
    }

    public boolean SaveUserAuditLogs(User user) {

        UserEventLogs userEventLogs = new UserEventLogs();
        userEventLogs.setUsername(user.getUsername());
        userEventLogs.setType(user.getProfile_type());
        userEventLogs.setFirst_name(user.getFirst_name());
        userEventLogs.setLast_name(user.getLast_name());
        userEventLogs.setMiddle_name(user.getMiddle_name());
        userEventLogs.setDob(user.getDob());
        userEventLogs.setEmail(user.getEmail());
        userEventLogs.setGender(user.getGender());
        userEventLogs.setAddress(user.getAddress());
        userEventLogs.setCity(user.getCity());
        userEventLogs.setDistrict(user.getDistrict());
      //  userEventLogs.setZip(Integer.toString(user.getZip()));
        userEventLogs.setState(user.getState());
        userEventLogs.setCountry(user.getCountry());
        userEventLogs.setLandline(user.getLandline());

        //Logging users date
        getSession().save(userEventLogs);
        return false;
    }
}
