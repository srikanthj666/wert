/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.oxigenwallet.userservice.dao;

import com.oxigenwallet.userservice.common.constants.Constants;
import com.oxigenwallet.userservice.common.json.request.JsonRequestBody;
import com.oxigenwallet.userservice.common.json.response.JsonResponseBody;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import com.oxigenwallet.userservice.model.User;

import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;

/**
 * @author mitz
 */
@Repository
@Transactional
public class UserDao {

    @Autowired
    private SessionFactory _sessionFactory;

    private Session getSession() {
        return _sessionFactory.getCurrentSession();
    }

    /**
     * @param username
     * @return
     */
    public User getByMdn(String username) {

        Criteria cr = getSession().createCriteria(User.class);
        cr.add(Restrictions.eq("username", username));
        cr.add(Restrictions.eq("status", 1));
        cr.setFirstResult(0);
        cr.setMaxResults(1);

        User userObject = null;
        try {
            //userObject = (User) cr.list().get(0);
            List<User> userList = cr.list();
            if (userList.size() > 0) {
                userObject = userList.get(0);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return userObject;
    }

    public void deleteByMdn(String username) {

        Criteria cr = getSession().createCriteria(User.class);
        cr.add(Restrictions.eq("username", username));
        User userObject = null;
        try {
            //userObject = (User) cr.list().get(0);
            List<User> userList = cr.list();
            for (User user : userList) {
                getSession().delete(user);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    /**
     * @param jsonRequestBody
     * @param userStatus
     */
    public void saveCreateUserRequest(JsonRequestBody jsonRequestBody, int userStatus) {

        //getting request objects
        com.oxigenwallet.userservice.common.json.request.Request requestJ = jsonRequestBody.getRequest();
        com.oxigenwallet.userservice.common.json.request.User userJ = requestJ.getUser();

        //CREATING NEW USER OBJECT FOR MYSQL
        com.oxigenwallet.userservice.model.User userM = new User();
        userM.setUsername(userJ.getUsername());
        setDataFromRequest(userM, userJ, userStatus);

        //SAVE TO DB
        getSession().save(userM);
    }

    /**
     * @param jsonRequestBody
     * @param userStatusEnabled
     */
    public void updateUpdateUserRequest(JsonRequestBody jsonRequestBody, int userStatusEnabled) {
        //getting request objects
        com.oxigenwallet.userservice.common.json.request.Request requestJ = jsonRequestBody.getRequest();
        com.oxigenwallet.userservice.common.json.request.User userJ = requestJ.getUser();
        System.out.println("Updating user..");
        System.out.println("userJ : " + userJ.toString());
        System.out.println("requestJ : " + requestJ.toString());

        int userExists = 1;
        //GETTING MYSQL OBJECT CORRESPONDING TO RECORD TO BE UPDATED
        com.oxigenwallet.userservice.model.User userM = getByMdn(userJ.getUsername());
        System.out.println("GetUsername : " + userJ.getUsername());
        if (userM == null) {
            System.out.println("User does not exist...");
            userM = new com.oxigenwallet.userservice.model.User();
            userM.setUsername(userJ.getUsername());
            userExists = 0;
        }
        //MODIFYING VALUES
        setDataFromRequest(userM, userJ, userStatusEnabled);
        //UPDATE IN DB
        if (userExists == 1) {
            System.out.println("Updating User...");
            getSession().update(userM);
        } else {
            System.out.println("Saving User..");
            getSession().save(userM);
        }

    }

    public void updateUpdateUserRequestKyc(JsonRequestBody jsonRequestBody, int userStatusEnabled) {
        //getting request objects
        com.oxigenwallet.userservice.common.json.request.Request requestJ = jsonRequestBody.getRequest();
        com.oxigenwallet.userservice.common.json.request.User userJ = requestJ.getUser();

        int userExists = 1;
        //GETTING MYSQL OBJECT CORRESPONDING TO RECORD TO BE UPDATED
        com.oxigenwallet.userservice.model.User userM = getByMdn(userJ.getUsername());
        if (userM == null) {
            userM = new com.oxigenwallet.userservice.model.User();
            userM.setUsername(userJ.getUsername());
            userExists = 0;
        }
        //MODIFYING VALUES
        setDataFromRequest(userM, userJ, userStatusEnabled);
        userM.setProfile_type(Constants.USER_PROFILE_INFO[1]);
        //UPDATE IN DB
        if (userExists == 1) {
            getSession().update(userM);
        } else {
            getSession().save(userM);
        }

    }

    /**
     * @param userInfoJsonResponse
     * @param userStatus
     * @param info
     */
    public void saveGetUserInfoResponse(JsonResponseBody userInfoJsonResponse, String userStatus, String info) throws ParseException {

        //getting request objects
        com.oxigenwallet.userservice.common.json.response.Response responseJ = userInfoJsonResponse.getResponse();
        com.oxigenwallet.userservice.common.json.response.User userJ = responseJ.getUser();

        //CREATING NEW USER OBJECT FOR MYSQL
        com.oxigenwallet.userservice.model.User userM = new User();
        userM.setUsername(userJ.getUsername());

        setDataFromResponse(userM, userJ, userStatus, info);
        //SAVE TO DB
        getSession().save(userM);
    }

    /**
     * @param userM
     * @param userInfoJsonResponse
     * @param userStatus
     * @param info
     */
    public void updateGetUserInfoRequest(User userM, JsonResponseBody userInfoJsonResponse, String userStatus, String info) throws ParseException {

        com.oxigenwallet.userservice.common.json.response.Response responseJ = userInfoJsonResponse.getResponse();
        com.oxigenwallet.userservice.common.json.response.User userJ = responseJ.getUser();
        //setting object values
        setDataFromResponse(userM, userJ, userStatus, info);
        //update user info
        getSession().update(userM);
    }

    private void setDataFromResponse(User userM, com.oxigenwallet.userservice.common.json.response.User userJ, String userStatus, String info) throws ParseException {

        userM.setFirst_name(userJ.getName());
        userM.setMiddle_name(userJ.getMiddle_name());
        userM.setLast_name(userJ.getLast_name());
        userM.setEmail(userJ.getEmail());
        userM.setDob(userJ.getDob());
        userM.setInfo_type(info);

        String gender = userJ.getGender();
        char genderChar = ' ';
        if (gender != null) {
            switch (gender) {
                case "M":
                    genderChar = 'M';
                    break;
                case "F":
                    genderChar = 'F';
            }
        }

        userM.setGender(genderChar);
        userM.setProfile_type(userJ.getProfile_type());
        userM.setAddress(userJ.getAddress());
        userM.setCity(userJ.getCity());
        userM.setDistrict(userJ.getDistrict());
        userM.setDocument( userJ.getDocument() );
        userM.setPan_Verified( userJ.getPan_Verified() );


        String zipcode = userJ.getZipcode();
        Integer zipInt = null;
        if ((zipcode != null) && (!zipcode.trim().equals(""))) {
            zipInt = Integer.parseInt(zipcode);
        }

        userM.setZip(zipInt);
        userM.setState(userJ.getState());
        userM.setCountry(userJ.getCountry());
        Integer userStatusDB = 0;
        if (userStatus.equals("ENABLED") || userStatus.equals("enabled")) {
            userStatusDB = 1;
        }else{
            userStatusDB = 0;
        }
        userM.setStatus(userStatusDB); //for success
        userM.setLandline(userJ.getLandline());

        Timestamp date = new Timestamp(new java.util.Date().getTime());
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        Date createdDate = dateFormat.parse(userJ.getDateCreated());
        userM.setDate_created(new Timestamp(createdDate.getTime()));

//       Timestamp date = new Timestamp(new java.util.Date().getTime());
//       userM.setDate_created(date);

        userM.setCreated_by(userJ.getCreatedBy());
        userM.setDate_updated(date);
    }

    private void setDataFromRequest(User userM, com.oxigenwallet.userservice.common.json.request.User userJ, int userStatusEnabled) {
        userM.setFirst_name(userJ.getName());
        userM.setMiddle_name(userJ.getMiddleName());
        userM.setLast_name(userJ.getLastName());
        userM.setEmail(userJ.getEmail());
        userM.setDob(userJ.getDob());
        //userM.setDocument( userJ.g );

        String gender = userJ.getGender();
        char genderChar = ' ';
        if (gender != null) {
            switch (gender) {
                case "M":
                    genderChar = 'M';
                    break;
                case "F":
                    genderChar = 'F';
                    break;
            }
        }

        userM.setGender(genderChar);
        userM.setAddress(userJ.getAddress());
        userM.setCity(userJ.getCity());
        userM.setDistrict(userJ.getDistrict());

        String zipcode = userJ.getZipcode();
        Integer zipInt = null;
        if (zipcode != null && !zipcode.trim().equals("")) {
            zipInt = Integer.parseInt(zipcode);
        }

        userM.setZip(zipInt);
        userM.setState(userJ.getState());
        userM.setCountry(userJ.getCountry());
        userM.setStatus(userStatusEnabled); //for success
        userM.setLandline(userJ.getLandline());
        userM.setProfile_type( userJ.getProfile_type() );

        Timestamp date = new Timestamp(new java.util.Date().getTime());

        userM.setDate_created(date);
        userM.setDate_updated(date);
    }

}
