/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.oxigenwallet.userservice.dao;

import com.oxigenwallet.userservice.model.NxtxnToken;
import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Asce
 */
@Repository
@Transactional
public class NxtxnTokenDao {

    @Autowired
    private SessionFactory _sessionFactory;

    private Session getSession() {
        return _sessionFactory.getCurrentSession();
    }

    /**
     *
     * @param nxtxnToken
     */
    public void save(NxtxnToken nxtxnToken) {
        getSession().save(nxtxnToken);
    }

    /**
     *
     * @param nxtxnToken
     */
    public void update(NxtxnToken nxtxnToken) {
        getSession().update(nxtxnToken);
    }

    /**
     *
     * @param username
     * @param type
     * @param merchantName
     * @return
     */
    public NxtxnToken getByUsernameTypeMerchantName(String username, String type, String merchantName) {
        Criteria cr = getSession().createCriteria(NxtxnToken.class);
        cr.add(Restrictions.and(Restrictions.eq("username", username), Restrictions.eq("token_type", type), Restrictions.eq("token_for", merchantName)));
        cr.setMaxResults(1);
        List<NxtxnToken> resultList = cr.list();
        if (resultList.size() == 0) {
            return null;
        }
        return resultList.get(0);
    }
    
    public NxtxnToken getNxtxnTokenByToken(String token) {
         Criteria cr = getSession().createCriteria(NxtxnToken.class);
        
        cr.add(Restrictions.eq("token", token));
        
        List<NxtxnToken> resultList = cr.list();
        if (resultList.size() == 0) {
            return null;
        }
        return resultList.get(0);

    }

}
