/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.oxigenwallet.userservice.common.json.response.oxiface;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 *
 * @author Asce
 */
public class User {

    @JsonProperty("mdn")
    String mdn;

    /**
     *
     */
    public User() {
    }

    /**
     *
     * @return
     */
    @JsonIgnore
    public String getMdn() {
        return mdn;
    }

    /**
     *
     * @param mdn
     */
    public void setMdn(String mdn) {
        this.mdn = mdn;
    }
    
    
}
