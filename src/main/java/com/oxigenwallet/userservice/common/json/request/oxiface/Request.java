/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.oxigenwallet.userservice.common.json.request.oxiface;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 *
 * @author mitz
 */
public class Request {

    @JsonProperty("_service")
    String service;

    @JsonProperty("_version")
    String version;

    @JsonProperty("service_request")
    ServiceRequest serviceRequest;

    /**
     *
     */
    public Request() {
    }

    /**
     *
     * @param service
     * @param version
     * @param serviceRequest
     */
    public Request(String service, String version, ServiceRequest serviceRequest) {
        this.service = service;
        this.version = version;
        this.serviceRequest = serviceRequest;
    }

    /**
     *
     * @return
     */
    @JsonIgnore
    public String getService() {
        return service;
    }

    /**
     *
     * @param service
     */
    public void setService(String service) {
        this.service = service;
    }

    /**
     *
     * @return
     */
    @JsonIgnore
    public String getVersion() {
        return version;
    }

    /**
     *
     * @param version
     */
    public void setVersion(String version) {
        this.version = version;
    }

    /**
     *
     * @return
     */
    @JsonIgnore
    public ServiceRequest getServiceRequest() {
        return serviceRequest;
    }

    /**
     *
     * @param serviceRequest
     */
    public void setServiceRequest(ServiceRequest serviceRequest) {
        this.serviceRequest = serviceRequest;
    }

}
