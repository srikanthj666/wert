/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.oxigenwallet.userservice.common.json;

import com.oxigenwallet.userservice.common.RedisCacheResource;
import com.oxigenwallet.userservice.common.RedisStorage;
import com.oxigenwallet.userservice.common.constants.Constants;
import com.oxigenwallet.userservice.common.constants.ResponseCodes;
import com.oxigenwallet.userservice.common.json.response.JsonResponseBody;
import com.oxigenwallet.userservice.common.xml.model.responseXml.NxtxnInterface;
import com.oxigenwallet.userservice.model.User;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import redis.clients.jedis.Jedis;

/**
 * @author mitz
 */
@Component
public class JsonGenerator {

    @Value("#{ @environment['oxiface.requester']}")
    private String requester;

    @Value("#{ @environment['oxiface.intrument']}")
    private String intrument;

    @Autowired
    private RedisCacheResource redisCacheResource;
    @Autowired
    private RedisStorage redisStorage;

    /**
     * @param nxtxnInterface NxtxnInterface
     * @return json
     */
    public JsonResponseBody getSuccessResponseJson(NxtxnInterface nxtxnInterface) {
        //getting xml objects
        com.oxigenwallet.userservice.common.xml.model.responseXml.B2cServiceInterface b2cServiceInterface = nxtxnInterface.getB2C_Service_Interface();
        com.oxigenwallet.userservice.common.xml.model.responseXml.ServiceResponse serviceResponse = b2cServiceInterface.getService_Response();
        com.oxigenwallet.userservice.common.xml.model.responseXml.ResponseInfo responseInfo = serviceResponse.getResponse_Info();
        com.oxigenwallet.userservice.common.xml.model.responseXml.User user = serviceResponse.getUser();
        com.oxigenwallet.userservice.common.xml.model.responseXml.UserInfo userInfo = user.getUser_Info();

        //setting json objects
        com.oxigenwallet.userservice.common.json.response.User userJson = new com.oxigenwallet.userservice.common.json.response.User();
        userJson.setUsername(userInfo.getUser_ID());
        com.oxigenwallet.userservice.common.json.response.Response responseJson = new com.oxigenwallet.userservice.common.json.response.Response();
        responseJson.setUser(userJson);
        JsonResponseBody successResponse = new JsonResponseBody();
        successResponse.setResponse(responseJson);
        successResponse.setStatus(Constants.SUCCESS);
        successResponse.setResponseCode(responseInfo.getHostCode());
        successResponse.setResponseDescription(ResponseCodes.ACCOUNT_CREATED_SUCCESSFULLY);

        return successResponse;
    }

    /**
     * @param nxtxnInterface NxtxnInterface
     * @return json
     */
    public JsonResponseBody getFailureResponseJson(NxtxnInterface nxtxnInterface) {
        //getting values from xml objects
        com.oxigenwallet.userservice.common.xml.model.responseXml.B2cServiceInterface b2cServiceInterface = nxtxnInterface.getB2C_Service_Interface();
        com.oxigenwallet.userservice.common.xml.model.responseXml.ServiceResponse serviceResponse = b2cServiceInterface.getService_Response();
        com.oxigenwallet.userservice.common.xml.model.responseXml.ResponseInfo responseInfo = serviceResponse.getResponse_Info();

        //populating json objects
        com.oxigenwallet.userservice.common.json.response.JsonResponseBody failureResponse = new com.oxigenwallet.userservice.common.json.response.JsonResponseBody();
        System.out.println("responseInfo.getHostCode() : " + responseInfo.getHostCode());
        System.out.println("responseInfo.getHostDescription() : " + responseInfo.getHostDescription());
        failureResponse.setStatus(Constants.FAILURE);
        failureResponse.setResponseCode(responseInfo.getHostCode());
        failureResponse.setResponseDescription(responseInfo.getHostDescription());

        return failureResponse;
    }

    /**
     * @param nxtxnInterface NxtxnInterface
     * @param userName       string
     * @return json
     */
    public JsonResponseBody getResetCustomerLpinSuccessResponseJson(NxtxnInterface nxtxnInterface, String userName) {
        //getting values from xml objects
        com.oxigenwallet.userservice.common.xml.model.responseXml.NxtxnSystemInterface nxtxnSystemInterface = nxtxnInterface.getNxTxN_System_Interface();
        com.oxigenwallet.userservice.common.xml.model.responseXml.SystemServiceResponse systemServiceResponse = nxtxnSystemInterface.getSystemService_Response();
        com.oxigenwallet.userservice.common.xml.model.responseXml.ResponseInfo responseInfo = systemServiceResponse.getResponse_Info();

        //setting json objects
        com.oxigenwallet.userservice.common.json.response.User userJson = new com.oxigenwallet.userservice.common.json.response.User();
        userJson.setUsername(userName);
        com.oxigenwallet.userservice.common.json.response.Response responseJson = new com.oxigenwallet.userservice.common.json.response.Response();
        responseJson.setUser(userJson);
        JsonResponseBody successResponse = new JsonResponseBody();
        successResponse.setResponse(responseJson);
        successResponse.setStatus(Constants.SUCCESS);
        successResponse.setResponseCode(responseInfo.getHostCode());
        successResponse.setResponseDescription(responseInfo.getHostDescription());

        return successResponse;

    }

    /**
     * @param nxtxnInterface NxtxnInterface
     * @return json
     */
    public JsonResponseBody getResetCustomerLpinFailureResponseJson(NxtxnInterface nxtxnInterface) {
        //getting values from xml objects
        com.oxigenwallet.userservice.common.xml.model.responseXml.NxtxnSystemInterface nxtxnSystemInterface = nxtxnInterface.getNxTxN_System_Interface();
        com.oxigenwallet.userservice.common.xml.model.responseXml.SystemServiceResponse systemServiceResponse = nxtxnSystemInterface.getSystemService_Response();
        com.oxigenwallet.userservice.common.xml.model.responseXml.ResponseInfo responseInfo = systemServiceResponse.getResponse_Info();

        //populating json objects
        com.oxigenwallet.userservice.common.json.response.JsonResponseBody failureResponse = new com.oxigenwallet.userservice.common.json.response.JsonResponseBody();
        failureResponse.setStatus(Constants.FAILURE);
        failureResponse.setResponseCode(responseInfo.getHostCode());
        failureResponse.setResponseDescription(responseInfo.getHostDescription());

        return failureResponse;
    }

    /**
     * @param nxtxnInterface NxtxnInterface
     * @param infoType       String
     * @return json
     */
    public JsonResponseBody getGetUserInfoSuccessResponseJson(NxtxnInterface nxtxnInterface, String infoType) {

        //getting values from xml objects
        com.oxigenwallet.userservice.common.xml.model.responseXml.B2cServiceInterface b2cServiceInterface = nxtxnInterface.getB2C_Service_Interface();
        com.oxigenwallet.userservice.common.xml.model.responseXml.ServiceResponse serviceResponse = b2cServiceInterface.getService_Response();
        com.oxigenwallet.userservice.common.xml.model.responseXml.ResponseInfo responseInfo = serviceResponse.getResponse_Info();
        com.oxigenwallet.userservice.common.xml.model.responseXml.User user = serviceResponse.getUser();
        com.oxigenwallet.userservice.common.xml.model.responseXml.UserInfo userInfo = user.getUser_Info();
        com.oxigenwallet.userservice.common.xml.model.responseXml.UserProfile userProfile = userInfo.getUser_Profile();
        com.oxigenwallet.userservice.common.xml.model.responseXml.UserPI userPi = userProfile.getUser_PI();
        com.oxigenwallet.userservice.common.xml.model.responseXml.UserPA userPa = userProfile.getUser_PA();
        com.oxigenwallet.userservice.common.xml.model.responseXml.UserPC userPc = userProfile.getUser_PC();

        //populating json objects
        com.oxigenwallet.userservice.common.json.response.User userJ = new com.oxigenwallet.userservice.common.json.response.User();
        userJ.setUsername(userInfo.getUser_ID());
        userJ.setName(userPi.getFirst_Name());
        userJ.setMiddle_name(userPi.getMiddle_Name());
        userJ.setLast_name(userPi.getLast_Name());
        userJ.setDob(userPi.getDob());
        userJ.setZipcode(userPa.getZip());
        userJ.setEmail(userPc.getEmail());
        userJ.setGender(userPi.getGender());
        userJ.setDateCreated(userProfile.getCreated_on());
        userJ.setCreatedBy(userProfile.getName());
        userJ.setProfile_type( userProfile.getType() );
        userJ.setDocument( userPc.getDocument());
        userJ.setPan_Verified( userPc.getPan_Verified() );

        if (infoType.equals("full")) {
            userJ.setAddress(userPa.getAddress());
            userJ.setCity(userPa.getCity());
            userJ.setDistrict(userPa.getDistrict());
            userJ.setState(userPa.getState());
            userJ.setCountry(userPa.getCountry());
            userJ.setLandline(userPc.getLandline());
            userJ.setPayback_account(Constants.PAYBACK_ACCOUNT_STATUS[0]);
            userJ.setStatus(userInfo.getStatus());
            userJ.setProfile_type(userProfile.getType());
        }

        com.oxigenwallet.userservice.common.json.response.Response response = new com.oxigenwallet.userservice.common.json.response.Response();
        response.setUser(userJ);
        com.oxigenwallet.userservice.common.json.response.JsonResponseBody successResponse = new com.oxigenwallet.userservice.common.json.response.JsonResponseBody();
        successResponse.setResponse(response);
        successResponse.setStatus(Constants.SUCCESS);
        successResponse.setResponseCode(responseInfo.getHostCode());
        successResponse.setResponseDescription(responseInfo.getHostDescription());

        return successResponse;
    }

    /**
     * @param nxtxnInterface   NxtxnInterface
     * @param internalResponse int
     * @return json
     */
    public JsonResponseBody getGetUserInfoFailureResponseJson(NxtxnInterface nxtxnInterface, int internalResponse) {
        //getting values from xml objects
        com.oxigenwallet.userservice.common.xml.model.responseXml.B2cServiceInterface b2cServiceInterface = nxtxnInterface.getB2C_Service_Interface();
        com.oxigenwallet.userservice.common.xml.model.responseXml.ServiceResponse serviceResponse = b2cServiceInterface.getService_Response();
        com.oxigenwallet.userservice.common.xml.model.responseXml.ResponseInfo responseInfo = serviceResponse.getResponse_Info();

        //populating json objects
        com.oxigenwallet.userservice.common.json.response.JsonResponseBody failureResponse = new com.oxigenwallet.userservice.common.json.response.JsonResponseBody();
        failureResponse.setStatus(Constants.FAILURE);
        if (internalResponse == 0) {
            failureResponse.setResponseCode(responseInfo.getHostCode());
            failureResponse.setResponseDescription(responseInfo.getHostDescription());
        } else if (internalResponse == 1) {
            failureResponse.setResponseCode(Constants.WALLET_ALREADY_EXIST_CODE); // user already exists
            failureResponse.setResponseDescription(Constants.WALLET_ALREADY_EXIST_MSG);
        }
        return failureResponse;
    }

    /**
     * @param username String
     * @param infoType String
     * @return json
     */
    public JsonResponseBody getJsonFromRedis(String username, String infoType) {
        //getting json object
        Jedis jedisObject = redisCacheResource.getCacheResource().getResource();
        String key = redisStorage.GenerateKey(username);
        //populating json objects
        com.oxigenwallet.userservice.common.json.response.User user = new com.oxigenwallet.userservice.common.json.response.User();
        Map<String, String> userMap = jedisObject.hgetAll(key);
        String Nu = "null";
        user.setUsername(Nu.equals(userMap.get("username")) ? null : userMap.get("username"));
        user.setName(Nu.equals(userMap.get("first_name")) ? null : userMap.get("first_name"));
        user.setMiddle_name(Nu.equals(userMap.get("middle_name")) ? null : userMap.get("middle_name"));
        user.setLast_name(Nu.equals(userMap.get("last_name")) ? null : userMap.get("last_name"));
        user.setCreatedBy(userMap.get("created_by"));
        user.setDateCreated(userMap.get("date_created"));
        user.setDocument( userMap.get( "document" ) );
        user.setPan_Verified( userMap.get( "Pan_Verified" ) );

        if (infoType.equals("full")) {
            user.setDob(Nu.equals(userMap.get("dob")) ? null : userMap.get("dob"));
            user.setEmail(Nu.equals(userMap.get("email")) ? null : userMap.get("email"));
            user.setZipcode(Nu.equals(userMap.get("zip")) ? null : userMap.get("zip"));
            user.setGender(Nu.equals(userMap.get("gender")) ? null : userMap.get("gender"));
            user.setAddress(Nu.equals(userMap.get("address")) ? null : userMap.get("address"));
            user.setCity(Nu.equals(userMap.get("city")) ? null : userMap.get("city"));
            user.setDistrict(Nu.equals(userMap.get("district")) ? null : userMap.get("district"));
            user.setState(Nu.equals(userMap.get("state")) ? null : userMap.get("state"));
            user.setCountry(Nu.equals(userMap.get("country")) ? null : userMap.get("country"));
            user.setLandline(Nu.equals(userMap.get("landline")) ? null : userMap.get("landline"));
            user.setProfile_type(Nu.equals(userMap.get("profile_type")) ? null : userMap.get("profile_type"));
        }

        // In case of lite
        if (infoType.equals("lite")) {
            user.setProfile_type(userMap.get("profile_type"));
        }
        user.setProfile_type(userMap.get("profile_type"));


        jedisObject.close();

        com.oxigenwallet.userservice.common.json.response.Response response = new com.oxigenwallet.userservice.common.json.response.Response();
        response.setUser(user);
        com.oxigenwallet.userservice.common.json.response.JsonResponseBody jsonResponse = new com.oxigenwallet.userservice.common.json.response.JsonResponseBody();
        jsonResponse.setResponse(response);
        jsonResponse.setStatus(Constants.SUCCESS);
        jsonResponse.setResponseCode(Constants.SUCCESSCODE);
        jsonResponse.setResponseDescription(Constants.SUCCESS);

        return jsonResponse;
    }

    /**
     * @param userM    User
     * @param infoType String
     * @return json
     */
    public JsonResponseBody getJsonFromDbObject(User userM, String infoType) {

        //populating json objects
        com.oxigenwallet.userservice.common.json.response.User userJ = new com.oxigenwallet.userservice.common.json.response.User();
        userJ.setUsername(userM.getUsername());
        userJ.setName(userM.getFirst_name());
        userJ.setMiddle_name(userM.getMiddle_name());
        userJ.setLast_name(userM.getLast_name());
        userJ.setEmail(userM.getEmail());
        userJ.setDateCreated(userM.getDate_created().toString());
        userJ.setCreatedBy(userM.getCreated_by());

        if (infoType.equals("full")) {
            userJ.setDob(userM.getDob());
            String zipcode = null;
            if (userM.getZip() != null) {
                zipcode = Integer.toString(userM.getZip());
            }
            userJ.setZipcode(zipcode);
            char genderC = userM.getGender();
            String genderS = null;
            if (genderC != 0) {
                if (genderC == 'M' || genderC == 'm') {
                    genderS = "M";
                } else if (genderC == 'F' || genderC == 'f') {
                    genderS = "F";
                }
            }
            userJ.setGender(genderS);
            userJ.setAddress(userM.getAddress());
            userJ.setCity(userM.getCity());
            userJ.setDistrict(userM.getDistrict());
            userJ.setState(userM.getState());
            userJ.setCountry(userM.getCountry());
            userJ.setLandline(userM.getLandline());
        }
        userJ.setProfile_type(userM.getProfile_type());
        userJ.setDocument( userM.getDocument() );
        userJ.setPan_Verified( userM.getPan_Verified() );

        com.oxigenwallet.userservice.common.json.response.Response response = new com.oxigenwallet.userservice.common.json.response.Response();
        response.setUser(userJ);

        com.oxigenwallet.userservice.common.json.response.JsonResponseBody jsonResponse = new com.oxigenwallet.userservice.common.json.response.JsonResponseBody();
        jsonResponse.setResponse(response);
        jsonResponse.setStatus(Constants.SUCCESS);
        jsonResponse.setResponseCode(Constants.SUCCESSCODE);
        jsonResponse.setResponseDescription(Constants.SUCCESS);

        return jsonResponse;
    }

}
