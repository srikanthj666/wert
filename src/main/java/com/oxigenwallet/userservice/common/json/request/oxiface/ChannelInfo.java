/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.oxigenwallet.userservice.common.json.request.oxiface;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 *
 * @author Asce
 */
public class ChannelInfo {

    @JsonProperty("requester")
    String requester;

    @JsonProperty("instrument")
    String instrument;

    @JsonProperty("channel_instance_id")
    String channelInstanceId;

    /**
     *
     */
    public ChannelInfo() {
    }

    /**
     *
     * @param requester
     * @param instrument
     * @param channelInstanceId
     */
    public ChannelInfo(String requester, String instrument, String channelInstanceId) {

        this.requester = requester;
        this.instrument = instrument;
        this.channelInstanceId = channelInstanceId;
    }

    /**
     *
     * @return
     */
    @JsonIgnore
    public String getRequester() {
        return requester;
    }

    /**
     *
     * @param requester
     */
    public void setRequester(String requester) {
        this.requester = requester;
    }

    /**
     *
     * @return
     */
    @JsonIgnore
    public String getInstrument() {
        return instrument;
    }

    /**
     *
     * @param instrument
     */
    public void setInstrument(String instrument) {
        this.instrument = instrument;
    }

    /**
     *
     * @return
     */
    @JsonIgnore
    public String getChannelInstanceId() {
        return channelInstanceId;
    }

    /**
     *
     * @param channelInstanceId
     */
    public void setChannelInstanceId(String channelInstanceId) {
        this.channelInstanceId = channelInstanceId;
    }

}
