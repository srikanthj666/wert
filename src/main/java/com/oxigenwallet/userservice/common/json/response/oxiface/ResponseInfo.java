/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.oxigenwallet.userservice.common.json.response.oxiface;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 *
 * @author Asce
 */
public class ResponseInfo {

    @JsonProperty("host_code")
    String hostCode;

    @JsonProperty("host_description")
    String hostDescription;

    @JsonProperty("class_code")
    String classCode;

    @JsonProperty("class_description")
    String classDescription;

    @JsonProperty("system_code")
    String systemCode;

    @JsonProperty("system_description")
    String systemDescription;

    @JsonProperty("key")
    String key;

    @JsonProperty("is_bind")
    String isBind;

    /**
     *
     */
    public ResponseInfo() {
    }

    /**
     *
     * @return
     */
    @JsonIgnore
    public String getHostCode() {
        return hostCode;
    }

    /**
     *
     * @param hostCode
     */
    public void setHostCode(String hostCode) {
        this.hostCode = hostCode;
    }

    /**
     *
     * @return
     */
    @JsonIgnore
    public String getHostDescription() {
        return hostDescription;
    }

    /**
     *
     * @param hostDescription
     */
    public void setHostDescription(String hostDescription) {
        this.hostDescription = hostDescription;
    }

    /**
     *
     * @return
     */
    @JsonIgnore
    public String getClassCode() {
        return classCode;
    }

    /**
     *
     * @param classCode
     */
    public void setClassCode(String classCode) {
        this.classCode = classCode;
    }

    /**
     *
     * @return
     */
    @JsonIgnore
    public String getClassDescription() {
        return classDescription;
    }

    /**
     *
     * @param classDescription
     */
    public void setClassDescription(String classDescription) {
        this.classDescription = classDescription;
    }

    /**
     *
     * @return
     */
    @JsonIgnore
    public String getSystemCode() {
        return systemCode;
    }

    /**
     *
     * @param systemCode
     */
    public void setSystemCode(String systemCode) {
        this.systemCode = systemCode;
    }

    /**
     *
     * @return
     */
    @JsonIgnore
    public String getSystemDescription() {
        return systemDescription;
    }

    /**
     *
     * @param systemDescription
     */
    public void setSystemDescription(String systemDescription) {
        this.systemDescription = systemDescription;
    }

    /**
     *
     * @return
     */
    @JsonIgnore
    public String getKey() {
        return key;
    }

    /**
     *
     * @param key
     */
    public void setKey(String key) {
        this.key = key;
    }

    /**
     *
     * @return
     */
    @JsonIgnore
    public String getIsBind() {
        return isBind;
    }

    /**
     *
     * @param isBind
     */
    public void setIsBind(String isBind) {
        this.isBind = isBind;
    }

}
