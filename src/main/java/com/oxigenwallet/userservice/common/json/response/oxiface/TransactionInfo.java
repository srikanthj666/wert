/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.oxigenwallet.userservice.common.json.response.oxiface;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 *
 * @author Asce
 */
public class TransactionInfo {

    @JsonProperty("transaction_info")
    String transactionInfo;

    @JsonProperty("time_stamp")
    String timeStamp;

    @JsonProperty("request_id")
    String requestId;

    @JsonProperty("txn_description")
    String txnDescription;

    @JsonProperty("transaction_no")
    String transactionNo;

    /**
     *
     */
    public TransactionInfo() {
    }

    /**
     *
     * @return
     */
    @JsonIgnore
    public String getTransactionInfo() {
        return transactionInfo;
    }

    /**
     *
     * @param transactionInfo
     */
    public void setTransactionInfo(String transactionInfo) {
        this.transactionInfo = transactionInfo;
    }

    /**
     *
     * @return
     */
    @JsonIgnore
    public String getTimeStamp() {
        return timeStamp;
    }

    /**
     *
     * @param timeStamp
     */
    public void setTimeStamp(String timeStamp) {
        this.timeStamp = timeStamp;
    }

    /**
     *
     * @return
     */
    @JsonIgnore
    public String getRequestId() {
        return requestId;
    }

    /**
     *
     * @param requestId
     */
    public void setRequestId(String requestId) {
        this.requestId = requestId;
    }

    /**
     *
     * @return
     */
    @JsonIgnore
    public String getTxnDescription() {
        return txnDescription;
    }

    /**
     *
     * @param txnDescription
     */
    public void setTxnDescription(String txnDescription) {
        this.txnDescription = txnDescription;
    }

    /**
     *
     * @return
     */
    @JsonIgnore
    public String getTransactionNo() {
        return transactionNo;
    }

    /**
     *
     * @param transactionNo
     */
    public void setTransactionNo(String transactionNo) {
        this.transactionNo = transactionNo;
    }

}
