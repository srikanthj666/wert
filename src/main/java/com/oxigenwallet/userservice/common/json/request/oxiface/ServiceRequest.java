/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.oxigenwallet.userservice.common.json.request.oxiface;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 *
 * @author Asce
 */
public class ServiceRequest {

    @JsonProperty("channel_info")
    ChannelInfo channelInfo;

    @JsonProperty("device_info")
    DeviceInfo deviceInfo;

    @JsonProperty("params")
    Params params;

    @JsonProperty("transaction_info")
    TransactionInfo transactionInfo;

    @JsonProperty("user")
    User user;

    /**
     *
     */
    public ServiceRequest() {
    }

    /**
     *
     * @param channelInfo
     * @param deviceInfo
     * @param params
     * @param transactionInfo
     * @param user
     */
    public ServiceRequest(ChannelInfo channelInfo, DeviceInfo deviceInfo, Params params, TransactionInfo transactionInfo, User user) {
        this.channelInfo = channelInfo;
        this.deviceInfo = deviceInfo;
        this.params = params;
        this.transactionInfo = transactionInfo;
        this.user = user;
    }
    
    /**
     *
     * @return
     */
    @JsonIgnore
    public ChannelInfo getChannelInfo() {
        return channelInfo;
    }

    /**
     *
     * @param channelInfo
     */
    public void setChannelInfo(ChannelInfo channelInfo) {
        this.channelInfo = channelInfo;
    }

    /**
     *
     * @return
     */
    @JsonIgnore
    public DeviceInfo getDeviceInfo() {
        return deviceInfo;
    }

    /**
     *
     * @param deviceInfo
     */
    public void setDeviceInfo(DeviceInfo deviceInfo) {
        this.deviceInfo = deviceInfo;
    }

    /**
     *
     * @return
     */
    @JsonIgnore
    public Params getParams() {
        return params;
    }

    /**
     *
     * @param params
     */
    public void setParams(Params params) {
        this.params = params;
    }

    /**
     *
     * @return
     */
    @JsonIgnore
    public TransactionInfo getTransactionInfo() {
        return transactionInfo;
    }

    /**
     *
     * @param transactionInfo
     */
    public void setTransactionInfo(TransactionInfo transactionInfo) {
        this.transactionInfo = transactionInfo;
    }

    /**
     *
     * @return
     */
    @JsonIgnore
    public User getUser() {
        return user;
    }

    /**
     *
     * @param user
     */
    public void setUser(User user) {
        this.user = user;
    }

}
