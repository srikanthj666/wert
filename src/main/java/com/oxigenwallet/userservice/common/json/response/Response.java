/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.oxigenwallet.userservice.common.json.response;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

/**
 *
 * @author mitz
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Response {

    @JsonProperty("user")
    private User user;
    @JsonProperty("session")
    private List<Session> session;

    @JsonProperty("token")
    TokenInfo tokenInfo;

    @JsonProperty("username")
    String username;

    @JsonProperty("nxtxn_token")
    String nxtxnToken;

    @JsonProperty("token_for")
    String tokenFor;

    @JsonProperty("merchant_name")
    String merchantName;

    /**
     *
     */
    public Response() {
    }

    /**
     *
     * @param user
     * @param tokenInfo
     * @param username
     * @param nxtxnToken
     * @param tokenFor
     * @param merchantName
     */
    public Response( User user, TokenInfo tokenInfo, String username, String nxtxnToken, String tokenFor, String merchantName) {
        
        this.user = user;
        this.tokenInfo = tokenInfo;
        this.username = username;
        this.nxtxnToken = nxtxnToken;
        this.tokenFor = tokenFor;
        this.merchantName = merchantName;
    }

    public List<Session> getSession() {
        return session;
    }

    public void setSession(List<Session> session) {
        this.session = session;
    }

    /**
     *
     * @return
     */
    @JsonIgnore
    public User getUser() {
        return user;
    }

    /**
     *
     * @param user
     */
    public void setUser(User user) {
        this.user = user;
    }

    /**
     *
     * @return
     */
    @JsonIgnore
    public TokenInfo getTokenInfo() {
        return tokenInfo;
    }

    /**
     *
     * @param tokenInfo
     */
    public void setTokenInfo(TokenInfo tokenInfo) {
        this.tokenInfo = tokenInfo;
    }

    /**
     *
     * @return
     */
    @JsonIgnore
    public String getUsername() {
        return username;
    }

    /**
     *
     * @param username
     */
    public void setUsername(String username) {
        this.username = username;
    }

    /**
     *
     * @return
     */
    @JsonIgnore
    public String getNxtxnToken() {
        return nxtxnToken;
    }

    /**
     *
     * @param nxtxnToken
     */
    public void setNxtxnToken(String nxtxnToken) {
        this.nxtxnToken = nxtxnToken;
    }

    /**
     *
     * @return
     */
    @JsonIgnore
    public String getTokenFor() {
        return tokenFor;
    }

    /**
     *
     * @param tokenFor
     */
    public void setTokenFor(String tokenFor) {
        this.tokenFor = tokenFor;
    }

    /**
     *
     * @return
     */
    @JsonIgnore
    public String getMerchantName() {
        return merchantName;
    }

    /**
     *
     * @param merchantName
     */
    public void setMerchantName(String merchantName) {
        this.merchantName = merchantName;
    }

}
