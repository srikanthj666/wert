/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.oxigenwallet.userservice.common.json.request;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 *
 * @author mitz
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class TokenInfo {

    @JsonProperty("grant_type")
    String grantType;

    @JsonProperty("username")
    String username;

    @JsonProperty("password")
    String password;

    @JsonProperty("scope")
    String scope;

    @JsonProperty("redirect_uri")
    String redirectUri;

    @JsonProperty("otp")
    String otp;

    @JsonProperty("auth_code")
    String authCode;

    @JsonProperty("type")
    String type;

    @JsonProperty("merchant_name")
    String merchantName;

    @JsonProperty("is_merchant")
    boolean isMerchant;

    /**
     *
     */
    public TokenInfo() {
    }

    /**
     *
     * @return String
     */
    @JsonIgnore
    public String getType() {
        return type;
    }

    /**
     *
     * @param type String
     */
    public void setType(String type) {
        this.type = type;
    }

    /**
     *
     * @return String
     */
    @JsonIgnore
    public String getMerchantName() {
        return merchantName;
    }

    /**
     *
     * @param merchantName  String
     */
    public void setMerchantName(String merchantName) {
        this.merchantName = merchantName;
    }

    /**
     *
     * @return String
     */
    @JsonIgnore
    public String getGrantType() {
        return grantType;
    }

    /**
     *
     * @param grantType String
     */
    public void setGrantType(String grantType) {
        this.grantType = grantType;
    }

    /**
     *
     * @return String
     */
    @JsonIgnore
    public String getUsername() {
        return username;
    }

    /**
     *
     * @param username String
     */
    public void setUsername(String username) {
        this.username = username;
    }

    /**
     *
     * @return String
     */
    @JsonIgnore
    public String getPassword() {
        return password;
    }

    /**
     *
     * @param password String
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     *
     * @return String
     */
    @JsonIgnore
    public String getScope() {
        return scope;
    }

    /**
     *
     * @param scope String
     */
    public void setScope(String scope) {
        this.scope = scope;
    }

    /**
     *
     * @return String
     */
    @JsonIgnore
    public String getRedirectUri() {
        return redirectUri;
    }

    /**
     *
     * @param redirectUri String
     */
    public void setRedirectUri(String redirectUri) {
        this.redirectUri = redirectUri;
    }

    /**
     *
     * @return String
     */
    @JsonIgnore
    public String getOtp() {
        return otp;
    }

    /**
     *
     * @param otp StringØ
     */
    public void setOtp(String otp) {
        this.otp = otp;
    }

    public String getAuthCode() {
        return authCode;
    }

    public void setAuthCode(String authCode) {
        this.authCode = authCode;
    }

    @JsonIgnore
    public boolean getIsMerchant() {
        return isMerchant;
    }

    public void setIsMerchant(boolean isMerchant) {
        this.isMerchant = isMerchant;
    }

}
