/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.oxigenwallet.userservice.common.json.request;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.List;

/**
 *
 * @author mitz
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Request {

    @JsonProperty("device_info")
    private DeviceInfo deviceInfo;
    
    @JsonProperty("user")
    private User user;

    @JsonProperty("transaction_data")
    private TransactionData transactionData;

    @JsonProperty("token")
    TokenInfo tokeninfo;
    
    @JsonProperty("document_info")
    private List<DocumentInfo> document_info;

    /**
     *
     * @return DeviceInfo
     */
    public DeviceInfo getDeviceInfo() {
        return deviceInfo;
    }

    /**
     *
     * @return List
     */
    public List<DocumentInfo> getDocument_info() {
        return document_info;
    }

    /**
     *
     * @param document_info List
     */
    public void setDocument_info(List<DocumentInfo> document_info) {
        this.document_info = document_info;
    }

    /**
     *
     * @param deviceInfo DeviceInfo
     */
    public void setDeviceInfo(DeviceInfo deviceInfo) {
        this.deviceInfo = deviceInfo;
    }

    /**
     *
     * @return User
     */
    @JsonIgnore
    public User getUser() {
        return user;
    }

    /**
     *
     * @param user User
     */
    public void setUser(User user) {
        this.user = user;
    }

    /**
     *
     * @return TransactionData
     */
    @JsonIgnore
    public TransactionData getTransactionData() {
        return transactionData;
    }

    /**
     *
     * @param transactionData  TransactionData
     */
    public void setTransactionData(TransactionData transactionData) {
        this.transactionData = transactionData;
    }

    /**
     *
     * @return TokenInfo
     */
    @JsonIgnore
    public TokenInfo getTokeninfo() {
        return tokeninfo;
    }

    /**
     *
     * @param tokeninfo TokenInfo
     */
    public void setTokeninfo(TokenInfo tokeninfo) {
        this.tokeninfo = tokeninfo;
    }

}
