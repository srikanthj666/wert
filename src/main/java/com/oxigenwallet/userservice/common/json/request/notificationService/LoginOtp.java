/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.oxigenwallet.userservice.common.json.request.notificationService;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 *
 * @author Asce
 */
public class LoginOtp {

    @JsonProperty("data")
    Data data;
    @JsonProperty("event")
    Event event;

    public LoginOtp() {
    }

    public LoginOtp(Data data, Event event) {
        this.data = data;
        this.event = event;
    }

    @JsonIgnore
    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    @JsonIgnore
    public Event getEvent() {
        return event;
    }

    public void setEvent(Event event) {
        this.event = event;
    }

}
