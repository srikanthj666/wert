/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.oxigenwallet.userservice.common.json.request;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 *
 * @author mitz
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class User {

    @JsonProperty("username")
    private String username;

    @JsonProperty("name")
    private String name;

    @JsonProperty("middle_name")
    private String middleName;

    @JsonProperty("last_name")
    private String lastName;

    @JsonProperty("dob")
    private String dob;

    @JsonProperty("zipcode")
    private String zipcode;

    @JsonProperty("email")
    private String email;

    @JsonProperty("gender")
    private String gender;

    @JsonProperty("address")
    private String address;

    @JsonProperty("city")
    private String city;

    @JsonProperty("district")
    private String district;

    @JsonProperty("state")
    private String state;

    @JsonProperty("country")
    private String country;

    @JsonProperty("landline")
    private String landline;

    @JsonProperty("referral_code")
    private String referralCode;

    @JsonProperty("password")
    private String password;

    @JsonProperty("otp")
    private String otp;

    @JsonProperty("new_password")
    private String newPassword;

    @JsonProperty("old_password")
    private String oldPassword;

    @JsonProperty("type")
    private String type;

    @JsonProperty("profile_type")
    private String profile_type;

    @JsonProperty("auth_code")
    private String authCode;

    @JsonProperty("is_merchant")
    boolean isMerchant;

    public String getProfile_type() {
        return profile_type;
    }

    public void setProfile_type(String profile_type) {
        this.profile_type = profile_type;
    }

    /**

     *
     * @return String
     */
    public String getNewPassword() {
        return newPassword;
    }

    /**
     *
     * @param newPassword String
     */
    public void setNewPassword(String newPassword) {
        this.newPassword = newPassword;
    }

    /**
     *
     * @return String
     */
    public String getOldPassword() {
        return oldPassword;
    }

    /**
     *
     * @return String
     */
    public String getUsername() {
        return username;
    }

    /**
     *
     * @param username String
     */
    public void setUsername(String username) {
        this.username = username;
    }

    /**
     *
     * @param oldPassword String
     */
    public void setOldPassword(String oldPassword) {
        this.oldPassword = oldPassword;
    }

    /**
     *
     * @return String
     */
    public String getType() {
        return type;
    }

    /**
     *
     * @param type String
     */
    public void setType(String type) {
        this.type = type;
    }

    /**
     *
     * @return String
     */
    public String getName() {
        return name;
    }

    /**
     *
     * @param name String
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     *
     * @return String
     */
    public String getMiddleName() {
        return middleName;
    }

    /**
     *
     * @param middleName String
     */
    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    /**
     *
     * @return String
     */
    public String getLastName() {
        return lastName;
    }

    /**
     *
     * @param lastName String
     */
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    /**
     *
     * @return String
     */
    public String getDob() {
        return dob;
    }

    /**
     *
     * @param dob String
     */
    public void setDob(String dob) {
        this.dob = dob;
    }

    /**
     *
     * @return String
     */
    public String getZipcode() {
        return zipcode;
    }

    /**
     *
     * @param zipcode String
     */
    public void setZipcode(String zipcode) {
        this.zipcode = zipcode;
    }

    /**
     *
     * @return String
     */
    public String getEmail() {
        return email;
    }

    /**
     *
     * @param email String
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     *
     * @return String
     */
    public String getGender() {
        return gender;
    }

    /**
     *
     * @param gender String
     */
    public void setGender(String gender) {
        this.gender = gender;
    }

    /**
     *
     * @return String
     */
    public String getAddress() {
        return address;
    }

    /**
     *
     * @param address String
     */
    public void setAddress(String address) {
        this.address = address;
    }

    /**
     *
     * @return String
     */
    public String getCity() {
        return city;
    }

    /**
     *
     * @param city String
     */
    public void setCity(String city) {
        this.city = city;
    }

    /**
     *
     * @return String
     */
    public String getDistrict() {
        return district;
    }

    /**
     *
     * @param district String
     */
    public void setDistrict(String district) {
        this.district = district;
    }

    /**
     *
     * @return String
     */
    public String getState() {
        return state;
    }

    /**
     *
     * @param state String
     */
    public void setState(String state) {
        this.state = state;
    }

    /**
     *
     * @return String
     */
    public String getCountry() {
        return country;
    }

    /**
     *
     * @param country String
     */
    public void setCountry(String country) {
        this.country = country;
    }

    /**
     *
     * @return String
     */
    public String getLandline() {
        return landline;
    }

    /**
     *
     * @param landline String
     */
    public void setLandline(String landline) {
        this.landline = landline;
    }

    /**
     *
     * @return String
     */
    public String getReferralCode() {
        return referralCode;
    }

    /**
     *
     * @param referralCode String
     */
    public void setReferralCode(String referralCode) {
        this.referralCode = referralCode;
    }

    /**
     *
     * @return String
     */
    public String getPassword() {
        return password;
    }

    /**
     *
     * @param password String
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     *
     * @return String
     */
    public String getOtp() {
        return otp;
    }

    /**
     *
     * @param otp String
     */
    public void setOtp(String otp) {
        this.otp = otp;
    }

    public String getAuthCode() {
        return authCode;
    }

    public void setAuthCode(String authCode) {
        this.authCode = authCode;
    }

    public boolean getIsMerchant() {
        return isMerchant;
    }

    public void setIsMerchant(boolean isMerchant) {
        this.isMerchant = isMerchant;
    }


}
