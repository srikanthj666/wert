/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.oxigenwallet.userservice.common.json;

import com.oxigenwallet.userservice.common.constants.Constants;
import com.oxigenwallet.userservice.common.json.request.oxiface.Request;
import com.oxigenwallet.userservice.utils.Util;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 *
 * @author Asce
 */
@Component
public class oxifaceJsonHelper {

    @Value("#{ @environment['oxiface.intrument']}")
    String oxifaceIntrument;

    @Value("#{ @environment['oxiface.requester']}")
    String oxifaceRequester;

    /**
     *
     * @param userName string
     * @return
     */
    public Request getVerifyUserServicerequest(String userName) {
        com.oxigenwallet.userservice.common.json.request.oxiface.ChannelInfo oxifaceChannelInfo
                = new com.oxigenwallet.userservice.common.json.request.oxiface.ChannelInfo(oxifaceRequester, oxifaceIntrument, "");

        com.oxigenwallet.userservice.common.json.request.oxiface.DeviceInfo oxifaceDeviceInfo
                = new com.oxigenwallet.userservice.common.json.request.oxiface.DeviceInfo("", "", "", "");

        com.oxigenwallet.userservice.common.json.request.oxiface.Params oxifaceParams
                = new com.oxigenwallet.userservice.common.json.request.oxiface.Params("", "", "", "", "");

        com.oxigenwallet.userservice.common.json.request.oxiface.TransactionInfo oxifaceTransactionInfo
                = new com.oxigenwallet.userservice.common.json.request.oxiface.TransactionInfo(Util.getRequestId(userName), Util.getTimeStamp(), "");

        com.oxigenwallet.userservice.common.json.request.oxiface.User oxifaceUser
                = new com.oxigenwallet.userservice.common.json.request.oxiface.User(userName, null, null);

        com.oxigenwallet.userservice.common.json.request.oxiface.ServiceRequest oxifaceServiceRequest
                = new com.oxigenwallet.userservice.common.json.request.oxiface.ServiceRequest(oxifaceChannelInfo, oxifaceDeviceInfo, oxifaceParams, oxifaceTransactionInfo, oxifaceUser);

        com.oxigenwallet.userservice.common.json.request.oxiface.Request oxifaceRequest
                = new com.oxigenwallet.userservice.common.json.request.oxiface.Request(Constants.OXIFACE_SERVICES[0], Constants.OXIFACE_VERSION, oxifaceServiceRequest);

        return oxifaceRequest;
    }

    /**
     *
     * @param userName string
     * @param refferalCode string
     * @return
     */
    public Request getAddUserServicerequest(String userName, String refferalCode, String nxtxnToken) {
        com.oxigenwallet.userservice.common.json.request.oxiface.ChannelInfo oxifaceChannelInfo
                = new com.oxigenwallet.userservice.common.json.request.oxiface.ChannelInfo(oxifaceRequester, oxifaceIntrument, "");

        com.oxigenwallet.userservice.common.json.request.oxiface.DeviceInfo oxifaceDeviceInfo
                = new com.oxigenwallet.userservice.common.json.request.oxiface.DeviceInfo("", "", "", "");

        com.oxigenwallet.userservice.common.json.request.oxiface.Params oxifaceParams
                = new com.oxigenwallet.userservice.common.json.request.oxiface.Params("", "", "", "", "");

        com.oxigenwallet.userservice.common.json.request.oxiface.TransactionInfo oxifaceTransactionInfo
                = new com.oxigenwallet.userservice.common.json.request.oxiface.TransactionInfo(Util.getRequestId(userName), Util.getTimeStamp(), "");

        com.oxigenwallet.userservice.common.json.request.oxiface.User oxifaceUser
                = new com.oxigenwallet.userservice.common.json.request.oxiface.User(userName, nxtxnToken, refferalCode);

        com.oxigenwallet.userservice.common.json.request.oxiface.ServiceRequest oxifaceServiceRequest
                = new com.oxigenwallet.userservice.common.json.request.oxiface.ServiceRequest(oxifaceChannelInfo, oxifaceDeviceInfo, oxifaceParams, oxifaceTransactionInfo, oxifaceUser);

        com.oxigenwallet.userservice.common.json.request.oxiface.Request oxifaceRequest
                = new com.oxigenwallet.userservice.common.json.request.oxiface.Request(Constants.OXIFACE_SERVICES[1], Constants.OXIFACE_VERSION, oxifaceServiceRequest);

        return oxifaceRequest;
    }
}
