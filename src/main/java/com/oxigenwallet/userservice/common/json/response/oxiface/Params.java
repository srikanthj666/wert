/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.oxigenwallet.userservice.common.json.response.oxiface;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 *
 * @author Asce
 */
public class Params {

    @JsonProperty("param_1")
    String param1;

    @JsonProperty("param_2")
    String param2;

    @JsonProperty("param_3")
    String param3;

    @JsonProperty("param_4")
    String param4;

    @JsonProperty("param_5")
    String param5;

    /**
     *
     */
    public Params() {
    }

    /**
     *
     * @param param1
     * @param param2
     * @param param3
     * @param param4
     * @param param5
     */
    public Params(String param1, String param2, String param3, String param4, String param5) {
        this.param1 = param1;
        this.param2 = param2;
        this.param3 = param3;
        this.param4 = param4;
        this.param5 = param5;
    }

    /**
     *
     * @return
     */
    @JsonIgnore
    public String getParam1() {
        return param1;
    }

    /**
     *
     * @param param1
     */
    public void setParam1(String param1) {
        this.param1 = param1;
    }

    /**
     *
     * @return
     */
    @JsonIgnore
    public String getParam2() {
        return param2;
    }

    /**
     *
     * @param param2
     */
    public void setParam2(String param2) {
        this.param2 = param2;
    }

    /**
     *
     * @return
     */
    @JsonIgnore
    public String getParam3() {
        return param3;
    }

    /**
     *
     * @param param3
     */
    public void setParam3(String param3) {
        this.param3 = param3;
    }

    /**
     *
     * @return
     */
    @JsonIgnore
    public String getParam4() {
        return param4;
    }

    /**
     *
     * @param param4
     */
    public void setParam4(String param4) {
        this.param4 = param4;
    }

    /**
     *
     * @return
     */
    @JsonIgnore
    public String getParam5() {
        return param5;
    }

    /**
     *
     * @param param5
     */
    public void setParam5(String param5) {
        this.param5 = param5;
    }

}