/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.oxigenwallet.userservice.common.json.response;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 *
 * @author mitz
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class User {

    @JsonProperty("username")
    private String username;
    @JsonProperty("name")
    private String name;
    @JsonProperty("middle_name")
    private String middle_name;
    @JsonProperty("last_name")
    private String last_name;
    @JsonProperty("dob")
    private String dob;
    @JsonProperty("zipcode")
    private String zipcode;
    @JsonProperty("email")
    private String email;
    @JsonProperty("gender")
    private String gender;
    @JsonProperty("address")
    private String address;
    @JsonProperty("city")
    private String city;
    @JsonProperty("district")
    private String district;
    @JsonProperty("state")
    private String state;
    @JsonProperty("country")
    private String country;
    @JsonProperty("landline")
    private String landline;
    @JsonProperty("payback_account")
    private String payback_account;
    @JsonProperty("status")
    private String status;
    @JsonProperty("profile_type")
    private String profile_type;
    @JsonProperty("date_created")
    private String dateCreated;
    @JsonProperty("created_by")
    private String createdBy;

    public String getDocument() {
        return document;
    }

    public void setDocument(String document) {
        this.document = document;
    }

    @JsonProperty("document")
    private String document;

    @JsonProperty("Pan_Verified")
    public String getPan_Verified() {
        return Pan_Verified;
    }

    public void setPan_Verified(String pan_Verified) {
        Pan_Verified = pan_Verified;
    }

    @JsonProperty("Pan_Verified")
    private  String Pan_Verified;

//    @JsonProperty("created_by")
//    private String Updated_Source;
//    @JsonProperty("reason")
//    private String reason;

    @JsonIgnore
    public User() {
    }

    /**
     *
     * @return
     */
    @JsonIgnore
    public String getProfile_type() {
        return profile_type;
    }

    /**
     *
     * @param profile_type
     */
    public void setProfile_type(String profile_type) {
        this.profile_type = profile_type;
    }

    /**
     *
     * @return
     */
    @JsonIgnore
    public String getUsername() {
        return username;
    }

    /**
     *
     * @param username
     */
    public void setUsername(String username) {
        this.username = username;
    }

    /**
     *
     * @return
     */
    @JsonIgnore
    public String getName() {
        return name;
    }

    /**
     *
     * @param name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     *
     * @return
     */
    @JsonIgnore
    public String getMiddle_name() {
        return middle_name;
    }

    /**
     *
     * @param middle_name
     */
    public void setMiddle_name(String middle_name) {
        this.middle_name = middle_name;
    }

    /**
     *
     * @return
     */
    @JsonIgnore
    public String getLast_name() {
        return last_name;
    }

    /**
     *
     * @param last_name
     */
    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    /**
     *
     * @return
     */
    @JsonIgnore
    public String getDob() {
        return dob;
    }

    /**
     *
     * @param dob
     */
    public void setDob(String dob) {
        this.dob = dob;
    }

    /**
     *
     * @return
     */
    @JsonIgnore
    public String getZipcode() {
        return zipcode;
    }

    /**
     *
     * @param zipcode
     */
    public void setZipcode(String zipcode) {
        this.zipcode = zipcode;
    }

    /**
     *
     * @return
     */
    @JsonIgnore
    public String getEmail() {
        return email;
    }

    /**
     *
     * @param email
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     *
     * @return
     */
    @JsonIgnore
    public String getGender() {
        return gender;
    }

    /**
     *
     * @param gender
     */
    public void setGender(String gender) {
        this.gender = gender;
    }

    /**
     *
     * @return
     */
    @JsonIgnore
    public String getAddress() {
        return address;
    }

    /**
     *
     * @param address
     */
    public void setAddress(String address) {
        this.address = address;
    }

    /**
     *
     * @return
     */
    @JsonIgnore
    public String getCity() {
        return city;
    }

    /**
     *
     * @param city
     */
    public void setCity(String city) {
        this.city = city;
    }

    /**
     *
     * @return
     */
    @JsonIgnore
    public String getDistrict() {
        return district;
    }

    /**
     *
     * @param district
     */
    public void setDistrict(String district) {
        this.district = district;
    }

    /**
     *
     * @return
     */
    @JsonIgnore
    public String getState() {
        return state;
    }

    /**
     *
     * @param state
     */
    public void setState(String state) {
        this.state = state;
    }

    /**
     *
     * @return
     */
    @JsonIgnore
    public String getCountry() {
        return country;
    }

    /**
     *
     * @param country
     */
    public void setCountry(String country) {
        this.country = country;
    }

    /**
     *
     * @return
     */
    @JsonIgnore
    public String getLandline() {
        return landline;
    }

    /**
     *
     * @param landline
     */
    public void setLandline(String landline) {
        this.landline = landline;
    }

    /**
     *
     * @return
     */
    @JsonIgnore
    public String getPayback_account() {
        return payback_account;
    }

    /**
     *
     * @param payback_account
     */
    public void setPayback_account(String payback_account) {
        this.payback_account = payback_account;
    }

    /**
     *
     * @return
     */
    @JsonIgnore
    public String getStatus() {
        return status;
    }

    /**
     *
     * @param status
     */
    public void setStatus(String status) {
        this.status = status;
    }

    @JsonIgnore
    public String getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(String dateCreated) {
        this.dateCreated = dateCreated;
    }

    @JsonIgnore
    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }
//
//    @JsonIgnore
//    public String getUpdated_Source() {
//        return Updated_Source;
//    }
//
//    public void setUpdated_Source(String Updated_Source) {
//        this.Updated_Source = Updated_Source;
//    }
//
//    @JsonIgnore
//    public String getReason() {
//        return reason;
//    }
//
//    public void setReason(String reason) {
//        this.reason = reason;
//    }

    
}
