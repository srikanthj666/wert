/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.oxigenwallet.userservice.common.json.request.oxiface;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 *
 * @author Asce
 */
public class TransactionInfo {

    @JsonProperty("request_id")
    String requestId;

    @JsonProperty("time_stamp")
    String timeStamp;

    @JsonProperty("txn_description")
    String txnDescription;

    /**
     *
     */
    public TransactionInfo() {
    }

    /**
     *
     * @param requestId
     * @param timeStamp
     * @param txnDescription
     */
    public TransactionInfo(String requestId, String timeStamp, String txnDescription) {
        this.requestId = requestId;
        this.timeStamp = timeStamp;
        this.txnDescription = txnDescription;
    }

    /**
     *
     * @return
     */
    @JsonIgnore
    public String getRequestId() {
        return requestId;
    }

    /**
     *
     * @param requestId
     */
    public void setRequestId(String requestId) {
        this.requestId = requestId;
    }

    /**
     *
     * @return
     */
    @JsonIgnore
    public String getTimeStamp() {
        return timeStamp;
    }

    /**
     *
     * @param timeStamp
     */
    public void setTimeStamp(String timeStamp) {
        this.timeStamp = timeStamp;
    }

    /**
     *
     * @return
     */
    @JsonIgnore
    public String getTxnDescription() {
        return txnDescription;
    }

    /**
     *
     * @param txnDescription
     */
    public void setTxnDescription(String txnDescription) {
        this.txnDescription = txnDescription;
    }

}
