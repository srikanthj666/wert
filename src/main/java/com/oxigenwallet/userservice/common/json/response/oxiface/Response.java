/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.oxigenwallet.userservice.common.json.response.oxiface;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 *
 * @author Asce
 */
public class Response {

    @JsonProperty("_service")
    String service;

    @JsonProperty("_version")
    String version;

    @JsonProperty("service_response")
    ServiceResponse serviceResponse;

    /**
     *
     */
    public Response() {
    }

    /**
     *
     * @return
     */
    @JsonIgnore
    public String getService() {
        return service;
    }

    /**
     *
     * @param service
     */
    public void setService(String service) {
        this.service = service;
    }

    /**
     *
     * @return
     */
    @JsonIgnore
    public String getVersion() {
        return version;
    }

    /**
     *
     * @param version
     */
    public void setVersion(String version) {
        this.version = version;
    }

    /**
     *
     * @return
     */
    @JsonIgnore
    public ServiceResponse getServiceResponse() {
        return serviceResponse;
    }

    /**
     *
     * @param serviceResponse
     */
    public void setServiceResponse(ServiceResponse serviceResponse) {
        this.serviceResponse = serviceResponse;
    }

}
