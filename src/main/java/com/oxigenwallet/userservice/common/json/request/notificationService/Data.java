/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.oxigenwallet.userservice.common.json.request.notificationService;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 *
 * @author Asce
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Data {

    @JsonProperty("mdn")
    String mdn;
    @JsonProperty("otp")
    int otp;
    @JsonProperty("username")
    String username;

    public Data() {
    }

    public Data(String mdn) {
        this.mdn = mdn;
    }

    public Data(String mdn, int otp) {
        this.mdn = mdn;
        this.otp = otp;
    }

    public Data(String mdn, int otp, String username) {
        this.mdn = mdn;
        this.otp = otp;
        this.username = username;
    }

    @JsonIgnore
    public String getMdn() {
        return mdn;
    }

    public void setMdn(String mdn) {
        this.mdn = mdn;
    }

    @JsonIgnore
    public int getOtp() {
        return otp;
    }

    public void setOtp(int otp) {
        this.otp = otp;
    }

}
