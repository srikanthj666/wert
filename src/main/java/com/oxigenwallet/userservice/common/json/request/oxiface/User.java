/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.oxigenwallet.userservice.common.json.request.oxiface;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 *
 * @author Asce
 */
public class User {

    @JsonProperty("mdn")
    String mdn;

    @JsonProperty("token")
    String token;

    @JsonProperty("referral_code")
    String referralCode;

    /**
     *
     */
    public User() {
    }

    /**
     *
     * @param mdn
     * @param token
     * @param referralCode
     */
    public User(String mdn, String token, String referralCode) {
        this.mdn = mdn;
        this.token = token;
        this.referralCode = referralCode;
    }

    /**
     *
     * @return
     */
    @JsonIgnore
    public String getMdn() {
        return mdn;
    }

    /**
     *
     * @param mdn
     */
    public void setMdn(String mdn) {
        this.mdn = mdn;
    }

    /**
     *
     * @return
     */
    @JsonIgnore
    public String getToken() {
        return token;
    }

    /**
     *
     * @param token
     */
    public void setToken(String token) {
        this.token = token;
    }

    /**
     *
     * @return
     */
    @JsonIgnore
    public String getReferralCode() {
        return referralCode;
    }

    /**
     *
     * @param referralCode
     */
    public void setReferralCode(String referralCode) {
        this.referralCode = referralCode;
    }

}
