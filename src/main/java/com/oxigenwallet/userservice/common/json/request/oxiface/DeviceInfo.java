/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.oxigenwallet.userservice.common.json.request.oxiface;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 *
 * @author Asce
 */
public class DeviceInfo {

    @JsonProperty("device_id")
    String deviceId;
    
    @JsonProperty("device_os")
    String deviceOs;
    
    @JsonProperty("latitude")
    String latitude;
    
    @JsonProperty("longitude")
    String longitude;

    /**
     *
     */
    public DeviceInfo() {
    }

    /**
     *
     * @param deviceId
     * @param deviceOs
     * @param latitude
     * @param longitude
     */
    public DeviceInfo(String deviceId, String deviceOs, String latitude, String longitude) {
        this.deviceId = deviceId;
        this.deviceOs = deviceOs;
        this.latitude = latitude;
        this.longitude = longitude;
    }

    /**
     *
     * @return
     */
    @JsonIgnore
    public String getDeviceId() {
        return deviceId;
    }

    /**
     *
     * @param deviceId
     */
    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    /**
     *
     * @return
     */
    @JsonIgnore
    public String getDeviceOs() {
        return deviceOs;
    }

    /**
     *
     * @param deviceOs
     */
    public void setDeviceOs(String deviceOs) {
        this.deviceOs = deviceOs;
    }

    /**
     *
     * @return
     */
    @JsonIgnore
    public String getLatitude() {
        return latitude;
    }

    /**
     *
     * @param latitude
     */
    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    /**
     *
     * @return
     */
    @JsonIgnore
    public String getLongitude() {
        return longitude;
    }

    /**
     *
     * @param longitude
     */
    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

}
