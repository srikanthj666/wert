/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.oxigenwallet.userservice.common.json.response;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 *
 * @author Asce
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Tokens {
    @JsonProperty("scope")
    String scope;
    
    @JsonProperty("client_id")
    String clientId;
    
    @JsonProperty("expire_in")
    String expireIn;
    
    @JsonProperty("timestamp")
    String timeStamp;
    
    @JsonProperty("device_os")
    String deviceOs;
    
    @JsonProperty("device_os_version")
    String deviceOsVersion;
    
    @JsonProperty("device_id")
    String deviceId;
    
    @JsonProperty("ip_address")
    String ipAddress;
    
    @JsonProperty("model_name")
    String modelName;
    public Tokens() {
    }

    public Tokens(String scope, String clientId, String expireIn, String timeStamp) {
        this.scope = scope;
        this.clientId = clientId;
        this.expireIn = expireIn;
        this.timeStamp = timeStamp;
    }
    
    

    @JsonIgnore
    public String getScope() {
        return scope;
    }

    public void setScope(String scope) {
        this.scope = scope;
    }

    @JsonIgnore
    public String getClientId() {
        return clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    @JsonIgnore
    public String getExpireIn() {
        return expireIn;
    }

    public void setExpireIn(String expireIn) {
        this.expireIn = expireIn;
    }

    @JsonIgnore
    public String getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(String timeStamp) {
        this.timeStamp = timeStamp;
    }

    public String getDeviceOs() {
        return deviceOs;
    }

    public void setDeviceOs(String deviceOs) {
        this.deviceOs = deviceOs;
    }

    public String getDeviceOsVersion() {
        return deviceOsVersion;
    }

    public void setDeviceOsVersion(String deviceOsVersion) {
        this.deviceOsVersion = deviceOsVersion;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public String getIpAddress() {
        return ipAddress;
    }

    public void setIpAddress(String ipAddress) {
        this.ipAddress = ipAddress;
    }

    public String getModelName() {
        return modelName;
    }

    public void setModelName(String modelName) {
        this.modelName = modelName;
    }
    
    
}
