/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.oxigenwallet.userservice.common.json.request;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.springframework.stereotype.Component;

/**
 *
 * @author mitz
 */
@Component
@JsonInclude(JsonInclude.Include.NON_NULL)
public class JsonRequestBody {

    @JsonProperty("request")
    private Request request;

    @JsonProperty("token")
    private String token;

    @JsonProperty("username")
    private String username;

    /**
     *
     */
    public JsonRequestBody() {
    }

    /**
     *
     * @param request Request
     */
    public JsonRequestBody(Request request) {
        this.request = request;
    }

    /**
     *
     * @return Request
     */
    @JsonIgnore
    public Request getRequest() {
        return request;
    }

    /**
     *
     * @param request Request
     */
    public void setRequest(Request request) {
        this.request = request;
    }

    /**
     *
     * @return String
     */
    @JsonIgnore
    public String getToken() {
        return token;
    }

    /**
     *
     * @param token String
     */
    public void setToken(String token) {
        this.token = token;
    }

    @JsonIgnore
    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

}
