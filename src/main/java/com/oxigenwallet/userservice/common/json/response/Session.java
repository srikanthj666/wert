package com.oxigenwallet.userservice.common.json.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class Session {

    @JsonProperty("is_current")
    String is_current;
    @JsonProperty("device_os")
    String device_os;
    @JsonProperty("device_os_version")
    String device_os_version;
    @JsonProperty("device_id")
    String device_id;
    @JsonProperty("imei")
    String imei;
    @JsonProperty("model_name")
    String model_name;
    @JsonProperty("ip_address")
    String ip_address;
    @JsonProperty("updated_on")
    String updated_on;

    public Session() {
    }

    public Session(String is_current, String device_os, String device_os_version, String device_id, String imei, String model_name, String ip_address, String updated_on) {
        this.is_current = is_current;
        this.device_os = device_os;
        this.device_os_version = device_os_version;
        this.device_id = device_id;
        this.imei = imei;
        this.model_name = model_name;
        this.ip_address = ip_address;
        this.updated_on = updated_on;
    }

    public String getIs_current() {
        return is_current;
    }

    public void setIs_current(String is_current) {
        this.is_current = is_current;
    }

    public String getDevice_os() {
        return device_os;
    }

    public void setDevice_os(String device_os) {
        this.device_os = device_os;
    }

    public String getDevice_os_version() {
        return device_os_version;
    }

    public void setDevice_os_version(String device_os_version) {
        this.device_os_version = device_os_version;
    }

    public String getDevice_id() {
        return device_id;
    }

    public void setDevice_id(String device_id) {
        this.device_id = device_id;
    }

    public String getImei() {
        return imei;
    }

    public void setImei(String imei) {
        this.imei = imei;
    }

    public String getModel_name() {
        return model_name;
    }

    public void setModel_name(String model_name) {
        this.model_name = model_name;
    }

    public String getIp_address() {
        return ip_address;
    }

    public void setIp_address(String ip_address) {
        this.ip_address = ip_address;
    }

    public String getUpdated_on() {
        return updated_on;
    }

    public void setUpdated_on(String updated_on) {
        this.updated_on = updated_on;
    }
}
