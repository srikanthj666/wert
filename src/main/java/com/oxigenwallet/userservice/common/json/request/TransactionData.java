/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.oxigenwallet.userservice.common.json.request;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 *
 * @author mitz
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class TransactionData {
    
    @JsonProperty("code")
    private String code;
    
    @JsonProperty("code_type")
    private String codeType;

    /**
     *
     * @return String
     */
    public String getCode() {
        return code;
    }

    /**
     *
     * @param code String
     */
    public void setCode(String code) {
        this.code = code;
    }

    /**
     *
     * @return String
     */
    public String getCodeType() {
        return codeType;
    }

    /**
     *
     * @param codeType StringØ
     */
    public void setCodeType(String codeType) {
        this.codeType = codeType;
    }
    
}
