/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.oxigenwallet.userservice.common.json.response;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 *
 * @author Asce
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class TokenInfo {

    @JsonProperty("username")
    String userName;

    @JsonProperty("token")
    String token;

    @JsonProperty("expires_in")
    String expiresIn;

    @JsonProperty("scope")
    String scope;

    @JsonProperty("token_type")
    String tokenType;

    @JsonProperty("oxiface_token")
    String oxifaceToken;

    /**
     *
     * @param userName
     * @param token
     * @param expiresIn
     * @param scope
     * @param tokenType
     * @param oxifaceToken
     */
    public TokenInfo(String userName, String token, String expiresIn, String scope, String tokenType, String oxifaceToken) {
        this.userName = userName;
        this.token = token;
        this.expiresIn = expiresIn;
        this.scope = scope;
        this.tokenType = tokenType;
        this.oxifaceToken = oxifaceToken;
    }

    /**
     *
     */
    public TokenInfo() {
    }

    /**
     *
     * @return
     */
    @JsonIgnore
    public String getUserName() {
        return userName;
    }

    /**
     *
     * @param userName
     */
    public void setUserName(String userName) {
        this.userName = userName;
    }

    /**
     *
     * @return
     */
    @JsonIgnore
    public String getToken() {
        return token;
    }

    /**
     *
     * @param token
     */
    public void setToken(String token) {
        this.token = token;
    }

    /**
     *
     * @return
     */
    @JsonIgnore
    public String getExpiresIn() {
        return expiresIn;
    }

    /**
     *
     * @param expiresIn
     */
    public void setExpiresIn(String expiresIn) {
        this.expiresIn = expiresIn;
    }

    /**
     *
     * @return
     */
    @JsonIgnore
    public String getScope() {
        return scope;
    }

    /**
     *
     * @param scope
     */
    public void setScope(String scope) {
        this.scope = scope;
    }

    /**
     *
     * @return
     */
    @JsonIgnore
    public String getTokenType() {
        return tokenType;
    }

    /**
     *
     * @param tokenType
     */
    public void setTokenType(String tokenType) {
        this.tokenType = tokenType;
    }

    /**
     *
     * @return
     */
    @JsonIgnore
    public String getOxifaceToken() {
        return oxifaceToken;
    }

    /**
     *
     * @param oxifaceToken
     */
    public void setOxifaceToken(String oxifaceToken) {
        this.oxifaceToken = oxifaceToken;
    }

}
