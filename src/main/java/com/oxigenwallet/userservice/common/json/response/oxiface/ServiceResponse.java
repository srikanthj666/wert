/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.oxigenwallet.userservice.common.json.response.oxiface;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 *
 * @author Asce
 */
public class ServiceResponse {

    @JsonProperty("response_info")
    ResponseInfo responseInfo;

    @JsonProperty("transaction_info")
    TransactionInfo transactionInfo;

    @JsonProperty("user")
    User user;

    @JsonProperty("params")
    Params params;

    /**
     *
     */
    public ServiceResponse() {
    }

    /**
     *
     * @return
     */
    @JsonIgnore
    public ResponseInfo getResponseInfo() {
        return responseInfo;
    }

    /**
     *
     * @param responseInfo
     */
    public void setResponseInfo(ResponseInfo responseInfo) {
        this.responseInfo = responseInfo;
    }

    /**
     *
     * @return
     */
    @JsonIgnore
    public TransactionInfo getTransactionInfo() {
        return transactionInfo;
    }

    /**
     *
     * @param transactionInfo
     */
    public void setTransactionInfo(TransactionInfo transactionInfo) {
        this.transactionInfo = transactionInfo;
    }

    /**
     *
     * @return
     */
    @JsonIgnore
    public User getUser() {
        return user;
    }

    /**
     *
     * @param user
     */
    public void setUser(User user) {
        this.user = user;
    }

    /**
     *
     * @return
     */
    @JsonIgnore
    public Params getParams() {
        return params;
    }

    /**
     *
     * @param params
     */
    public void setParams(Params params) {
        this.params = params;
    }

}
