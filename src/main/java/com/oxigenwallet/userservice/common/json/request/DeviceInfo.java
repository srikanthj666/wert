/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.oxigenwallet.userservice.common.json.request;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 *
 * @author mitz
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class DeviceInfo {

    @JsonProperty("device_os")
    private String deviceOs;

    @JsonProperty("device_os_version")
    private String deviceOsVersion;

    @JsonProperty("device_id")
    private String deviceId;

    @JsonProperty("imei")
    private String imei;

    @JsonProperty("latitude")
    private String latitude;

    @JsonProperty("longitude")
    private String longitude;

    @JsonProperty("model_name")
    private String modelName;

    @JsonProperty("ip_address")
    private String ipAddress;

    @JsonProperty("time_stamp")
    String timeStamp;

    /**
     *
     * @return String
     */
    @JsonIgnore
    public String getDeviceOs() {
        return deviceOs;
    }

    /**
     *
     * @param deviceOs String
     */
    public void setDeviceOs(String deviceOs) {
        this.deviceOs = deviceOs;
    }

    /**
     *
     * @return String
     */
    @JsonIgnore
    public String getDeviceOsVersion() {
        return deviceOsVersion;
    }

    /**
     *
     * @param deviceOsVersion String
     */
    public void setDeviceOsVersion(String deviceOsVersion) {
        this.deviceOsVersion = deviceOsVersion;
    }

    /**
     *
     * @return String
     */
    @JsonIgnore
    public String getDeviceId() {
        return deviceId;
    }

    /**
     *
     * @param deviceId String
     */
    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    /**
     *
     * @return String
     */
    @JsonIgnore
    public String getImei() {
        return imei;
    }

    /**
     *
     * @param imei String
     */
    public void setImei(String imei) {
        this.imei = imei;
    }

    /**
     *
     * @return String
     */
    @JsonIgnore
    public String getLatitude() {
        return latitude;
    }

    /**
     *
     * @param latitude String
     */
    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    /**
     *
     * @return String
     */
    @JsonIgnore
    public String getLongitude() {
        return longitude;
    }

    /**
     *
     * @param longitude String
     */
    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    /**
     *
     * @return String
     */
    @JsonIgnore
    public String getModelName() {
        return modelName;
    }

    /**
     *
     * @param modelName String
     */
    public void setModelName(String modelName) {
        this.modelName = modelName;
    }

    /**
     *
     * @return String
     */
    @JsonIgnore
    public String getIpAddress() {
        return ipAddress;
    }

    /**
     *
     * @param ipAddress String
     */
    public void setIpAddress(String ipAddress) {
        this.ipAddress = ipAddress;
    }

    /**
     *
     * @return String
     */
    @JsonIgnore
    public String getTimeStamp() {
        return timeStamp;
    }

    /**
     *
     * @param timeStamp String
     */
    public void setTimeStamp(String timeStamp) {
        this.timeStamp = timeStamp;
    }

}
