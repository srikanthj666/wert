/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.oxigenwallet.userservice.common.json.request;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 *
 * @author mitz
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class DocumentInfo {
    @JsonProperty("kyc_type")
    private String kyc_type;
    @JsonProperty("type")
    private String type;
    @JsonProperty("type_id")
    private String type_id;
    @JsonProperty("ref_param1")
    private String ref_param1;
    @JsonProperty("ref_param2")
    private String ref_param2;
    @JsonProperty("vcopy")
    private String vcopy;

    /**
     *
     */
    public DocumentInfo() {
    }

    /**
     *
     * @return String
     */
    public String getKyc_type() {
        return kyc_type;
    }

    /**
     *
     * @param kyc_type String
     */
    public void setKyc_type(String kyc_type) {
        this.kyc_type = kyc_type;
    }

    /**
     *
     * @return String
     */
    public String getType() {
        return type;
    }

    /**
     *
     * @param type String
     */
    public void setType(String type) {
        this.type = type;
    }

    /**
     *
     * @return String
     */
    public String getType_id() {
        return type_id;
    }

    /**
     *
     * @param type_id String
     */
    public void setType_id(String type_id) {
        this.type_id = type_id;
    }

    /**
     *
     * @return String
     */
    public String getRef_param1() {
        return ref_param1;
    }

    /**
     *
     * @param ref_param1 String
     */
    public void setRef_param1(String ref_param1) {
        this.ref_param1 = ref_param1;
    }

    /**
     *
     * @return String
     */
    public String getRef_param2() {
        return ref_param2;
    }

    /**
     *
     * @param ref_param2 String
     */
    public void setRef_param2(String ref_param2) {
        this.ref_param2 = ref_param2;
    }

    /**
     *
     * @return String
     */
    public String getVcopy() {
        return vcopy;
    }

    /**
     *
     * @param vcopy String
     */
    public void setVcopy(String vcopy) {
        this.vcopy = vcopy;
    }

}
