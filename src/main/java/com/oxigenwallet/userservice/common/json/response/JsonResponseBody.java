/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.oxigenwallet.userservice.common.json.response;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author mitz
 */
@JsonInclude(Include.NON_NULL)
public class JsonResponseBody {

    @JsonProperty("tokens")
    private ArrayList<Tokens> tokens;
    
    @JsonProperty("user")
    private User user;

    @JsonProperty("response")
    private Response response;

    @JsonProperty("status")
    private String status;

    @JsonProperty("response_code")
    private int responseCode;

    @JsonProperty("response_description")
    private String responseDescription;

    /**
     *
     */
    public JsonResponseBody() {
    }

    /**
     *
     * @param response
     * @param status
     * @param responseCode
     * @param responseDescription
     */
    public JsonResponseBody(Response response, String status, int responseCode, String responseDescription) {
        this.response = response;
        this.status = status;
        this.responseCode = responseCode;
        this.responseDescription = responseDescription;
    }

    /**
     *
     * @return
     */
    @JsonIgnore
    public User getUser() {
        return user;
    }

    /**
     *
     * @param user
     */
    public void setUser(User user) {
        this.user = user;
    }

    /**
     *
     * @return
     */
    @JsonIgnore
    public Response getResponse() {
        return response;
    }

    /**
     *
     * @param response
     */
    public void setResponse(Response response) {
        this.response = response;
    }

    /**
     *
     * @return
     */
    @JsonIgnore
    public String getStatus() {
        return status;
    }

    /**
     *
     * @param status
     */
    public void setStatus(String status) {
        this.status = status;
    }

    /**
     *
     * @return
     */
    @JsonIgnore
    public int getResponseCode() {
        return responseCode;
    }

    /**
     *
     * @param responseCode
     */
    public void setResponseCode(int responseCode) {
        this.responseCode = responseCode;
    }

    /**
     *
     * @return
     */
    @JsonIgnore
    public String getResponseDescription() {
        return responseDescription;
    }

    /**
     *
     * @param responseDescription
     */
    public void setResponseDescription(String responseDescription) {
        this.responseDescription = responseDescription;
    }

    @JsonIgnore
    public List<Tokens> getTokens() {
        return tokens;
    }

    public void setTokens(ArrayList<Tokens> tokens) {
        this.tokens = tokens;
    }

}
