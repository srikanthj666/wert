/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.oxigenwallet.userservice.common;

import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.sql.Timestamp;
import java.util.Calendar;
import java.util.Date;
import java.util.UUID;

import org.springframework.stereotype.Component;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;

/**
 * @author sagarsharma
 */
@Component
public class GenerateToken {

    /**
     * @return Token
     */
//    public String getToken() {
//        return UUID.fromString(UUID.nameUUIDFromBytes(UUID.randomUUID().toString().getBytes()).toString()).toString();
//    }

    public static String salt = "OWAPP9921";

    public String getToken(String mdn) {
        String plainText = mdn + "," + UUID.randomUUID() + "," + this.getTimestamp();
        String hashText = hmacDigest(plainText, salt, "HmacSHA1");
        System.out.println(plainText);
        System.out.println(hashText);
        return plainText + ":" + hashText;
    }

    private long getTimestamp() {
        Calendar cal = Calendar.getInstance();
        Timestamp timestamp = new Timestamp(new Date().getTime());
        cal.setTimeInMillis(timestamp.getTime());

        cal.add(Calendar.MONTH, 12);
        timestamp = new Timestamp(cal.getTime().getTime());

        return timestamp.getTime();
    }

    public static String hmacDigest(String msg, String keyString, String algo) {
        String digest = null;
        try {
            SecretKeySpec key = new SecretKeySpec((keyString).getBytes("UTF-8"), algo);
            Mac mac = Mac.getInstance(algo);
            mac.init(key);

            byte[] bytes = mac.doFinal(msg.getBytes("ASCII"));

            StringBuffer hash = new StringBuffer();
            for (int i = 0; i < bytes.length; i++) {
                String hex = Integer.toHexString(0xFF & bytes[i]);
                if (hex.length() == 1) {
                    hash.append('0');
                }
                hash.append(hex);
            }
            digest = hash.toString();
        } catch (UnsupportedEncodingException e) {
        } catch (InvalidKeyException e) {
        } catch (NoSuchAlgorithmException e) {
        }
        return digest;
    }
}
