/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.oxigenwallet.userservice.common;

import com.oxigenwallet.userservice.common.constants.Constants;
import com.oxigenwallet.userservice.common.constants.ErrorCodes;
import com.oxigenwallet.userservice.common.json.request.DeviceInfo;
import com.oxigenwallet.userservice.common.json.request.JsonRequestBody;
import com.oxigenwallet.userservice.common.json.request.Request;
import com.oxigenwallet.userservice.common.json.request.User;
import com.oxigenwallet.userservice.common.json.response.JsonResponseBody;
import com.oxigenwallet.userservice.model.Token;
import com.oxigenwallet.userservice.utils.RedisToken;
import com.oxigenwallet.userservice.utils.Util;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.regex.Pattern;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.stereotype.Component;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.Pipeline;
import redis.clients.jedis.Response;

/**
 *
 * @author Asce
 */
@SpringBootApplication(scanBasePackages = {"com.oxigenwallet.userservice.common"})
@Component
public class RedisStorage {

    @Value("${redis.dbName:US}")
    String redisDbName;

    @Autowired
    RedisCacheResource redisCacheResource;

    /**
     *
     * @param namespace
     * @return string
     */
    public String GenerateKey(String... namespace) {
        String key = redisDbName + ":";
        for (String s : namespace) {
            key = key + s + "_";
        }
        return key.substring(0, key.length() - 1);
    }

    /**
     *
     * @param userName userName
     * @param tokenDetails tokenDetails
     * @param clientId clientId
     * @param expiresIn expiresIn
     */
    public void saveAccessToken(String userName, Map<String, String> tokenDetails, String clientId, int expiresIn) {

        String key = GenerateKey(Constants.REDIS_NAMESPACE[0], userName, tokenDetails.get("token"), clientId);

        Jedis jedis = redisCacheResource.getCacheResource().getResource();
        Pipeline pipeline = jedis.pipelined();

        pipeline.multi();
        pipeline.hmset(key, tokenDetails);
        pipeline.expire(key, expiresIn);

        Response<List<Object>> response = pipeline.exec();

        pipeline.sync();
        jedis.close();

        String hmsetResult = response.get().get(0).toString();
        long expireResult = (long) response.get().get(1);

        if (hmsetResult == null || expireResult == 0) {
            return;// exception 
        }
    }

    public void setAllActiveToken(List<Token> activeTokens) {
        String username;
        String token;
        String clientId;
        String scope;
        Timestamp expireAt;
        Long currTime;

        Jedis jedis = redisCacheResource.getCacheResource().getResource();
        Pipeline pipeline = jedis.pipelined();
        pipeline.multi();

        for (Token activeToken : activeTokens) {
            username = activeToken.getUsername();
            token = activeToken.getToken();
            clientId = activeToken.getClient_id();
            expireAt = activeToken.getExpired_at();

            currTime = System.currentTimeMillis();

            int expireIn = (int) ((expireAt.getTime() - currTime) / 1000);

            String key = GenerateKey(Constants.REDIS_NAMESPACE[0], username, token, clientId);

            Map<String, String> tokenMap = new HashMap<String, String>();

            tokenMap.put("token", token);
            tokenMap.put("time_stamp", Util.getTimeStamp());
            tokenMap.put("expires_in", expireIn + "");
            pipeline.hmset(key, tokenMap);
            pipeline.expire(key, expireIn);

        }
        pipeline.exec();
        pipeline.sync();
        jedis.close();
    }

    /**
     *
     * @param userName userName
     * @param token token
     * @return Boolean
     */
//    public boolean isAccessTokenExist(String userName, String token) {
//        String key = GenerateKey(Constants.REDIS_NAMESPACE[0], userName, token);
//        Jedis jedis = redisCacheResource.getCacheResource().getResource();
//        String newkey = key + "_OWAPPKEY7773";
//        System.out.println("IsAccessTokenExist:" + newkey);
//        boolean flag = jedis.exists(newkey);
////        List<String> keysResult = new ArrayList<String>( jedis.keys(newkey));
////        List<String> keysResult = new ArrayList<String>(jedis.keys(key + "_*"));
//
//        jedis.close();
//        return flag;
//    }

    public boolean isAccessTokenExist(String userName, String token) {
//        String key = GenerateKey(Constants.REDIS_NAMESPACE[0], userName, token);
//        Jedis jedis = RedisCacheResource.getPoolInstance().getResource();
//        List<String> keysResult = new ArrayList<String>(jedis.keys(key + "_*"));
//        jedis.close();
//        return keysResult.size() == 0;

        String[] parts = token.split(":");
        if (parts.length == 1) {
            return false;
        }

        String part1 = parts[0];
        String part2 = parts[1];

        String hash = GenerateToken.hmacDigest(part1, GenerateToken.salt, "HmacSHA1");

        if (!hash.equals(part2)) {
            System.out.printf("Invalid token");
            return false;
        }

        String[] data = parts[0].split(",");
        if (data.length == 1) {
            return false;
        }
        if (!data[0].equals(userName)) {
            System.out.printf("Invalid token");
            return false;
        }

        Date now = new Date();
        Timestamp tokenTs = new Timestamp(Long.parseLong(data[2]));

        Timestamp nowTs = new Timestamp(now.getTime());
        if (!tokenTs.after(nowTs)) {
            System.out.printf("Token Expired");
            return false;
        }

        return true;
    }


    public boolean isDeviceAccessTokenExist(String userName, String token, DeviceInfo deviceInfo){
        String key = GenerateKey(Constants.REDIS_NAMESPACE[0], userName, token);
        Jedis jedis = redisCacheResource.getCacheResource().getResource();
        List<String> keysResult = new ArrayList<String>(jedis.keys(key + "_*"));

        Iterator<String> iterator = keysResult.iterator();
        String listKey = null;
        String device_id = null;
        if(!deviceInfo.getDeviceId().isEmpty() && deviceInfo.getDeviceId() !=null){
            while(iterator.hasNext()){
                listKey = iterator.next();
                device_id = jedis.hget(listKey, Constants.REDIS_ATTR.get("device_id"));
                if(device_id.equalsIgnoreCase(deviceInfo.getDeviceId())){
                    jedis.close();
                    return true;
                }
            }
            jedis.close();
            return false;
        }else{
            jedis.close();
            return keysResult.size() != 0;
        }
    }

    /**
     *
     * @param token token
     * @return boolean
     */
    public boolean deleteAccessToken(String token) {

        Jedis jedis = redisCacheResource.getCacheResource().getResource();
        List<String> list = new ArrayList<String>(jedis.keys("*_" + token + "_*"));
        if (list.size() == 0) {
            jedis.close();
            return false;
        }
        String key = list.get(0);

        long result = jedis.del(key);
        if (result == 0) {
            jedis.close();
            return false;
        }
        jedis.close();
        return true;

    }

    public void deleteAllAccessToken(String username) {

        Jedis jedis = redisCacheResource.getCacheResource().getResource();
        String pattern = GenerateKey(Constants.REDIS_NAMESPACE[0], username);
        pattern = pattern + "_*";
        List<String> keys = new ArrayList<String>(jedis.keys(pattern));
        Pipeline pipeline = jedis.pipelined();
        pipeline.multi();
        for (String key : keys) {
            pipeline.del(key);
        }
        pipeline.exec();
        pipeline.sync();
        jedis.close();
    }

    public ArrayList<RedisToken> getAllAccessToken(String username) {

        Jedis jedis = redisCacheResource.getCacheResource().getResource();
        String pattern = GenerateKey(Constants.REDIS_NAMESPACE[0], username);
        pattern = pattern + "_*";
        List<String> keys = new ArrayList<String>(jedis.keys(pattern));
        Pipeline pipeline = jedis.pipelined();
        pipeline.multi();
        for (String key : keys) {
            pipeline.hgetAll(key);
        }
        Response<List<Object>> response = pipeline.exec();
        pipeline.sync();
        jedis.close();

        List<Object> keysResult = response.get();

        ArrayList<RedisToken> allTokenResult = new ArrayList<RedisToken>();

        for (Object obj : keysResult) {
            HashMap<String, String> map = (HashMap<String, String>) obj;

            RedisToken token = new RedisToken();
            for (Map.Entry<String, String> entry : map.entrySet()) {
                String key = entry.getKey();
                String value = entry.getValue();

                if (key.equals("time_stamp")) {
                    token.setTimeStamp(value);
                } else if (key.equals("expires_in")) {
                    token.setExpireIn(value);
                } else if (key.equals("scope")) {
                    token.setScope(value);
                } else if (key.equals("token")) {
                    token.setToken(value);
                } else if (key.equals("client_id")) {
                    token.setClientId(value);
                } else if (key.equals("device_os")) {
                    token.setDeviceOs(value);
                } else if (key.equals("ip")) {
                    token.setIp(value);
                } else if (key.equals("device_id")) {
                    token.setDeviceId(value);
                } else if (key.equals("device_version")) {
                    token.setDeviceVersion(value);
                } else if (key.equals("model_name")) {
                    token.setModelName(value);
                }

            }
            allTokenResult.add(token);
        }

        return allTokenResult;
    }

    /**
     *
     * @param userName username
     * @param nxtxnToken nxtxnToken
     * @param merchantName merchant Name
     */
    public void saveNxtxnToken(String userName, String nxtxnToken, String merchantName) {

        String key = GenerateKey(Constants.REDIS_NAMESPACE[1], userName, merchantName);
        Jedis jedis = redisCacheResource.getCacheResource().getResource();
        String result = jedis.set(key, nxtxnToken);
        jedis.close();
        if (result == null) {
            return; // throws exception
        }

    }

    /**
     *
     * @param userName username
     * @param tokenFor tokenfor
     * @return result
     */
    public String getNxtxnToken(String userName, String tokenFor) {
        String key = GenerateKey(Constants.REDIS_NAMESPACE[1], userName, tokenFor);
        System.out.println("NxTxN token: "+ key);
        Jedis jedis = redisCacheResource.getCacheResource().getResource();
        String result = jedis.get(key);
        jedis.close();
        return result;
    }

    /**
     *
     * @param clientId clientId
     * @return result
     */
    public Map<String, String> getClient(String clientId) {
        String key = GenerateKey(clientId);
        Jedis jedis = redisCacheResource.getCacheResource().getResource();
        Map<String, String> result = jedis.hgetAll(key);
        jedis.close();
        return result;
    }

    /**
     *
     * @param clientId clientId
     * @return result
     */
    public String getClientPassword(String clientId) {
        String key = GenerateKey(clientId);
        Jedis jedis = redisCacheResource.getCacheResource().getResource();
        String result = jedis.hget(key, "password");
        jedis.close();
        return result;
    }

    public Map<String, String> getUserDetails(String username) {
        String key = GenerateKey(username);
        Jedis jedis = redisCacheResource.getCacheResource().getResource();
        Map<String, String> redisResult = jedis.hgetAll(key);

        jedis.close();

        return redisResult;
    }

    /**
     *
     * @param jsonRequestBody jsonRequestBody
     * @param userStatus userStatus
     */
    public void saveUserDetails(JsonRequestBody jsonRequestBody, int userStatus) {

        Request request = jsonRequestBody.getRequest();
        User userJ = request.getUser();

        Jedis jedisObject = redisCacheResource.getCacheResource().getResource();
        Map<String, String> hmap = new HashMap<>();

        hmap.put("username", userJ.getUsername() + "");
        hmap.put("first_name", userJ.getName() + "");
        hmap.put("middle_name", userJ.getMiddleName() + "");
        hmap.put("last_name", userJ.getLastName() + "");
        hmap.put("email", userJ.getEmail() + "");
        hmap.put("dob", userJ.getDob() + "");
        hmap.put("gender", userJ.getGender() + "");
        hmap.put("address", userJ.getAddress() + "");
        hmap.put("city", userJ.getCity() + "");
        hmap.put("district", userJ.getDistrict() + "");
        hmap.put("zip", userJ.getZipcode() + "");
        hmap.put("state", userJ.getState() + "");
        hmap.put("country", userJ.getCountry() + "");
        hmap.put("status", String.valueOf(userStatus));
        hmap.put("landline", userJ.getLandline() + "");
        Timestamp date = new Timestamp(new java.util.Date().getTime());
        hmap.put("date_created", date.toString());
        hmap.put("date_updated", date.toString());
        hmap.put("info_type", null + "");
       // hmap.put( "profile_type", userJ.getProfile_type() );

        String key = GenerateKey(userJ.getUsername());
        jedisObject.hmset(key, hmap);
        jedisObject.close();
    }

    public void deleteUserDetails(String username) {
        String key = GenerateKey(username);
        Jedis jedisObject = redisCacheResource.getCacheResource().getResource();
        jedisObject.del(key);
        jedisObject.close();
    }

    public void saveUserDetailsKyc(JsonRequestBody jsonRequestBody, int userStatus) {

        Request request = jsonRequestBody.getRequest();
        User userJ = request.getUser();

        Jedis jedisObject = redisCacheResource.getCacheResource().getResource();
        Map<String, String> hmap = new HashMap<>();

        hmap.put("username", userJ.getUsername() + "");
        hmap.put("first_name", userJ.getName() + "");
        hmap.put("middle_name", userJ.getMiddleName() + "");
        hmap.put("last_name", userJ.getLastName() + "");
        hmap.put("email", userJ.getEmail() + "");
        hmap.put("dob", userJ.getDob() + "");
        hmap.put("gender", userJ.getGender() + "");
        hmap.put("address", userJ.getAddress() + "");
        hmap.put("city", userJ.getCity() + "");
        hmap.put("district", userJ.getDistrict() + "");
        hmap.put("zip", userJ.getZipcode() + "");
        hmap.put("state", userJ.getState() + "");
        hmap.put("country", userJ.getCountry() + "");
        hmap.put("status", String.valueOf(userStatus));
        hmap.put("landline", userJ.getLandline() + "");
        hmap.put("profile_type", Constants.USER_PROFILE_INFO[1]);
        Timestamp date = new Timestamp(new java.util.Date().getTime());
        hmap.put("date_created", date.toString());
        hmap.put("date_updated", date.toString());
        hmap.put("info_type", null + "");

        String key = GenerateKey(userJ.getUsername());
        jedisObject.hmset(key, hmap);
        jedisObject.close();
    }

    /**
     *
     * @param userM userM
     */
    public void saveUserDetails(com.oxigenwallet.userservice.model.User userM) {

        Jedis jedisObject = redisCacheResource.getCacheResource().getResource();
        String key = GenerateKey(userM.getUsername());

        Map<String, String> hmap = new HashMap<>();
        hmap.put("username", userM.getUsername() + "");
        hmap.put("first_name", userM.getFirst_name() + "");
        hmap.put("middle_name", userM.getMiddle_name() + "");
        hmap.put("last_name", userM.getLast_name() + "");
        hmap.put("email", userM.getEmail() + "");
        hmap.put("dob", userM.getDob() + "");
        String genderString = "";
        if (userM.getGender() != '\0') {
            genderString = Character.toString(userM.getGender());
        }
        hmap.put("gender", genderString + "");
        hmap.put("profile_type", userM.getProfile_type() + "");
        hmap.put("address", userM.getAddress() + "");
        hmap.put("city", userM.getCity() + "");
        hmap.put("district", userM.getDistrict() + "");
        hmap.put("info_type", userM.getInfo_type() + "");
        String zipCode = null;
        if (userM.getZip() != null) {
            zipCode = Integer.toString(userM.getZip());
        }
        hmap.put("zip", zipCode + "");
        hmap.put("state", userM.getState() + "");
        hmap.put("country", userM.getCountry() + "");
        String userStatus = " ";
        if (userM.getStatus() != null) {
            userStatus = Integer.toString(userM.getStatus());
        }
        hmap.put("status", userStatus + "");
        hmap.put("landline", userM.getLandline() + "");

        Timestamp date = new Timestamp(new java.util.Date().getTime());
        hmap.put("date_created", userM.getDate_created() + "");
        hmap.put("created_by", userM.getCreated_by() + "");
        hmap.put("date_updated", date.toString());
        hmap.put( "document", userM.getDocument()+ "");
        hmap.put( "Pan_Verified", userM.getPan_Verified()+ "");



        jedisObject.hmset(key, hmap);
        jedisObject.close();
    }

    /**
     *
     * @param userInfoJsonResponse userInfoJsonResponse
     * @param userStatus userStatus
     * @param info info
     */
//    public void saveGetUserInfoResponse(JsonResponseBody userInfoJsonResponse, String userStatus, String info) {
//        //getting request objects
//        com.oxigenwallet.userservice.common.json.response.Response responseJ = userInfoJsonResponse.getResponse();
//        com.oxigenwallet.userservice.common.json.response.User userJ = responseJ.getUser();
//
//        Jedis jedisObject = redisCacheResource.getCacheResource().getResource();
//        Map<String, String> hmap = new HashMap<>();
//
//        hmap.put("username", userJ.getUsername() + "");
//        hmap.put("first_name", userJ.getName() + "");
//        hmap.put("middle_name", userJ.getMiddle_name() + "");
//        hmap.put("last_name", userJ.getLast_name() + "");
//        hmap.put("email", userJ.getEmail() + "");
//        hmap.put("dob", userJ.getDob() + "");
//        hmap.put("gender", userJ.getGender() + "");
//        hmap.put("profile_type", userJ.getProfile_type() + "");
//        hmap.put("address", userJ.getAddress() + "");
//        hmap.put("city", userJ.getCity() + "");
//        hmap.put("district", userJ.getDistrict() + "");
//        hmap.put("zip", userJ.getZipcode() + "");
//        hmap.put("state", userJ.getState() + "");
//        hmap.put("country", userJ.getCountry() + "");
//        String userStatusCache = "0";
//        if (userStatus.equals("ENABLED") || userStatus.equals("enabled")) {
//            userStatusCache = "1";
//        }
//        hmap.put("status", userStatusCache);
//        hmap.put("landline", userJ.getLandline() + "");
//        Timestamp date = new Timestamp(new java.util.Date().getTime());
//
//        hmap.put("date_created", userJ.getDateCreated());
//
//
//        hmap.put("created_by", userJ.getCreatedBy());
//        hmap.put("date_updated", date.toString());
//        hmap.put("info_type", info);
//        hmap.put( "document", userJ.getDocument() );
//        hmap.put( "Pan_Verified", userJ.getPan_Verified());
//
//        String key = GenerateKey(userJ.getUsername());
//        jedisObject.hmset(key, hmap);
//        jedisObject.close();
//    }

    public void saveGetUserInfoResponse(JsonResponseBody userInfoJsonResponse, String userStatus, String info) {
        //getting request objects
        com.oxigenwallet.userservice.common.json.response.Response responseJ = userInfoJsonResponse.getResponse();
        com.oxigenwallet.userservice.common.json.response.User userJ = responseJ.getUser();
        SimpleDateFormat inSDF = new SimpleDateFormat( "dd/mm/yyyy" );
        SimpleDateFormat outSDF = new SimpleDateFormat( "yyyy-mm-dd HH:mm:ss:SSS" );

        Jedis jedisObject = redisCacheResource.getCacheResource().getResource();
        Map<String, String> hmap = new HashMap<>();

        hmap.put("username", userJ.getUsername() + "");
        hmap.put("first_name", userJ.getName() + "");
        hmap.put("middle_name", userJ.getMiddle_name() + "");
        hmap.put("last_name", userJ.getLast_name() + "");
        hmap.put("email", userJ.getEmail() + "");
        hmap.put("dob", userJ.getDob() + "");
        hmap.put("gender", userJ.getGender() + "");
        hmap.put("profile_type", userJ.getProfile_type() + "");
        hmap.put("address", userJ.getAddress() + "");
        hmap.put("city", userJ.getCity() + "");
        hmap.put("district", userJ.getDistrict() + "");
        hmap.put("zip", userJ.getZipcode() + "");
        hmap.put("state", userJ.getState() + "");
        hmap.put("country", userJ.getCountry() + "");
        String userStatusCache = "0";
        if (userStatus.equals("ENABLED") || userStatus.equals("enabled")) {
            userStatusCache = "1";
        }
        hmap.put("status", userStatusCache);
        hmap.put("landline", userJ.getLandline() + "");
        Timestamp date = new Timestamp(new java.util.Date().getTime());
        // hmap.put("date_created", userJ.getDateCreated());

        //  Timestamp date = new Timestamp(new java.util.Date().getTime());
        //  hmap.put("date_created", userJ.getDateCreated());

        String  inDate = userJ.getDateCreated();

        if (inDate != null) {
            if (Pattern.matches( Constants.DATE_PATTERN, inDate )) {
                String outDate = "";
                Date dateredis = null;

                try {
                    dateredis = inSDF.parse( inDate );
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                try {
                    outDate = outSDF.format( dateredis );
                } catch (Exception ex) {
                }
                hmap.put("date_created",outDate);
            } else {
                hmap.put("date_created",inDate);

            }
        }

        hmap.put("created_by", userJ.getCreatedBy());
        hmap.put("date_updated", date.toString());
        hmap.put("info_type", info);
        hmap.put( "document", userJ.getDocument() );
        hmap.put( "Pan_Verified", userJ.getPan_Verified());

        String key = GenerateKey(userJ.getUsername());
        jedisObject.hmset(key, hmap);
        jedisObject.close();
    }

    /**
     *
     * @param userName userName
     * @param otp otp
     */
    public void saveOtp(String userName, int otp, int count) {
        String key = GenerateKey(Constants.REDIS_NAMESPACE[3], userName, otp + "");

        Jedis jedis = redisCacheResource.getCacheResource().getResource();
        HashMap<String, String> m = new HashMap<String, String>();
        m.put("count", count + "");
        m.put("otp", otp + "");
        String result = jedis.hmset(key, m);
        jedis.expire(key, Constants.OTP_VALIDITY);
        jedis.close();
    }

    /**
     *
     * @param userName userName
     * @param otp otp
     * @return result
     * @throws CustomException CustomException
     */
    public boolean getOtp(String userName, String otp) throws CustomException {
        String key = GenerateKey(Constants.REDIS_NAMESPACE[3], userName, otp);
        System.out.println("OTP Key: "+ key);
        Jedis jedis = redisCacheResource.getCacheResource().getResource();
        String result = jedis.hget(key, "otp");
        jedis.close();
        if (result == null) {
            return false;
        }
        System.out.println(result.toString());
        return result.equals(otp);
    }

    public Map<String, String> getOtp(String userName) throws CustomException {
        String key = GenerateKey(Constants.REDIS_NAMESPACE[3], userName);

        Jedis jedis = redisCacheResource.getCacheResource().getResource();
        List<String> list = new ArrayList<String>(jedis.keys(key + "*"));
        jedis.close();
        if (list.isEmpty()) {
            return null;
        }
        String otpKey = list.get(0);
        if (otpKey == null) {
            return null;
        }

        jedis = redisCacheResource.getCacheResource().getResource();
        Map<String, String> otpResult = jedis.hgetAll(otpKey);
        jedis.close();

        return otpResult;
    }

    public void delOtp(String userName, String otp) throws CustomException {
        String key = GenerateKey(Constants.REDIS_NAMESPACE[3], userName, otp);

        Jedis jedis = redisCacheResource.getCacheResource().getResource();
        Long result = jedis.del(key);
        jedis.close();
    }

//    public void delPreviousOtp(String userName) throws CustomException {
//        String pattern = GenerateKey(Constants.REDIS_NAMESPACE[3], userName, "");
//        Jedis jedis = redisCacheResource.getCacheResource().getResource();
//
//        pattern = pattern+"*";
//        List<String> keys = new ArrayList<String>(jedis.keys(pattern));
//
//        Pipeline pipeline = jedis.pipelined();
//        pipeline.multi();
//        for (String key : keys) {
//            pipeline.del(key);
//        }
//        pipeline.exec();
//        pipeline.sync();
//        jedis.close();
//    }

    /**
     *
     * @param userName userName
     * @param token token
     * @param clientId clientId
     * @return result
     * @throws CustomException CustomException
     */
    public boolean getAuthCode(String userName, String token, String clientId, DeviceInfo deviceInfo) throws CustomException {

        String key = GenerateKey(Constants.REDIS_NAMESPACE[2], userName, token, clientId);
        System.out.println("getAuthCode Redis Key:" + key);
        Jedis jedis = redisCacheResource.getCacheResource().getResource();
//        String result = jedis.get(key);
        Map<String,String> result = jedis.hgetAll(key);
        jedis.close();
        for(Map.Entry<String,String> entry : result.entrySet()){
            System.out.println(entry.getKey()+":"+entry.getValue());
        }
        if(result == null || result.get(Constants.REDIS_ATTR.get("token")) == null || (!result.get(Constants.REDIS_ATTR.get("token")).equals(token))) {
            return false;
            //throw new CustomException(ErrorCodes.CUSTOM_EXCEPTION.getCode(), ErrorCodes.AUTH_CODE_MISMATCH);
        }
        if(deviceInfo.getDeviceId()!=null && !deviceInfo.getDeviceId().isEmpty()){
            String device_id = result.get(Constants.REDIS_ATTR.get("device_id"));
            return device_id.equalsIgnoreCase(deviceInfo.getDeviceId());
        }
        return true;
        //System.out.println("Auth Code:" + result.toString());
        //return result.equals(token);

    }

    /**
     *
     * @param userName userName
     * @param token token
     * @param clientId clientId
     */
    public void saveAuthCode(String userName, String token, String clientId, DeviceInfo deviceInfo) {

        String key = GenerateKey(Constants.REDIS_NAMESPACE[2], userName, token, clientId);
        Jedis jedis = redisCacheResource.getCacheResource().getResource();
//        jedis.setex(key, Constants.AUTH_CODE_VALIDITY, token);
        Map<String,String> mp = new HashMap<>();
        Timestamp date = new Timestamp(new java.util.Date().getTime());
        mp.put(Constants.REDIS_ATTR.get("token"),token);
        mp.put(Constants.REDIS_ATTR.get("device_id"),deviceInfo.getDeviceId()+"");
        mp.put(Constants.REDIS_ATTR.get("device_os"),deviceInfo.getDeviceOs()+"");
        mp.put(Constants.REDIS_ATTR.get("device_os_version"),deviceInfo.getDeviceOsVersion()+"");
        mp.put(Constants.REDIS_ATTR.get("model_name"),deviceInfo.getModelName()+"");
        mp.put(Constants.REDIS_ATTR.get("imei"),deviceInfo.getImei()+"");
        mp.put(Constants.REDIS_ATTR.get("ip_address"),deviceInfo.getIpAddress()+"");
        mp.put(Constants.REDIS_ATTR.get("date_updated"), date.toString());
        mp.put(Constants.REDIS_ATTR.get("data_created"), date.toString());
        Pipeline pipeline = jedis.pipelined();
        pipeline.multi();
        pipeline.hmset(key, mp);
        pipeline.expire(key, Constants.AUTH_CODE_VALIDITY);
        pipeline.exec();
        pipeline.sync();
        jedis.close();

    }

    public void delAuthCode(String userName, String token, String clientId) throws CustomException {

        String key = GenerateKey(Constants.REDIS_NAMESPACE[2], userName, token, clientId);
        Jedis jedis = redisCacheResource.getCacheResource().getResource();
        Long result = jedis.del(key);
        jedis.close();
    }
}
