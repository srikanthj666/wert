package com.oxigenwallet.userservice.common;

import com.oxigenwallet.userservice.common.constants.Constants;
import com.oxigenwallet.userservice.common.constants.ErrorCodes;
import com.oxigenwallet.userservice.common.json.response.JsonResponseBody;
import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.SignatureException;
import javax.xml.parsers.ParserConfigurationException;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.xml.sax.SAXException;

/**
 *
 * @author Asce
 */
@Component
public class ExceptionHandling {

    /**
     *
     * @param ex Exception
     * @return  response
     */
    @ExceptionHandler(Exception.class)
    public JsonResponseBody exceptionHandling(Exception ex) {

        ex.printStackTrace();
        JsonResponseBody exceptionResponse = new JsonResponseBody(null, Constants.FAILURE, ErrorCodes.EXCEPTION.getCode(), ex.getMessage());
        return exceptionResponse;

    }
    
    /**
     *
     * @param ex Exception
     * @return response
     */
    @ExceptionHandler(org.hibernate.exception.SQLGrammarException.class)
    public JsonResponseBody sqlGrammerException(org.hibernate.exception.SQLGrammarException ex) {

        ex.printStackTrace();
        JsonResponseBody exceptionResponse = new JsonResponseBody(null, Constants.FAILURE, ErrorCodes.SQL_EXCEPTION.getCode(), ex.getMessage());
        return exceptionResponse;

    }
    
    /**
     *
     * @param ex Exception
     * @return response
     */
    @ExceptionHandler(org.springframework.web.bind.ServletRequestBindingException.class)
    public JsonResponseBody requestBindingExceptionHandling(org.springframework.web.bind.ServletRequestBindingException ex) {

        ex.printStackTrace();
        JsonResponseBody exceptionResponse = new JsonResponseBody(null, Constants.FAILURE, ErrorCodes.REQUEST_BINDING.getCode(), ex.getMessage());
        return exceptionResponse;

    }

    /**
     *
     * @param ex Exception
     * @return response
     */
    @ExceptionHandler(java.lang.NullPointerException.class)
    public JsonResponseBody nullPointerExceptionHandling(java.lang.NullPointerException ex) {

        ex.printStackTrace();
        JsonResponseBody exceptionResponse = new JsonResponseBody(null, Constants.FAILURE, ErrorCodes.NULL_POINTER.getCode(), ex.getMessage());
        return exceptionResponse;

    }

    /**
     *
     * @param ex exception
     * @return response
     */
    @ExceptionHandler(ParserConfigurationException.class)
    public JsonResponseBody parserConfigurationExceptionHandling(ParserConfigurationException ex) {

        ex.printStackTrace();
        JsonResponseBody exceptionResponse = new JsonResponseBody(null, Constants.FAILURE, ErrorCodes.PARSER_CONFIGURATION.getCode(), ex.getMessage());
        return exceptionResponse;

    }

    /**
     *
     * @param ex exception
     * @return response
     */
    @ExceptionHandler(SAXException.class)
    public JsonResponseBody saxExceptionHandling(SAXException ex) {

        ex.printStackTrace();
        JsonResponseBody exceptionResponse = new JsonResponseBody(null, Constants.FAILURE, ErrorCodes.SAX_EXCEPTION.getCode(), ex.getMessage());
        return exceptionResponse;

    }

    /**
     *
     * @param ex exception
     * @return response
     */
    @ExceptionHandler(IOException.class)
    public JsonResponseBody ioExceptionHandling(IOException ex) {

        ex.printStackTrace();
        JsonResponseBody exceptionResponse = new JsonResponseBody(null, Constants.FAILURE, ErrorCodes.IO.getCode(), ex.getMessage());
        return exceptionResponse;

    }

    /**
     *
     * @param ex Exception
     * @return response
     */
    @ExceptionHandler(SignatureException.class)
    public JsonResponseBody signatureExceptionHandling(SignatureException ex) {

        ex.printStackTrace();
        JsonResponseBody exceptionResponse = new JsonResponseBody(null, Constants.FAILURE, ErrorCodes.SIGNATURE.getCode(), ex.getMessage());
        return exceptionResponse;

    }

    /**
     *
     * @param ex Exception
     * @return response
     */
    @ExceptionHandler(NoSuchAlgorithmException.class)
    public JsonResponseBody noSuchAlgoExceptionHandling(NoSuchAlgorithmException ex) {

        ex.printStackTrace();
        JsonResponseBody exceptionResponse = new JsonResponseBody(null, Constants.FAILURE, ErrorCodes.NO_SUCH_ALGORITHM.getCode(), ex.getMessage());
        return exceptionResponse;

    }

    /**
     *
     * @param ex Exception
     * @return response
     */
    @ExceptionHandler(InvalidKeyException.class)
    public JsonResponseBody invalidKeyExceptionHandling(InvalidKeyException ex) {

        ex.printStackTrace();
        JsonResponseBody exceptionResponse = new JsonResponseBody(null, Constants.FAILURE, ErrorCodes.INVALID_KEY_EXCEPTION.getCode(), ex.getMessage());
        return exceptionResponse;

    }

    /**
     *
     * @param ex Exception
     * @return response
     */
    @ExceptionHandler(CustomException.class)
    public JsonResponseBody customException(CustomException ex) {

        ex.printStackTrace();
        JsonResponseBody exceptionResponse = new JsonResponseBody(null, Constants.FAILURE, ex.getCode(), ex.getMessage());
        return exceptionResponse;

    }

}
