/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.oxigenwallet.userservice.common;

import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.actuate.health.AbstractHealthIndicator;
import org.springframework.boot.actuate.health.Health;
import org.springframework.stereotype.Component;

/**
 *
 * @author sagarsharma
 */
@Component
public class CustomHealthCheck extends AbstractHealthIndicator {

    /*
    private final FileStore fileStore;
    private final long thresholdBytes;

    @Autowired
    public CustomHealthCheck(@Value("${health.filestore.path:${user.dir}}") String path,
                                    @Value("${health.filestore.threshold.bytes:10485760}") long thresholdBytes) throws IOException {
        fileStore = Files.getFileStore(Paths.get(path));
        this.thresholdBytes = thresholdBytes;
    }
     */
    @Value("${actuator.allowed.IP}")
    private String allowedIP;

    @Autowired
    private HttpServletRequest request;

    @Override
    protected void doHealthCheck(Health.Builder builder) throws Exception {

        if (!(request.getRemoteAddr().equals(allowedIP))) {
            builder.withDetail("description", "not trusted client IP");
            builder.status("Forbidden");
            builder.outOfService();
        } else{
            builder.up();
        }
    }

}
