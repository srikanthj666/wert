/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.oxigenwallet.userservice.common;

/**
 *
 * @author Asce
 */
public class CustomException extends Exception {

    int code;
    String message;

    /**
     *
     * @param code code 
     * @param message message
     */
    public CustomException(int code, String message) {
        this.code = code;
        this.message = message;
    }

    /**
     *
     * @return
     */
    public int getCode() {
        return code;
    }

    /**
     *
     * @param code code
     */
    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    /**
     *
     * @param message message
     */
    public void setMessage(String message) {
        this.message = message;
    }
   
}
