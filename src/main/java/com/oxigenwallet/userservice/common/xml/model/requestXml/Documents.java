/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.oxigenwallet.userservice.common.xml.model.requestXml;

import java.util.ArrayList;

/**
 *
 * @author mitz
 */
public class Documents {
    ArrayList<DocumentInfo> Document_info;
    private String count;

    /**
     *
     */
    public Documents() {
    }

    /**
     *
     * @param Document_info
     * @param count
     */
    public Documents(ArrayList<DocumentInfo> Document_info, String count) {
        this.Document_info = Document_info;
        this.count = count;
    }

    /**
     *
     * @return
     */
    public ArrayList<DocumentInfo> getDocument_info() {
        return Document_info;
    }

    /**
     *
     * @param Document_info
     */
    public void setDocument_info(ArrayList<DocumentInfo> Document_info) {
        this.Document_info = Document_info;
    }

    /**
     *
     * @return
     */
    public String getCount() {
        return count;
    }

    /**
     *
     * @param count
     */
    public void setCount(String count) {
        this.count = count;
    }
}
