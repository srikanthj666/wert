/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.oxigenwallet.userservice.common.xml.model.requestXml;

/**
 *
 * @author mitz
 */
public class SystemServiceInfo {
    private String Platform_Name;
    private String Role;
    private String Service;
    private String SubService;
    private String Operation;
    private String ServiceName;

    /**
     *
     * @param Platform_Name
     * @param Role
     * @param Service
     * @param SubService
     * @param Operation
     * @param ServiceName
     */
    public SystemServiceInfo(String Platform_Name, String Role, String Service, String SubService, String Operation, String ServiceName) {
        this.Platform_Name = Platform_Name;
        this.Role = Role;
        this.Service = Service;
        this.SubService = SubService;
        this.Operation = Operation;
        this.ServiceName = ServiceName;
    }

    /**
     *
     */
    public SystemServiceInfo() {
    }

    /**
     *
     * @return
     */
    public String getPlatform_Name() {
        return Platform_Name;
    }

    /**
     *
     * @param Platform_Name
     */
    public void setPlatform_Name(String Platform_Name) {
        this.Platform_Name = Platform_Name;
    }

    /**
     *
     * @return
     */
    public String getRole() {
        return Role;
    }

    /**
     *
     * @param Role
     */
    public void setRole(String Role) {
        this.Role = Role;
    }

    /**
     *
     * @return
     */
    public String getService() {
        return Service;
    }

    /**
     *
     * @param Service
     */
    public void setService(String Service) {
        this.Service = Service;
    }

    /**
     *
     * @return
     */
    public String getSubService() {
        return SubService;
    }

    /**
     *
     * @param SubService
     */
    public void setSubService(String SubService) {
        this.SubService = SubService;
    }

    /**
     *
     * @return
     */
    public String getOperation() {
        return Operation;
    }

    /**
     *
     * @param Operation
     */
    public void setOperation(String Operation) {
        this.Operation = Operation;
    }

    /**
     *
     * @return
     */
    public String getServiceName() {
        return ServiceName;
    }

    /**
     *
     * @param ServiceName
     */
    public void setServiceName(String ServiceName) {
        this.ServiceName = ServiceName;
    }
    
}
