/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.oxigenwallet.userservice.common.xml.model.responseXml;

/**
 *
 * @author mitz
 */
public class SystemServiceResponse {
    private ResponseInfo Response_Info;
    private String name;
    private String version;
    private TransactionInfo Transaction_Info;
    private String User;
    private String id;
    private String status;
    private String username;
    private String username_type;
    private SystemUser SystemUser;

    /**
     *
     */
    public SystemServiceResponse() {
    }

    /**
     *
     * @param Response_Info
     * @param name
     * @param version
     * @param Transaction_Info
     * @param User
     * @param id
     * @param status
     * @param username
     * @param username_type
     * @param SystemUser
     */
    public SystemServiceResponse(ResponseInfo Response_Info, String name, String version, TransactionInfo Transaction_Info, String User, String id, String status, String username, String username_type, SystemUser SystemUser) {
        this.Response_Info = Response_Info;
        this.name = name;
        this.version = version;
        this.Transaction_Info = Transaction_Info;
        this.User = User;
        this.id = id;
        this.status = status;
        this.username = username;
        this.username_type = username_type;
        this.SystemUser = SystemUser;
    }

    /**
     *
     * @return
     */
    public ResponseInfo getResponse_Info() {
        return Response_Info;
    }

    /**
     *
     * @param Response_Info
     */
    public void setResponse_Info(ResponseInfo Response_Info) {
        this.Response_Info = Response_Info;
    }

    /**
     *
     * @return
     */
    public String getName() {
        return name;
    }

    /**
     *
     * @param name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     *
     * @return
     */
    public String getVersion() {
        return version;
    }

    /**
     *
     * @param version
     */
    public void setVersion(String version) {
        this.version = version;
    }

    /**
     *
     * @return
     */
    public TransactionInfo getTransaction_Info() {
        return Transaction_Info;
    }

    /**
     *
     * @param Transaction_Info
     */
    public void setTransaction_Info(TransactionInfo Transaction_Info) {
        this.Transaction_Info = Transaction_Info;
    }

    /**
     *
     * @return
     */
    public String getUser() {
        return User;
    }

    /**
     *
     * @param User
     */
    public void setUser(String User) {
        this.User = User;
    }

    /**
     *
     * @return
     */
    public String getId() {
        return id;
    }

    /**
     *
     * @param id
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     *
     * @return
     */
    public String getStatus() {
        return status;
    }

    /**
     *
     * @param status
     */
    public void setStatus(String status) {
        this.status = status;
    }

    /**
     *
     * @return
     */
    public String getUsername() {
        return username;
    }

    /**
     *
     * @param username
     */
    public void setUsername(String username) {
        this.username = username;
    }

    /**
     *
     * @return
     */
    public String getUsername_type() {
        return username_type;
    }

    /**
     *
     * @param username_type
     */
    public void setUsername_type(String username_type) {
        this.username_type = username_type;
    }

    /**
     *
     * @return
     */
    public SystemUser getSystemUser() {
        return SystemUser;
    }

    /**
     *
     * @param SystemUser
     */
    public void setSystemUser(SystemUser SystemUser) {
        this.SystemUser = SystemUser;
    }
    
    
}
