/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.oxigenwallet.userservice.common.xml.model.requestXml;

/**
 *
 * @author mitz
 */
public class UserPA {
    private String Address;
    private String City;
    private String District;
    private String State;
    private String Country;
    private String Zip;

    /**
     *
     */
    public UserPA() {
    }

    /**
     *
     * @param Address
     * @param City
     * @param District
     * @param State
     * @param Country
     * @param Zip
     */
    public UserPA(String Address, String City, String District, String State, String Country, String Zip) {
        this.Address = Address;
        this.City = City;
        this.District = District;
        this.State = State;
        this.Country = Country;
        this.Zip = Zip;
    }

    /**
     *
     * @return
     */
    public String getAddress() {
        return Address;
    }

    /**
     *
     * @param Address
     */
    public void setAddress(String Address) {
        this.Address = Address;
    }

    /**
     *
     * @return
     */
    public String getCity() {
        return City;
    }

    /**
     *
     * @param City
     */
    public void setCity(String City) {
        this.City = City;
    }

    /**
     *
     * @return
     */
    public String getDistrict() {
        return District;
    }

    /**
     *
     * @param District
     */
    public void setDistrict(String District) {
        this.District = District;
    }

    /**
     *
     * @return
     */
    public String getState() {
        return State;
    }

    /**
     *
     * @param State
     */
    public void setState(String State) {
        this.State = State;
    }

    /**
     *
     * @return
     */
    public String getCountry() {
        return Country;
    }

    /**
     *
     * @param Country
     */
    public void setCountry(String Country) {
        this.Country = Country;
    }

    /**
     *
     * @return
     */
    public String getZip() {
        return Zip;
    }

    /**
     *
     * @param Zip
     */
    public void setZip(String Zip) {
        this.Zip = Zip;
    }
}
