/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.oxigenwallet.userservice.common.xml.model.responseXml;

/**
 *
 * @author mitz
 */
public class Documents {
    private String Document_info;
    private String count;
    private String type;
    private String id;
    private String ref_no;
    private String validfrom;
    private String validto;
    private String vcopy;

    /**
     *
     */
    public Documents() {
    }

    /**
     *
     * @param Document_info
     * @param count
     * @param type
     * @param id
     * @param ref_no
     * @param validfrom
     * @param validto
     * @param vcopy
     */
    public Documents(String Document_info, String count, String type, String id, String ref_no, String validfrom, String validto, String vcopy) {
        this.Document_info = Document_info;
        this.count = count;
        this.type = type;
        this.id = id;
        this.ref_no = ref_no;
        this.validfrom = validfrom;
        this.validto = validto;
        this.vcopy = vcopy;
    }

    /**
     *
     * @return
     */
    public String getDocument_info() {
        return Document_info;
    }

    /**
     *
     * @param Document_info
     */
    public void setDocument_info(String Document_info) {
        this.Document_info = Document_info;
    }

    /**
     *
     * @return
     */
    public String getType() {
        return type;
    }

    /**
     *
     * @param type
     */
    public void setType(String type) {
        this.type = type;
    }

    /**
     *
     * @return
     */
    public String getId() {
        return id;
    }

    /**
     *
     * @param id
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     *
     * @return
     */
    public String getRef_no() {
        return ref_no;
    }

    /**
     *
     * @param ref_no
     */
    public void setRef_no(String ref_no) {
        this.ref_no = ref_no;
    }

    /**
     *
     * @return
     */
    public String getValidfrom() {
        return validfrom;
    }

    /**
     *
     * @param validfrom
     */
    public void setValidfrom(String validfrom) {
        this.validfrom = validfrom;
    }

    /**
     *
     * @return
     */
    public String getValidto() {
        return validto;
    }

    /**
     *
     * @param validto
     */
    public void setValidto(String validto) {
        this.validto = validto;
    }

    /**
     *
     * @return
     */
    public String getVcopy() {
        return vcopy;
    }

    /**
     *
     * @param vcopy
     */
    public void setVcopy(String vcopy) {
        this.vcopy = vcopy;
    }

    /**
     *
     * @return
     */
    public String getCount() {
        return count;
    }

    /**
     *
     * @param count
     */
    public void setCount(String count) {
        this.count = count;
    }
    
    
}
