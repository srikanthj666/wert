/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.oxigenwallet.userservice.common.xml.model.responseXml;

/**
 *
 * @author mitz
 */
public class UserPA {
    private String Address;
    private String City;
    private String District;
    private String State;
    private String Country;
    private String Zip;

    /**
     *
     */
    public UserPA() {
    }

    /**
     *
     * @return
     */
    public String getAddress() {
        return Address;
    }

    /**
     *
     * @param Address
     */
    public void setAddress(String Address) {
        this.Address = Address;
    }

    /**
     *
     * @return
     */
    public String getCity() {
        return City;
    }

    /**
     *
     * @param City
     */
    public void setCity(String City) {
        this.City = City;
    }

    /**
     *
     * @return
     */
    public String getDistrict() {
        return District;
    }

    /**
     *
     * @param District
     */
    public void setDistrict(String District) {
        this.District = District;
    }

    /**
     *
     * @return
     */
    public String getState() {
        return State;
    }

    /**
     *
     * @param State
     */
    public void setState(String State) {
        this.State = State;
    }

    /**
     *
     * @return
     */
    public String getCountry() {
        return Country;
    }

    /**
     *
     * @param Country
     */
    public void setCountry(String Country) {
        this.Country = Country;
    }

    /**
     *
     * @return
     */
    public String getZip() {
        return Zip;
    }

    /**
     *
     * @param Zip
     */
    public void setZip(String Zip) {
        this.Zip = Zip;
    }
    
}
