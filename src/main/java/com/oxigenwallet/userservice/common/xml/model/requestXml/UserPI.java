/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.oxigenwallet.userservice.common.xml.model.requestXml;

/**
 *
 * @author mitz
 */
public class UserPI {
    private String First_Name;
    private String Middle_Name;
    private String Last_Name;
    private String Dob;
    private String Gender;

    /**
     *
     */
    public UserPI() {
    }

    /**
     *
     * @param First_Name
     * @param Middle_Name
     * @param Last_Name
     * @param Dob
     * @param Gender
     */
    public UserPI(String First_Name, String Middle_Name, String Last_Name, String Dob, String Gender) {
        this.First_Name = First_Name;
        this.Middle_Name = Middle_Name;
        this.Last_Name = Last_Name;
        this.Dob = Dob;
        this.Gender = Gender;
    }

    /**
     *
     * @return
     */
    public String getFirst_Name() {
        return First_Name;
    }

    /**
     *
     * @param First_Name
     */
    public void setFirst_Name(String First_Name) {
        this.First_Name = First_Name;
    }

    /**
     *
     * @return
     */
    public String getMiddle_Name() {
        return Middle_Name;
    }

    /**
     *
     * @param Middle_Name
     */
    public void setMiddle_Name(String Middle_Name) {
        this.Middle_Name = Middle_Name;
    }

    /**
     *
     * @return
     */
    public String getLast_Name() {
        return Last_Name;
    }

    /**
     *
     * @param Last_Name
     */
    public void setLast_Name(String Last_Name) {
        this.Last_Name = Last_Name;
    }

    /**
     *
     * @return
     */
    public String getDob() {
        return Dob;
    }

    /**
     *
     * @param Dob
     */
    public void setDob(String Dob) {
        this.Dob = Dob;
    }

    /**
     *
     * @return
     */
    public String getGender() {
        return Gender;
    }

    /**
     *
     * @param Gender
     */
    public void setGender(String Gender) {
        this.Gender = Gender;
    }
}
