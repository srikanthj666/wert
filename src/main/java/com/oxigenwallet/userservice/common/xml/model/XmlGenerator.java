/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.oxigenwallet.userservice.common.xml.model;

import com.oxigenwallet.userservice.common.RedisCacheResource;
import com.oxigenwallet.userservice.common.RedisStorage;
import com.oxigenwallet.userservice.common.RestCalling;
import com.oxigenwallet.userservice.common.constants.Constants;
import com.oxigenwallet.userservice.common.json.request.Request;
import com.oxigenwallet.userservice.common.xml.config.CreateUserConverter;
import com.oxigenwallet.userservice.common.xml.config.UpdateUserConverter;
import com.oxigenwallet.userservice.common.xml.model.requestXml.Accounts;
import com.oxigenwallet.userservice.common.xml.model.requestXml.B2cServiceInterface;
import com.oxigenwallet.userservice.common.xml.model.requestXml.ChannelInfo;
import com.oxigenwallet.userservice.common.xml.model.requestXml.DeviceInfo;
import com.oxigenwallet.userservice.common.xml.model.requestXml.EncryptionInfo;
import com.oxigenwallet.userservice.common.xml.model.requestXml.NxtxnInterface;
import com.oxigenwallet.userservice.common.xml.model.requestXml.OperatorInfo;
import com.oxigenwallet.userservice.common.xml.model.requestXml.SecurityInfo;
import com.oxigenwallet.userservice.common.xml.model.requestXml.ServiceData;
import com.oxigenwallet.userservice.common.xml.model.requestXml.ServiceInfo;
import com.oxigenwallet.userservice.common.xml.model.requestXml.ServiceRequest;
import com.oxigenwallet.userservice.common.xml.model.requestXml.TransactionInfo;
import com.oxigenwallet.userservice.common.xml.model.requestXml.User;
import com.oxigenwallet.userservice.common.xml.model.requestXml.UserInfo;
import com.oxigenwallet.userservice.common.xml.model.requestXml.UserPA;
import com.oxigenwallet.userservice.common.xml.model.requestXml.UserPC;
import com.oxigenwallet.userservice.common.xml.model.requestXml.UserPI;
import com.oxigenwallet.userservice.common.xml.model.requestXml.UserProfile;
import com.oxigenwallet.userservice.common.xml.config.VerifyUserConverter;
import com.oxigenwallet.userservice.common.xml.config.CreateAndSendOtpConverter;
import com.oxigenwallet.userservice.common.xml.config.UpdateUserOtpConverter;
import com.oxigenwallet.userservice.common.xml.model.requestXml.TransactionData;
import com.oxigenwallet.userservice.common.json.request.JsonRequestBody;
import com.oxigenwallet.userservice.common.xml.config.GetUserInfoConverter;
import com.oxigenwallet.userservice.common.xml.config.GetUserInfoKycConverter;
import com.oxigenwallet.userservice.common.xml.config.GetUserInfoLiteConverter;
import com.oxigenwallet.userservice.common.xml.config.GetWalletInfoConverter;
import com.oxigenwallet.userservice.common.xml.config.ResetCustomerLpinConverter;
import com.oxigenwallet.userservice.common.xml.config.UpdateFraudStatusConverter;
import com.oxigenwallet.userservice.common.xml.config.UpdatePasswordConverter;
import com.oxigenwallet.userservice.common.xml.model.requestXml.CustomerData;
import com.oxigenwallet.userservice.common.xml.model.requestXml.NxtxnSystemInterface;
import com.oxigenwallet.userservice.common.xml.model.requestXml.SystemServiceInfo;
import com.oxigenwallet.userservice.common.xml.model.requestXml.SystemServiceRequest;
import com.oxigenwallet.userservice.common.xml.model.requestXml.UserTransactionInfo;
import com.oxigenwallet.userservice.dao.UserDao;
import com.oxigenwallet.userservice.utils.Util;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import redis.clients.jedis.Jedis;

/**
 *
 * @author mitz
 */
@Component
public class XmlGenerator {

    @Autowired
    private CreateUserConverter createUserConverter;
    @Autowired
    private UpdateUserConverter updateUserConverter;
    @Autowired
    private CreateAndSendOtpConverter createAndSendOtpConverter;
    @Autowired
    private UpdateUserOtpConverter updateUserOtpConverter;
    @Autowired
    private ResetCustomerLpinConverter resetCustomerLpinConverter;
    @Autowired
    private UpdatePasswordConverter updatePasswordConverter;
    @Autowired
    private GetUserInfoLiteConverter getUserInfoLiteConverter;
    @Autowired
    private GetUserInfoConverter getUserInfoConverter;
    @Autowired
    private GetUserInfoKycConverter getUserInfoKycConverter;
    @Autowired
    private VerifyUserConverter verifyUserConverter;
    @Autowired
    private UpdateFraudStatusConverter updateFraudStatusConverter;
    @Autowired
    private GetWalletInfoConverter getWalletInfoConverter;
    @Autowired
    RedisStorage redisStorage;
    @Autowired
    private RedisCacheResource redisCacheResource;
    @Autowired
    private UserDao userDao;
    @Autowired
    private XmlGenerator xmlGenerator;
    @Autowired
    private RestCalling restCalling;


    @Value("${nxtxn.admin.email}")
    private String nxtxnAdminEmail;
    @Value("${nxtxnUrl.getUserInfo}")
    private String getUserInfoUrl;
    @Value("${nxtxn.admin.userId}")
    private String nxtxnAdminUserId;
    /**
     *
     * @param jsonRequestBody
     * @return
     * @throws IOException
     */
    public String getCreateUserRequestXml(JsonRequestBody jsonRequestBody) throws IOException {

        //GETTING ALL THE REQUESET JSON MODEL OBJECT
        com.oxigenwallet.userservice.common.json.request.Request request = jsonRequestBody.getRequest();
        com.oxigenwallet.userservice.common.json.request.DeviceInfo deviceInfo = request.getDeviceInfo();
        com.oxigenwallet.userservice.common.json.request.User user = request.getUser();

        //POPULATING REQUEST XML MODEL OBJECT USING VALUES FROM REQUEST JSON MODEL OBJECT
        UserPI userPI = new UserPI(user.getName(), user.getMiddleName(), user.getLastName(), user.getDob(), user.getGender());
        UserPA userPA = new UserPA(user.getAddress(), user.getCity(), user.getDistrict(), user.getState(), user.getCountry(), user.getZipcode());
        UserPC userPC = new UserPC(user.getUsername(), user.getLandline(), user.getEmail());
        UserProfile userProfile = new UserProfile(userPI, userPA, userPC, Constants.USER_PROFILE_INFO[0]);
        Accounts accounts = new Accounts(null, "1", Constants.ACCOUNT_TYPE[0], Constants.ACCOUNT_NAME[0]);

        ArrayList<com.oxigenwallet.userservice.common.json.request.DocumentInfo> documentListJson = (ArrayList<com.oxigenwallet.userservice.common.json.request.DocumentInfo>) jsonRequestBody.getRequest().getDocument_info();
        int docCount = 0;
        if (documentListJson != null) {
            docCount = documentListJson.size();
        }
        ArrayList<com.oxigenwallet.userservice.common.xml.model.requestXml.DocumentInfo> documentList = new ArrayList<>();
        for (int i = 0; i < docCount; i++) {
            String kycType = documentListJson.get(i).getKyc_type();
            String type = documentListJson.get(i).getType();
            String id = documentListJson.get(i).getType_id();
            String refParam1 = documentListJson.get(i).getRef_param1();
            String refParam2 = documentListJson.get(i).getRef_param2();
            String vCopy = documentListJson.get(i).getVcopy();
            com.oxigenwallet.userservice.common.xml.model.requestXml.DocumentInfo documentInfoXml = new com.oxigenwallet.userservice.common.xml.model.requestXml.DocumentInfo(kycType, type, id, refParam1, refParam2, vCopy);
            documentList.add(documentInfoXml);
        }
        com.oxigenwallet.userservice.common.xml.model.requestXml.Documents documents = new com.oxigenwallet.userservice.common.xml.model.requestXml.Documents(documentList, String.valueOf(docCount));

        UserInfo userInfo = new UserInfo(user.getUsername(), userProfile, accounts, null, null, documents, Constants.STATUS[0], "R-OTP verification pending", Constants.USER_ID_TYPE[0], null);
        User userX = new User(userInfo, "1");

        EncryptionInfo encryptionInfo = new EncryptionInfo(Constants.PIN_TYPE[0], Constants.ENC_TYPE[0], user.getPassword());
        SecurityInfo securityInfo = new SecurityInfo(encryptionInfo, "1");
        OperatorInfo operatorInfo = new OperatorInfo(Constants.TRUSTED_OPERATOR[0], Constants.BUISNESS_CATEGORY[0]);
        ServiceData serviceData = new ServiceData(Constants.SERVICE_TYPE[4]);
        ServiceInfo serviceInfo = new ServiceInfo(operatorInfo, serviceData);
        TransactionInfo transactionInfoX = new TransactionInfo(Util.getTimeStamp(), Util.getRequestId(user.getUsername()), null);
        ChannelInfo channelInfo = new ChannelInfo(Constants.CHANNEL_NAME[0], Constants.CHANNEL_TYPE[0], Constants.Ip_ADDRESS, Constants.CHANNEL_INSTANCE_ID[0], "9988776655", Constants.IP_TYPE[0]);
        DeviceInfo deviceInfoX = new DeviceInfo(deviceInfo.getDeviceOs(), deviceInfo.getDeviceOsVersion(), deviceInfo.getDeviceId());
        ServiceRequest serviceRequest = new ServiceRequest(channelInfo, deviceInfoX, transactionInfoX, serviceInfo, userX, securityInfo, Constants.VERSION, Constants.SERVICE_TYPE[4]);
        B2cServiceInterface b2cServiceInterface = new B2cServiceInterface(serviceRequest, Constants.VERSION);
        NxtxnInterface nxtxnInterface = new NxtxnInterface(b2cServiceInterface, Constants.VERSION);

        //MARSHALING REQUEST XML MODEL OBJECT TO STRING USING PREDEFINED MAPPING
        String createUserRequestXml = createUserConverter.doMarshaling(nxtxnInterface);

        return createUserRequestXml;
    }

    /**
     *
     * @param body
     * @return
     * @throws IOException
     */
    public com.oxigenwallet.userservice.common.xml.model.responseXml.NxtxnInterface getCreateUserResponseXml(String body) throws IOException {
        //UNMARSHALING XML RESPONSE STRING USING PREDEFINED MAPPING
        com.oxigenwallet.userservice.common.xml.model.responseXml.NxtxnInterface nxtxnInterface = (com.oxigenwallet.userservice.common.xml.model.responseXml.NxtxnInterface) createUserConverter.doUnMarshaling(body);
        return nxtxnInterface;
    }

    /**
     *
     * @param jsonRequestBody
     * @return
     * @throws IOException
     */
    public String getUpdateUserRequestXml(JsonRequestBody jsonRequestBody, String kycStatus) throws IOException {

        //GETTING ALL THE REQUESET JSON MODEL OBJECT
        com.oxigenwallet.userservice.common.json.request.Request request = jsonRequestBody.getRequest();
        com.oxigenwallet.userservice.common.json.request.DeviceInfo deviceInfo = request.getDeviceInfo();
        com.oxigenwallet.userservice.common.json.request.User user = request.getUser();
        //POPULATING REQUEST XML MODEL OBJECT USING VALUES FROM REQUEST JSON MODEL OBJECT
        UserPI userPI = new UserPI(user.getName(), user.getMiddleName(), user.getLastName(), user.getDob(), user.getGender());
        UserPA userPA = new UserPA(user.getAddress(), user.getCity(), user.getDistrict(), user.getState(), user.getCountry(), user.getZipcode());
        UserPC userPC = new UserPC(user.getUsername(), user.getLandline(), user.getEmail());
        UserProfile userProfile = null;
        if (kycStatus.equals(Constants.USER_PROFILE_INFO[2])) {
            userProfile = new UserProfile(userPI, userPA, userPC, Constants.USER_PROFILE_INFO[1]);
        } else {
            userProfile = new UserProfile(userPI, userPA, userPC, kycStatus);
        }

        String accType = Constants.ACCOUNT_TYPE[0];
        if (kycStatus.equals(Constants.USER_PROFILE_INFO[1])) {
            accType = Constants.ACCOUNT_TYPE[1];
        } else if (kycStatus.equals(Constants.USER_PROFILE_INFO[2])) {
            accType = Constants.ACCOUNT_TYPE[2];
        }
        Accounts accounts = new Accounts(null, "1", accType, Constants.ACCOUNT_NAME[0]);

        ArrayList<com.oxigenwallet.userservice.common.json.request.DocumentInfo> documentListJson = (ArrayList<com.oxigenwallet.userservice.common.json.request.DocumentInfo>) jsonRequestBody.getRequest().getDocument_info();
        int docCount = 0;
        if (documentListJson != null) {
            docCount = documentListJson.size();
        }
        ArrayList<com.oxigenwallet.userservice.common.xml.model.requestXml.DocumentInfo> documentList = new ArrayList<>();
        for (int i = 0; i < docCount; i++) {
            String kycType = documentListJson.get(i).getKyc_type();
            String type = documentListJson.get(i).getType();
            String id = documentListJson.get(i).getType_id();
            String refParam1 = documentListJson.get(i).getRef_param1();
            String refParam2 = documentListJson.get(i).getRef_param2();
            String vCopy = documentListJson.get(i).getVcopy();
            com.oxigenwallet.userservice.common.xml.model.requestXml.DocumentInfo documentInfoXml = new com.oxigenwallet.userservice.common.xml.model.requestXml.DocumentInfo(kycType, type, id, refParam1, refParam2, vCopy);
            documentList.add(documentInfoXml);
        }
        com.oxigenwallet.userservice.common.xml.model.requestXml.Documents documents = new com.oxigenwallet.userservice.common.xml.model.requestXml.Documents(documentList, String.valueOf(docCount));

        UserInfo userInfo = new UserInfo(user.getUsername(), userProfile, accounts, null, null, documents, Constants.STATUS[1], "Registered", Constants.USER_ID_TYPE[0], null);
        User userX = new User(userInfo, "1");
        OperatorInfo operatorInfo = new OperatorInfo(Constants.TRUSTED_OPERATOR[0], Constants.BUISNESS_CATEGORY[0]);
        ServiceData serviceData = new ServiceData(Constants.SERVICE_TYPE[1]);
        ServiceInfo serviceInfo = new ServiceInfo(operatorInfo, serviceData);

        TransactionInfo transactionInfoX = new TransactionInfo(Util.getTimeStamp(), Util.getRequestId(user.getUsername()), null);

        DeviceInfo deviceInfoNxtxn;
        if (deviceInfo == null) {
            deviceInfoNxtxn = new DeviceInfo("", "", "");
        } else {
            deviceInfo.setDeviceOs(deviceInfo.getDeviceOs() == null ? "" : deviceInfo.getDeviceOs());
            deviceInfo.setDeviceOsVersion(deviceInfo.getDeviceOsVersion() == null ? "" : deviceInfo.getDeviceOsVersion());
            deviceInfo.setDeviceId(deviceInfo.getDeviceId() == null ? "" : deviceInfo.getDeviceId());
            deviceInfo.setDeviceOs(deviceInfo.getDeviceOs() == null ? "" : deviceInfo.getDeviceOs());

            deviceInfoNxtxn = new DeviceInfo(deviceInfo.getDeviceOs(), deviceInfo.getDeviceOsVersion(), deviceInfo.getDeviceId());

        }
        ChannelInfo channelInfo = new ChannelInfo(Constants.CHANNEL_NAME[0], Constants.CHANNEL_TYPE[0], Constants.Ip_ADDRESS, Constants.CHANNEL_INSTANCE_ID[0], "9988776655", Constants.IP_TYPE[0]);

        ServiceRequest serviceRequest = new ServiceRequest(channelInfo, deviceInfoNxtxn, transactionInfoX, serviceInfo, userX, Constants.VERSION, Constants.SERVICE_TYPE[1]);
        B2cServiceInterface b2cServiceInterface = new B2cServiceInterface(serviceRequest, Constants.VERSION);
        NxtxnInterface nxtxnInterface = new NxtxnInterface(b2cServiceInterface, Constants.VERSION);

        //MARSHALING REQUEST XML MODEL OBJECT TO STRING USING PREDEFINED MAPPING
        String createUserRequestXml = updateUserConverter.doMarshaling(nxtxnInterface);

        return createUserRequestXml;
    }

    private boolean isNotEmpty(String str){
        if(str !=null && (!str.trim().isEmpty()) && (!str.equalsIgnoreCase("null"))){
            return true;
        }
        return false;
    }

    private String getUserEmail(com.oxigenwallet.userservice.common.json.request.User user) throws IOException {
        String userEmail = user.getEmail();
        String username = user.getUsername();
        if(this.isNotEmpty(userEmail)){
            return userEmail;
        }
        //checking in redis
        Jedis jedisObject = redisCacheResource.getCacheResource().getResource();
        String key = redisStorage.GenerateKey(username);
        if (jedisObject.exists(key)) {
            Map<String, String> userMap = jedisObject.hgetAll(key);
            userEmail = userMap.get("email");
            if(this.isNotEmpty(userEmail)){
                jedisObject.close();
                return userEmail;
            }
        }
        jedisObject.close();
        //checking in database
        com.oxigenwallet.userservice.model.User userM = userDao.getByMdn(username);
        if(userM != null && isNotEmpty(userM.getEmail())){
            return userM.getEmail();
        }
        //nxtxn call
        String xmlRequestBody = xmlGenerator.getGetUserInfoRequest(username, "full");
        ResponseEntity<String> response = restCalling.postNxtxn(xmlRequestBody, getUserInfoUrl);
        if(response.getBody() != null){
            com.oxigenwallet.userservice.common.xml.model.responseXml.NxtxnInterface responseXmlObject = xmlGenerator.getGetUserInfoResponseXml(response.getBody(), "full");
            userEmail = responseXmlObject.getB2C_Service_Interface().getService_Response().getUser().getUser_Info().getUser_Profile().getUser_PC().getEmail();
            if(this.isNotEmpty(userEmail)){
                return userEmail;
            }
        }
        return "";
    }

    public String getUpgradeKycRequestXml(JsonRequestBody jsonRequestBody) throws IOException {

        //GETTING ALL THE REQUESET JSON MODEL OBJECT
        com.oxigenwallet.userservice.common.json.request.Request request = jsonRequestBody.getRequest();
        com.oxigenwallet.userservice.common.json.request.DeviceInfo deviceInfo = request.getDeviceInfo();
        com.oxigenwallet.userservice.common.json.request.User user = request.getUser();
        //POPULATING REQUEST XML MODEL OBJECT USING VALUES FROM REQUEST JSON MODEL OBJECT
        UserPI userPI = new UserPI(user.getName(), user.getMiddleName(), user.getLastName(), user.getDob(), user.getGender());
        if(user.getCountry() == null || user.getCountry().equals("")) {
            user.setCountry(Constants.DEFAULT_COUNTRY);
        }


        UserPA userPA = new UserPA(user.getAddress(), user.getCity(), user.getDistrict(), user.getState(), user.getCountry(), user.getZipcode());
        UserPC userPC = new UserPC(user.getUsername(), user.getLandline(), this.getUserEmail(user));
        String userProfileType = user.getProfile_type();
        String accountType = Constants.ACCOUNT_TYPE[0];
        String userProfileTag = Constants.USER_PROFILE_INFO[0];
        //for softkyc and fullkyc , user profile type is fullkyc only
        if(userProfileType.equalsIgnoreCase(Constants.USER_PROFILE_INFO[1])){
            accountType = Constants.ACCOUNT_TYPE[1];
            userProfileTag = Constants.USER_PROFILE_INFO[1];
        }else if(userProfileType.equalsIgnoreCase(Constants.USER_PROFILE_INFO[2])){
            accountType = Constants.ACCOUNT_TYPE[2];
            userProfileTag = Constants.USER_PROFILE_INFO[1];
        }
        UserProfile userProfile = new UserProfile(userPI, userPA, userPC, userProfileTag);
        Accounts accounts = new Accounts(null, "1", accountType, Constants.ACCOUNT_NAME[0]);

        ArrayList<com.oxigenwallet.userservice.common.json.request.DocumentInfo> documentListJson = (ArrayList<com.oxigenwallet.userservice.common.json.request.DocumentInfo>) jsonRequestBody.getRequest().getDocument_info();
        int docCount = 0;
        if (documentListJson != null) {
            docCount = documentListJson.size();
        }
        ArrayList<com.oxigenwallet.userservice.common.xml.model.requestXml.DocumentInfo> documentList = new ArrayList<>();
        for (int i = 0; i < docCount; i++) {
            String kycType = documentListJson.get(i).getKyc_type();
            String type = documentListJson.get(i).getType();
            String id = documentListJson.get(i).getType_id();
            String refParam1 = documentListJson.get(i).getRef_param1();
            String refParam2 = documentListJson.get(i).getRef_param2();
            String vCopy = documentListJson.get(i).getVcopy();
            com.oxigenwallet.userservice.common.xml.model.requestXml.DocumentInfo documentInfoXml = new com.oxigenwallet.userservice.common.xml.model.requestXml.DocumentInfo(kycType, type, id, refParam1, refParam2, vCopy);
            documentList.add(documentInfoXml);
        }
        com.oxigenwallet.userservice.common.xml.model.requestXml.Documents documents = new com.oxigenwallet.userservice.common.xml.model.requestXml.Documents(documentList, String.valueOf(docCount));

        UserInfo userInfo = new UserInfo(user.getUsername(), userProfile, accounts, null, null, documents, Constants.STATUS[1], "Registered", Constants.USER_ID_TYPE[0], null);
        User userX = new User(userInfo, "1");
        OperatorInfo operatorInfo = new OperatorInfo(Constants.TRUSTED_OPERATOR[0], Constants.BUISNESS_CATEGORY[0]);
        ServiceData serviceData = new ServiceData(Constants.SERVICE_TYPE[1]);
        ServiceInfo serviceInfo = new ServiceInfo(operatorInfo, serviceData);

        TransactionInfo transactionInfoX = new TransactionInfo(Util.getTimeStamp(), Util.getRequestId(user.getUsername()), null);

        DeviceInfo deviceInfoNxtxn;
        if (deviceInfo == null) {
            deviceInfoNxtxn = new DeviceInfo("", "", "");
        } else {
            deviceInfo.setDeviceOs(deviceInfo.getDeviceOs() == null ? "" : deviceInfo.getDeviceOs());
            deviceInfo.setDeviceOsVersion(deviceInfo.getDeviceOsVersion() == null ? "" : deviceInfo.getDeviceOsVersion());
            deviceInfo.setDeviceId(deviceInfo.getDeviceId() == null ? "" : deviceInfo.getDeviceId());
            deviceInfo.setDeviceOs(deviceInfo.getDeviceOs() == null ? "" : deviceInfo.getDeviceOs());

            deviceInfoNxtxn = new DeviceInfo(deviceInfo.getDeviceOs(), deviceInfo.getDeviceOsVersion(), deviceInfo.getDeviceId());

        }
        ChannelInfo channelInfo = new ChannelInfo(Constants.CHANNEL_NAME[0], Constants.CHANNEL_TYPE[0], Constants.Ip_ADDRESS, Constants.CHANNEL_INSTANCE_ID[0], "9988776655", Constants.IP_TYPE[0]);

        ServiceRequest serviceRequest = new ServiceRequest(channelInfo, deviceInfoNxtxn, transactionInfoX, serviceInfo, userX, Constants.VERSION, Constants.SERVICE_TYPE[1]);
        B2cServiceInterface b2cServiceInterface = new B2cServiceInterface(serviceRequest, Constants.VERSION);
        NxtxnInterface nxtxnInterface = new NxtxnInterface(b2cServiceInterface, Constants.VERSION);

        //MARSHALING REQUEST XML MODEL OBJECT TO STRING USING PREDEFINED MAPPING
        String createUserRequestXml = updateUserConverter.doMarshaling(nxtxnInterface);

        return createUserRequestXml;
    }

    /**
     *
     * @param body
     * @return
     * @throws IOException
     */
    public com.oxigenwallet.userservice.common.xml.model.responseXml.NxtxnInterface getUpdateUserResponseXml(String body) throws IOException {
        //UNMARSHALING XML RESPONSE STRING USING PREDEFINED MAPPING
        com.oxigenwallet.userservice.common.xml.model.responseXml.NxtxnInterface nxtxnInterface = (com.oxigenwallet.userservice.common.xml.model.responseXml.NxtxnInterface) updateUserConverter.doUnMarshaling(body);
        return nxtxnInterface;
    }

    /**
     *
     * @param jsonRequestBody
     * @return
     * @throws IOException
     */
    public String getVerifyUserRequestXml(JsonRequestBody jsonRequestBody) throws IOException {

        //GETTING ALL THE REQUESET JSON MODEL OBJECT
        Request request = jsonRequestBody.getRequest();
        com.oxigenwallet.userservice.common.json.request.DeviceInfo deviceInfo = request.getDeviceInfo();
        com.oxigenwallet.userservice.common.json.request.TokenInfo tokenInfo = request.getTokeninfo();
        //com.oxigenwallet.userservice.common.json.requestJson.TransactionInfo transactionInfo = request.getTransactionInfo();

//        String deviceOs = deviceInfo.getDeviceOs()==null?"":deviceInfo.getDeviceOs();
//        String deviceOsVersion = deviceInfo.getDeviceOsVersion()==null?"":deviceInfo.getDeviceOsVersion();
//        String deviceId = deviceInfo.getDeviceId()==null?"":deviceInfo.getDeviceId();
//        
        //POPULATING REQUEST XML MODEL OBJECT USING VALUES FROM REQUEST JSON MODEL OBJECT
        UserInfo userInfo = new UserInfo(tokenInfo.getUsername(), Constants.USER_ID_TYPE[0]);
        User userX = new User(userInfo, "1");
        EncryptionInfo encryptionInfo = new EncryptionInfo(Constants.PIN_TYPE[0], Constants.ENC_TYPE[0], tokenInfo.getPassword());
        SecurityInfo securityInfo = new SecurityInfo(encryptionInfo, "1");
        OperatorInfo operatorInfo = new OperatorInfo(Constants.TRUSTED_OPERATOR[0], Constants.BUISNESS_CATEGORY[0]);
        ServiceData serviceData = new ServiceData(Constants.SERVICE_TYPE[5]);
        ServiceInfo serviceInfo = new ServiceInfo(operatorInfo, serviceData);
        TransactionInfo transactionInfoX = new TransactionInfo(Util.getTimeStamp(), Util.getRequestId(tokenInfo.getUsername()), "description");
        ChannelInfo channelInfo = new ChannelInfo(Constants.CHANNEL_NAME[0], Constants.CHANNEL_TYPE[0], Constants.Ip_ADDRESS, Constants.CHANNEL_INSTANCE_ID[0], Constants.CHANNEL_CONSUMER_ID, Constants.IP_TYPE[0]);
        DeviceInfo deviceInfoX = new DeviceInfo(
                "",
                "",
                "");

        ServiceRequest serviceRequest = new ServiceRequest(channelInfo, deviceInfoX, transactionInfoX, serviceInfo, userX, securityInfo, Constants.VERSION, Constants.SERVICE_TYPE[5]);
        B2cServiceInterface b2cServiceInterface = new B2cServiceInterface(serviceRequest, Constants.VERSION);
        NxtxnInterface nxtxnInterface = new NxtxnInterface(b2cServiceInterface, Constants.VERSION);

        //MARSHALING REQUEST XML MODEL OBJECT TO STRING USING PREDEFINED MAPPING
        String verifyUserRequestXml = verifyUserConverter.doMarshaling(nxtxnInterface);

        return verifyUserRequestXml;
    }

    /**
     *
     * @param jsonRequestBody
     * @return
     * @throws IOException
     */
    public String getCreateAndSendRequestXml(JsonRequestBody jsonRequestBody) throws IOException {

        //GETTING ALL THE REQUESET JSON MODEL OBJECT
        com.oxigenwallet.userservice.common.json.request.Request request = jsonRequestBody.getRequest();
        com.oxigenwallet.userservice.common.json.request.DeviceInfo deviceInfo = request.getDeviceInfo();
        com.oxigenwallet.userservice.common.json.request.User user = request.getUser();

        //POPULATING REQUEST XML MODEL OBJECT USING VALUES FROM REQUEST JSON MODEL OBJECT
        ChannelInfo channelInfo = new ChannelInfo(Constants.CHANNEL_NAME[0], Constants.CHANNEL_TYPE[0], Constants.Ip_ADDRESS, Constants.CHANNEL_INSTANCE_ID[0], Constants.IP_TYPE[0]);
        DeviceInfo deviceInfoX = new DeviceInfo(deviceInfo.getDeviceOs(), deviceInfo.getDeviceOsVersion(), deviceInfo.getDeviceId());
        TransactionInfo transactionInfoX = new TransactionInfo(Util.getTimeStamp(), Util.getRequestId(user.getUsername()), "Create and Send OTP to mobile" + user.getUsername() + " via SMS");
        TransactionData transactionDataX = new TransactionData(null, null, Constants.OTP_TYPE[1], Constants.TRUSTED_OPERATOR[0]);
        UserInfo userInfo = new UserInfo(user.getUsername(), transactionDataX, Constants.USER_ID_TYPE[0]);
        User userX = new User(userInfo, "1");
        OperatorInfo operatorInfo = new OperatorInfo(Constants.TRUSTED_OPERATOR[0], Constants.BUISNESS_CATEGORY[0]);
        ServiceData serviceData = new ServiceData(Constants.SERVICE_TYPE[0]);
        ServiceInfo serviceInfo = new ServiceInfo(operatorInfo, serviceData);
        ServiceRequest serviceRequest = new ServiceRequest(channelInfo, deviceInfoX, transactionInfoX, serviceInfo, userX, Constants.VERSION, Constants.SERVICE_TYPE[0]);
        B2cServiceInterface b2cServiceInterface = new B2cServiceInterface(serviceRequest, Constants.VERSION);
        NxtxnInterface nxtxnInterface = new NxtxnInterface(b2cServiceInterface, Constants.VERSION);

        //MARSHALING REQUEST XML MODEL OBJECT TO STRING USING PREDEFINED MAPPING
        String createAndSendOtpRequestXml = createAndSendOtpConverter.doMarshaling(nxtxnInterface);

        return createAndSendOtpRequestXml;
    }

    /**
     *
     * @param body
     * @return
     * @throws IOException
     */
    public com.oxigenwallet.userservice.common.xml.model.responseXml.NxtxnInterface getCreateAndSendResponseXml(String body) throws IOException {
        //UNMARSHALING XML RESPONSE STRING USING PREDEFINED MAPPING
        com.oxigenwallet.userservice.common.xml.model.responseXml.NxtxnInterface nxtxnInterface = (com.oxigenwallet.userservice.common.xml.model.responseXml.NxtxnInterface) createAndSendOtpConverter.doUnMarshaling(body);
        return nxtxnInterface;
    }

    /**
     *
     * @param jsonRequestBody
     * @param otp
     * @param password
     * @return
     * @throws IOException
     */
    public String getUpdateUserRequestXml(JsonRequestBody jsonRequestBody, String otp, String password) throws IOException {

        //GETTING ALL THE REQUESET JSON MODEL OBJECT
        com.oxigenwallet.userservice.common.json.request.Request request = jsonRequestBody.getRequest();
        com.oxigenwallet.userservice.common.json.request.DeviceInfo deviceInfo = request.getDeviceInfo();
        com.oxigenwallet.userservice.common.json.request.User user = request.getUser();

        //POPULATING REQUEST XML MODEL OBJECT USING VALUES FROM REQUEST JSON MODEL OBJECT
        UserPI userPI = new UserPI(user.getName(), user.getMiddleName(), user.getLastName(), user.getDob(), user.getGender());
        UserPA userPA = new UserPA(user.getAddress(), user.getCity(), user.getDistrict(), user.getState(), user.getCountry(), user.getZipcode());
        UserPC userPC = new UserPC(user.getUsername(), user.getLandline(), user.getEmail());
        UserProfile userProfile = new UserProfile(userPI, userPA, userPC, Constants.USER_PROFILE_INFO[0]);
        Accounts accounts = new Accounts(null, "1", Constants.ACCOUNT_TYPE[0], Constants.ACCOUNT_NAME[0]);


        ArrayList<com.oxigenwallet.userservice.common.json.request.DocumentInfo> documentListJson = (ArrayList<com.oxigenwallet.userservice.common.json.request.DocumentInfo>) jsonRequestBody.getRequest().getDocument_info();
        int docCount = 0;
        if (documentListJson != null) {
            docCount = documentListJson.size();
        }
        ArrayList<com.oxigenwallet.userservice.common.xml.model.requestXml.DocumentInfo> documentList = new ArrayList<>();
        for (int i = 0; i < docCount; i++) {
            String kycType = documentListJson.get(i).getKyc_type();
            String type = documentListJson.get(i).getType();
            String id = documentListJson.get(i).getType_id();
            String refParam1 = documentListJson.get(i).getRef_param1();
            String refParam2 = documentListJson.get(i).getRef_param2();
            String vCopy = documentListJson.get(i).getVcopy();
            com.oxigenwallet.userservice.common.xml.model.requestXml.DocumentInfo documentInfoXml = new com.oxigenwallet.userservice.common.xml.model.requestXml.DocumentInfo(kycType, type, id, refParam1, refParam2, vCopy);
            documentList.add(documentInfoXml);
        }
        com.oxigenwallet.userservice.common.xml.model.requestXml.Documents documents = new com.oxigenwallet.userservice.common.xml.model.requestXml.Documents(documentList, String.valueOf(docCount));
        UserInfo userInfo = new UserInfo(user.getUsername(), userProfile, accounts, Constants.STATUS[1], "Registered", Constants.USER_ID_TYPE[0], documents);
        User userX = new User(userInfo, "1");
        OperatorInfo operatorInfo = new OperatorInfo(Constants.TRUSTED_OPERATOR[0], Constants.BUISNESS_CATEGORY[0]);
        ServiceData serviceData = new ServiceData(Constants.SERVICE_TYPE[1]);
        ServiceInfo serviceInfo = new ServiceInfo(operatorInfo, serviceData);
        TransactionInfo transactionInfoX = new TransactionInfo(Util.getTimeStamp(), Util.getRequestId(user.getUsername()), "B2C Update User for mobile " + user.getUsername());
        ChannelInfo channelInfo = new ChannelInfo(Constants.CHANNEL_NAME[0], Constants.CHANNEL_TYPE[0], Constants.Ip_ADDRESS, Constants.CHANNEL_INSTANCE_ID[0], Constants.CHANNEL_CONSUMER_ID, Constants.IP_TYPE[0]);
        DeviceInfo deviceInfoX = new DeviceInfo(deviceInfo.getDeviceOs(), deviceInfo.getDeviceOsVersion(), deviceInfo.getDeviceId());

        EncryptionInfo encryptionInfo1 = new EncryptionInfo(Constants.PIN_TYPE[0], Constants.ENC_TYPE[0], password);
        EncryptionInfo encryptionInfo2 = new EncryptionInfo(Constants.OTP_TYPE[1], Constants.ENC_TYPE[0], otp);
        SecurityInfo securityInfo = new SecurityInfo(encryptionInfo1, encryptionInfo2, "2");

        ServiceRequest serviceRequest = new ServiceRequest(channelInfo, deviceInfoX, transactionInfoX, serviceInfo, userX, securityInfo, Constants.VERSION, Constants.SERVICE_TYPE[1]);
        B2cServiceInterface b2cServiceInterface = new B2cServiceInterface(serviceRequest, Constants.VERSION);
        NxtxnInterface nxtxnInterface = new NxtxnInterface(b2cServiceInterface, Constants.VERSION);

        //MARSHALING REQUEST XML MODEL OBJECT TO STRING USING PREDEFINED MAPPING
        String updateUserOtpRequestXml = updateUserOtpConverter.doMarshaling(nxtxnInterface);
        return updateUserOtpRequestXml;
    }

    /**
     *
     * @param jsonRequestBody
     * @return
     * @throws IOException
     */
    public String getResetCustomerLpinRequestXml(JsonRequestBody jsonRequestBody) throws IOException {

        //GETTING ALL THE REQUESET JSON MODEL OBJECT
        com.oxigenwallet.userservice.common.json.request.Request request = jsonRequestBody.getRequest();
        com.oxigenwallet.userservice.common.json.request.DeviceInfo deviceInfo = request.getDeviceInfo();
        com.oxigenwallet.userservice.common.json.request.User user = request.getUser();

        //POPULATING REQUEST XML MODEL OBJECT USING VALUES FROM REQUEST JSON MODEL OBJECT
        ChannelInfo channelInfo = new ChannelInfo(Constants.CHANNEL_NAME[1], Constants.CHANNEL_TYPE[0], Constants.Ip_ADDRESS, Constants.CHANNEL_INSTANCE_ID[0], Constants.CHANNEL_CONSUMER_ID, Constants.IP_TYPE[0]);
        DeviceInfo deviceInfoX = new DeviceInfo(deviceInfo.getDeviceOs(), deviceInfo.getDeviceOsVersion(), deviceInfo.getDeviceId());
        TransactionInfo transactionInfo = new TransactionInfo(Util.getTimeStamp(), Util.getRequestId(user.getUsername()), Constants.SERVICE_TYPE[2]);
        SystemServiceInfo systemServiceInfo = new SystemServiceInfo(Constants.PLATFORM_NAME[0], Constants.ROLE_NAME[0], Constants.SERVICE_NAME[0], Constants.PIN_TYPE[2], "Update", Constants.SERVICE_TYPE[2]);
        CustomerData customerData = new CustomerData(null, "1", Constants.USER_ID_TYPE[0], user.getUsername());
        SystemServiceRequest systemServiceRequest = new SystemServiceRequest(Constants.SERVICE_TYPE[2], Constants.VERSION, channelInfo, deviceInfoX, transactionInfo, null, nxtxnAdminUserId, "email", nxtxnAdminEmail, systemServiceInfo, customerData, "1");
        NxtxnSystemInterface nxtxnSystemInterface = new NxtxnSystemInterface(systemServiceRequest, Constants.VERSION);
        NxtxnInterface nxtxnInterface = new NxtxnInterface(nxtxnSystemInterface, Constants.VERSION);

        //MARSHALING REQUEST XML MODEL OBJECT TO STRING USING PREDEFINED MAPPING
        return resetCustomerLpinConverter.doMarshaling(nxtxnInterface);
    }

    /**
     *
     * @param body
     * @return
     * @throws IOException
     */
    public com.oxigenwallet.userservice.common.xml.model.responseXml.NxtxnInterface getResetCustomerLpinResponseXml(String body) throws IOException {
        //UNMARSHALING XML RESPONSE STRING USING PREDEFINED MAPPING
        com.oxigenwallet.userservice.common.xml.model.responseXml.NxtxnInterface nxtxnInterface = (com.oxigenwallet.userservice.common.xml.model.responseXml.NxtxnInterface) resetCustomerLpinConverter.doUnMarshaling(body);
        return nxtxnInterface;
    }

    /**
     *
     * @param jsonRequestBody
     * @return
     * @throws IOException
     */
    public String getUpdatePasswordXml(JsonRequestBody jsonRequestBody) throws IOException {

        //GETTING ALL THE REQUESET JSON MODEL OBJECT
        com.oxigenwallet.userservice.common.json.request.Request request = jsonRequestBody.getRequest();
        com.oxigenwallet.userservice.common.json.request.DeviceInfo deviceInfo = request.getDeviceInfo();
        com.oxigenwallet.userservice.common.json.request.User user = request.getUser();

        //POPULATING REQUEST XML MODEL OBJECT USING VALUES FROM REQUEST JSON MODEL OBJECT
        ChannelInfo channelInfo = new ChannelInfo(Constants.CHANNEL_NAME[0], Constants.CHANNEL_TYPE[0], Constants.Ip_ADDRESS, Constants.CHANNEL_INSTANCE_ID[0], Constants.CHANNEL_CONSUMER_ID, Constants.IP_TYPE[0]);
        DeviceInfo deviceInfoX = new DeviceInfo(deviceInfo.getDeviceOs(), deviceInfo.getDeviceOsVersion(), deviceInfo.getDeviceId());
        TransactionInfo transactionInfoX = new TransactionInfo(Util.getTimeStamp(), Util.getRequestId(user.getUsername()), "Update Password for wallet " + user.getUsername());
        UserInfo userInfo = new UserInfo(user.getUsername(), Constants.USER_ID_TYPE[0]);
        User userX = new User(userInfo, "1");
        OperatorInfo operatorInfo = new OperatorInfo(Constants.TRUSTED_OPERATOR[0], Constants.BUISNESS_CATEGORY[0]);
        ServiceData serviceData = new ServiceData(Constants.SERVICE_TYPE[3]);
        ServiceInfo serviceInfo = new ServiceInfo(operatorInfo, serviceData);
        EncryptionInfo encryptionInfo1 = new EncryptionInfo(Constants.PIN_TYPE[0], Constants.ENC_TYPE[0], user.getNewPassword());
        EncryptionInfo encryptionInfo2 = null;
        if ((user.getType()).equals("FORGOT")) {
            encryptionInfo2 = new EncryptionInfo(Constants.OTP_TYPE[3], Constants.ENC_TYPE[0], user.getOtp());
        } else if ((user.getType()).equals("RESET")) {
            encryptionInfo2 = new EncryptionInfo(Constants.PIN_TYPE[1], Constants.ENC_TYPE[0], user.getOldPassword());
        }
        SecurityInfo securityInfo = new SecurityInfo(encryptionInfo1, encryptionInfo2, "2");
        ServiceRequest serviceRequest = new ServiceRequest(channelInfo, deviceInfoX, transactionInfoX, serviceInfo, userX, securityInfo, Constants.VERSION, Constants.SERVICE_TYPE[3]);
        B2cServiceInterface b2cServiceInterface = new B2cServiceInterface(serviceRequest, Constants.VERSION);
        NxtxnInterface nxtxnInterface = new NxtxnInterface(b2cServiceInterface, Constants.VERSION);

        return updatePasswordConverter.doMarshaling(nxtxnInterface);
    }

    /**
     *
     * @param body
     * @return
     * @throws IOException
     */
    public com.oxigenwallet.userservice.common.xml.model.responseXml.NxtxnInterface getUpdatePasswordResponseXml(String body) throws IOException {
        //UNMARSHALING XML RESPONSE STRING USING PREDEFINED MAPPING
        com.oxigenwallet.userservice.common.xml.model.responseXml.NxtxnInterface nxtxnInterface = (com.oxigenwallet.userservice.common.xml.model.responseXml.NxtxnInterface) updateUserConverter.doUnMarshaling(body);
        return nxtxnInterface;
    }

    /**
     *
     * @param username
     * @param info
     * @return
     * @throws IOException
     */
    public String getGetUserInfoRequest(String username, String info) throws IOException {

        //POPULATING REQUEST XML MODEL OBJECT USING VALUES FROM REQUEST JSON MODEL OBJECT
        ChannelInfo channelInfo = new ChannelInfo(Constants.CHANNEL_NAME[0], Constants.CHANNEL_TYPE[0], Constants.Ip_ADDRESS, Constants.CHANNEL_INSTANCE_ID[0], Constants.CHANNEL_CONSUMER_ID, Constants.IP_TYPE[0]);
        DeviceInfo deviceInfo = new DeviceInfo();
        TransactionInfo transactionInfoX = new TransactionInfo(Util.getTimeStamp(), Util.getRequestId(username), "Get B2C User Info for mobile " + username);
        OperatorInfo operatorInfo = new OperatorInfo(Constants.TRUSTED_OPERATOR[0], Constants.BUISNESS_CATEGORY[0]);

        UserInfo userInfo = null;
        User user = null;
        ServiceRequest serviceRequest = null;
        B2cServiceInterface b2cServiceInterface = null;
        NxtxnInterface nxtxnInterface = null;
        String xmlRequestString = null;
        ServiceData serviceData = null;
        ServiceInfo serviceInfo = null;
        switch (info) {
            case "lite":
                serviceData = new ServiceData("GET_USERINFO_LITE");
                serviceInfo = new ServiceInfo(operatorInfo, serviceData);
                TransactionData transactionData = new TransactionData("BASIC", null, null, null);
                userInfo = new UserInfo(username, transactionData, Constants.USER_ID_TYPE[0]);
                user = new User(userInfo, "1");
                serviceRequest = new ServiceRequest(channelInfo, deviceInfo, transactionInfoX, serviceInfo, user, null, Constants.VERSION, "GET_USERINFO_LITE");
                b2cServiceInterface = new B2cServiceInterface(serviceRequest, Constants.VERSION);
                nxtxnInterface = new NxtxnInterface(b2cServiceInterface, Constants.VERSION);
                xmlRequestString = getUserInfoLiteConverter.doMarshaling(nxtxnInterface);
                break;
            case "full":
                serviceData = new ServiceData("GET_USER_INFO");
                serviceInfo = new ServiceInfo(operatorInfo, serviceData);
                userInfo = new UserInfo(username, Constants.USER_ID_TYPE[0]);
                user = new User(userInfo, "1");
                serviceRequest = new ServiceRequest(channelInfo, deviceInfo, transactionInfoX, serviceInfo, user, null, Constants.VERSION, "GET_USER_INFO");
                b2cServiceInterface = new B2cServiceInterface(serviceRequest, Constants.VERSION);
                nxtxnInterface = new NxtxnInterface(b2cServiceInterface, Constants.VERSION);
                xmlRequestString = getUserInfoConverter.doMarshaling(nxtxnInterface);
                break;
            case "kyc":
                serviceData = new ServiceData("GET_USER_KYCINFO");
                serviceInfo = new ServiceInfo(operatorInfo, serviceData);
                UserTransactionInfo userTransactionInfo = new UserTransactionInfo(Util.getRequestId(username), null, Constants.KYC_TYPE[0], "1234654321");
                userInfo = new UserInfo(username, userTransactionInfo, Constants.USER_ID_TYPE[0]);
                user = new User(userInfo, "1");
                serviceRequest = new ServiceRequest(channelInfo, deviceInfo, transactionInfoX, serviceInfo, user, null, Constants.VERSION, "GET_USER_KYCINFO");
                b2cServiceInterface = new B2cServiceInterface(serviceRequest, Constants.VERSION);
                nxtxnInterface = new NxtxnInterface(b2cServiceInterface, Constants.VERSION);
                xmlRequestString = getUserInfoKycConverter.doMarshaling(nxtxnInterface);
                break;
        }
        return xmlRequestString;
    }

    /**
     *
     * @param body
     * @param infoType
     * @return
     * @throws IOException
     */
    public com.oxigenwallet.userservice.common.xml.model.responseXml.NxtxnInterface getGetUserInfoResponseXml(String body, String infoType) throws IOException {
        com.oxigenwallet.userservice.common.xml.model.responseXml.NxtxnInterface nxtxnInterface = null;
        //UNMARSHALING XML RESPONSE STRING USING PREDEFINED MAPPING

        System.out.println(body);

        switch (infoType) {
            case "lite":
                nxtxnInterface = (com.oxigenwallet.userservice.common.xml.model.responseXml.NxtxnInterface) getUserInfoLiteConverter.doUnMarshaling(body);
                break;
            case "full":
                nxtxnInterface = (com.oxigenwallet.userservice.common.xml.model.responseXml.NxtxnInterface) getUserInfoConverter.doUnMarshaling(body);
                break;
            case "kyc":
                nxtxnInterface = (com.oxigenwallet.userservice.common.xml.model.responseXml.NxtxnInterface) getUserInfoKycConverter.doUnMarshaling(body);
                break;
        }
        return nxtxnInterface;
    }

    public String getVerifyUserRequestXml(String username, String password) throws IOException {

        //GETTING ALL THE REQUESET JSON MODEL OBJECT
        //POPULATING REQUEST XML MODEL OBJECT USING VALUES FROM REQUEST JSON MODEL OBJECT
        UserInfo userInfo = new UserInfo(username, Constants.USER_ID_TYPE[0]);
        User userX = new User(userInfo, "1");
        EncryptionInfo encryptionInfo = new EncryptionInfo(Constants.PIN_TYPE[0], Constants.ENC_TYPE[0], password);
        SecurityInfo securityInfo = new SecurityInfo(encryptionInfo, "1");
        OperatorInfo operatorInfo = new OperatorInfo(Constants.TRUSTED_OPERATOR[0], Constants.BUISNESS_CATEGORY[0]);
        ServiceData serviceData = new ServiceData(Constants.SERVICE_TYPE[5]);
        ServiceInfo serviceInfo = new ServiceInfo(operatorInfo, serviceData);
        TransactionInfo transactionInfoX = new TransactionInfo(Util.getTimeStamp(), Util.getRequestId(username), "description");
        ChannelInfo channelInfo = new ChannelInfo(Constants.CHANNEL_NAME[0], Constants.CHANNEL_TYPE[0], Constants.Ip_ADDRESS, Constants.CHANNEL_INSTANCE_ID[0], Constants.CHANNEL_CONSUMER_ID, Constants.IP_TYPE[0]);
        DeviceInfo deviceInfoX = new DeviceInfo(
                "",
                "",
                "");

        ServiceRequest serviceRequest = new ServiceRequest(channelInfo, deviceInfoX, transactionInfoX, serviceInfo, userX, securityInfo, Constants.VERSION, Constants.SERVICE_TYPE[5]);
        B2cServiceInterface b2cServiceInterface = new B2cServiceInterface(serviceRequest, Constants.VERSION);
        NxtxnInterface nxtxnInterface = new NxtxnInterface(b2cServiceInterface, Constants.VERSION);

        //MARSHALING REQUEST XML MODEL OBJECT TO STRING USING PREDEFINED MAPPING
        String verifyUserRequestXml = verifyUserConverter.doMarshaling(nxtxnInterface);

        return verifyUserRequestXml;
    }

    public String getUpdateFraudStatusRequestXml(JsonRequestBody jsonRequestBody) throws IOException {

        //GETTING ALL THE REQUESET JSON MODEL OBJECT
        com.oxigenwallet.userservice.common.json.request.Request request = jsonRequestBody.getRequest();
        com.oxigenwallet.userservice.common.json.request.DeviceInfo deviceInfo = request.getDeviceInfo();
        com.oxigenwallet.userservice.common.json.request.TokenInfo tokenInfo = request.getTokeninfo();

        //POPULATING REQUEST XML MODEL OBJECT USING VALUES FROM REQUEST JSON MODEL OBJECT
        ChannelInfo channelInfo = new ChannelInfo(Constants.CHANNEL_NAME[1], Constants.CHANNEL_TYPE[0], Constants.Ip_ADDRESS, Constants.CHANNEL_INSTANCE_ID[0], Constants.CHANNEL_CONSUMER_ID, Constants.IP_TYPE[0]);
        DeviceInfo deviceInfoX = new DeviceInfo(deviceInfo.getDeviceOs(), deviceInfo.getDeviceOsVersion(), deviceInfo.getDeviceId());
        TransactionInfo transactionInfo = new TransactionInfo(Util.getTimeStamp(), Util.getRequestId(tokenInfo.getUsername()), Constants.SERVICE_TYPE[2]);
        SystemServiceInfo systemServiceInfo = new SystemServiceInfo(Constants.PLATFORM_NAME[0], Constants.ROLE_NAME[0], Constants.SERVICE_NAME[0], Constants.SUB_SERVICE_NAME[0], "Update", Constants.SERVICE_TYPE[6]);

        CustomerData customerData = new CustomerData(null, "1", Constants.USER_ID_TYPE[0], tokenInfo.getUsername(), "Wrong otp entered", "L-PIN attempts exceeded", "DISABLED");

//        customerData.setReason("L-PIN attempts exceeded");
//        customerData.setStatus("DISABLED");
//        customerData.setStatus_description("Wrong otp entered");
        SystemServiceRequest systemServiceRequest = new SystemServiceRequest(Constants.SERVICE_TYPE[6], Constants.VERSION, channelInfo, deviceInfoX, transactionInfo, null, nxtxnAdminUserId, "email", nxtxnAdminEmail, systemServiceInfo, customerData, "1");
        NxtxnSystemInterface nxtxnSystemInterface = new NxtxnSystemInterface(systemServiceRequest, Constants.VERSION);
        NxtxnInterface nxtxnInterface = new NxtxnInterface(nxtxnSystemInterface, Constants.VERSION);

        //MARSHALING REQUEST XML MODEL OBJECT TO STRING USING PREDEFINED MAPPING
        return updateFraudStatusConverter.doMarshaling(nxtxnInterface);
    }

    public String getWalletInfoRequest(String username) throws IOException {

        //GETTING ALL THE REQUESET JSON MODEL OBJECT
        //POPULATING REQUEST XML MODEL OBJECT USING VALUES FROM REQUEST JSON MODEL OBJECT
        ChannelInfo channelInfo = new ChannelInfo(Constants.CHANNEL_NAME[0], Constants.CHANNEL_TYPE[0], Constants.Ip_ADDRESS, Constants.CHANNEL_INSTANCE_ID[0], Constants.CHANNEL_CONSUMER_ID, Constants.IP_TYPE[0]);
        DeviceInfo deviceInfoX = new DeviceInfo("", "", "");
        TransactionInfo transactionInfo = new TransactionInfo(Util.getTimeStamp(), Util.getRequestId(username), Constants.SERVICE_TYPE[2]);

        Accounts accounts = new Accounts(null, "1", null, Constants.ACCOUNT_NAME[0]);

        UserInfo userInfo = new UserInfo(username, null, accounts, null, null, null, null, null, Constants.USER_ID_TYPE[0], null);
        User userX = new User(userInfo, "1");

        OperatorInfo operatorInfo = new OperatorInfo(Constants.TRUSTED_OPERATOR[0], Constants.BUISNESS_CATEGORY[0]);
        ServiceData serviceData = new ServiceData(Constants.SERVICE_TYPE[7]);
        ServiceInfo serviceInfo = new ServiceInfo(operatorInfo, serviceData);

//        customerData.setReason("L-PIN attempts exceeded");
//        customerData.setStatus("DISABLED");
//        customerData.setStatus_description("Wrong otp entered");
        ServiceRequest serviceRequest = new ServiceRequest(channelInfo, deviceInfoX, transactionInfo, serviceInfo, userX, null, Constants.VERSION, Constants.SERVICE_TYPE[7]);

        B2cServiceInterface b2cServiceInterface = new B2cServiceInterface(serviceRequest, Constants.VERSION);
        NxtxnInterface nxtxnInterface = new NxtxnInterface(b2cServiceInterface, Constants.VERSION);

        //MARSHALING REQUEST XML MODEL OBJECT TO STRING USING PREDEFINED MAPPING
        return getWalletInfoConverter.doMarshaling(nxtxnInterface);
    }
}
