/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.oxigenwallet.userservice.common.xml.model.requestXml;

/**
 *
 * @author mitz
 */
public class OperatorInfo {
    private String Operator;
    private String Business_Category;

    /**
     *
     */
    public OperatorInfo() {
    }

    /**
     *
     * @param Operator
     * @param Business_Category
     */
    public OperatorInfo(String Operator, String Business_Category) {
        this.Operator = Operator;
        this.Business_Category = Business_Category;
    }

    /**
     *
     * @return
     */
    public String getOperator() {
        return Operator;
    }

    /**
     *
     * @param Operator
     */
    public void setOperator(String Operator) {
        this.Operator = Operator;
    }

    /**
     *
     * @return
     */
    public String getBusiness_Category() {
        return Business_Category;
    }

    /**
     *
     * @param Business_Category
     */
    public void setBusiness_Category(String Business_Category) {
        this.Business_Category = Business_Category;
    }
    
}
