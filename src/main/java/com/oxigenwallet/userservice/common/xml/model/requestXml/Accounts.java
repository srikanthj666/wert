/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.oxigenwallet.userservice.common.xml.model.requestXml;

/**
 *
 * @author mitz
 */
public class Accounts {
    private String Account_info;
    
    private String count;
    private String type;
    private String name;

    /**
     *
     */
    public Accounts() {
    }

    /**
     *
     * @param Account_info
     * @param count
     * @param type
     * @param name
     */
    public Accounts(String Account_info, String count, String type, String name) {
        this.Account_info = Account_info;
        this.count = count;
        this.type = type;
        this.name = name;
    }

    /**
     *
     * @return
     */
    public String getAccount_info() {
        return Account_info;
    }

    /**
     *
     * @param Account_info
     */
    public void setAccount_info(String Account_info) {
        this.Account_info = Account_info;
    }

    /**
     *
     * @return
     */
    public String getCount() {
        return count;
    }

    /**
     *
     * @param count
     */
    public void setCount(String count) {
        this.count = count;
    }

    /**
     *
     * @return
     */
    public String getType() {
        return type;
    }

    /**
     *
     * @param type
     */
    public void setType(String type) {
        this.type = type;
    }

    /**
     *
     * @return
     */
    public String getName() {
        return name;
    }

    /**
     *
     * @param name
     */
    public void setName(String name) {
        this.name = name;
    }
}
