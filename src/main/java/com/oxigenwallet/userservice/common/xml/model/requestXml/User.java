/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.oxigenwallet.userservice.common.xml.model.requestXml;

/**
 *
 * @author mitz
 */
public class User {
    private UserInfo User_Info;
    private String count;
    private String id;
    private String username_type;
    private String username;

    /**
     *
     */
    public User() {
    }

    /**
     *
     * @param id
     * @param username_type
     * @param username
     */
    public User(String id, String username_type, String username) {
        this.id = id;
        this.username_type = username_type;
        this.username = username;
    }

    /**
     *
     * @param User_Info
     * @param count
     */
    public User(UserInfo User_Info, String count) {
        this.User_Info = User_Info;
        this.count = count;
    }

    /**
     *
     * @return
     */
    public UserInfo getUser_Info() {
        return User_Info;
    }

    /**
     *
     * @param User_Info
     */
    public void setUser_Info(UserInfo User_Info) {
        this.User_Info = User_Info;
    }

    /**
     *
     * @return
     */
    public String getCount() {
        return count;
    }

    /**
     *
     * @param count
     */
    public void setCount(String count) {
        this.count = count;
    }

    /**
     *
     * @return
     */
    public String getId() {
        return id;
    }

    /**
     *
     * @param id
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     *
     * @return
     */
    public String getUsername_type() {
        return username_type;
    }

    /**
     *
     * @param username_type
     */
    public void setUsername_type(String username_type) {
        this.username_type = username_type;
    }

    /**
     *
     * @return
     */
    public String getUsername() {
        return username;
    }

    /**
     *
     * @param username
     */
    public void setUsername(String username) {
        this.username = username;
    }
}
