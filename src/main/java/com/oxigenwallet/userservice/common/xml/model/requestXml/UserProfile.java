/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.oxigenwallet.userservice.common.xml.model.requestXml;

/**
 *
 * @author mitz
 */
public class UserProfile {
    private UserPI User_PI;
    private UserPA User_PA;
    private UserPC User_PC;
    
    private String type;

    /**
     *
     * @param User_PI
     * @param User_PA
     * @param User_PC
     * @param type
     */
    public UserProfile(UserPI User_PI, UserPA User_PA, UserPC User_PC, String type) {
        this.User_PI = User_PI;
        this.User_PA = User_PA;
        this.User_PC = User_PC;
        this.type = type;
    }

    /**
     *
     */
    public UserProfile() {
    }

    /**
     *
     * @return
     */
    public UserPI getUser_PI() {
        return User_PI;
    }

    /**
     *
     * @param User_PI
     */
    public void setUser_PI(UserPI User_PI) {
        this.User_PI = User_PI;
    }

    /**
     *
     * @return
     */
    public UserPA getUser_PA() {
        return User_PA;
    }

    /**
     *
     * @param User_PA
     */
    public void setUser_PA(UserPA User_PA) {
        this.User_PA = User_PA;
    }

    /**
     *
     * @return
     */
    public UserPC getUser_PC() {
        return User_PC;
    }

    /**
     *
     * @param User_PC
     */
    public void setUser_PC(UserPC User_PC) {
        this.User_PC = User_PC;
    }

    /**
     *
     * @return
     */
    public String getType() {
        return type;
    }

    /**
     *
     * @param type
     */
    public void setType(String type) {
        this.type = type;
    }
    
}
