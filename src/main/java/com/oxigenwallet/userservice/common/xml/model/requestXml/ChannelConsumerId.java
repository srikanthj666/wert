/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.oxigenwallet.userservice.common.xml.model.requestXml;

/**
 *
 * @author mitz
 */
public class ChannelConsumerId {
    private String Channel_Consumer_Id;
    
    /**
     *
     */
    public ChannelConsumerId() {
    }

    /**
     *
     * @param Channel_Consumer_Id
     */
    public ChannelConsumerId(String Channel_Consumer_Id) {
        this.Channel_Consumer_Id = Channel_Consumer_Id;
    }

    /**
     *
     * @return
     */
    public String getChannel_Consumer_Id() {
        return Channel_Consumer_Id;
    }

    /**
     *
     * @param Channel_Consumer_Id
     */
    public void setChannel_Consumer_Id(String Channel_Consumer_Id) {
        this.Channel_Consumer_Id = Channel_Consumer_Id;
    }
}
