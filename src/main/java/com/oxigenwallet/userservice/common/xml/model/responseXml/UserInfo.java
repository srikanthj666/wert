/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.oxigenwallet.userservice.common.xml.model.responseXml;

/**
 *
 * @author mitz
 */
public class UserInfo {
    private String User_ID;
    private String type;
    private UserProfile User_Profile;
    private String status;
    private String reason;
    private Documents documents;

    /**
     *
     */
    public UserInfo() {
    }

    /**
     *
     * @return
     */
    public UserProfile getUser_Profile() {
        return User_Profile;
    }

    /**
     *
     * @param User_ID
     * @param type
     * @param User_Profile
     * @param status
     * @param reason
     */
    public UserInfo(String User_ID, String type, UserProfile User_Profile, String status, String reason) {
        this.User_ID = User_ID;
        this.type = type;
        this.User_Profile = User_Profile;
        this.status = status;
        this.reason = reason;
    }

    /**
     *
     * @param User_Profile
     */
    public void setUser_Profile(UserProfile User_Profile) {
        this.User_Profile = User_Profile;
    }

    /**
     *
     * @param User_ID
     * @param type
     */
    public UserInfo(String User_ID, String type) {
        this.User_ID = User_ID;
        this.type = type;
    }

    /**
     *
     * @return
     */
    public String getUser_ID() {
        return User_ID;
    }

    /**
     *
     * @param User_ID
     */
    public void setUser_ID(String User_ID) {
        this.User_ID = User_ID;
    }

    /**
     *
     * @return
     */
    public String getType() {
        return type;
    }

    /**
     *
     * @param type
     */
    public void setType(String type) {
        this.type = type;
    }

    /**
     *
     * @return
     */
    public String getStatus() {
        return status;
    }

    /**
     *
     * @param status
     */
    public void setStatus(String status) {
        this.status = status;
    }

    /**
     *
     * @return
     */
    public String getReason() {
        return reason;
    }

    /**
     *
     * @param reason
     */
    public void setReason(String reason) {
        this.reason = reason;
    }

    /**
     *
     * @return
     */
    public Documents getDocuments() {
        return documents;
    }

    /**
     *
     * @param documents
     */
    public void setDocuments(Documents documents) {
        this.documents = documents;
    }
}
