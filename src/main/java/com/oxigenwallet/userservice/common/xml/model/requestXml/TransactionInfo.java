/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.oxigenwallet.userservice.common.xml.model.requestXml;

/**
 *
 * @author mitz
 */
public class TransactionInfo {
    private String Time_Stamp;
    private String Request_Id;
    private String Txn_Description;

    /**
     *
     */
    public TransactionInfo() {
    }

    /**
     *
     * @param Time_Stamp
     * @param Request_Id
     * @param Txn_Description
     */
    public TransactionInfo(String Time_Stamp, String Request_Id, String Txn_Description) {
        this.Time_Stamp = Time_Stamp;
        this.Request_Id = Request_Id;
        this.Txn_Description = Txn_Description;
    }

    /**
     *
     * @return
     */
    public String getTime_Stamp() {
        return Time_Stamp;
    }

    /**
     *
     * @param Time_Stamp
     */
    public void setTime_Stamp(String Time_Stamp) {
        this.Time_Stamp = Time_Stamp;
    }

    /**
     *
     * @return
     */
    public String getRequest_Id() {
        return Request_Id;
    }

    /**
     *
     * @param Request_Id
     */
    public void setRequest_Id(String Request_Id) {
        this.Request_Id = Request_Id;
    }

    /**
     *
     * @return
     */
    public String getTxn_Description() {
        return Txn_Description;
    }

    /**
     *
     * @param Txn_Description
     */
    public void setTxn_Description(String Txn_Description) {
        this.Txn_Description = Txn_Description;
    }
    
    
}
