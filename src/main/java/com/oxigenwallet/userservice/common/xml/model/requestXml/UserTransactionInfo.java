/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.oxigenwallet.userservice.common.xml.model.requestXml;

/**
 *
 * @author mitz
 */
public class UserTransactionInfo {
    private String Request_Id;
    private String Document_Id;
    private String type;
    private String value;

    /**
     *
     */
    public UserTransactionInfo() {
    }

    /**
     *
     * @param Request_Id
     * @param Document_Id
     * @param type
     * @param value
     */
    public UserTransactionInfo(String Request_Id, String Document_Id, String type, String value) {
        this.Request_Id = Request_Id;
        this.Document_Id = Document_Id;
        this.type = type;
        this.value = value;
    }

    /**
     *
     * @return
     */
    public String getRequest_Id() {
        return Request_Id;
    }

    /**
     *
     * @param Request_Id
     */
    public void setRequest_Id(String Request_Id) {
        this.Request_Id = Request_Id;
    }

    /**
     *
     * @return
     */
    public String getDocument_Id() {
        return Document_Id;
    }

    /**
     *
     * @param Document_Id
     */
    public void setDocument_Id(String Document_Id) {
        this.Document_Id = Document_Id;
    }

    /**
     *
     * @return
     */
    public String getType() {
        return type;
    }

    /**
     *
     * @param type
     */
    public void setType(String type) {
        this.type = type;
    }

    /**
     *
     * @return
     */
    public String getValue() {
        return value;
    }

    /**
     *
     * @param value
     */
    public void setValue(String value) {
        this.value = value;
    }
    
}
