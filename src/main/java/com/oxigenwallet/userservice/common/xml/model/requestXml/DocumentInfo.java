/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.oxigenwallet.userservice.common.xml.model.requestXml;

/**
 *
 * @author mitz
 */
public class DocumentInfo {
    private String kyc_type;
    private String type;
    private String id;
    private String ref_param1;
    private String ref_param2;
    private String vcopy;

    /**
     *
     */
    public DocumentInfo() {
    }

    /**
     *
     * @param kyc_type
     * @param type
     * @param id
     * @param ref_param1
     * @param ref_param2
     * @param vcopy
     */
    public DocumentInfo(String kyc_type, String type, String id, String ref_param1, String ref_param2, String vcopy) {
        this.kyc_type = kyc_type;
        this.type = type;
        this.id = id;
        this.ref_param1 = ref_param1;
        this.ref_param2 = ref_param2;
        this.vcopy = vcopy;
    }

    /**
     *
     * @return
     */
    public String getRef_param1() {
        return ref_param1;
    }

    /**
     *
     * @param ref_param1
     */
    public void setRef_param1(String ref_param1) {
        this.ref_param1 = ref_param1;
    }

    /**
     *
     * @return
     */
    public String getKyc_type() {
        return kyc_type;
    }

    /**
     *
     * @param kyc_type
     */
    public void setKyc_type(String kyc_type) {
        this.kyc_type = kyc_type;
    }

    /**
     *
     * @return
     */
    public String getType() {
        return type;
    }

    /**
     *
     * @param type
     */
    public void setType(String type) {
        this.type = type;
    }

    /**
     *
     * @return
     */
    public String getId() {
        return id;
    }

    /**
     *
     * @param id
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     *
     * @return
     */
    public String getRef_param2() {
        return ref_param2;
    }

    /**
     *
     * @param ref_param2
     */
    public void setRef_param2(String ref_param2) {
        this.ref_param2 = ref_param2;
    }

    /**
     *
     * @return
     */
    public String getVcopy() {
        return vcopy;
    }

    /**
     *
     * @param vcopy
     */
    public void setVcopy(String vcopy) {
        this.vcopy = vcopy;
    }
}
