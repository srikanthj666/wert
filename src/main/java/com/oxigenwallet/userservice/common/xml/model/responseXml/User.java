/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.oxigenwallet.userservice.common.xml.model.responseXml;

/**
 *
 * @author mitz
 */
public class User {
    private UserInfo User_Info;
    private String count;

    /**
     *
     */
    public User() {
    }

    /**
     *
     * @param User_Info
     * @param count
     */
    public User(UserInfo User_Info, String count) {
        this.User_Info = User_Info;
        this.count = count;
    }

    /**
     *
     * @return
     */
    public UserInfo getUser_Info() {
        return User_Info;
    }

    /**
     *
     * @param User_Info
     */
    public void setUser_Info(UserInfo User_Info) {
        this.User_Info = User_Info;
    }

    /**
     *
     * @return
     */
    public String getCount() {
        return count;
    }

    /**
     *
     * @param count
     */
    public void setCount(String count) {
        this.count = count;
    }
    
}
