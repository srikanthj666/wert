/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.oxigenwallet.userservice.common.xml.model;

import com.oxigenwallet.userservice.common.constants.Constants;
import com.oxigenwallet.userservice.common.xml.config.TokenManagerConverter;
import com.oxigenwallet.userservice.common.xml.model.requestXml.B2cServiceInterface;
import com.oxigenwallet.userservice.common.xml.model.requestXml.ChannelInfo;
import com.oxigenwallet.userservice.common.xml.model.requestXml.DeviceInfo;
import com.oxigenwallet.userservice.common.xml.model.requestXml.NxtxnInterface;
import com.oxigenwallet.userservice.common.xml.model.requestXml.OperatorInfo;
import com.oxigenwallet.userservice.common.xml.model.requestXml.ServiceData;
import com.oxigenwallet.userservice.common.xml.model.requestXml.ServiceInfo;
import com.oxigenwallet.userservice.common.xml.model.requestXml.ServiceRequest;
import com.oxigenwallet.userservice.common.xml.model.requestXml.TransactionData;
import com.oxigenwallet.userservice.common.xml.model.requestXml.TransactionInfo;
import com.oxigenwallet.userservice.common.xml.model.requestXml.User;
import com.oxigenwallet.userservice.common.xml.model.requestXml.UserInfo;
import com.oxigenwallet.userservice.utils.Util;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 *
 * @author Asce
 */
@Component
public class TokenXml {
    @Autowired
    private TokenManagerConverter tokenManagerConverter;
    
    /**
     *
     * @param operator
     * @param userName
     * @return
     */
    public String tokenManagerXml(String operator, String userName,String nxtxnOperation) {

        
        ChannelInfo channelInfo = new ChannelInfo("OW", "WEB",Constants.Ip_ADDRESS , "01", null,Constants.IP_TYPE[0]);
        DeviceInfo deviceInfo = new DeviceInfo("", "", "");
        TransactionInfo transactionInfo = new TransactionInfo(Util.getTimeStamp(),Util.getRequestId(userName) , "");

        
        TransactionData transactionData = new TransactionData(nxtxnOperation,operator,null,null);
        UserInfo userInfo = new UserInfo(userName,null,null, transactionData,null,null,Constants.USER_ID_TYPE[0]);
        User user = new User(userInfo, "1");

        OperatorInfo Operator_Info = new OperatorInfo(operator,Constants.BUISNESS_CATEGORY[0]);
        ServiceData Service_Data = new ServiceData(Constants.NXTXN_SERVICES[0]);
        ServiceInfo serviceInfo = new ServiceInfo(Operator_Info, Service_Data);

        
        ServiceRequest servicerequest = new ServiceRequest(channelInfo,deviceInfo,transactionInfo,serviceInfo,user,Constants.NXTXN_VERSION,Constants.NXTXN_SERVICES[0]);
        B2cServiceInterface b2cSerice = new B2cServiceInterface(servicerequest, Constants.NXTXN_VERSION);
        NxtxnInterface token = new NxtxnInterface(b2cSerice, Constants.NXTXN_VERSION);
        

        try {
            return tokenManagerConverter.doMarshaling(token);//doMarshaling(token);
        } catch (IOException ex) {
            Logger.getLogger(TokenXml.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
}
