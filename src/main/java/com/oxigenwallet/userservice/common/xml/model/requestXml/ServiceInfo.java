/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.oxigenwallet.userservice.common.xml.model.requestXml;

/**
 *
 * @author mitz
 */
public class ServiceInfo {
    private OperatorInfo Operator_Info;
    private ServiceData Service_Data;

    /**
     *
     */
    public ServiceInfo() {
    }

    /**
     *
     * @param Operator_Info
     * @param Service_Data
     */
    public ServiceInfo(OperatorInfo Operator_Info, ServiceData Service_Data) {
        this.Operator_Info = Operator_Info;
        this.Service_Data = Service_Data;
    }

    /**
     *
     * @return
     */
    public OperatorInfo getOperator_Info() {
        return Operator_Info;
    }

    /**
     *
     * @param Operator_Info
     */
    public void setOperator_Info(OperatorInfo Operator_Info) {
        this.Operator_Info = Operator_Info;
    }

    /**
     *
     * @return
     */
    public ServiceData getService_Data() {
        return Service_Data;
    }

    /**
     *
     * @param Service_Data
     */
    public void setService_Data(ServiceData Service_Data) {
        this.Service_Data = Service_Data;
    }
    
}
