/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.oxigenwallet.userservice.common.xml.model.responseXml;

/**
 *
 * @author mitz
 */
public class NxtxnSystemInterface {
    private SystemServiceResponse SystemService_Response;
    private String version;

    /**
     *
     */
    public NxtxnSystemInterface() {
    }

    /**
     *
     * @param SystemService_Response
     * @param version
     */
    public NxtxnSystemInterface(SystemServiceResponse SystemService_Response, String version) {
        this.SystemService_Response = SystemService_Response;
        this.version = version;
    }

    /**
     *
     * @return
     */
    public SystemServiceResponse getSystemService_Response() {
        return SystemService_Response;
    }

    /**
     *
     * @param SystemService_Response
     */
    public void setSystemService_Response(SystemServiceResponse SystemService_Response) {
        this.SystemService_Response = SystemService_Response;
    }

    /**
     *
     * @return
     */
    public String getVersion() {
        return version;
    }

    /**
     *
     * @param version
     */
    public void setVersion(String version) {
        this.version = version;
    }
}
