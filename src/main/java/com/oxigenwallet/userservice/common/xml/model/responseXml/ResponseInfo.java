/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.oxigenwallet.userservice.common.xml.model.responseXml;

/**
 *
 * @author mitz
 */
public class ResponseInfo {
    private int HostCode;
    private String HostDescription;
    private String ClassCode;
    private String ClassDescription;
    private String SystemCode;
    private String SystemDescription;

    /**
     *
     */
    public ResponseInfo() {
    }

    /**
     *
     * @param HostCode
     * @param HostDescription
     * @param ClassCode
     * @param ClassDescription
     * @param SystemCode
     * @param SystemDescription
     */
    public ResponseInfo(int HostCode, String HostDescription, String ClassCode, String ClassDescription, String SystemCode, String SystemDescription) {
        this.HostCode = HostCode;
        this.HostDescription = HostDescription;
        this.ClassCode = ClassCode;
        this.ClassDescription = ClassDescription;
        this.SystemCode = SystemCode;
        this.SystemDescription = SystemDescription;
    }

    /**
     *
     * @return
     */
    public int getHostCode() {
        return HostCode;
    }

    /**
     *
     * @param HostCode
     */
    public void setHostCode(int HostCode) {
        this.HostCode = HostCode;
    }

    /**
     *
     * @return
     */
    public String getHostDescription() {
        return HostDescription;
    }

    /**
     *
     * @param HostDescription
     */
    public void setHostDescription(String HostDescription) {
        this.HostDescription = HostDescription;
    }

    /**
     *
     * @return
     */
    public String getClassCode() {
        return ClassCode;
    }

    /**
     *
     * @param ClassCode
     */
    public void setClassCode(String ClassCode) {
        this.ClassCode = ClassCode;
    }

    /**
     *
     * @return
     */
    public String getClassDescription() {
        return ClassDescription;
    }

    /**
     *
     * @param ClassDescription
     */
    public void setClassDescription(String ClassDescription) {
        this.ClassDescription = ClassDescription;
    }

    /**
     *
     * @return
     */
    public String getSystemCode() {
        return SystemCode;
    }

    /**
     *
     * @param SystemCode
     */
    public void setSystemCode(String SystemCode) {
        this.SystemCode = SystemCode;
    }

    /**
     *
     * @return
     */
    public String getSystemDescription() {
        return SystemDescription;
    }

    /**
     *
     * @param SystemDescription
     */
    public void setSystemDescription(String SystemDescription) {
        this.SystemDescription = SystemDescription;
    }
    
    
}
