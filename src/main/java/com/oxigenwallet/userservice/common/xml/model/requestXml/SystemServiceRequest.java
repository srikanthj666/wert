/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.oxigenwallet.userservice.common.xml.model.requestXml;

/**
 *
 * @author mitz
 */
public class SystemServiceRequest {
    private String name;
    private String version;
    private ChannelInfo Channel_Info;
    private DeviceInfo Device_Info;
    private TransactionInfo Transaction_Info;
    private String User;
    private String id;
    private String username_type;
    private String username;
    private SystemServiceInfo SystemService_Info;
    private CustomerData CustomerData;
    private String count;

    /**
     *
     * @param name
     * @param version
     * @param Channel_Info
     * @param Device_Info
     * @param Transaction_Info
     * @param User
     * @param id
     * @param username_type
     * @param username
     * @param SystemService_Info
     * @param CustomerData
     * @param count
     */
    public SystemServiceRequest(String name, String version, ChannelInfo Channel_Info, DeviceInfo Device_Info, TransactionInfo Transaction_Info, String User, String id, String username_type, String username, SystemServiceInfo SystemService_Info, CustomerData CustomerData, String count) {
        this.name = name;
        this.version = version;
        this.Channel_Info = Channel_Info;
        this.Device_Info = Device_Info;
        this.Transaction_Info = Transaction_Info;
        this.User = User;
        this.id = id;
        this.username_type = username_type;
        this.username = username;
        this.SystemService_Info = SystemService_Info;
        this.CustomerData = CustomerData;
        this.count = count;
    }

    /**
     *
     */
    public SystemServiceRequest() {
    }

    /**
     *
     * @return
     */
    public ChannelInfo getChannel_Info() {
        return Channel_Info;
    }

    /**
     *
     * @return
     */
    public String getName() {
        return name;
    }

    /**
     *
     * @param name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     *
     * @return
     */
    public String getVersion() {
        return version;
    }

    /**
     *
     * @param version
     */
    public void setVersion(String version) {
        this.version = version;
    }

    /**
     *
     * @return
     */
    public String getCount() {
        return count;
    }

    /**
     *
     * @param count
     */
    public void setCount(String count) {
        this.count = count;
    }

    /**
     *
     * @param Channel_Info
     */
    public void setChannel_Info(ChannelInfo Channel_Info) {
        this.Channel_Info = Channel_Info;
    }

    /**
     *
     * @return
     */
    public DeviceInfo getDevice_Info() {
        return Device_Info;
    }

    /**
     *
     * @param Device_Info
     */
    public void setDevice_Info(DeviceInfo Device_Info) {
        this.Device_Info = Device_Info;
    }

    /**
     *
     * @return
     */
    public TransactionInfo getTransaction_Info() {
        return Transaction_Info;
    }

    /**
     *
     * @param Transaction_Info
     */
    public void setTransaction_Info(TransactionInfo Transaction_Info) {
        this.Transaction_Info = Transaction_Info;
    }

    /**
     *
     * @return
     */
    public String getUser() {
        return User;
    }

    /**
     *
     * @param User
     */
    public void setUser(String User) {
        this.User = User;
    }

    /**
     *
     * @return
     */
    public String getId() {
        return id;
    }

    /**
     *
     * @param id
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     *
     * @return
     */
    public String getUsername_type() {
        return username_type;
    }

    /**
     *
     * @param username_type
     */
    public void setUsername_type(String username_type) {
        this.username_type = username_type;
    }

    /**
     *
     * @return
     */
    public String getUsername() {
        return username;
    }

    /**
     *
     * @param username
     */
    public void setUsername(String username) {
        this.username = username;
    }

    /**
     *
     * @return
     */
    public SystemServiceInfo getSystemService_Info() {
        return SystemService_Info;
    }

    /**
     *
     * @param SystemService_Info
     */
    public void setSystemService_Info(SystemServiceInfo SystemService_Info) {
        this.SystemService_Info = SystemService_Info;
    }

    /**
     *
     * @return
     */
    public CustomerData getCustomerData() {
        return CustomerData;
    }

    /**
     *
     * @param CustomerData
     */
    public void setCustomerData(CustomerData CustomerData) {
        this.CustomerData = CustomerData;
    }
    
}

