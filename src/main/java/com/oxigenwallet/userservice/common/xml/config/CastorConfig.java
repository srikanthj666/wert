/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.oxigenwallet.userservice.common.xml.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.oxm.Marshaller;
import org.springframework.oxm.Unmarshaller;
import org.springframework.oxm.castor.CastorMarshaller;

/**
 *
 * @author mitz
 */
@Configuration
public class CastorConfig {

    //CREATEUSER
    /**
     *
     * @return
     */
    @Bean
    public CreateUserConverter getCreateUserHandler() {
        CreateUserConverter handler = new CreateUserConverter();
        handler.setMarshaller(getCreateUserCastorMarshaller());
        handler.setUnmarshaller(getCastorUnMarshaller());
        return handler;
    }

    /**
     *
     * @return
     */
    @Bean
    public CastorMarshaller getCreateUserCastorMarshaller() {
        CastorMarshaller castorMarshaller = new CastorMarshaller();
        Resource resource = new ClassPathResource("mapping/CreateUserRequest.xml");
        castorMarshaller.setMappingLocation(resource);
        return castorMarshaller;
    }

    /**
     *
     * @return
     */
    @Bean
    public Unmarshaller getCastorUnMarshaller() {
        CastorMarshaller castorMarshaller = new CastorMarshaller();
        Resource resource = new ClassPathResource("mapping/Response.xml");
        castorMarshaller.setMappingLocation(resource);
        return castorMarshaller;
    }

    //UPDATEUSER
    /**
     *
     * @return
     */
    @Bean
    public UpdateUserConverter getUpdateUserHandler() {
        UpdateUserConverter handler = new UpdateUserConverter();
        handler.setMarshaller(getUpdateUserCastorMarshaller());
        handler.setUnmarshaller(getCastorUnMarshaller());
        return handler;
    }

    /**
     *
     * @return
     */
    @Bean
    public CastorMarshaller getUpdateUserCastorMarshaller() {
        CastorMarshaller castorMarshaller = new CastorMarshaller();
        Resource resource = new ClassPathResource("mapping/UpdateUserRequest.xml");
        castorMarshaller.setMappingLocation(resource);
        return castorMarshaller;
    }

    //CREATE AND SEND OTP
    /**
     *
     * @return
     */
    @Bean
    public CreateAndSendOtpConverter getCreateAndSendOtpHandler() {
        CreateAndSendOtpConverter handler = new CreateAndSendOtpConverter();
        handler.setMarshaller(getCreateAndSendOtpCastorMarshaller());
        handler.setUnmarshaller(getCastorUnMarshaller());
        return handler;
    }

    /**
     *
     * @return
     */
    @Bean
    public CastorMarshaller getCreateAndSendOtpCastorMarshaller() {
        CastorMarshaller castorMarshaller = new CastorMarshaller();
        Resource resource = new ClassPathResource("mapping/CreateAndSendOtpRequest.xml");
        castorMarshaller.setMappingLocation(resource);
        return castorMarshaller;
    }

    //TOKEN MANAGER
    /**
     *
     * @return
     */
    @Bean
    public VerifyUserConverter getVerifyUserHandler() {
        VerifyUserConverter handler = new VerifyUserConverter();
        handler.setMarshaller(getVerifyUserCastorMarshaller());
        handler.setUnmarshaller(getCastorUnMarshaller());
        return handler;
    }

    /**
     *
     * @return
     */
    @Bean
    public TokenManagerConverter getHandler() {
        TokenManagerConverter handler = new TokenManagerConverter();
        handler.setMarshaller(getTokenManagerCastorMarshaller());
        handler.setUnmarshaller(getCastorUnMarshaller());
        return handler;
    }

    /**
     *
     * @return
     */
    @Bean
    public CastorMarshaller getVerifyUserCastorMarshaller() {
        CastorMarshaller castorMarshaller = new CastorMarshaller();
        Resource resource = new ClassPathResource("mapping/VerifyUserRequest.xml");
        castorMarshaller.setMappingLocation(resource);
        return castorMarshaller;
    }

    /**
     *
     * @return
     */
    @Bean
    public CastorMarshaller getTokenManagerCastorMarshaller() {
        CastorMarshaller castorMarshaller = new CastorMarshaller();
        Resource resource = new ClassPathResource("mapping/TokenManagerRequest.xml");
        castorMarshaller.setMappingLocation(resource);
        return castorMarshaller;
    }

    //UPDATE USER WITH OTP
    /**
     *
     * @return
     */
    @Bean
    public UpdateUserOtpConverter getUpdateOtpUserHandler() {
        UpdateUserOtpConverter handler = new UpdateUserOtpConverter();
        handler.setMarshaller(getUpdateUserOtpCastorMarshaller());
        handler.setUnmarshaller(getCastorUnMarshaller());
        return handler;
    }

    /**
     *
     * @return
     */
    @Bean
    public CastorMarshaller getUpdateUserOtpCastorMarshaller() {
        CastorMarshaller castorMarshaller = new CastorMarshaller();
        Resource resource = new ClassPathResource("mapping/UpdateUserOtpRequest.xml");
        castorMarshaller.setMappingLocation(resource);
        return castorMarshaller;
    }

    //RESET CUSTOMER LPIN
    /**
     *
     * @return
     */
    @Bean
    public ResetCustomerLpinConverter getResetCustomerLpinConverterHandler() {
        ResetCustomerLpinConverter handler = new ResetCustomerLpinConverter();
        handler.setMarshaller(getResetCustomerLpinCastorMarshaller());
        handler.setUnmarshaller(getResetCustomerLpinCastorUnMarshaller());
        return handler;
    }

    /**
     *
     * @return
     */
    @Bean
    public Marshaller getResetCustomerLpinCastorMarshaller() {
        CastorMarshaller castorMarshaller = new CastorMarshaller();
        Resource resource = new ClassPathResource("mapping/ResetCustomerLpinRequest.xml");
        castorMarshaller.setMappingLocation(resource);
        return castorMarshaller;
    }

    /**
     *
     * @return
     */
    @Bean
    public Unmarshaller getResetCustomerLpinCastorUnMarshaller() {
        CastorMarshaller castorMarshaller = new CastorMarshaller();
        Resource resource = new ClassPathResource("mapping/ResetCustomerLpinResponse.xml");
        castorMarshaller.setMappingLocation(resource);
        return castorMarshaller;
    }

    //UPDATEPASSWORD 
    /**
     *
     * @return
     */
    @Bean
    public UpdatePasswordConverter getUpdatePasswordConverterHandler() {
        UpdatePasswordConverter handler = new UpdatePasswordConverter();
        handler.setMarshaller(getUpdatePasswordCastorMarshaller());
        handler.setUnmarshaller(getCastorUnMarshaller());
        return handler;
    }

    /**
     *
     * @return
     */
    @Bean
    public Marshaller getUpdatePasswordCastorMarshaller() {
        CastorMarshaller castorMarshaller = new CastorMarshaller();
        Resource resource = new ClassPathResource("mapping/UpdatePasswordRequest.xml");
        castorMarshaller.setMappingLocation(resource);
        return castorMarshaller;
    }
    //GETUSERINFOLITE

    /**
     *
     * @return
     */
    @Bean
    public GetUserInfoLiteConverter getGetUserInfoLiteConverterConverterHandler() {
        GetUserInfoLiteConverter handler = new GetUserInfoLiteConverter();
        handler.setMarshaller(getGetUserInfoLiteCastorMarshaller());
        handler.setUnmarshaller(getCastorUnMarshaller());
        return handler;
    }

    /**
     *
     * @return
     */
    @Bean
    public Marshaller getGetUserInfoLiteCastorMarshaller() {
        CastorMarshaller castorMarshaller = new CastorMarshaller();
        Resource resource = new ClassPathResource("mapping/GetUserInfoLiteRequest.xml");
        castorMarshaller.setMappingLocation(resource);
        return castorMarshaller;
    }
    //GETUSERINFO

    /**
     *
     * @return
     */
    @Bean
    public GetUserInfoConverter getGetUserInfoConverterConverterHandler() {
        GetUserInfoConverter handler = new GetUserInfoConverter();
        handler.setMarshaller(getGetUserInfoCastorMarshaller());
        handler.setUnmarshaller(getCastorUnMarshaller());
        return handler;
    }

    /**
     *
     * @return
     */
    @Bean
    public Marshaller getGetUserInfoCastorMarshaller() {
        CastorMarshaller castorMarshaller = new CastorMarshaller();
        Resource resource = new ClassPathResource("mapping/GetUserInfoRequest.xml");
        castorMarshaller.setMappingLocation(resource);
        return castorMarshaller;
    }
    //GETUSERINFOKYC

    /**
     *
     * @return
     */
    @Bean
    public GetUserInfoKycConverter getGetUserInfoKycConverterConverterHandler() {
        GetUserInfoKycConverter handler = new GetUserInfoKycConverter();
        handler.setMarshaller(getGetUserInfoKycCastorMarshaller());
        handler.setUnmarshaller(getCastorUnMarshaller());
        return handler;
    }

    /**
     *
     * @return
     */
    @Bean
    public Marshaller getGetUserInfoKycCastorMarshaller() {
        CastorMarshaller castorMarshaller = new CastorMarshaller();
        Resource resource = new ClassPathResource("mapping/GetUserInfoKycRequest.xml");
        castorMarshaller.setMappingLocation(resource);
        return castorMarshaller;
    }

    @Bean
    public UpdateFraudStatusConverter getUpdateFraudStatusConverterHandler() {
        UpdateFraudStatusConverter handler = new UpdateFraudStatusConverter();
        handler.setMarshaller(getUpdateFraudStatusConverterMarshaller());
        handler.setUnmarshaller(getCastorUnMarshaller());
        return handler;
    }

    /**
     *
     * @return
     */
    @Bean
    public Marshaller getUpdateFraudStatusConverterMarshaller() {
        CastorMarshaller castorMarshaller = new CastorMarshaller();
        Resource resource = new ClassPathResource("mapping/UpdateFraudStatusRequest.xml");
        castorMarshaller.setMappingLocation(resource);
        return castorMarshaller;
    }

    @Bean
    public GetWalletInfoConverter getWalletInfoConverterHandler() {
        GetWalletInfoConverter handler = new GetWalletInfoConverter();
        handler.setMarshaller(getWalletInfoConverterMarshaller());
        handler.setUnmarshaller(getCastorUnMarshaller());
        return handler;
    }

    /**
     *
     * @return
     */
    @Bean
    public Marshaller getWalletInfoConverterMarshaller() {
        CastorMarshaller castorMarshaller = new CastorMarshaller();
        Resource resource = new ClassPathResource("mapping/GetWalletInfoRequest.xml");
        castorMarshaller.setMappingLocation(resource);
        return castorMarshaller;
    }
}
