/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.oxigenwallet.userservice.common.xml.model.requestXml;

/**
 *
 * @author mitz
 */
public class UserInfo {

    private String User_ID;
    private UserProfile User_Profile;
    private Accounts Accounts;
    private TransactionData Transaction_Data;
    private UserTransactionInfo User_Transaction_Info;
    private Documents Documents; 
    

    private String status;
    private String reason;
    private String type;
    private String count;

    /**
     *
     */
    public UserInfo() {
    }

    /**
     *
     * @param User_ID
     * @param User_Profile
     * @param Accounts
     * @param Transaction_Data
     * @param User_Transaction_Info
     * @param Documents
     * @param status
     * @param reason
     * @param type
     * @param count
     */
    public UserInfo(String User_ID, UserProfile User_Profile, Accounts Accounts, TransactionData Transaction_Data, UserTransactionInfo User_Transaction_Info, Documents Documents, String status, String reason, String type, String count) {
        this.User_ID = User_ID;
        this.User_Profile = User_Profile;
        this.Accounts = Accounts;
        this.Transaction_Data = Transaction_Data;
        this.User_Transaction_Info = User_Transaction_Info;
        this.Documents = Documents;
        this.status = status;
        this.reason = reason;
        this.type = type;
        this.count = count;
    }

    /**
     *
     * @return
     */
    public Documents getDocuments() {
        return Documents;
    }

    /**
     *
     * @param Documents
     */
    public void setDocuments(Documents Documents) {
        this.Documents = Documents;
    }

    /**
     *
     * @return
     */
    public String getCount() {
        return count;
    }

    /**
     *
     * @param count
     */
    public void setCount(String count) {
        this.count = count;
    }

    /**
     *
     * @param User_ID
     * @param type
     */
    public UserInfo(String User_ID, String type) {
        this.User_ID = User_ID;
        this.type = type;
    }

    /**
     *
     * @param User_ID
     * @param User_Transaction_Info
     * @param type
     */
    public UserInfo(String User_ID, UserTransactionInfo User_Transaction_Info, String type) {
        this.User_ID = User_ID;
        this.User_Transaction_Info = User_Transaction_Info;
        this.type = type;
    }

    /**
     *
     * @param User_ID
     * @param User_Profile
     * @param Accounts
     * @param status
     * @param reason
     * @param type
     */
    public UserInfo(String User_ID, UserProfile User_Profile, Accounts Accounts, String status, String reason, String type) {
        this.User_ID = User_ID;
        this.User_Profile = User_Profile;
        this.Accounts = Accounts;
        this.status = status;
        this.reason = reason;
        this.type = type;
    }


    /**
     *
     * @param User_ID
     * @param User_Profile
     * @param Accounts
     * @param status
     * @param reason
     * @param type
     */
    public UserInfo(String User_ID, UserProfile User_Profile, Accounts Accounts, String status, String reason, String type,Documents Documents) {
        this.User_ID = User_ID;
        this.User_Profile = User_Profile;
        this.Accounts = Accounts;
        this.status = status;
        this.reason = reason;
        this.type = type;
        this.Documents = Documents;
    }


    /**
     *
     * @param User_ID
     * @param User_Profile
     * @param Accounts
     * @param Transaction_Data
     * @param status
     * @param reason
     * @param type
     */
    public UserInfo(String User_ID, UserProfile User_Profile, Accounts Accounts, TransactionData Transaction_Data, String status, String reason, String type) {
        this.User_ID = User_ID;
        this.User_Profile = User_Profile;
        this.Accounts = Accounts;
        this.Transaction_Data = Transaction_Data;
        this.status = status;
        this.reason = reason;
        this.type = type;
    }

    /**
     *
     * @param User_ID
     * @param Transaction_Data
     * @param type
     */
    public UserInfo(String User_ID, TransactionData Transaction_Data, String type) {
        this.User_ID = User_ID;
        this.Transaction_Data = Transaction_Data;
        this.type = type;
    }

    /**
     *
     * @return
     */
    public TransactionData getTransaction_Data() {
        return Transaction_Data;
    }

    /**
     *
     * @param Transaction_Data
     */
    public void setTransaction_Data(TransactionData Transaction_Data) {
        this.Transaction_Data = Transaction_Data;
    }

    /**
     *
     * @return
     */
    public String getUser_ID() {
        return User_ID;
    }

    /**
     *
     * @param User_ID
     */
    public void setUser_ID(String User_ID) {
        this.User_ID = User_ID;
    }

    /**
     *
     * @return
     */
    public UserProfile getUser_Profile() {
        return User_Profile;
    }

    /**
     *
     * @param User_Profile
     */
    public void setUser_Profile(UserProfile User_Profile) {
        this.User_Profile = User_Profile;
    }

    /**
     *
     * @return
     */
    public Accounts getAccounts() {
        return Accounts;
    }

    /**
     *
     * @param Accounts
     */
    public void setAccounts(Accounts Accounts) {
        this.Accounts = Accounts;
    }

    /**
     *
     * @return
     */
    public String getStatus() {
        return status;
    }

    /**
     *
     * @param status
     */
    public void setStatus(String status) {
        this.status = status;
    }

    /**
     *
     * @return
     */
    public String getReason() {
        return reason;
    }

    /**
     *
     * @param reason
     */
    public void setReason(String reason) {
        this.reason = reason;
    }

    /**
     *
     * @return
     */
    public String getType() {
        return type;
    }

    /**
     *
     * @param type
     */
    public void setType(String type) {
        this.type = type;
    }

    /**
     *
     * @return
     */
    public UserTransactionInfo getUser_Transaction_Info() {
        return User_Transaction_Info;
    }

    /**
     *
     * @param User_Transaction_Info
     */
    public void setUser_Transaction_Info(UserTransactionInfo User_Transaction_Info) {
        this.User_Transaction_Info = User_Transaction_Info;
    }

}
