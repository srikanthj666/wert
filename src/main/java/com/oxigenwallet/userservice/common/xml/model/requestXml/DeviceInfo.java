/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.oxigenwallet.userservice.common.xml.model.requestXml;

/**
 *
 * @author mitz
 */
public class DeviceInfo {
    private String Device_OS;
    private String Device_OS_version;
    private String Device_ID;

    /**
     *
     */
    public DeviceInfo() {
    }

    /**
     *
     * @param Device_OS
     * @param Device_OS_version
     * @param Device_ID
     */
    public DeviceInfo(String Device_OS, String Device_OS_version, String Device_ID) {
        this.Device_OS = Device_OS;
        this.Device_OS_version = Device_OS_version;
        this.Device_ID = Device_ID;
    }

    /**
     *
     * @return
     */
    public String getDevice_OS() {
        return Device_OS;
    }

    /**
     *
     * @param Device_OS
     */
    public void setDevice_OS(String Device_OS) {
        this.Device_OS = Device_OS;
    }

    /**
     *
     * @return
     */
    public String getDevice_OS_version() {
        return Device_OS_version;
    }

    /**
     *
     * @param Device_OS_version
     */
    public void setDevice_OS_version(String Device_OS_version) {
        this.Device_OS_version = Device_OS_version;
    }

    /**
     *
     * @return
     */
    public String getDevice_ID() {
        return Device_ID;
    }

    /**
     *
     * @param Device_ID
     */
    public void setDevice_ID(String Device_ID) {
        this.Device_ID = Device_ID;
    }
    
}
