/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.oxigenwallet.userservice.common.xml.config;

import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
import org.springframework.oxm.Marshaller;
import org.springframework.oxm.Unmarshaller;

/**
 *
 * @author mitz
 */
public class CreateAndSendOtpConverter {

    private Marshaller marshaller;
    private Unmarshaller unmarshaller;

    /**
     *
     * @param marshaller
     */
    public void setMarshaller(Marshaller marshaller) {
        this.marshaller = marshaller;
    }

    /**
     *
     * @param unmarshaller
     */
    public void setUnmarshaller(Unmarshaller unmarshaller) {
        this.unmarshaller = unmarshaller;
    }

    //Converts Object to XML 

    /**
     *
     * @param object
     * @return
     * @throws IOException
     */
    public String doMarshaling(Object object) throws IOException {
        //String s = null;
        StringWriter sw;
        sw = new StringWriter();
        marshaller.marshal(object, new StreamResult(sw));
        return sw.toString();
    }

    //Converts XML to Java Object

    /**
     *
     * @param xmlResponse
     * @return
     * @throws IOException
     */
    public Object doUnMarshaling(String xmlResponse) throws IOException {
        StringReader sr = new StringReader(xmlResponse);
        return unmarshaller.unmarshal(new StreamSource(sr));

    }
}
