/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.oxigenwallet.userservice.common.xml.model.requestXml;

/**
 *
 * @author mitz
 */
public class ServiceRequest {
    
    private ChannelInfo Channel_Info;
    private DeviceInfo Device_Info;
    private TransactionInfo Transaction_Info;
    private ServiceInfo Service_Info;
    private User User;
    private SecurityInfo Security_Info;
    
    
    private String version;
    private String name;

    /**
     *
     */
    public ServiceRequest() {
    }

    /**
     *
     * @param Channel_Info
     * @param Device_Info
     * @param Transaction_Info
     * @param Service_Info
     * @param User
     * @param Security_Info
     * @param version
     * @param name
     */
    public ServiceRequest(ChannelInfo Channel_Info, DeviceInfo Device_Info, TransactionInfo Transaction_Info, ServiceInfo Service_Info, User User, SecurityInfo Security_Info, String version, String name) {
        this.Channel_Info = Channel_Info;
        this.Device_Info = Device_Info;
        this.Transaction_Info = Transaction_Info;
        this.Service_Info = Service_Info;
        this.User = User;
        this.Security_Info = Security_Info;
        this.version = version;
        this.name = name;
    }

    /**
     *
     * @param Channel_Info
     * @param Device_Info
     * @param Transaction_Info
     * @param Service_Info
     * @param User
     * @param version
     * @param name
     */
    public ServiceRequest(ChannelInfo Channel_Info, DeviceInfo Device_Info, TransactionInfo Transaction_Info, ServiceInfo Service_Info, User User, String version, String name) {
        this.Channel_Info = Channel_Info;
        this.Device_Info = Device_Info;
        this.Transaction_Info = Transaction_Info;
        this.Service_Info = Service_Info;
        this.User = User;
        this.version = version;
        this.name = name;
    }

    /**
     *
     * @return
     */
    public ChannelInfo getChannel_Info() {
        return Channel_Info;
    }

    /**
     *
     * @param Channel_Info
     */
    public void setChannel_Info(ChannelInfo Channel_Info) {
        this.Channel_Info = Channel_Info;
    }

    /**
     *
     * @return
     */
    public DeviceInfo getDevice_Info() {
        return Device_Info;
    }

    /**
     *
     * @param Device_Info
     */
    public void setDevice_Info(DeviceInfo Device_Info) {
        this.Device_Info = Device_Info;
    }

    /**
     *
     * @return
     */
    public TransactionInfo getTransaction_Info() {
        return Transaction_Info;
    }

    /**
     *
     * @param Transaction_Info
     */
    public void setTransaction_Info(TransactionInfo Transaction_Info) {
        this.Transaction_Info = Transaction_Info;
    }

    /**
     *
     * @return
     */
    public ServiceInfo getService_Info() {
        return Service_Info;
    }

    /**
     *
     * @param Service_Info
     */
    public void setService_Info(ServiceInfo Service_Info) {
        this.Service_Info = Service_Info;
    }

    /**
     *
     * @return
     */
    public User getUser() {
        return User;
    }

    /**
     *
     * @param User
     */
    public void setUser(User User) {
        this.User = User;
    }

    /**
     *
     * @return
     */
    public SecurityInfo getSecurity_Info() {
        return Security_Info;
    }

    /**
     *
     * @param Security_Info
     */
    public void setSecurity_Info(SecurityInfo Security_Info) {
        this.Security_Info = Security_Info;
    }

    /**
     *
     * @return
     */
    public String getVersion() {
        return version;
    }

    /**
     *
     * @param version
     */
    public void setVersion(String version) {
        this.version = version;
    }

    /**
     *
     * @return
     */
    public String getName() {
        return name;
    }

    /**
     *
     * @param name
     */
    public void setName(String name) {
        this.name = name;
    }

            
}
