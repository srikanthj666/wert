/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.oxigenwallet.userservice.common.xml.model.responseXml;

/**
 *
 * @author mitz
 */
public class UserPC {
    private String Mobile;
    private String Landline;
    private String Email;
    private String Document;


    public String getPan_Verified() {
        return Pan_Verified;
    }

    public void setPan_Verified(String pan_Verified) {
        Pan_Verified = pan_Verified;
    }

    private String Pan_Verified;


    /**
     *
     */
    public UserPC() {
    }

    public String getDocument() {
        return Document;
    }

    public void setDocument(String document) {
        Document = document;
    }

    /**
     *
     * @return
     */
    public String getMobile() {
        return Mobile;
    }

    /**
     *
     * @param Mobile
     */
    public void setMobile(String Mobile) {
        this.Mobile = Mobile;
    }

    /**
     *
     * @return
     */
    public String getLandline() {
        return Landline;
    }

    /**
     *
     * @param Landline
     */
    public void setLandline(String Landline) {
        this.Landline = Landline;
    }

    /**
     *
     * @return
     */
    public String getEmail() {
        return Email;
    }

    /**
     *
     * @param Email
     */
    public void setEmail(String Email) {
        this.Email = Email;
    }
    
}
