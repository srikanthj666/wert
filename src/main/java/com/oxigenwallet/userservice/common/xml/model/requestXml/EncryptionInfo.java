/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.oxigenwallet.userservice.common.xml.model.requestXml;

/**
 *
 * @author mitz
 */
public class EncryptionInfo {
    private String Data_Type;
    private String Security_Type;
    private String Data;

    /**
     *
     * @param Data_Type
     * @param Security_Type
     * @param Data
     */
    public EncryptionInfo(String Data_Type, String Security_Type, String Data) {
        this.Data_Type = Data_Type;
        this.Security_Type = Security_Type;
        this.Data = Data;
    }

    /**
     *
     */
    public EncryptionInfo() {
    }

    /**
     *
     * @return
     */
    public String getData_Type() {
        return Data_Type;
    }

    /**
     *
     * @param Data_Type
     */
    public void setData_Type(String Data_Type) {
        this.Data_Type = Data_Type;
    }

    /**
     *
     * @return
     */
    public String getSecurity_Type() {
        return Security_Type;
    }

    /**
     *
     * @param Security_Type
     */
    public void setSecurity_Type(String Security_Type) {
        this.Security_Type = Security_Type;
    }

    /**
     *
     * @return
     */
    public String getData() {
        return Data;
    }

    /**
     *
     * @param Data
     */
    public void setData(String Data) {
        this.Data = Data;
    }
}
