/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.oxigenwallet.userservice.common.xml.model.responseXml;

/**
 *
 * @author mitz
 */
public class UserProfile {

    private String type;
    private UserPI User_PI;
    private UserPA User_PA;
    private UserPC User_PC;
    private String Updated_Source;
    private String name;
    private String created_on;

    /**
     *
     */
    public UserProfile() {
    }

    /**
     *
     * @return
     */
    public String getType() {
        return type;
    }

    /**
     *
     * @param type
     */
    public void setType(String type) {
        this.type = type;
    }

    /**
     *
     * @return
     */
    public UserPI getUser_PI() {
        return User_PI;
    }

    /**
     *
     * @param User_PI
     */
    public void setUser_PI(UserPI User_PI) {
        this.User_PI = User_PI;
    }

    /**
     *
     * @return
     */
    public UserPA getUser_PA() {
        return User_PA;
    }

    /**
     *
     * @param User_PA
     */
    public void setUser_PA(UserPA User_PA) {
        this.User_PA = User_PA;
    }

    /**
     *
     * @return
     */
    public UserPC getUser_PC() {
        return User_PC;
    }

    /**
     *
     * @param User_PC
     */
    public void setUser_PC(UserPC User_PC) {
        this.User_PC = User_PC;
    }

    public String getUpdated_Source() {
        return Updated_Source;
    }

    public void setUpdated_Source(String Updated_Source) {
        this.Updated_Source = Updated_Source;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCreated_on() {
        return created_on;
    }

    public void setCreated_on(String created_on) {
        this.created_on = created_on;
    }

}
