/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.oxigenwallet.userservice.common.xml.model.responseXml;

/**
 *
 * @author mitz
 */
public class B2cServiceInterface {
    
    private ServiceResponse Service_Response;
    private String version;

    /**
     *
     */
    public B2cServiceInterface() {
    }

    /**
     *
     * @param Service_Response
     * @param version
     */
    public B2cServiceInterface(ServiceResponse Service_Response, String version) {
        this.Service_Response = Service_Response;
        this.version = version;
    }

    /**
     *
     * @return
     */
    public ServiceResponse getService_Response() {
        return Service_Response;
    }

    /**
     *
     * @param Service_Response
     */
    public void setService_Response(ServiceResponse Service_Response) {
        this.Service_Response = Service_Response;
    }

    /**
     *
     * @return
     */
    public String getVersion() {
        return version;
    }

    /**
     *
     * @param version
     */
    public void setVersion(String version) {
        this.version = version;
    }

        
}
