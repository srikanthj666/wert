/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.oxigenwallet.userservice.common.xml.model.responseXml;

/**
 *
 * @author mitz
 */
public class ServiceResponse {
    private ResponseInfo Response_Info;
    private TransactionInfo Transaction_Info;
    private User User;
    
    private String name;
    private String version;

    /**
     *
     */
    public ServiceResponse() {
    }

    /**
     *
     * @param Response_Info
     * @param Transaction_Info
     * @param User
     * @param name
     * @param version
     */
    public ServiceResponse(ResponseInfo Response_Info, TransactionInfo Transaction_Info, User User, String name, String version) {
        this.Response_Info = Response_Info;
        this.Transaction_Info = Transaction_Info;
        this.User = User;
        this.name = name;
        this.version = version;
    }

    /**
     *
     * @return
     */
    public ResponseInfo getResponse_Info() {
        return Response_Info;
    }

    /**
     *
     * @param Response_Info
     */
    public void setResponse_Info(ResponseInfo Response_Info) {
        this.Response_Info = Response_Info;
    }

    /**
     *
     * @return
     */
    public TransactionInfo getTransaction_Info() {
        return Transaction_Info;
    }

    /**
     *
     * @param Transaction_Info
     */
    public void setTransaction_Info(TransactionInfo Transaction_Info) {
        this.Transaction_Info = Transaction_Info;
    }

    /**
     *
     * @return
     */
    public User getUser() {
        return User;
    }

    /**
     *
     * @param User
     */
    public void setUser(User User) {
        this.User = User;
    }

    /**
     *
     * @return
     */
    public String getName() {
        return name;
    }

    /**
     *
     * @param name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     *
     * @return
     */
    public String getVersion() {
        return version;
    }

    /**
     *
     * @param version
     */
    public void setVersion(String version) {
        this.version = version;
    }
    
    
}
