/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.oxigenwallet.userservice.common.xml.model.requestXml;

/**
 *
 * @author mitz
 */
public class SecurityInfo {
    private EncryptionInfo Encryption_Info;
    private EncryptionInfo Encryption_Info1;
    private EncryptionInfo Encryption_Info2;
    private String count;

    /**
     *
     * @param Encryption_Info1
     * @param Encryption_Info2
     * @param count
     */
    public SecurityInfo(EncryptionInfo Encryption_Info1, EncryptionInfo Encryption_Info2, String count) {
        this.Encryption_Info1 = Encryption_Info1;
        this.Encryption_Info2 = Encryption_Info2;
        this.count = count;
    }

    /**
     *
     * @return
     */
    public EncryptionInfo getEncryption_Info1() {
        return Encryption_Info1;
    }

    /**
     *
     * @param Encryption_Info1
     */
    public void setEncryption_Info1(EncryptionInfo Encryption_Info1) {
        this.Encryption_Info1 = Encryption_Info1;
    }

    /**
     *
     * @return
     */
    public EncryptionInfo getEncryption_Info2() {
        return Encryption_Info2;
    }

    /**
     *
     * @param Encryption_Info2
     */
    public void setEncryption_Info2(EncryptionInfo Encryption_Info2) {
        this.Encryption_Info2 = Encryption_Info2;
    }

    /**
     *
     * @param Encryption_Info
     * @param count
     */
    public SecurityInfo(EncryptionInfo Encryption_Info, String count) {
        this.Encryption_Info = Encryption_Info;
        this.count = count;
    }

    /**
     *
     */
    public SecurityInfo() {
    }

    /**
     *
     * @return
     */
    public EncryptionInfo getEncryption_Info() {
        return Encryption_Info;
    }

    /**
     *
     * @param Encryption_Info
     */
    public void setEncryption_Info(EncryptionInfo Encryption_Info) {
        this.Encryption_Info = Encryption_Info;
    }
    
    /**
     *
     * @return
     */
    public String getCount() {
        return count;
    }

    /**
     *
     * @param count
     */
    public void setCount(String count) {
        this.count = count;
    }
}
