/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.oxigenwallet.userservice.common.xml.model.requestXml;

/**
 *
 * @author mitz
 */
public class NxtxnSystemInterface {
    private SystemServiceRequest SystemService_Request;
    private String version;

    /**
     *
     * @param SystemService_Request
     * @param version
     */
    public NxtxnSystemInterface(SystemServiceRequest SystemService_Request, String version) {
        this.SystemService_Request = SystemService_Request;
        this.version = version;
    }

    /**
     *
     */
    public NxtxnSystemInterface() {
    }

    /**
     *
     * @return
     */
    public SystemServiceRequest getSystemService_Request() {
        return SystemService_Request;
    }

    /**
     *
     * @param SystemService_Request
     */
    public void setSystemService_Request(SystemServiceRequest SystemService_Request) {
        this.SystemService_Request = SystemService_Request;
    }

    /**
     *
     * @return
     */
    public String getVersion() {
        return version;
    }

    /**
     *
     * @param version
     */
    public void setVersion(String version) {
        this.version = version;
    }
    
}
