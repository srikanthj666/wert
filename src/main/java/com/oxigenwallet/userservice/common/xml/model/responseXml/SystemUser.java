/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.oxigenwallet.userservice.common.xml.model.responseXml;

/**
 *
 * @author mitz
 */
public class SystemUser {
    private String AccessControl;

    /**
     *
     */
    public SystemUser() {
    }

    /**
     *
     * @param AccessControl
     */
    public SystemUser(String AccessControl) {
        this.AccessControl = AccessControl;
    }

    /**
     *
     * @return
     */
    public String getAccessControl() {
        return AccessControl;
    }

    /**
     *
     * @param AccessControl
     */
    public void setAccessControl(String AccessControl) {
        this.AccessControl = AccessControl;
    }
    
}
