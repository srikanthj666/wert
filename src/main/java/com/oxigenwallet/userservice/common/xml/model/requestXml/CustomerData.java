/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.oxigenwallet.userservice.common.xml.model.requestXml;

/**
 *
 * @author mitz
 */
public class CustomerData {

    private String Customer;
    private String count;
    private String username_type;
    private String username;
    private String status_description;
    private String reason;
    private String status;

    /**
     *
     * @param Customer
     * @param count
     * @param username_type
     * @param username
     */
    public CustomerData(String Customer, String count, String username_type, String username) {
        this.Customer = Customer;
        this.count = count;
        this.username_type = username_type;
        this.username = username;
    }

    public CustomerData(String Customer, String count, String username_type, String username, String status_description, String reason, String status) {
        this.Customer = Customer;
        this.count = count;
        this.username_type = username_type;
        this.username = username;
        this.status_description = status_description;
        this.reason = reason;
        this.status = status;
    }

    /**
     *
     */
    public CustomerData() {
    }

    /**
     *
     * @return
     */
    public String getCustomer() {
        return Customer;
    }

    /**
     *
     * @param Customer
     */
    public void setCustomer(String Customer) {
        this.Customer = Customer;
    }

    /**
     *
     * @return
     */
    public String getCount() {
        return count;
    }

    /**
     *
     * @param count
     */
    public void setCount(String count) {
        this.count = count;
    }

    /**
     *
     * @return
     */
    public String getUsername_type() {
        return username_type;
    }

    /**
     *
     * @param username_type
     */
    public void setUsername_type(String username_type) {
        this.username_type = username_type;
    }

    /**
     *
     * @return
     */
    public String getUsername() {
        return username;
    }

    /**
     *
     * @param username
     */
    public void setUsername(String username) {
        this.username = username;
    }

    public String getStatus_description() {
        return status_description;
    }

    public void setStatus_description(String status_description) {
        this.status_description = status_description;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

}
