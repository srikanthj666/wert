/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.oxigenwallet.userservice.common.xml.model.requestXml;

import org.springframework.stereotype.Component;

/**
 *
 * @author mitz
 */
@Component
public class TransactionData {

    private String Operation;
    private String Token_For;
    private String OTP_Type;
    private String OTP_For;

    /**
     *
     * @param Operation
     * @param Token_For
     * @param OTP_Type
     * @param OTP_For
     */
    public TransactionData(String Operation, String Token_For, String OTP_Type, String OTP_For) {
        this.Operation = Operation;
        this.Token_For = Token_For;
        this.OTP_Type = OTP_Type;
        this.OTP_For = OTP_For;
    }

    /**
     *
     */
    public TransactionData() {
    }
    
    /**
     *
     * @return
     */
    public String getOperation() {
        return Operation;
    }

    /**
     *
     * @param Operation
     */
    public void setOperation(String Operation) {
        this.Operation = Operation;
    }

    /**
     *
     * @return
     */
    public String getToken_For() {
        return Token_For;
    }

    /**
     *
     * @param Token_For
     */
    public void setToken_For(String Token_For) {
        this.Token_For = Token_For;
    }

    /**
     *
     * @return
     */
    public String getOTP_Type() {
        return OTP_Type;
    }

    /**
     *
     * @param OTP_Type
     */
    public void setOTP_Type(String OTP_Type) {
        this.OTP_Type = OTP_Type;
    }

    /**
     *
     * @return
     */
    public String getOTP_For() {
        return OTP_For;
    }

    /**
     *
     * @param OTP_For
     */
    public void setOTP_For(String OTP_For) {
        this.OTP_For = OTP_For;
    }
}
