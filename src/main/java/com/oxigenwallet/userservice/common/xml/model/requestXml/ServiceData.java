/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.oxigenwallet.userservice.common.xml.model.requestXml;

/**
 *
 * @author mitz
 */
public class ServiceData {
    private String Category;

    /**
     *
     */
    public ServiceData() {
    }

    /**
     *
     * @param Category
     */
    public ServiceData(String Category) {
        this.Category = Category;
    }

    /**
     *
     * @return
     */
    public String getCategory() {
        return Category;
    }

    /**
     *
     * @param Category
     */
    public void setCategory(String Category) {
        this.Category = Category;
    }
    
}
