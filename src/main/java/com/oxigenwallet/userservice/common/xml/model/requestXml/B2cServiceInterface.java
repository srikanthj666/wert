/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.oxigenwallet.userservice.common.xml.model.requestXml;

/**
 *
 * @author mitz
 */
public class B2cServiceInterface {
    
    private ServiceRequest Service_Request;
    private String version;

    /**
     *
     * @param Service_Request
     * @param version
     */
    public B2cServiceInterface(ServiceRequest Service_Request, String version) {
        this.Service_Request = Service_Request;
        this.version = version;
    }

    /**
     *
     */
    public B2cServiceInterface() {
    }

    /**
     *
     * @return
     */
    public ServiceRequest getService_Request() {
        return Service_Request;
    }

    /**
     *
     * @param Service_Request
     */
    public void setService_Request(ServiceRequest Service_Request) {
        this.Service_Request = Service_Request;
    }

    /**
     *
     * @return
     */
    public String getVersion() {
        return version;
    }

    /**
     *
     * @param version
     */
    public void setVersion(String version) {
        this.version = version;
    }

    
}
