/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.oxigenwallet.userservice.common.xml.model.requestXml;

/**
 *
 * @author mitz
 */
public class ChannelInfo {
    
    private String Channel_Name;
    private String Channel_Type;
    private String Channel_IP_Address;
    private String Channel_Instance_Id;
    private String Channel_Consumer_Id;
    
    private String type;

    /**
     *
     */
    public ChannelInfo() {
    }

    /**
     *
     * @param Channel_Name
     * @param Channel_Type
     * @param Channel_IP_Address
     * @param Channel_Instance_Id
     * @param type
     */
    public ChannelInfo(String Channel_Name, String Channel_Type, String Channel_IP_Address, String Channel_Instance_Id, String type) {
        this.Channel_Name = Channel_Name;
        this.Channel_Type = Channel_Type;
        this.Channel_IP_Address = Channel_IP_Address;
        this.Channel_Instance_Id = Channel_Instance_Id;
        this.type = type;
    }

    /**
     *
     * @param Channel_Name
     * @param Channel_Type
     * @param Channel_IP_Address
     * @param Channel_Instance_Id
     * @param Channel_Consumer_Id
     * @param type
     */
    public ChannelInfo(String Channel_Name, String Channel_Type, String Channel_IP_Address, String Channel_Instance_Id, String Channel_Consumer_Id, String type) {
        this.Channel_Name = Channel_Name;
        this.Channel_Type = Channel_Type;
        this.Channel_IP_Address = Channel_IP_Address;
        this.Channel_Instance_Id = Channel_Instance_Id;
        this.Channel_Consumer_Id = Channel_Consumer_Id;
        this.type = type;
    }

    /**
     *
     * @return
     */
    public String getChannel_Name() {
        return Channel_Name;
    }

    /**
     *
     * @param Channel_Name
     */
    public void setChannel_Name(String Channel_Name) {
        this.Channel_Name = Channel_Name;
    }

    /**
     *
     * @return
     */
    public String getChannel_Type() {
        return Channel_Type;
    }

    /**
     *
     * @param Channel_Type
     */
    public void setChannel_Type(String Channel_Type) {
        this.Channel_Type = Channel_Type;
    }

    /**
     *
     * @return
     */
    public String getChannel_IP_Address() {
        return Channel_IP_Address;
    }

    /**
     *
     * @param Channel_IP_Address
     */
    public void setChannel_IP_Address(String Channel_IP_Address) {
        this.Channel_IP_Address = Channel_IP_Address;
    }

    /**
     *
     * @return
     */
    public String getChannel_Instance_Id() {
        return Channel_Instance_Id;
    }

    /**
     *
     * @param Channel_Instance_Id
     */
    public void setChannel_Instance_Id(String Channel_Instance_Id) {
        this.Channel_Instance_Id = Channel_Instance_Id;
    }

    /**
     *
     * @return
     */
    public String getChannel_Consumer_Id() {
        return Channel_Consumer_Id;
    }

    /**
     *
     * @param Channel_Consumer_Id
     */
    public void setChannel_Consumer_Id(String Channel_Consumer_Id) {
        this.Channel_Consumer_Id = Channel_Consumer_Id;
    }

    /**
     *
     * @return
     */
    public String getType() {
        return type;
    }

    /**
     *
     * @param type
     */
    public void setType(String type) {
        this.type = type;
    }

                    
}
