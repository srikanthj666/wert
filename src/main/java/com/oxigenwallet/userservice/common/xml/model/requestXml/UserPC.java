/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.oxigenwallet.userservice.common.xml.model.requestXml;

/**
 *
 * @author mitz
 */
public class UserPC {
    private String Mobile;
    private String Landline;
    private String Email;

    /**
     *
     */
    public UserPC() {
    }

    /**
     *
     * @param Mobile
     * @param Landline
     * @param Email
     */
    public UserPC(String Mobile, String Landline, String Email) {
        this.Mobile = Mobile;
        this.Landline = Landline;
        this.Email = Email;
    }

    /**
     *
     * @return
     */
    public String getMobile() {
        return Mobile;
    }

    /**
     *
     * @param Mobile
     */
    public void setMobile(String Mobile) {
        this.Mobile = Mobile;
    }

    /**
     *
     * @return
     */
    public String getLandline() {
        return Landline;
    }

    /**
     *
     * @param Landline
     */
    public void setLandline(String Landline) {
        this.Landline = Landline;
    }

    /**
     *
     * @return
     */
    public String getEmail() {
        return Email;
    }

    /**
     *
     * @param Email
     */
    public void setEmail(String Email) {
        this.Email = Email;
    }
}
