/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.oxigenwallet.userservice.common.xml.model.responseXml;


/**
 *
 * @author mitz
 */
public class NxtxnInterface {
    
    private NxtxnSystemInterface NxTxN_System_Interface;
    private B2cServiceInterface B2C_Service_Interface;
    private String version;

    /**
     *
     * @param NxTxN_System_Interface
     * @param version
     */
    public NxtxnInterface(NxtxnSystemInterface NxTxN_System_Interface, String version) {
        this.NxTxN_System_Interface = NxTxN_System_Interface;
        this.version = version;
    }

    /**
     *
     * @param B2C_Service_Interface
     * @param version
     */
    public NxtxnInterface(B2cServiceInterface B2C_Service_Interface, String version) {
        this.B2C_Service_Interface = B2C_Service_Interface;
        this.version = version;
    }

    /**
     *
     */
    public NxtxnInterface() {
    }

    /**
     *
     * @return
     */
    public B2cServiceInterface getB2C_Service_Interface() {
        return B2C_Service_Interface;
    }

    /**
     *
     * @param B2C_Service_Interface
     */
    public void setB2C_Service_Interface(B2cServiceInterface B2C_Service_Interface) {
        this.B2C_Service_Interface = B2C_Service_Interface;
    }

    /**
     *
     * @return
     */
    public String getVersion() {
        return version;
    }

    /**
     *
     * @param version
     */
    public void setVersion(String version) {
        this.version = version;
    }

    /**
     *
     * @return
     */
    public NxtxnSystemInterface getNxTxN_System_Interface() {
        return NxTxN_System_Interface;
    }

    /**
     *
     * @param NxTxN_System_Interface
     */
    public void setNxTxN_System_Interface(NxtxnSystemInterface NxTxN_System_Interface) {
        this.NxTxN_System_Interface = NxTxN_System_Interface;
    }
    
    
    
    
}
