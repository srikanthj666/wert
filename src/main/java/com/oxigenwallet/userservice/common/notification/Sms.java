/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.oxigenwallet.userservice.common.notification;

import org.springframework.stereotype.Component;

/**
 *
 * @author Asce
 */
@Component
public interface Sms {

    /**
     *
     * @param len
     * @return
     */
    public int createOtp(int len);

    /**
     *
     * @param otp
     * @param mdn
     * @return
     */
    public boolean sendOtp(int otp, String mdn);

}