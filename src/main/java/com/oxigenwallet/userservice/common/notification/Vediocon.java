/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.oxigenwallet.userservice.common.notification;

import java.net.HttpURLConnection;
import java.net.URI;
import java.net.URL;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import org.apache.http.client.utils.URIBuilder;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 *
 * @author mitz
 */
@Component
public class Vediocon implements Sms {

    @Value("${sms.vediocon.baseUrl}")
    private String url;

    @Value("#{ @environment['sms.vediocon.username']}")
    private String userName;

    @Value("${sms.vediocon.password}")
    private String password;

    @Value("${sms.vediocon.senderId}")
    private String senderId;

    @Value("${sms.vediocon.messageType}")
    private String messageType;

    /**
     *
     * @param len
     * @return
     */
    @Override
    public int createOtp(int len) {
        Random rnd = new Random();
        int base = (int) Math.pow(10, len - 1);
        int limit = 9 * base;
        int otp = base + rnd.nextInt(limit);
        return otp;
    }

    /**
     *
     * @param otp
     * @param mdn
     * @return Boolean
     */
    @Override
    public boolean sendOtp(int otp, String mdn) {

        String message = String.format("Hello %s your one Time Password is %d", mdn, otp);
        
        URI uri = null;
        try {
            uri = new URIBuilder("https://bulksmsapi.videoconsolutions.com")
                    .addParameter("username", userName)
                    .addParameter("password", password)
                    .addParameter("messageType", messageType)
                    .addParameter("mobile", mdn)
                    .addParameter("senderId", senderId)
                    .addParameter("message", message)
                    .build();

            URL urlObj = null;

            urlObj = uri.toURL();
            TrustManager[] trustAllCerts = new TrustManager[]{new X509TrustManager() {

                public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                    return null;
                }

                public void checkClientTrusted(
                        java.security.cert.X509Certificate[] certs, String authType) {
                }

                public void checkServerTrusted(
                        java.security.cert.X509Certificate[] certs, String authType) {
                }
            }};


            SSLContext sc = SSLContext.getInstance("SSL");
            sc.init(null, trustAllCerts, new java.security.SecureRandom());
            HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());

            HttpURLConnection connection = null;

            connection = (HttpURLConnection) urlObj.openConnection();

            connection.setRequestMethod("GET");

            int responseCode = 0;

            responseCode = connection.getResponseCode();

            if (responseCode == HttpURLConnection.HTTP_OK) {
                return true;
            }
            return false;
        } catch (Exception ex) {
            Logger.getLogger(Vediocon.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            return false;
        }
    }

}
