/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.oxigenwallet.userservice.common.validation;

import com.oxigenwallet.userservice.common.constants.Codes;
import com.oxigenwallet.userservice.common.constants.Constants;
import com.oxigenwallet.userservice.common.constants.ErrorCodes;

/**
 *
 * @author Asce
 */
public class Validation {

    /**
     *
     * @param username
     * @return
     */
    public static Codes validateUsername(String username) {
        if (username == null || username.length() == 0) {
            return ErrorCodes.EMPTY_USERNAME;
        } else if (username.length() > Constants.USERNAME_MAX_LENGTH) {
            return ErrorCodes.INVALID_USERNAME_LENGTH;
        }
        return ErrorCodes.VALID;
    }

    public static Codes validateOtp(String otp) {
        if (otp == null || otp.equals("")) {
            return ErrorCodes.EMPTY_OTP;
        }
        return ErrorCodes.VALID;
    }

    public static Codes validatePassword(String password) {
        if (password == null || password.equals("")) {
            return ErrorCodes.EMPTY_PASSWORD;
        }
        return ErrorCodes.VALID;
    }
}
