/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.oxigenwallet.userservice.common.validation;

import com.oxigenwallet.userservice.common.constants.Codes;
import com.oxigenwallet.userservice.common.constants.ErrorCodes;
import com.oxigenwallet.userservice.common.json.request.JsonRequestBody;
import static com.oxigenwallet.userservice.common.validation.DeviceInfoValidation.validateDeviceInfo;
import static com.oxigenwallet.userservice.common.validation.UserValidation.validateUser;
import static com.oxigenwallet.userservice.common.validation.Validation.validateOtp;
import static com.oxigenwallet.userservice.common.validation.Validation.validatePassword;
import static com.oxigenwallet.userservice.common.validation.Validation.validateUsername;

/**
 *
 * @author mitz
 */
public class CreateUserValidation {

    public static Codes validate(JsonRequestBody jsonRequestBody) {
        //username validation
        Codes usernameCode = validateUsername(jsonRequestBody.getRequest().getUser().getUsername());
        if (usernameCode.getCode() != ErrorCodes.VALID.getCode()) {
            return usernameCode;
        }
        //device info validation
        Codes deviceInfoCode = validateDeviceInfo(jsonRequestBody.getRequest().getDeviceInfo());
        if (deviceInfoCode.getCode() != ErrorCodes.VALID.getCode()) {
            return deviceInfoCode;
        }
        //userinfo validation
        Codes userCode = validateUser(jsonRequestBody.getRequest().getUser());
        if (userCode.getCode() != ErrorCodes.VALID.getCode()) {
            return userCode;
        }
        //otp valiadation
        Codes otpCode = validateOtp(jsonRequestBody.getRequest().getUser().getOtp());
        if (otpCode.getCode() != ErrorCodes.VALID.getCode()) {
            return otpCode;
        }
        //password validation
        Codes passwordCode = validatePassword(jsonRequestBody.getRequest().getUser().getPassword());
        if (passwordCode.getCode() != ErrorCodes.VALID.getCode()) {
            return passwordCode;
        }
        return ErrorCodes.VALID;

    }
}
