/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.oxigenwallet.userservice.common.validation;

import com.oxigenwallet.userservice.common.constants.Codes;
import com.oxigenwallet.userservice.common.constants.Constants;
import com.oxigenwallet.userservice.common.constants.ErrorCodes;
import com.oxigenwallet.userservice.common.json.request.User;

/**
 *
 * @author mitz
 */
public class UserValidation {

    public static Codes validateUser(User user) {
        if (user.getName() == null || user.getName().length() > Constants.FIRST_NAME_LENGTH) {
            return ErrorCodes.INVALID_FIRST_NAME;
        }
        if (user.getMiddleName() != null && user.getMiddleName().length() > Constants.MIDDLE_NAME_LENGTH) {
            return ErrorCodes.INVALID_MIDDLE_NAME;
        }
        if (user.getLastName() == null || user.getLastName().length() > Constants.LAST_NAME_LENGTH) {
            return ErrorCodes.INVALID_LAST_NAME;
        }
        if (user.getDob() == null || user.getDob() == "") {
            return ErrorCodes.INVALID_DOB;
        }
        if (user.getZipcode() != null && user.getZipcode().length() > Constants.ZIP_CODE_LENGTH) {
            return ErrorCodes.INVALID_ZIP_CODE;
        }
        if (user.getEmail() != null && user.getEmail().length() > Constants.EMAIL_LENGTH) {
            return ErrorCodes.INVALID_EMAIL;
        }
        if (user.getGender() != null && user.getGender().length() > Constants.GENDER_LENGTH) {
            return ErrorCodes.INVALID_GENDER;
        }
        if (user.getAddress() != null && user.getAddress().length() > Constants.ADDRESS_LENGTH) {
            return ErrorCodes.INVALID_ADDRESS;
        }
        if (user.getCity() != null && user.getCity().length() > Constants.CITY_LENGTH) {
            return ErrorCodes.INVALID_CITY;
        }
        if (user.getDistrict() != null && user.getDistrict().length() > Constants.DISTRICT_LENGTH) {
            return ErrorCodes.INVALID_DISTRICT;
        }
        if (user.getState() != null && user.getState().length() > Constants.STATE_LENGTH) {
            return ErrorCodes.INVALID_STATE;
        }
        if (user.getCountry() != null && user.getCountry().length() > Constants.COUNTRY_LENGTH) {
            return ErrorCodes.INVALID_COUNTRY;
        }
        if (user.getLandline() != null && user.getLandline().length() > Constants.LANDLINE_LENGTH) {
            return ErrorCodes.INVALID_LANDLINE;
        }
        return ErrorCodes.VALID;
    }

    public static Codes validateUserUpdate(User user) {
        if (user.getName() != null && user.getName().length() > Constants.FIRST_NAME_LENGTH) {
            return ErrorCodes.INVALID_FIRST_NAME;
        }
        if (user.getMiddleName() != null && user.getMiddleName().length() > Constants.MIDDLE_NAME_LENGTH) {
            return ErrorCodes.INVALID_MIDDLE_NAME;
        }
        if (user.getLastName() != null && user.getLastName().length() > Constants.LAST_NAME_LENGTH) {
            return ErrorCodes.INVALID_LAST_NAME;
        }
        
        if (user.getZipcode() != null && user.getZipcode().length() > Constants.ZIP_CODE_LENGTH) {
            return ErrorCodes.INVALID_ZIP_CODE;
        }
        if (user.getEmail() != null && user.getEmail().length() > Constants.EMAIL_LENGTH) {
            return ErrorCodes.INVALID_EMAIL;
        }
        if (user.getGender() != null && user.getGender().length() > Constants.GENDER_LENGTH) {
            return ErrorCodes.INVALID_GENDER;
        }
        if (user.getAddress() != null && user.getAddress().length() > Constants.ADDRESS_LENGTH) {
            return ErrorCodes.INVALID_ADDRESS;
        }
        if (user.getCity() != null && user.getCity().length() > Constants.CITY_LENGTH) {
            return ErrorCodes.INVALID_CITY;
        }
        if (user.getDistrict() != null && user.getDistrict().length() > Constants.DISTRICT_LENGTH) {
            return ErrorCodes.INVALID_DISTRICT;
        }
        if (user.getState() != null && user.getState().length() > Constants.STATE_LENGTH) {
            return ErrorCodes.INVALID_STATE;
        }
        if (user.getCountry() != null && user.getCountry().length() > Constants.COUNTRY_LENGTH) {
            return ErrorCodes.INVALID_COUNTRY;
        }
        if (user.getLandline() != null && user.getLandline().length() > Constants.LANDLINE_LENGTH) {
            return ErrorCodes.INVALID_LANDLINE;
        }
        return ErrorCodes.VALID;
    }
}
