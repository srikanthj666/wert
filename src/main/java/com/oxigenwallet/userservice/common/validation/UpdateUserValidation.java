/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.oxigenwallet.userservice.common.validation;

import com.oxigenwallet.userservice.common.constants.Codes;
import com.oxigenwallet.userservice.common.constants.ErrorCodes;
import com.oxigenwallet.userservice.common.json.request.JsonRequestBody;
import static com.oxigenwallet.userservice.common.validation.Validation.validateUsername;
import static com.oxigenwallet.userservice.common.validation.DeviceInfoValidation.validateDeviceInfo;
import static com.oxigenwallet.userservice.common.validation.UserValidation.validateUserUpdate;

/**
 *
 * @author mitz
 */
public class UpdateUserValidation {
    
    public static Codes validate(JsonRequestBody jsonRequestBody){
        //username validation
        Codes usernameCode = validateUsername(jsonRequestBody.getRequest().getUser().getUsername());
        if(usernameCode.getCode()!=0){
            return usernameCode;
        }
        //device info validation
        Codes deviceInfoCode = validateDeviceInfo(jsonRequestBody.getRequest().getDeviceInfo());
        if(deviceInfoCode.getCode() !=0){
            return deviceInfoCode;
        }
        //userinfo validation
        Codes userCode = validateUserUpdate(jsonRequestBody.getRequest().getUser());
        if(userCode.getCode() != 0){
            return userCode;
        }
        return ErrorCodes.VALID;
        
    }
    
}
