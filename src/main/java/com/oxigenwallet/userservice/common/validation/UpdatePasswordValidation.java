/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.oxigenwallet.userservice.common.validation;

import com.oxigenwallet.userservice.common.constants.Codes;
import com.oxigenwallet.userservice.common.constants.Constants;
import com.oxigenwallet.userservice.common.constants.ErrorCodes;
import com.oxigenwallet.userservice.common.json.request.JsonRequestBody;
import static com.oxigenwallet.userservice.common.validation.DeviceInfoValidation.validateDeviceInfo;
import static com.oxigenwallet.userservice.common.validation.Validation.validateUsername;
import java.util.Arrays;

/**
 *
 * @author Asce
 */
public class UpdatePasswordValidation {

    public static Codes validate(JsonRequestBody jsonRequestBody) {
        //username validation
        Codes usernameCode = validateUsername(jsonRequestBody.getRequest().getUser().getUsername());
        if (usernameCode.getCode() != ErrorCodes.VALID.getCode()) {
            return usernameCode;
        }
        //device info validation
        Codes deviceInfoCode = validateDeviceInfo(jsonRequestBody.getRequest().getDeviceInfo());
        if (deviceInfoCode.getCode() != ErrorCodes.VALID.getCode()) {
            return deviceInfoCode;
        }
        if (jsonRequestBody.getRequest().getUser().getType() == null
                || !Arrays.asList(Constants.PASSWORD_UPDATE_TYPE).contains(jsonRequestBody.getRequest().getUser().getType().toUpperCase())) {
            return ErrorCodes.INVALID_UPDATE_PASSWORD_TYPE;
        }
        return ErrorCodes.VALID;

    }
}
