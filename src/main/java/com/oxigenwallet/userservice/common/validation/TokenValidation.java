/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.oxigenwallet.userservice.common.validation;

import com.oxigenwallet.userservice.common.constants.Codes;
import com.oxigenwallet.userservice.common.constants.ErrorCodes;
import com.oxigenwallet.userservice.common.json.request.JsonRequestBody;

/**
 *
 * @author Asce
 */
public class TokenValidation {

    public static Codes validate(JsonRequestBody request) {
        Codes error = TokenInfoValidation.validateTokenInfo(request.getRequest().getTokeninfo());
        if (error.getCode() != ErrorCodes.VALID.getCode()) {
            return error;
        }
        error = DeviceInfoValidation.validateDeviceInfo(request.getRequest().getDeviceInfo());
        if (error.getCode() != ErrorCodes.VALID.getCode()) {
            return error;
        }
        return ErrorCodes.VALID;
    }
}
