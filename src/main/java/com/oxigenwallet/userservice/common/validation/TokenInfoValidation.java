/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.oxigenwallet.userservice.common.validation;

import com.oxigenwallet.userservice.common.constants.Codes;
import com.oxigenwallet.userservice.common.constants.Constants;
import com.oxigenwallet.userservice.common.constants.ErrorCodes;
import com.oxigenwallet.userservice.common.json.request.TokenInfo;
import static com.oxigenwallet.userservice.common.validation.Validation.validatePassword;
import static com.oxigenwallet.userservice.common.validation.Validation.validateUsername;
import java.util.Arrays;

/**
 *
 * @author Asce
 */
public class TokenInfoValidation {

    public static Codes validateTokenInfo(TokenInfo tokenInfo) {

        if (!Arrays.asList(Constants.GRANT_TYPE).contains(tokenInfo.getGrantType().toUpperCase())) {
            return ErrorCodes.INVALID_GRANT_TYPE;
        }

        if (!Arrays.asList(Constants.US_SCOPE).contains(tokenInfo.getScope().toUpperCase())) {
            return ErrorCodes.INVALID_SCOPE;
        }
        if (tokenInfo.getRedirectUri() == null) {
            return ErrorCodes.INVALID_REDIRECT_URI;
        }
        Codes error = validateUsername(tokenInfo.getUsername());
        if (error.getCode() != ErrorCodes.VALID.getCode()) {
            return error;
        }
        if (tokenInfo.getGrantType().equalsIgnoreCase(Constants.GRANT_TYPE[0]) ) {
            if (tokenInfo.getOtp() == null || tokenInfo.getOtp().length() != Constants.LOGIN_OTP_LENGTH) {
                return ErrorCodes.INVALID_OTP;
            }
            if (tokenInfo.getAuthCode()== null || tokenInfo.getAuthCode()== "") {
                return ErrorCodes.INVALID_AUTH_CODE;
            }
        }
        if (tokenInfo.getGrantType().equalsIgnoreCase(Constants.GRANT_TYPE[1])) {
            Codes passwordCode = validatePassword(tokenInfo.getPassword());
            if (passwordCode.getCode() != ErrorCodes.VALID.getCode()) {
                return passwordCode;
            }
        }

        return ErrorCodes.VALID;
    }
}
