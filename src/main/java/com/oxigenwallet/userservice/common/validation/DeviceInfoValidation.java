/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.oxigenwallet.userservice.common.validation;

import com.oxigenwallet.userservice.common.constants.Codes;
import com.oxigenwallet.userservice.common.constants.Constants;
import com.oxigenwallet.userservice.common.constants.ErrorCodes;
import com.oxigenwallet.userservice.common.json.request.DeviceInfo;

/**
 *
 * @author Asce
 */
public class DeviceInfoValidation {

    public static Codes validateDeviceInfo(DeviceInfo deviceInfo) {
        if (deviceInfo != null) {
            if (deviceInfo.getDeviceOs() != null && deviceInfo.getDeviceOs().length() > Constants.DEVICE_OS_MAX_LENGTH) {
                return ErrorCodes.INVALID_DEVICE_OS_LENGTH;
            }
            if (deviceInfo.getDeviceOsVersion() != null && deviceInfo.getDeviceOsVersion().length() > Constants.DEVICE_OS_VERSION_MAX_LENGTH) {
                return ErrorCodes.INVALID_DEVICE_OS_VERSION_LENGTH;
            }
            if (deviceInfo.getDeviceId() != null && deviceInfo.getDeviceId().length() > Constants.DEVICE_ID_MAX_LENGTH) {
                return ErrorCodes.INVALID_DEVICE_ID_LENGTH;
            }
            if (deviceInfo.getImei() != null && deviceInfo.getImei().length() > Constants.IMEI_MAX_LENGTH) {
                return ErrorCodes.INVALID_IMEI_LENGTH;
            }
            if (deviceInfo.getIpAddress() != null && deviceInfo.getIpAddress().length() > Constants.IP_ADDRESS_MAX_LENGTH) {
                return ErrorCodes.INVALID_IP_ADDRESS_LENGTH;
            }
            if (deviceInfo.getLatitude() != null && deviceInfo.getLatitude().length() > Constants.LATITUDE_MAX_LENGTH) {
                return ErrorCodes.INVALID_LATITUDE_LENGTH;
            }
            if (deviceInfo.getLongitude() != null && deviceInfo.getLongitude().length() > Constants.LONGITUDE_MAX_LENGTH) {
                return ErrorCodes.INVALID_LONGITUDE_LENGTH;
            }
            if (deviceInfo.getModelName() != null && deviceInfo.getModelName().length() > Constants.MODEL_MAX_LENGTH) {
                return ErrorCodes.INVALID_MODEL_LENGTH;
            }
            if (deviceInfo.getTimeStamp() != null && deviceInfo.getTimeStamp().length() > Constants.TIMESTAMP_LENGTH) {
                return ErrorCodes.INVALID_TIMESTAMP_LENGTH;
            }
        }
        return ErrorCodes.VALID;
    }
}
