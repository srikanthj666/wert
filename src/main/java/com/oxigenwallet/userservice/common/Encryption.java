/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.oxigenwallet.userservice.common;

import java.io.UnsupportedEncodingException;
import java.nio.charset.Charset;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.SignatureException;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;

import org.apache.tomcat.util.codec.binary.Base64;
import org.springframework.stereotype.Component;
import org.springframework.beans.factory.annotation.Value;

/**
 *
 * @author mitz
 */
@Component
public class Encryption {

    /**
     *
     */
    public static final String HMAC_SHA1_ALGORITHM = "HmacSHA1";

    /**
     *
     */
    public static final String HMAC_SHA256_ALGORITHM = "HmacSHA256";

    @Value("#{ @environment['oxiface.PvtKey']}")
    private String oxifaceSalt;

    @Value("#{ @environment['oxiface.salt']}")
    private String oxifaceKey;


    public static boolean validateHash(String algorithm, String code, String request, String key) throws SignatureException, NoSuchAlgorithmException, InvalidKeyException, UnsupportedEncodingException {
        String result = Base64Hmac(key, request, algorithm);
        return result.equals(code);

    }

    /**
     *
     * @param key key
     * @param data data
     * @param algorithm algorithm
     * @return string
     */
    public static String Base64Hmac(String key, String data, String algorithm) {

        StringBuilder result = new StringBuilder();
//        data = data.replaceAll("\\s+", "");;
        try {
            final Charset asciiCs = Charset.forName("US-ASCII");
            final Mac sha256_HMAC = Mac.getInstance(algorithm);
            final SecretKeySpec secret_key = new javax.crypto.spec.SecretKeySpec(asciiCs.encode(key).array(), algorithm);

            try {
                sha256_HMAC.init(secret_key);
            } catch (InvalidKeyException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            final byte[] mac_data = sha256_HMAC.doFinal(asciiCs.encode(data).array());

            for (final byte element : mac_data) {
                result.append(Integer.toString((element & 0xff) + 0x100, 16).substring(1));// += Integer.toString((element & 0xff) + 0x100, 16).substring(1);
            }

        } catch (NoSuchAlgorithmException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        byte[] dataByte;
        String base64 = "";
        try {
            dataByte = result.toString().getBytes("UTF-8");
            Base64 base = new Base64();
            base64 = base.encodeToString(dataByte);
        } catch (UnsupportedEncodingException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return base64;
    }
}
