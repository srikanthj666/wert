/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.oxigenwallet.userservice.common;

import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.SignatureException;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

/**
 *
 * @author Asce
 */
@Component
public class RestCalling {

    /**
     *
     */
    public RestCalling() {
    }

    @Value("#{ @environment['redis.dbName'] ?: 'USSS' }")
    private String redisDbName;

    @Value("#{ @environment['header.contentType']}")
    private String NxtxnContentType;

    @Value("#{ @environment['header.channelUsername']}")
    private String NxtxnChannelUserName;

    @Value("#{ @environment['header.channelUserPassword']}")
    private String NxtxnChannelUserPassword;

    @Value("#{ @environment['oxiface.salt']}")
    private String oxifaceSalt;

    @Value("#{ @environment['oxiface.PvtKey']}")
    private String oxifacePvtKey;

    @Value("#{ @environment['oxifaceUrl']}")
    private String oxifaceUrl;

    /**
     *
     * @param xmlRequest xmlRequest
     * @param url url
     * @return string
     */
    public ResponseEntity<String> postNxtxn(String xmlRequest, String url) {

        MultiValueMap<String, String> header = new LinkedMultiValueMap<>();

        //requestHeaders.setAuthorization(httpAuthentication);
        header.add("Content-Type", NxtxnContentType);
        header.add("ChannelUserName", NxtxnChannelUserName);
        header.add("ChannelUserPassword", NxtxnChannelUserPassword);

// Note the body object as first parameter!
        try {
            HttpEntity<String> httpEntity = new HttpEntity<>(xmlRequest, header);
            RestTemplate restTemplate = new RestTemplate();
            return restTemplate.exchange(url, HttpMethod.POST, httpEntity, String.class);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.CONFLICT);
        }
    }

    public ResponseEntity<String> postSMS(String request, String url) {

        MultiValueMap<String, String> header = new LinkedMultiValueMap<>();
        header.add("Content-Type", "application/json");
        header.add("Authorization", "Basic 9073ffd2-10ce-4786-ab71-9b657b9a9489");

        try {
            HttpEntity<String> httpEntity = new HttpEntity<>(request, header);
            RestTemplate restTemplate = new RestTemplate();
            return restTemplate.exchange(url, HttpMethod.POST, httpEntity, String.class);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.CONFLICT);
        }
    }

    public ResponseEntity<String> postOxiface(String request) throws SignatureException, NoSuchAlgorithmException, InvalidKeyException, UnsupportedEncodingException {
        MultiValueMap<String, String> header = new LinkedMultiValueMap<>();

        String time = String.valueOf(java.lang.System.currentTimeMillis());

        String stringHash = Encryption.Base64Hmac(oxifacePvtKey, request, Encryption.HMAC_SHA256_ALGORITHM);

//        header.add("API-ID", oxifaceSalt);
//        header.add("API-TIME", time);
        header.add("code", stringHash);
//        header.add("ENC-STR", oxifacePvtKey);

// Note the body object as first parameter!
        try {
            HttpEntity<String> httpEntity = new HttpEntity<>(request, header);
            RestTemplate restTemplate = new RestTemplate();
            return restTemplate.exchange(oxifaceUrl, HttpMethod.POST, httpEntity, String.class);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.CONFLICT);
        }
    }
}
