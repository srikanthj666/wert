/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.oxigenwallet.userservice.common;

import org.springframework.beans.factory.annotation.*;
import org.springframework.stereotype.Component;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;
import redis.clients.jedis.Protocol;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author sagarsharma
 */
@Component
public class RedisCacheResource  {

    final private String cacheServerAddress;
    final private int cacheServerPort;
    final private JedisPool jedisPool;

    /**
     *
     * @param cacheServerPort cacheServerPort 
     * @param cacheServerAddress cacheServerAddress
     */
    @Autowired
    public RedisCacheResource(
            @Value("#{ @environment['redis.port'] ?: 6379 }") int cacheServerPort,
            @Value("#{ @environment['redis.address'] ?: 'localhost' }") String cacheServerAddress
    ) {
        this.cacheServerPort = cacheServerPort;
        this.cacheServerAddress = cacheServerAddress;
        JedisPoolConfig poolConfig = new JedisPoolConfig();
        poolConfig.setMaxTotal(128);
        jedisPool = new JedisPool(
                poolConfig,
                cacheServerAddress,
                cacheServerPort,
                Protocol.DEFAULT_TIMEOUT,
                null
        );
    }

    /**
     *
     * @return jedisPool
     */
    public JedisPool getCacheResource() {
        

        return jedisPool;
    }

}
