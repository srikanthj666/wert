/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.oxigenwallet.userservice.common;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.oxigenwallet.userservice.common.json.response.JsonResponseBody;
import java.io.PrintWriter;
import java.io.StringWriter;
import org.apache.log4j.Logger;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 *
 * @author Asce
 */
@Aspect
@Component
public class Logging {

    @Autowired
    ObjectMapper mapper;

//    final static Logger logger = Logger.getLogger(Logging.class);
    static final Logger logger = Logger.getLogger("debugLogger");
    static final Logger resultLog = Logger.getLogger("reportsLogger");

    /**
     *
     * @param joinPoint jointPoint
     * @param result result
     * @throws JsonProcessingException JsonProcessingException
     */
    @AfterReturning(
            pointcut = "execution(public * com.oxigenwallet.userservice.controller.TokenController.token(..))  || execution(public * com.oxigenwallet.userservice.controller.TokenController.validateToken(..))",
            returning = "result")
    public void tokenLog(JoinPoint joinPoint, Object result) throws JsonProcessingException {
        mapper.disable(SerializationFeature.INDENT_OUTPUT);
        String response = mapper.writeValueAsString(result);
        String request = mapper.writeValueAsString(joinPoint.getArgs()[0]);
        logger.info("Request\t" + request);
        logger.info("Response\t" + response + "\n");
    }

    @AfterThrowing(
            pointcut = "execution (* com.oxigenwallet.userservice.controller.TokenController.token(..))||"
                    + "execution (* com.oxigenwallet.userservice.controller.TokenController.validateToken(..))||"
                    + "execution (* com.oxigenwallet.userservice.controller.TokenController.tokenDelete(..))|| "
                    + "execution (* com.oxigenwallet.userservice.controller.TokenController.tokenAllDelete(..)) ||"
                    + "execution (* com.oxigenwallet.userservice.controller.OtpController.generateOtp(..)) ||"
                    + "execution (* com.oxigenwallet.userservice.controller.UserController.createUser(..))||"
                    + "execution (* com.oxigenwallet.userservice.controller.UserController.updateUser(..))||"
                    + "execution (* com.oxigenwallet.userservice.controller.UserController.updatePassword(..)) ||"
                    + "execution (* com.oxigenwallet.userservice.controller.UserController.getUserInfo(..)) ||"
                    + "execution (* com.oxigenwallet.userservice.controller.UserController.updateUserKyc(..))",
            throwing = "ex")
    public void ExcptionLog(Exception ex) {

        StringWriter errors = new StringWriter();
        ex.printStackTrace(new PrintWriter(errors));
        
        resultLog.error(errors.toString());
    }

    /**
     *
     * @param joinPoint jointpoint
     * @param result result
     * @throws JsonProcessingException JsonProcessingException
     */
    @AfterReturning(
            pointcut = "execution(public * com.oxigenwallet.userservice.controller.TokenController.tokenDelete(..))",
            returning = "result")
    public void deletetokenLog(JoinPoint joinPoint, Object result) throws JsonProcessingException {
        mapper.disable(SerializationFeature.INDENT_OUTPUT);
        String response = mapper.writeValueAsString(result);
        String request = mapper.writeValueAsString(joinPoint.getArgs()[2]);
        logger.info("Request\t" + request);
        logger.info("Response\t" + response + "\n");
    }
    
    /**
     *
     * @param joinPoint jointPoint
     * @param result result
     * @throws JsonProcessingException exception
     */
    @AfterReturning(
            pointcut = "execution(public * com.oxigenwallet.userservice.common.ExceptionHandling.*(..))",
            returning = "result")
    public void exceptionLog(JoinPoint joinPoint, Object result) throws JsonProcessingException {
        mapper.disable(SerializationFeature.INDENT_OUTPUT);
        String exceptionResponse = mapper.writeValueAsString(result);
        logger.info("Response\t" + exceptionResponse + "\n");
    }

    /**
     *
     * @param joinPoint jointPoint
     * @param excep Exception
     * @throws JsonProcessingException  Exception
     */
    @AfterThrowing(
            pointcut = "execution(public * com.oxigenwallet.userservice.controller.OtpController.generateOtp(..)) || execution(public * com.oxigenwallet.userservice.controller.UserController.*(..))",
            throwing = "excep")
    public void exceptionThrowableLog(JoinPoint joinPoint, Throwable excep) throws JsonProcessingException {
        mapper.disable(SerializationFeature.INDENT_OUTPUT);
        String jsonRequestString = mapper.writeValueAsString(joinPoint.getArgs()[0]);
        logger.info("Exception\t" + excep);
        logger.info("Request\t" + jsonRequestString);
    }

    /**
     *
     * @param joinPoint JointPoint
     * @param response response
     */
    @AfterReturning(
            pointcut = "execution(public * com.oxigenwallet.userservice.common.RestCalling.postNxtxn(..))",
            returning = "response")
    public void nxtxnLog(JoinPoint joinPoint, Object response) {
        String request = (String) joinPoint.getArgs()[0];
        logger.info("Request\t" + request);
        logger.info("Response\t" + response + "\n");
    }

    /**
     *
     * @param joinPoint joinPoint
     * @param response response
     */
    @AfterReturning(
            pointcut = "execution(public * com.oxigenwallet.userservice.common.RestCalling.postOxiface(..))",
            returning = "response")
    public void oxifaceLog(JoinPoint joinPoint, Object response) {
        String request = (String) joinPoint.getArgs()[0];
        logger.info("Request\t" + request);
        logger.info("Response\t" + response + "\n");
    }

    /**
     *
     * @param joinPoint joinPoint
     * @param response response
     * @throws JsonProcessingException Exception
     */
    @AfterReturning(
            pointcut = "execution(public * com.oxigenwallet.userservice.controller.OtpController.generateOtp(..)) || execution(public * com.oxigenwallet.userservice.controller.UserController.*(..)) || execution(public * com.oxigenwallet.userservice.controller.AdminController.*(..))",
            returning = "response")
    public void otpLog(JoinPoint joinPoint, JsonResponseBody response) throws JsonProcessingException {
        mapper.disable(SerializationFeature.INDENT_OUTPUT);
        String jsonResponseString = mapper.writeValueAsString(response);
        String jsonRequestString = mapper.writeValueAsString(joinPoint.getArgs()[0]);
        logger.info("Request\t" + jsonRequestString);
        logger.info("Response\t" + jsonResponseString + "\n");
    }
}
