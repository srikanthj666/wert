/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.oxigenwallet.userservice.common.constants;

/**
 *
 * @author Asce
 */
public class Codes {
    


    int code;
    String description;

    /**
     *
     * @param code code
     * @param description description 
     */
    public Codes(int code, String description) {
        this.code = code;
        this.description = description;
    }

    /**
     *
     * @param code code
     */
    public Codes(int code) {
        this.code = code;
    }

    /**
     *
     */
    public Codes() {
    }

    /**
     *
     * @param description description
     */
    public Codes(String description) {
        this.description = description;
    }
    
    /**
     *
     * @return code
     */
    public int getCode() {
        return code;
    }

    /**
     * 
     * @param code Code
     */
    public void setCode(int code) {
        this.code = code;
    }

    /**
     *
     * @return description
     */
    public String getDescription() {
        return description;
    }

    /**
     *
     * @param description description
     */
    public void setDescription(String description) {
        this.description = description;
    }

}