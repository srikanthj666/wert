/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.oxigenwallet.userservice.common.constants;

/**
 *
 * @author Asce
 */
public class ResponseCodes {

    /**
     *
     */
    public static final String TOKEN_GENERATED = "Token Generated Successfully.";

    /**
     *
     */
    public static final String TOKEN_VALIDATED = "Token Validated Successfully.";

    /**
     *
     */
    public static final String TOKEN_DELETED = "Token Deleted Successfully.";
    public static final String OTP_SENTSUCCESSFULLY = "OTP successfully sent to ";
    public static final String ACCOUNT_CREATED_SUCCESSFULLY = "Account successfully created";
    public static final String ACCOUNT_CREATED_UPDATED = "Account successfully Updated";

}
