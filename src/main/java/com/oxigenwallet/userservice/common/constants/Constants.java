/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.oxigenwallet.userservice.common.constants;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author Asce
 */
public class Constants {

    // constants for every api
    public static final String FAILURE = "FAILURE";
    public static final String SUCCESS = "SUCCESS";
    public static final String CREATE_USER_OPERATOR = "oxigen";
    public static final int SUCCESSCODE = 0;
    public static final String OXIFACE_VERSION = "1.0";
    public static final String NXTXN_VERSION = "1.0";
    public static final String NXTXN_SUCCESS_CODE = "0000";
    public static final String Ip_ADDRESS = "127.0.0.1";
    public static final String CHANNEL_CONSUMER_ID = "9988776655";
    public static final int OTP_VALIDITY = 900;
    public static final int AUTH_CODE_VALIDITY = 900;
    public static final String[] NXTXN_SERVICES = {"TOKEN_MANAGER", "VERIFY_USER"};
    public static final String[] NXTXN_SERVICES_URL = {"TokenManager", "VerifyUser"};
    public static final String[] OXIFACE_SERVICES = {"USER_VERIFYUSERSERVICE", "USER_ADDUSERSERVICE"};
    public static final String[] US_TOKEN_TYPE = {"OTP", "TOKEN"};
    public static final String[] GRANT_TYPE = {"OTP", "PASSWORD"};
    public static final String[] US_SCOPE = {"OW"};
    public static final String DEFAULT_COUNTRY = "India";
    // channel info for nxtxn api

    // notification service constants
    public static final int[] NOTIFICATION_EVENT_ID = {1000, 1001, 1002};
    public static final String[] NOTIFICATION_EVENT_NAME = {"login", "update_user", "create_user"};
    public static final String NOTIFICATION_CHANNEL = "ow";
    public static final String NOTIFICATION_BUSINESS = "ow";
    //device id
    public static final String[] REDIS_TOKEN_KEY = {"device_id","model_name","updated_on","created_on","ip_address","imei","device_os","device_os_version"};
    public static HashMap<String, String> REDIS_ATTR;
    static {
        REDIS_ATTR = new HashMap<>();
        REDIS_ATTR.put("token","token");
        REDIS_ATTR.put("device_id","device_id");
        REDIS_ATTR.put("device_os","device_os");
        REDIS_ATTR.put("device_os_version","device_os_version");
        REDIS_ATTR.put("model_name","model_name");
        REDIS_ATTR.put("imei","imei");
        REDIS_ATTR.put("ip_address","ip_address");
        REDIS_ATTR.put("date_updated","date_updated");
        REDIS_ATTR.put("data_created","data_created");
    }


    public static final String[] CHANNEL_NAME = {"OW", "ow_admin"};
    public static final String[] CHANNEL_TYPE = {"WEB"};
    public static final String[] CHANNEL_INSTANCE_ID = {"01"};
    public static final String[] IP_TYPE = {"IPv4", "IPv6"};
    public static final String[] OTP_TYPE = {"T_OTP", "R_OTP", "L_OTP", "U_OTP"};
    public static final String[] PIN_TYPE = {"L_PIN", "L_PIN_OLD", "L-PIN"};
    public static final String[] ENC_TYPE = {"TYPE_0", "TYPE_1"};
    public static final String[] REDIS_NAMESPACE = {"AT", "NT", "AUT", "OTP"};
    public static final String[] TOKEN_TYPE = {"debit wallet","p2p","p2m","ekyc"};
    public static final String[] TRUSTED_OPERATOR = {"OXIGEN"}; // operator who can do p2p and p2m
    public static final String[] USER_ID_TYPE = {"MSISDN"};
    public static final String[] TOKEN_NXTXN_OPERATION = {"GET", "CREATE"};
    public static final String[] BUISNESS_CATEGORY = {"B2C"};
    public static final String WALLET_ALREADY_EXIST_MSG = "wallet already exists.";
    public static final int WALLET_ALREADY_EXIST_CODE = 38;
    public static final String[] PAYBACK_ACCOUNT_STATUS = {"N", "Y"};
    public static final String[] STATUS = {"DISABLED", "ENABLED"};
    public static final String VERSION = "1.0";
    public static final String[] USER_PROFILE_INFO = {"nil_kyc", "full_kyc", "soft_kyc"};
    public static final String[] ACCOUNT_TYPE = {"semi_closed", "kyc", "soft_kyc"};
    public static final String[] ACCOUNT_NAME = {"oxigen_wallet"};
    public static final String[] PLATFORM_NAME = {"nxtxn"};
    public static final String[] ROLE_NAME = {"oxiface"};
    public static final String[] SERVICE_NAME = {"user"};
    public static final String[] SUB_SERVICE_NAME = {"Customer"};
    public static final String[] KYC_TYPE = {"AdhaarID"};
    public static final String[] SERVICE_TYPE = {"CREATE_SEND_OTP", "UPDATE_USER", "ResetCustomerLPIN", "UPDATE_PASSWORD", "CREATE_USER", "VERIFY_USER", "UpdateFraudStatus", "GET_WALLET_INFO"};
    public static final String[] USER_INFO_API_TYPE = {"lite", "full", "kyc"};
    public static final String[] GENERATEOTP_API = {"REGISTRATION", "FORGOT", "LOGIN"};
    public static final String[] PASSWORD_UPDATE_TYPE = {"FORGOT", "RESET"};
    public static final int LOGIN_OTP_LENGTH = 6;
    public static final int USERNAME_MAX_LENGTH = 12;
    public static final int DEVICE_OS_MAX_LENGTH = 20;
    public static final int DEVICE_OS_VERSION_MAX_LENGTH = 50;
    public static final int DEVICE_ID_MAX_LENGTH = 100;
    public static final int IMEI_MAX_LENGTH = 50;
    public static final int IP_ADDRESS_MAX_LENGTH = 30;
    public static final int LATITUDE_MAX_LENGTH = 100;
    public static final int LONGITUDE_MAX_LENGTH = 100;
    public static final int MODEL_MAX_LENGTH = 100;
    public static final int TIMESTAMP_LENGTH = 100;

    public static final int FIRST_NAME_LENGTH = 100;
    public static final int MIDDLE_NAME_LENGTH = 100;
    public static final int LAST_NAME_LENGTH = 100;
    public static final int ZIP_CODE_LENGTH = 6;
    public static final int LANDLINE_LENGTH = 15;
    public static final int LANDLINE_MIN_LENGTH = 6;
    public static final int ADDRESS_LENGTH = 1000;
    public static final int EMAIL_LENGTH = 100;
    public static final int GENDER_LENGTH = 1;
    public static final int CITY_LENGTH = 50;
    public static final int DISTRICT_LENGTH = 50;
    public static final int STATE_LENGTH = 50;
    public static final int COUNTRY_LENGTH = 50;
    public static final int MAX_OTP_ATTEMPTS = 2; //2+1
    public static final  String DATE_PATTERN = "(^(((0[1-9]|1[0-9]|2[0-8])[\\/](0[1-9]|1[012]))|((29|30|31)[\\/](0[13578]|1[02]))|((29|30)[\\/](0[4,6,9]|11)))[\\/](19|[2-9][0-9])\\d\\d$)|(^29[\\/]02[\\/](19|[2-9][0-9])(00|04|08|12|16|20|24|28|32|36|40|44|48|52|56|60|64|68|72|76|80|84|88|92|96)$)";
}
