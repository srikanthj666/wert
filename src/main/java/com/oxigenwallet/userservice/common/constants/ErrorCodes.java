/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.oxigenwallet.userservice.common.constants;

/**
 *
 * @author Asce
 */
public class ErrorCodes {

    public static final Codes MDN_INVALID = new Codes(6001, "Mdn field is in");
    public static final Codes HEADER_MISSING = new Codes(6002, "Header are missing in request");
    public static final Codes MERCHANTNAME_REQUIRED = new Codes(6003, "Merchant name required.");
    public static final Codes INVALID_OPERATOR_TYPE = new Codes(6004, "Invalid Operator Type");
    public static final Codes INVALID_GRANT_TYPE = new Codes(6005, "Invalid grant type.");
    public static final Codes INVALID_CLIENT_OR_HASH = new Codes(6006, "Invalid client or hash code.");
    public static final Codes INVALID_OTP_TYPE = new Codes(6007, "Invalid Otp Type.");
    public static final Codes INVALID_TOKEN = new Codes(6503, "Invalid token.");
    public static final Codes INVALID_AUTH_TOKEN_OR_OTP = new Codes(6504, "Invalid OTP.");//Invalid auth code or otp.
    public static final Codes ACCOUNT_LOCKED_INVALID_TOKEN = new Codes(6641, "Your Account is Locked. Please reset your password.");//Invalid auth code or otp.

    // exception handling
    public static final Codes EXCEPTION = new Codes(6600, "Exception");
    public static final Codes REQUEST_BINDING = new Codes(6601, "Exception");
    public static final Codes NULL_POINTER = new Codes(6602, "Error in marshalling.");
    public static final Codes PARSER_CONFIGURATION = new Codes(6603, "Exception");
    public static final Codes SAX_EXCEPTION = new Codes(6604, "Exception");
    public static final Codes IO = new Codes(6605, "Exception");
    public static final Codes SIGNATURE = new Codes(6606, "Exception");
    public static final Codes NO_SUCH_ALGORITHM = new Codes(6607, "Exception");
    public static final Codes INVALID_KEY_EXCEPTION = new Codes(6608, "Exception");
    public static final Codes CUSTOM_EXCEPTION = new Codes(6609, "CustomException");
    public static final Codes SQL_EXCEPTION = new Codes(6610, "SQL Exception");

    // validations
    public static final Codes VALID = new Codes(0, "Valid Attribute");
    public static final Codes EMPTY_USERNAME = new Codes(6611, "Empty username");
    public static final Codes INVALID_USERNAME_LENGTH = new Codes(6612, "Invalid username length");
    public static final Codes INVALID_DEVICE_OS_LENGTH = new Codes(6613, "Invalid device os length");
    public static final Codes INVALID_DEVICE_OS_VERSION_LENGTH = new Codes(6614, "Invalid device os version length");
    public static final Codes INVALID_DEVICE_ID_LENGTH = new Codes(6615, "Invalid device id length");

    public static final Codes INVALID_IMEI_LENGTH = new Codes(6616, "Invalid imei length");
    public static final Codes INVALID_IP_ADDRESS_LENGTH = new Codes(6617, "Invalid ip address length");
    public static final Codes INVALID_LATITUDE_LENGTH = new Codes(6618, "Invalid ip address length");
    public static final Codes INVALID_LONGITUDE_LENGTH = new Codes(6619, "Invalid ip address length");
    public static final Codes INVALID_MODEL_LENGTH = new Codes(6620, "Invalid model name length");
    public static final Codes INVALID_TIMESTAMP_LENGTH = new Codes(6621, "Timestamp too long");

    public static final Codes INVALID_FIRST_NAME = new Codes(6622, "Invalid first name length");
    public static final Codes INVALID_MIDDLE_NAME = new Codes(6623, "Invalid middle name length");
    public static final Codes INVALID_LAST_NAME = new Codes(6624, "Invalid last name length");
    public static final Codes INVALID_DOB = new Codes(6624, "Invalid DOB");
    public static final Codes INVALID_ZIP_CODE = new Codes(6625, "Invalid  Zip code  length");
    public static final Codes INVALID_EMAIL = new Codes(6626, "Invalid  email length");
    public static final Codes INVALID_GENDER = new Codes(6627, "Invalid  gender  length");
    public static final Codes INVALID_ADDRESS = new Codes(6628, "Invalid  address  length");
    public static final Codes INVALID_CITY = new Codes(6630, "Invalid  city  length");
    public static final Codes INVALID_DISTRICT = new Codes(6631, "Invalid  district  length");
    public static final Codes INVALID_STATE = new Codes(6632, "Invalid  state  length");
    public static final Codes INVALID_COUNTRY = new Codes(6633, "Invalid  country  length");
    public static final Codes INVALID_LANDLINE = new Codes(6634, "Invalid  landline  length");
    public static final Codes EMPTY_OTP = new Codes(6635, "Empty Otp");
    public static final Codes EMPTY_PASSWORD = new Codes(6636, "Empty password");
    public static final Codes INVALID_DETAILS = new Codes(6637, "Invalid user details");

    public static final Codes INVALID_SCOPE = new Codes(6640, "Invalid scope.");
    public static final Codes INVALID_REDIRECT_URI = new Codes(6641, "Invalid redirect uri.");
    public static final Codes INVALID_USERNAME = new Codes(6642, "Invalid  username.");
    public static final Codes INVALID_PASSWORD = new Codes(6643, "Invalid  password.");
    public static final Codes INVALID_OTP = new Codes(6644, "Invalid  otp.");
    public static final Codes INVALID_AUTH_CODE = new Codes(6644, "Invalid  auth code.");
    public static final Codes TOO_MANY_OTP_ATTEMPTS = new Codes(6645, "Too many OTP Attempts.");
    public static final Codes OTP_EXPIRE = new Codes(6646, "OTP Expired");
    public static final Codes NXTXN_RESPONSE_ERROR = new Codes(6647, "Unable to connect, please try again later.");
    public static final Codes INVALID_UPDATE_PASSWORD_TYPE = new Codes(6640, "Invalid type.");
    public static final Codes USER_RESPONSE_ERROR = new Codes(6651, "User does not exist");
    public static final Codes USER_ALREADY_ERROR = new Codes(6651, "User already not exist");

    public static final Codes TYPE_ID_RESPONSE_ERROR = new Codes(6652, "Document info type id cannot be null");
    public static final Codes USER_PAN_PRESENT_RESPONSE_ERROR = new Codes(6653, "Editing of name is not allowed after submission of KYC details.");

    // redis storage exception
    public static final String OTP_MISMATCH = "Invalid  OTP.";
    public static final String AUTH_CODE_MISMATCH = "Authentication failed due to mismatch in Auth token";

}
