/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.oxigenwallet.userservice.common;

import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.stereotype.Component;

/**
 *
 * @author sagarsharma
 */
@Component
public class MessagingQueueFactory {

    private RabbitTemplate rabbitTemplate;

    MessagingQueueFactory() {
        ApplicationContext ctx = new AnnotationConfigApplicationContext(RabbitMqConfig.class);
        RabbitTemplate rabbitTemplate = ctx.getBean(RabbitTemplate.class);
        this.rabbitTemplate = rabbitTemplate;
    }

    public RabbitTemplate getMessagingTemplate() {
        return this.rabbitTemplate;
    }

}
