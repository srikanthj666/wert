/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.oxigenwallet.userservice.common;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.oxigenwallet.userservice.common.json.request.RabbitMq.NxtxnRabbitMq;
import com.oxigenwallet.userservice.dao.UserDao;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.MessageListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

/**
 *
 * @author Asce
 */
@Configuration
public class RabbitMqHelper implements MessageListener {

    @Autowired
    private MessagingQueueFactory messagingQueueFactory;

    @Autowired
    ObjectMapper mapper;

    @Autowired
    UserDao userDao;
    @Autowired
    RedisStorage redisStorage;

    @Value("#{ @environment['updateuser.exchange']}")
    String updateExchange;

    @Value("#{ @environment['key.updateuser']}")
    String updateKey;

    @Override
    public void onMessage(Message message) {
        String request = new String(message.getBody());
        try {

            NxtxnRabbitMq requestObj = mapper.readValue(request, NxtxnRabbitMq.class);
            String mdn = requestObj.getMdn();
            userDao.deleteByMdn(mdn);
            redisStorage.deleteUserDetails(mdn);
        } catch (IOException ex) {
            Logger.getLogger(RabbitMqHelper.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
