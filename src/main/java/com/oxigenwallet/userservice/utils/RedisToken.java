/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.oxigenwallet.userservice.utils;

/**
 *
 * @author Asce
 */
public class RedisToken {

    String token;
    String clientId;
    String expireIn;
    String scope;
    String timeStamp;
    String Ip;
    String deviceId;
    String deviceOs;
    String deviceVersion;
    String modelName;

    public RedisToken() {
    }

    public RedisToken(String token, String clientId, String expireIn, String scope) {
        this.token = token;
        this.clientId = clientId;
        this.expireIn = expireIn;
        this.scope = scope;
    }

    public RedisToken(String token, String clientId, String expireIn, String scope, String Ip, String deviceId, String deviceOs, String deviceVersion, String modelName) {
        this.token = token;
        this.clientId = clientId;
        this.expireIn = expireIn;
        this.scope = scope;
        this.Ip = Ip;
        this.deviceId = deviceId;
        this.deviceOs = deviceOs;
        this.deviceVersion = deviceVersion;
        this.modelName = modelName;
    }

    
    
    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getClientId() {
        return clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public String getExpireIn() {
        return expireIn;
    }

    public void setExpireIn(String expireIn) {
        this.expireIn = expireIn;
    }

    public String getScope() {
        return scope;
    }

    public void setScope(String scope) {
        this.scope = scope;
    }

    public String getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(String timeStamp) {
        this.timeStamp = timeStamp;
    }

    public String getIp() {
        return Ip;
    }

    public void setIp(String Ip) {
        this.Ip = Ip;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public String getDeviceOs() {
        return deviceOs;
    }

    public void setDeviceOs(String deviceOs) {
        this.deviceOs = deviceOs;
    }

    public String getDeviceVersion() {
        return deviceVersion;
    }

    public void setDeviceVersion(String deviceVersion) {
        this.deviceVersion = deviceVersion;
    }

    public String getModelName() {
        return modelName;
    }

    public void setModelName(String modelName) {
        this.modelName = modelName;
    }

    
}
