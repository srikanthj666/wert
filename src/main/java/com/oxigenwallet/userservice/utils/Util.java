/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.oxigenwallet.userservice.utils;

import com.oxigenwallet.userservice.common.Encryption;
import com.oxigenwallet.userservice.common.RedisStorage;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Random;
import java.util.TimeZone;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 *
 * @author Asce
 */
@Component
public class Util {

    @Value("#{ @environment['encrypt.salt']}")
    String accessTokenSalt;

    @Value("#{ @environment['encrypt.algorithm']}")
    String encAlgo;

    @Value("#{ @environment['login.otp.switch']}")
    int loginOtpSwitch;
    
    @Value("#{ @environment['login.otp.switch.foreverylogin']}")
    int loginOtpSwitchForEveryLogin;

    @Autowired
    RedisStorage redisStorage;

    /**
     *
     * @param username
     * @return
     */
    public static String getRequestId(String username) {
        String requestId = "";
        if (username.length() > 10) {
            username = username.substring(2, 12);
        }
        requestId = username + new StringBuilder(System.currentTimeMillis() + "").toString();
        if (requestId.length() < 20) {
            long number = (long) Math.floor(Math.random() * 9000000000L) + 1000000000L;
            requestId = requestId + number;
        }
        requestId = requestId.substring(0, 20);
        return requestId;
    }

    /**
     *
     * @return
     */
    public static String getTimeStamp() {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSXXX");
        sdf.setTimeZone(TimeZone.getTimeZone("IST"));
        String currentTimeStamp = sdf.format(new Date());
        return currentTimeStamp;
    }

    /**
     *
     * @param userName
     * @return
     */
    public boolean checkOtpStatus(String userName) {
        if (loginOtpSwitch == 0) {//IF OTP Disable
            return false;
        }
        if (loginOtpSwitchForEveryLogin == 1) {
            return true; //Enabling OTP in Every Login
        }
        //Check if token(Valid Session exist of User)
        ArrayList<RedisToken> allAccessToken = redisStorage.getAllAccessToken(userName);
        if (allAccessToken.size() == 0) {
            return false;
        }
        return true;
    }

    public String encrypt(String data) {
        return Encryption.Base64Hmac(accessTokenSalt, data, encAlgo);
    }

    public static int getPassword(int len) {
        Random rnd = new Random();
        int base = (int) Math.pow(10, len - 1);
        int limit = 9 * base;
        int otp = base + rnd.nextInt(limit);
        return otp;
    }
}
