package com.oxigenwallet.userservice.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.oxigenwallet.userservice.common.CustomException;
import com.oxigenwallet.userservice.common.Encryption;
import com.oxigenwallet.userservice.common.RedisStorage;
import com.oxigenwallet.userservice.common.constants.Constants;
import com.oxigenwallet.userservice.common.constants.ErrorCodes;
import com.oxigenwallet.userservice.common.constants.ResponseCodes;
import com.oxigenwallet.userservice.common.json.request.JsonRequestBody;
import com.oxigenwallet.userservice.common.json.request.Request;
import com.oxigenwallet.userservice.common.json.response.JsonResponseBody;
import com.oxigenwallet.userservice.common.json.response.Response;
import com.oxigenwallet.userservice.common.json.response.Session;
import com.oxigenwallet.userservice.common.json.response.User;
import com.oxigenwallet.userservice.utils.RedisToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.SignatureException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

@RestController
public class SessionController {

    @Autowired
    RedisStorage redisStorage;
    @Autowired
    ObjectMapper mapper;

    @Value("#{ @environment['encryption.switch']}")
    int encryptionSwitch;

    @RequestMapping(
            value = {"/get-active-sessions"},
            method = RequestMethod.POST,
            produces = "application/json")
    public @ResponseBody
    JsonResponseBody activeSessions(
            @RequestBody String tokenRequest,
            @RequestHeader(value = "code", required = true) String code,
            @RequestHeader(value = "client_id", required = true) String clientId
    ) throws IOException, SignatureException, NoSuchAlgorithmException, InvalidKeyException, SAXException, ParserConfigurationException, CustomException {
        JsonRequestBody request
                = mapper.readValue(tokenRequest, JsonRequestBody.class);
        Map<String, String> clientResult = redisStorage.getClient(clientId);
        if (encryptionSwitch == 1) {
            String clientPassword = clientResult.get("secret");
            if (!Encryption.validateHash(Encryption.HMAC_SHA256_ALGORITHM, code, tokenRequest, clientId + ":" + clientPassword)) {
                return new JsonResponseBody(null, Constants.FAILURE, ErrorCodes.INVALID_CLIENT_OR_HASH.getCode(), ErrorCodes.INVALID_CLIENT_OR_HASH.getDescription());
            }
        }
        String username = request.getRequest().getUser().getUsername();
        ArrayList<RedisToken> redisTokens = redisStorage.getAllAccessToken(username);

        return this.getAllSessionResponse(redisTokens, request);
    }

    @RequestMapping(
            value = {"/delete-sessions"},
            method = RequestMethod.POST,
            produces = "application/json")
    public @ResponseBody
    JsonResponseBody deleteSessions(
            @RequestBody String tokenRequest,
            @RequestHeader(value = "code", required = true) String code,
            @RequestHeader(value = "client_id", required = true) String clientId
    ) throws IOException, SignatureException, NoSuchAlgorithmException, InvalidKeyException, SAXException, ParserConfigurationException, CustomException {
        JsonRequestBody request
                = mapper.readValue(tokenRequest, JsonRequestBody.class);
        Map<String, String> clientResult = redisStorage.getClient(clientId);
        if (encryptionSwitch == 1) {
            String clientPassword = clientResult.get("secret");
            if (!Encryption.validateHash(Encryption.HMAC_SHA256_ALGORITHM, code, tokenRequest, clientId + ":" + clientPassword)) {
                return new JsonResponseBody(null, Constants.FAILURE, ErrorCodes.INVALID_CLIENT_OR_HASH.getCode(), ErrorCodes.INVALID_CLIENT_OR_HASH.getDescription());
            }
        }
        String username = request.getRequest().getUser().getUsername();
        redisStorage.deleteAllAccessToken(username);

        //generating response
        User user = new User();
        user.setUsername(username);
        Response response = new Response();
        response.setUser(user);
        return new JsonResponseBody(response, Constants.SUCCESS, Constants.SUCCESSCODE, ResponseCodes.TOKEN_DELETED);
    }

    private JsonResponseBody getAllSessionResponse(ArrayList<RedisToken> redisTokens, JsonRequestBody jsonRequest) {

        User user = new User();
        user.setUsername(jsonRequest.getRequest().getUser().getUsername());
        ArrayList<Session> sessionList = new ArrayList();
        String requestDeviceId = jsonRequest.getRequest().getDeviceInfo().getDeviceId();

        // Sorting redis tokens by date
//        Collections.sort(redisTokens, new SessionDateCompare());

        ArrayList<String> deviceIds = new ArrayList<String>();
        for (RedisToken token : redisTokens) {
            Session session = new Session();
            session.setDevice_id(token.getDeviceId());
            session.setDevice_os(token.getDeviceOs());
            session.setDevice_os_version(token.getDeviceVersion());
            session.setIp_address(token.getIp());
            session.setModel_name(token.getModelName());
            session.setUpdated_on(token.getTimeStamp());

            if (!deviceIds.contains(token.getDeviceId())) {

                if ((requestDeviceId != null && requestDeviceId.equalsIgnoreCase(token.getDeviceId()))) {
                    session.setIs_current("1");
                } else {
                    session.setIs_current("0");
                }
                deviceIds.add(token.getDeviceId());
                sessionList.add(session);
            }
        }
        Collections.sort(sessionList, new SessionCompare());
        Collections.sort(sessionList.subList(1,sessionList.size()), new SessionCompareByDate());
        Response response = new Response(user, null, null, null, null, null);
        response.setSession(sessionList);
        return new JsonResponseBody(response, Constants.SUCCESS, Constants.SUCCESSCODE, Constants.SUCCESS);
    }
}

class SessionCompareByDate implements Comparator<Session> {

    @Override
    public int compare(Session s1, Session s2) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS");
        try {
            if(simpleDateFormat.parse(s1.getUpdated_on()).before(simpleDateFormat.parse(s2.getUpdated_on()))){
                return 1;
            }else{
                return -1;
            }
        } catch (ParseException e) {
            e.printStackTrace();
            return 0;
        }
    }
}

class SessionCompare implements Comparator<Session> {

    @Override
    public int compare(Session s1, Session s2) {
        if (Integer.parseInt(s1.getIs_current()) < Integer.parseInt(s2.getIs_current())) {
            return 1;
        } else {
            return -1;
        }
    }
}

class SessionDateCompare implements Comparator<RedisToken> {

    @Override
    public int compare(RedisToken s1, RedisToken s2) {

        try {
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS");
            if (simpleDateFormat.parse(s1.getTimeStamp()).before(simpleDateFormat.parse(s2.getTimeStamp()))) {
                return 1;
            } else {
                return -1;
            }

        } catch (ParseException ex) {
            System.out.println(ex);
            return 0;
        }
    }
}
