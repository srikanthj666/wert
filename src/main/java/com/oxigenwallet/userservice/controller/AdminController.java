/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.oxigenwallet.userservice.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.oxigenwallet.userservice.common.Encryption;
import com.oxigenwallet.userservice.common.ExceptionHandling;
import com.oxigenwallet.userservice.common.GenerateToken;
import com.oxigenwallet.userservice.common.MessagingQueueFactory;
import com.oxigenwallet.userservice.common.RedisCacheResource;
import com.oxigenwallet.userservice.common.RedisStorage;
import com.oxigenwallet.userservice.common.RestCalling;
import com.oxigenwallet.userservice.common.constants.Codes;
import com.oxigenwallet.userservice.common.constants.Constants;
import com.oxigenwallet.userservice.common.constants.ErrorCodes;
import com.oxigenwallet.userservice.common.constants.ResponseCodes;
import com.oxigenwallet.userservice.common.json.JsonGenerator;
import com.oxigenwallet.userservice.common.json.oxifaceJsonHelper;
import com.oxigenwallet.userservice.common.json.request.JsonRequestBody;
import com.oxigenwallet.userservice.common.json.request.notificationService.Data;
import com.oxigenwallet.userservice.common.json.request.notificationService.Event;
import com.oxigenwallet.userservice.common.json.request.notificationService.LoginOtp;
import com.oxigenwallet.userservice.common.json.response.JsonResponseBody;
import com.oxigenwallet.userservice.common.json.response.Tokens;
import com.oxigenwallet.userservice.common.validation.UpdateUserValidation;
import static com.oxigenwallet.userservice.common.validation.Validation.validateUsername;
import com.oxigenwallet.userservice.common.xml.model.TokenXml;
import com.oxigenwallet.userservice.common.xml.model.XmlGenerator;
import com.oxigenwallet.userservice.dao.TokenDao;
import com.oxigenwallet.userservice.dao.UserDao;
import com.oxigenwallet.userservice.utils.RedisToken;
import com.oxigenwallet.userservice.utils.Util;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.SignatureException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Map;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.xml.sax.SAXException;
import redis.clients.jedis.Jedis;

/**
 *
 * @author Asce
 */
@RestController
public class AdminController extends ExceptionHandling {

    @Value("${nxtxnUrl.createUser}")
    private String createUserUrl;
    @Value("${nxtxnUrl.updateUser}")
    private String updateUserUrl;
    @Value("${nxtxnUrl.updatePassword}")
    private String updatePasswordUrl;
    @Value("${nxtxnUrl.getUserInfo}")
    private String getUserInfoUrl;
    @Value("${nxtxnUrl.getUserInfoLite}")
    private String getUserInfoLiteUrl;
    @Value("${user.status.disabled}")
    private int userStatusDisabled;
    @Value("${user.status.enabled}")
    private int userStatusEnabled;
    @Value("#{ @environment['encryption.switch']}")
    int encryptionSwitch;
    @Value("#{ @environment['authorization.switch']}")
    int authorizationSwitch;
    @Value("#{ @environment['nxtxnUrl.tokenManager']}")
    String tokenManagerNxtxnUrl;
    @Value("#{ @environment['notificationService.switch']}")
    int notificationServiceSwitch;
    @Value("#{ @environment['exchange.updateUSer']}")
    String updateUserExchange;
    //@Value("#{ @environment['rabbitmq.switch']}")
    //int rabbitmqSwitch;

    @Value("#{ @environment['key.updateUser']}")
    String updateUserKey;
    @Value("#{ @environment['exchange.createUser']}")
    String createUserExchange;

    @Value("#{ @environment['key.createUser']}")
    String createUserKey;

    @Value("#{ @environment['accessTokenSalt']}")
    String accessTokenSalt;

    @Value("#{ @environment['admin']}")
    String privilageAdmin;
    @Autowired
    private XmlGenerator xmlGenerator;
    @Autowired
    private JsonGenerator jsonGenerator;
    @Autowired
    private UserDao userDao;
    @Autowired
    private RedisStorage redisStorage;
    @Autowired
    private RestCalling restCalling;
    @Autowired
    private RedisCacheResource redisCacheResource;
    @Autowired
    private ObjectMapper mapper;
    @Autowired
    private oxifaceJsonHelper oxifaceRequestHelper;
    @Autowired
    TokenDao tokenDao;
    @Autowired
    GenerateToken generateToken;
    @Autowired
    TokenXml tokenXml;
    @Autowired
    private MessagingQueueFactory messagingQueueFactory;
    @Autowired
    Util util;

    @RequestMapping(value = "/customer", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    JsonResponseBody getUserInfo(
            @RequestParam("username") String username,
            @RequestParam("info") String info,
            @RequestHeader("code") String code)
            throws IOException, ParserConfigurationException, SAXException, TransformerException, ParseException, org.json.simple.parser.ParseException, SignatureException, NoSuchAlgorithmException, InvalidKeyException {

        if (encryptionSwitch == 1) {

            if (!Encryption.validateHash(Encryption.HMAC_SHA256_ALGORITHM, code, username + ":" + info, privilageAdmin)) {
                return new JsonResponseBody(null, Constants.FAILURE, ErrorCodes.INVALID_CLIENT_OR_HASH.getCode(), ErrorCodes.INVALID_CLIENT_OR_HASH.getDescription());
            }
        }
        Codes validationCode = validateUsername(username);
        if (validationCode.getCode() != ErrorCodes.VALID.getCode()) {
            JsonResponseBody validationResponse = new JsonResponseBody(null, Constants.FAILURE, validationCode.getCode(), validationCode.getDescription());
            return validationResponse;
        }

        //checking in redis
//        Jedis jedisObject = redisCacheResource.getCacheResource().getResource();
//        String key = redisStorage.GenerateKey(username);
//        if (jedisObject.exists(key)) {
//            if (info.equals(jedisObject.hget(key, "info_type")) || info.equals("lite")) {
//                jedisObject.close();
//                return jsonGenerator.getJsonFromRedis(username, info);
//            }
//        }
//        jedisObject.close();
        //checking in mysql
        com.oxigenwallet.userservice.model.User userM = userDao.getByMdn(username);
//        if (userM != null) {
//            if (info.equals(userM.getInfo_type()) || info.equals("lite")) {
//                redisStorage.saveUserDetails(userM);
//                return jsonGenerator.getJsonFromDbObject(userM, info);
//            }
//        }
        String xmlRequestBody = xmlGenerator.getGetUserInfoRequest(username, info);

        ResponseEntity<String> response = null;
        JsonResponseBody getUserInfoJsonResponse = null;

        if (info.equals(Constants.USER_INFO_API_TYPE[1])) {
            response = restCalling.postNxtxn(xmlRequestBody, getUserInfoUrl);
        } else if (info.equals(Constants.USER_INFO_API_TYPE[0])) {
            response = restCalling.postNxtxn(xmlRequestBody, getUserInfoLiteUrl);
        }
        com.oxigenwallet.userservice.common.xml.model.responseXml.NxtxnInterface responseXmlObject = xmlGenerator.getGetUserInfoResponseXml(response.getBody(), info);
        int status = responseXmlObject.getB2C_Service_Interface().getService_Response().getResponse_Info().getHostCode();
        String userStatus = responseXmlObject.getB2C_Service_Interface().getService_Response().getUser().getUser_Info().getStatus();
        if (status == 0) {
            getUserInfoJsonResponse = jsonGenerator.getGetUserInfoSuccessResponseJson(responseXmlObject, info);
            if (userM != null) {
                userDao.updateGetUserInfoRequest(userM, getUserInfoJsonResponse, userStatus, info);
            } else {
                userDao.saveGetUserInfoResponse(getUserInfoJsonResponse, userStatus, info);
            }
            //redisStorage.saveGetUserInfoResponse(getUserInfoJsonResponse, userStatus, info);
        } else {
            getUserInfoJsonResponse = jsonGenerator.getGetUserInfoFailureResponseJson(responseXmlObject, 0);  // 0 failure for getuserinfo
        }
        return getUserInfoJsonResponse;
    }

    @RequestMapping(value = "/customer", method = RequestMethod.POST, produces = "application/json")
    public @ResponseBody
    JsonResponseBody updateUser(
            @RequestBody String requestBody,
            @RequestHeader("code") String code)
            throws IOException, ParserConfigurationException, SAXException, TransformerException, ParseException, org.json.simple.parser.ParseException, SignatureException, NoSuchAlgorithmException, InvalidKeyException {

        JsonRequestBody jsonRequestBody = mapper.readValue(requestBody, JsonRequestBody.class);

        if (encryptionSwitch == 1) {

            if (!Encryption.validateHash(Encryption.HMAC_SHA256_ALGORITHM, code, requestBody, privilageAdmin)) {
                return new JsonResponseBody(null, Constants.FAILURE, ErrorCodes.INVALID_CLIENT_OR_HASH.getCode(), ErrorCodes.INVALID_CLIENT_OR_HASH.getDescription());
            }
        }

        Codes validationCode = UpdateUserValidation.validate(jsonRequestBody);
        if (validationCode.getCode() != ErrorCodes.VALID.getCode()) {
            return new JsonResponseBody(null, Constants.FAILURE, validationCode.getCode(), validationCode.getDescription());
        }

        String username = jsonRequestBody.getRequest().getUser().getUsername();
        String kycStatus = Constants.USER_PROFILE_INFO[0];

        com.oxigenwallet.userservice.common.json.request.User userDetails = jsonRequestBody.getRequest().getUser();
        if (userDetails.getName() == null || userDetails.getName().equals("") || userDetails.getLastName() == null || userDetails.getLastName().equals("") || userDetails.getDob() == null || userDetails.getDob().equals("")) {

            Map<String, String> redisResult = redisStorage.getUserDetails(userDetails.getUsername());
            if (redisResult.size() == 0 || redisResult.get("profile_type").equals("null")) {

                com.oxigenwallet.userservice.model.User userDatabase = userDao.getByMdn(userDetails.getUsername());
                if (userDatabase != null && userDatabase.getProfile_type() != null) {
                    redisStorage.saveUserDetails(userDatabase);
                    userDetails.setName(userDetails.getName() == null || userDetails.getName().equals("") ? userDatabase.getFirst_name() : userDetails.getName());
                    userDetails.setLastName(userDetails.getLastName() == null || userDetails.getLastName().equals("") ? userDatabase.getLast_name() : userDetails.getLastName());
                    userDetails.setDob(userDetails.getDob() == null || userDetails.getDob().equals("") ? userDatabase.getDob() : userDetails.getDob());
                    kycStatus = userDatabase.getProfile_type();
                } else {
                    String XmlRequestUserInfo = xmlGenerator.getGetUserInfoRequest(username, Constants.USER_INFO_API_TYPE[1]);

                    ResponseEntity<String> userInfoResponse = null;
                    JsonResponseBody getUserInfoJsonResponse = null;
                    userInfoResponse = restCalling.postNxtxn(XmlRequestUserInfo, getUserInfoUrl);
                    com.oxigenwallet.userservice.common.xml.model.responseXml.NxtxnInterface userResponseXmlObject = xmlGenerator.getGetUserInfoResponseXml(userInfoResponse.getBody(), Constants.USER_INFO_API_TYPE[1]);

                    int status = userResponseXmlObject.getB2C_Service_Interface().getService_Response().getResponse_Info().getHostCode();
                    String userStatus = userResponseXmlObject.getB2C_Service_Interface().getService_Response().getUser().getUser_Info().getStatus();
                    if (status == 0) {
                        getUserInfoJsonResponse = jsonGenerator.getGetUserInfoSuccessResponseJson(userResponseXmlObject, Constants.USER_INFO_API_TYPE[1]);
                        kycStatus = getUserInfoJsonResponse.getResponse().getUser().getProfile_type();
                        if (userDatabase == null) {
                            userDao.saveGetUserInfoResponse(getUserInfoJsonResponse, userStatus, Constants.USER_INFO_API_TYPE[1]);
                        } else {
                            userDao.updateGetUserInfoRequest(userDatabase, getUserInfoJsonResponse, userStatus, Constants.USER_INFO_API_TYPE[1]);
                        }
                        redisStorage.saveGetUserInfoResponse(getUserInfoJsonResponse, userStatus, Constants.USER_INFO_API_TYPE[1]);

                        String firstName = userResponseXmlObject.getB2C_Service_Interface().getService_Response().getUser().getUser_Info().getUser_Profile().getUser_PI().getFirst_Name();
                        String lastName = userResponseXmlObject.getB2C_Service_Interface().getService_Response().getUser().getUser_Info().getUser_Profile().getUser_PI().getLast_Name();
                        String dob = userResponseXmlObject.getB2C_Service_Interface().getService_Response().getUser().getUser_Info().getUser_Profile().getUser_PI().getDob();

                        userDetails.setName(userDetails.getName() == null || userDetails.getName().equals("") ? firstName : userDetails.getName());
                        userDetails.setLastName(userDetails.getLastName() == null || userDetails.getLastName().equals("") ? lastName : userDetails.getLastName());
                        userDetails.setDob(userDetails.getDob() == null || userDetails.getDob().equals("") ? dob : userDetails.getDob());

                    } else {
                        return new JsonResponseBody(null, Constants.FAILURE, ErrorCodes.INVALID_DETAILS.getCode(), ErrorCodes.INVALID_DETAILS.getDescription());
                    }
                }
            } else {
                userDetails.setName(userDetails.getName() == null || userDetails.getName().equals("") ? redisResult.get("first_name") : userDetails.getName());
                userDetails.setLastName(userDetails.getLastName() == null || userDetails.getLastName().equals("") ? redisResult.get("last_name") : userDetails.getLastName());
                userDetails.setDob(userDetails.getDob() == null || userDetails.getDob().equals("") ? redisResult.get("dob") : userDetails.getDob());
                kycStatus = redisResult.get("profile_type");
            }
        }
        //GETTING XML REQUEST STRING
        String xmlRequestBody = xmlGenerator.getUpdateUserRequestXml(jsonRequestBody, kycStatus);
        //MAKING REQUEST
        ResponseEntity<String> response = restCalling.postNxtxn(xmlRequestBody, updateUserUrl);
        //UNMARSHALING RESPONSE FROM TO XML OBJECTS
        com.oxigenwallet.userservice.common.xml.model.responseXml.NxtxnInterface responseXmlObject = xmlGenerator.getUpdateUserResponseXml(response.getBody());

        //GETTING SUCCESS / FAILURE RESPONSE
        int status = responseXmlObject.getB2C_Service_Interface().getService_Response().getResponse_Info().getHostCode();

        JsonResponseBody updateUserJsonResponse;
        if (status == 0) {
            userDao.updateUpdateUserRequest(jsonRequestBody, userStatusEnabled);
            redisStorage.saveUserDetails(jsonRequestBody, userStatusEnabled);

            LoginOtp loginEvent = new LoginOtp(new Data(username), new Event(Constants.NOTIFICATION_EVENT_ID[1], Constants.NOTIFICATION_EVENT_NAME[1], Constants.NOTIFICATION_BUSINESS, Constants.NOTIFICATION_CHANNEL));
            try {
                RabbitTemplate rabbitTemplate = messagingQueueFactory.getMessagingTemplate();
                rabbitTemplate.setExchange(updateUserExchange);
                rabbitTemplate.setRoutingKey(updateUserKey);
                Object rabbitMqResult = rabbitTemplate.convertSendAndReceive(mapper.writeValueAsString(loginEvent));
            } catch (Exception ex) {
                ex.printStackTrace();
            }
            updateUserJsonResponse = jsonGenerator.getSuccessResponseJson(responseXmlObject);
        } else {
            updateUserJsonResponse = jsonGenerator.getFailureResponseJson(responseXmlObject);
        }

        return updateUserJsonResponse;
    }

    @RequestMapping(
            value = {"/sessions"},
            method = RequestMethod.DELETE,
            produces = "application/json")
    public @ResponseBody
    JsonResponseBody tokenAllDelete(
            @RequestHeader(value = "code", required = true) String code,
            @RequestBody String request
    ) throws SignatureException, NoSuchAlgorithmException, InvalidKeyException, UnsupportedEncodingException, IOException {

        if (encryptionSwitch == 1) {

            if (!Encryption.validateHash(Encryption.HMAC_SHA256_ALGORITHM, code, request, privilageAdmin)) {
                return new JsonResponseBody(null, Constants.FAILURE, ErrorCodes.INVALID_CLIENT_OR_HASH.getCode(), ErrorCodes.INVALID_CLIENT_OR_HASH.getDescription());
            }
        }
        JsonRequestBody requestObj
                = mapper.readValue(request, JsonRequestBody.class);

        Codes error = validateUsername(requestObj.getUsername());
        if (error.getCode() != ErrorCodes.VALID.getCode()) {
            return new JsonResponseBody(null, Constants.FAILURE, error.getCode(), error.getDescription());
        }
        //redisStorage.deleteAllAccessToken(requestObj.getUsername());
        return new JsonResponseBody(null, Constants.SUCCESS, Constants.SUCCESSCODE, ResponseCodes.TOKEN_DELETED);

    }

//    @RequestMapping(
//            value = {"/sessions"},
//            method = RequestMethod.GET,
//            produces = "application/json")
//    public @ResponseBody
//    JsonResponseBody getTokenAll(
//            @RequestHeader(value = "code", required = true) String code,
//            @RequestParam("username") String username
//    ) throws SignatureException, NoSuchAlgorithmException, InvalidKeyException, UnsupportedEncodingException, IOException {
//
//        if (encryptionSwitch == 1) {
//
//            if (!Encryption.validateHash(Encryption.HMAC_SHA256_ALGORITHM, code, username, privilageAdmin)) {
//                return new JsonResponseBody(null, Constants.FAILURE, ErrorCodes.INVALID_CLIENT_OR_HASH.getCode(), ErrorCodes.INVALID_CLIENT_OR_HASH.getDescription());
//            }
//        }
//        ArrayList<RedisToken> allToken = redisStorage.getAllAccessToken(username);
//        ArrayList<Tokens> responseTokens = new ArrayList<Tokens>();
//
//        for (RedisToken redisToken : allToken) {
//            Tokens token = new Tokens();
//            token.setClientId(redisToken.getClientId());
//            token.setExpireIn(redisToken.getExpireIn());
//            token.setScope(redisToken.getScope());
//            token.setTimeStamp(redisToken.getTimeStamp());
//            token.setIpAddress(redisToken.getIp());
//            token.setDeviceId(redisToken.getDeviceId());
//            token.setDeviceOs(redisToken.getDeviceOs());
//            token.setDeviceOsVersion(redisToken.getDeviceVersion());
//            token.setModelName(redisToken.getModelName());
//
//            responseTokens.add(token);
//        }
//        JsonResponseBody response = new JsonResponseBody(null, Constants.SUCCESS, Constants.SUCCESSCODE, ResponseCodes.TOKEN_DELETED);
//        response.setTokens(responseTokens);
//        return response;
//    }
}
