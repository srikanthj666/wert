/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.oxigenwallet.userservice.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.oxigenwallet.userservice.common.Encryption;
import com.oxigenwallet.userservice.common.ExceptionHandling;
import com.oxigenwallet.userservice.common.GenerateToken;
import com.oxigenwallet.userservice.common.MessagingQueueFactory;
import com.oxigenwallet.userservice.common.RedisCacheResource;
import com.oxigenwallet.userservice.common.RedisStorage;
import com.oxigenwallet.userservice.common.RestCalling;
import com.oxigenwallet.userservice.common.constants.Codes;
import com.oxigenwallet.userservice.common.constants.Constants;
import com.oxigenwallet.userservice.common.constants.ErrorCodes;
import com.oxigenwallet.userservice.common.constants.ResponseCodes;
import com.oxigenwallet.userservice.common.xml.model.XmlGenerator;
import com.oxigenwallet.userservice.common.json.JsonGenerator;
import com.oxigenwallet.userservice.common.json.oxifaceJsonHelper;
import com.oxigenwallet.userservice.common.json.request.JsonRequestBody;
import com.oxigenwallet.userservice.common.json.request.notificationService.Data;
import com.oxigenwallet.userservice.common.json.request.notificationService.Event;
import com.oxigenwallet.userservice.common.json.request.notificationService.LoginOtp;
import com.oxigenwallet.userservice.common.json.request.oxiface.Request;
import com.oxigenwallet.userservice.common.json.response.JsonResponseBody;
import com.oxigenwallet.userservice.common.json.response.Response;
import com.oxigenwallet.userservice.common.validation.CreateUserValidation;
import com.oxigenwallet.userservice.common.validation.UpdatePasswordValidation;
import com.oxigenwallet.userservice.common.validation.UpdateUserValidation;

import static com.oxigenwallet.userservice.common.validation.Validation.validateUsername;

import com.oxigenwallet.userservice.common.xml.model.TokenXml;
import com.oxigenwallet.userservice.dao.TokenDao;
import com.oxigenwallet.userservice.dao.UserDao;
import com.oxigenwallet.userservice.dao.UserEventLogDao;
import com.oxigenwallet.userservice.model.NxtxnToken;
import com.oxigenwallet.userservice.model.Token;
import com.oxigenwallet.userservice.model.User;
import com.oxigenwallet.userservice.utils.RedisToken;
import com.oxigenwallet.userservice.utils.Util;

import java.io.IOException;
import java.io.StringReader;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.SignatureException;
import java.sql.Timestamp;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.xml.sax.SAXException;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Pattern;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.xml.sax.InputSource;
import redis.clients.jedis.Jedis;

/**
 * @author mitz
 */
@RestController
public class UserController extends ExceptionHandling {

    @Value("${nxtxnUrl.createUser}")
    private String createUserUrl;
    @Value("${nxtxnUrl.updateUser}")
    private String updateUserUrl;
    @Value("${nxtxnUrl.updatePassword}")
    private String updatePasswordUrl;
    @Value("${nxtxnUrl.getUserInfo}")
    private String getUserInfoUrl;
    @Value("${nxtxnUrl.getUserInfoLite}")
    private String getUserInfoLiteUrl;
    @Value("${user.status.disabled}")
    private int userStatusDisabled;
    @Value("${user.status.enabled}")
    private int userStatusEnabled;
    @Value("#{ @environment['encryption.switch']}")
    int encryptionSwitch;
    @Value("#{ @environment['authorization.switch']}")
    int authorizationSwitch;
    @Value("#{ @environment['nxtxnUrl.tokenManager']}")
    String tokenManagerNxtxnUrl;
    @Value("#{ @environment['notificationService.switch']}")
    int notificationServiceSwitch;
    @Value("#{ @environment['exchange.updateUSer']}")
    String updateUserExchange;

    @Value("#{ @environment['key.updateUser']}")
    String updateUserKey;
    @Value("#{ @environment['exchange.createUser']}")
    String createUserExchange;

    @Value("#{ @environment['key.createUser']}")
    String createUserKey;

    @Value("#{ @environment['accessTokenSalt']}")
    String accessTokenSalt;

    @Value("#{ @environment['user.getinfo']}")
    String privilageGetInfo;

    @Value("#{ @environment['user.updatepassword']}")
    String privilageUpdatePassword;

    @Value("#{ @environment['user.updateuser']}")
    String privilageUpdateUser;

    @Value("${sms.sendsmsurl}")
    private String sendSmsURL;
    //@Value("#{ @environment['rabbitmq.switch']}")
    //int rabbitmqSwitch;

    @Autowired
    private XmlGenerator xmlGenerator;
    @Autowired
    private JsonGenerator jsonGenerator;
    @Autowired
    private UserDao userDao;
    @Autowired
    private RedisStorage redisStorage;
    @Autowired
    private RestCalling restCalling;
    @Autowired
    private RedisCacheResource redisCacheResource;
    @Autowired
    private ObjectMapper mapper;
    @Autowired
    private oxifaceJsonHelper oxifaceRequestHelper;
    @Autowired
    TokenDao tokenDao;

    @Autowired
    UserEventLogDao userEventLogDao;

    @Autowired
    GenerateToken generateToken;
    @Autowired
    TokenXml tokenXml;
    @Autowired
    private MessagingQueueFactory messagingQueueFactory;
    @Autowired
    Util util;

    // Create user
    @RequestMapping(value = "/user", method = RequestMethod.PUT, produces = "application/json")
    public @ResponseBody
    JsonResponseBody createUser(@RequestBody String requestBody,
                                @RequestHeader("code") String code,
                                @RequestHeader("client_id") String clientId)
            throws IOException, ParserConfigurationException, SAXException, TransformerException, ParseException, org.json.simple.parser.ParseException, SignatureException, NoSuchAlgorithmException, InvalidKeyException {
        return this.createUserData(requestBody, code, clientId);
    }

    @RequestMapping(value = "/internal/user", method = RequestMethod.POST, produces = "application/json")
    public @ResponseBody
    JsonResponseBody internalCreateUser(@RequestBody String requestBody)
            throws IOException, ParserConfigurationException, SAXException, TransformerException, ParseException, org.json.simple.parser.ParseException, SignatureException, NoSuchAlgorithmException, InvalidKeyException {
        return this.createUserData(requestBody, "", "");
    }

    // Update userinfo
    @RequestMapping(value = "/user", method = RequestMethod.POST, produces = "application/json")
    public @ResponseBody
    JsonResponseBody updateUser(
            @RequestBody String requestBody,
            @RequestHeader("Authorisation") String authorization,
            @RequestHeader("code") String code,
            @RequestHeader("client_id") String clientId)
            throws IOException, ParserConfigurationException, SAXException, TransformerException, ParseException, org.json.simple.parser.ParseException, SignatureException, NoSuchAlgorithmException, InvalidKeyException {
        return this.udpateUserInfo(requestBody, clientId, code, authorization);
    }

    // Update userinfo without authentication
    @RequestMapping(value = "/internal/user", method = RequestMethod.PUT, produces = "application/json")
    public @ResponseBody
    JsonResponseBody internalUpdateUser(@RequestBody String requestBody)
            throws IOException, ParserConfigurationException, SAXException, TransformerException, ParseException, org.json.simple.parser.ParseException, SignatureException, NoSuchAlgorithmException, InvalidKeyException {
        return this.udpateUserInfo(requestBody, "", "", "");
    }

    // Get userinfo
    @RequestMapping(value = "/user", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    JsonResponseBody getUserInfo(
            @RequestParam("username") String username,
            @RequestParam("info") String info,
            @RequestHeader("Authorisation") String authorization,
            @RequestHeader("code") String code,
            @RequestHeader("client_id") String clientId)
            throws IOException, ParserConfigurationException, SAXException, TransformerException, ParseException, org.json.simple.parser.ParseException, SignatureException, NoSuchAlgorithmException, InvalidKeyException {
        return this.getUserRecords(username, info, "", "", "");
    }

    // get userinfo without authentication
    @RequestMapping(value = "/internal/user", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    JsonResponseBody internalGetUserInfo(@RequestParam("username") String username,
                                         @RequestParam("info") String info)
            throws IOException, ParserConfigurationException, SAXException, TransformerException, ParseException, org.json.simple.parser.ParseException, SignatureException, NoSuchAlgorithmException, InvalidKeyException {
        return this.getUserRecords(username, info, "", "", "");
    }

    // update user password
    @RequestMapping(value = "/update-password", method = RequestMethod.POST, consumes = "application/json", produces = "application/json")
    public @ResponseBody
    JsonResponseBody updatePassword(
            @RequestBody String requestBody,
            @RequestHeader("code") String code,
            @RequestHeader("client_id") String clientId,
            @RequestHeader(value = "Authorisation", required = false) String authorization) throws IOException, ParserConfigurationException, SAXException, TransformerException, ParseException, org.json.simple.parser.ParseException, SignatureException, NoSuchAlgorithmException, InvalidKeyException {

        JsonRequestBody jsonRequestBody = mapper.readValue(requestBody, JsonRequestBody.class);
        if (encryptionSwitch == 1) {
            Map<String, String> clientResult = redisStorage.getClient(clientId);
            String clientPassword = clientResult.get("secret");
            //String clientPassword = redisStorage.getClientPassword(clientId);
            if (!Encryption.validateHash(Encryption.HMAC_SHA256_ALGORITHM, code, requestBody, clientId + ":" + clientPassword)) {
                return new JsonResponseBody(null, Constants.FAILURE, ErrorCodes.INVALID_CLIENT_OR_HASH.getCode(), ErrorCodes.INVALID_CLIENT_OR_HASH.getDescription());
            }
        }

        //validation
        Codes validationCode = UpdatePasswordValidation.validate(jsonRequestBody);
        if (validationCode.getCode() != ErrorCodes.VALID.getCode()) {
            return new JsonResponseBody(null, Constants.FAILURE, validationCode.getCode(), validationCode.getDescription());

        }
        String username = jsonRequestBody.getRequest().getUser().getUsername();
        String type = jsonRequestBody.getRequest().getUser().getType();

        if (authorizationSwitch == 1 && type.equals(Constants.PASSWORD_UPDATE_TYPE[1])) {
            // finding token from storage
            boolean tokenRedisResult = redisStorage.isAccessTokenExist(username, util.encrypt(authorization));
            // reponse code if token in not found in redis and sql
            if (tokenRedisResult && !privilageUpdatePassword.equals(authorization)) {
                return new JsonResponseBody(null, Constants.FAILURE, ErrorCodes.INVALID_TOKEN.getCode(), ErrorCodes.INVALID_TOKEN.getDescription());
            }
        }
        //GETTING XML REQUEST STRING
        String xmlRequestBody = xmlGenerator.getUpdatePasswordXml(jsonRequestBody);
        //MAKING REQUEST
        ResponseEntity<String> response = restCalling.postNxtxn(xmlRequestBody, updatePasswordUrl);
        if (response.getBody() == null) {
            return new JsonResponseBody(null, Constants.FAILURE, ErrorCodes.NXTXN_RESPONSE_ERROR.getCode(), ErrorCodes.NXTXN_RESPONSE_ERROR.getDescription());
        }
        //UNMARSHALING RESPONSE FROM TO XML OBJECTS
        com.oxigenwallet.userservice.common.xml.model.responseXml.NxtxnInterface responseXmlObject = xmlGenerator.getUpdatePasswordResponseXml(response.getBody());
        //GETTING SUCCESS / FAILURE RESPONSE
        int status = responseXmlObject.getB2C_Service_Interface().getService_Response().getResponse_Info().getHostCode();

        JsonResponseBody updateUserJsonResponse;
        if (status == 0) {
            updateUserJsonResponse = jsonGenerator.getSuccessResponseJson(responseXmlObject);
        } else {
            updateUserJsonResponse = jsonGenerator.getFailureResponseJson(responseXmlObject);
        }

        return updateUserJsonResponse;
    }

    // Upgrade kyc users
    @RequestMapping(value = {"/upgrade-kyc"}, method = RequestMethod.POST, produces = "application/json")
    public @ResponseBody
    JsonResponseBody updateUserKyc(
            @RequestBody String requestBody,
            @RequestHeader("Authorisation") String authorization,
            @RequestHeader("code") String code,
            @RequestHeader("client_id") String clientId)
            throws IOException, ParserConfigurationException, SAXException, TransformerException, ParseException, org.json.simple.parser.ParseException, SignatureException, NoSuchAlgorithmException, InvalidKeyException {
        return this.upgradeKyc(requestBody, clientId, code, authorization);
    }

    // upgrade kyc users without authentication headers
    @RequestMapping(value = {"/internal/upgrade-kyc"}, method = RequestMethod.POST, produces = "application/json")
    public @ResponseBody
    JsonResponseBody internalUpdateUserKyc(@RequestBody String requestBody)
            throws IOException, ParserConfigurationException, SAXException, TransformerException, ParseException, org.json.simple.parser.ParseException, SignatureException, NoSuchAlgorithmException, InvalidKeyException {
        return this.upgradeKyc(requestBody, "", "", "");
    }

    public JsonResponseBody getTokenResponse(String userName, RedisToken redisToken, String referralCode, boolean isMerchant, String nxtxnToken) {
        Map<String, String> tokenMap = new HashMap<String, String>();
        tokenMap.put("token", util.encrypt(redisToken.getToken()));
        tokenMap.put("scope", redisToken.getScope());
        tokenMap.put("time_stamp", Util.getTimeStamp());
        tokenMap.put("expires_in", redisToken.getExpireIn());
        tokenMap.put("client_id", redisToken.getClientId());
        tokenMap.put("ip", redisToken.getIp());
        tokenMap.put("device_id", redisToken.getDeviceId());
        tokenMap.put("device_os", redisToken.getDeviceOs());
        tokenMap.put("device_version", redisToken.getDeviceVersion());
        tokenMap.put("model_name", redisToken.getModelName());

        System.out.println("token : " + util.encrypt(redisToken.getToken()));
        System.out.println("scope : " + redisToken.getScope());
        System.out.println("time_stamp : " + Util.getTimeStamp());
        System.out.println("expires_in : " + redisToken.getExpireIn());
        System.out.println("client_id : " + redisToken.getClientId());
        System.out.println("ip :" + redisToken.getIp());
        System.out.println("device_id :" + redisToken.getDeviceId());
        System.out.println("device_os :" + redisToken.getDeviceOs());
        System.out.println("device_version : " + redisToken.getDeviceVersion());
        System.out.println("model_name : " + redisToken.getModelName());
        System.out.println("redisToken.getExpireIn() : " + redisToken.getExpireIn());
        System.out.println("ParseInt : " + Integer.parseInt(redisToken.getExpireIn()));

        //redisStorage.saveAccessToken( userName, tokenMap, redisToken.getClientId(), Integer.parseInt( redisToken.getExpireIn() ) );

        Long currentTime = System.currentTimeMillis();
        Token token = new Token(userName, redisToken.getClientId(), util.encrypt(redisToken.getToken()), Constants.US_TOKEN_TYPE[1], true, new Timestamp(currentTime), new Timestamp(currentTime + Long.parseLong(redisToken.getExpireIn()) * 1000));
        tokenDao.save(token);

        com.oxigenwallet.userservice.common.json.response.TokenInfo tokenInfo = new com.oxigenwallet.userservice.common.json.response.TokenInfo(userName, redisToken.getToken(), redisToken.getExpireIn(), redisToken.getScope(), Constants.US_TOKEN_TYPE[1], addUserAtOxiface(userName, referralCode, isMerchant, nxtxnToken));
        Response responseInfo = new Response(null, tokenInfo, null, null, null, null);

        JsonResponseBody tokenResponse = new JsonResponseBody(responseInfo, Constants.SUCCESS, Constants.SUCCESSCODE, ResponseCodes.TOKEN_GENERATED);
        tokenResponse.setResponse(responseInfo);
        return tokenResponse;
    }

    // Registering user at oxiface as well becuase of syncing purpose
    private String addUserAtOxiface(String userName, String code, boolean isMerchant, String nxtxnToken) {
        String oxifaceToken = "";
        try {
            Request oxiRequest = oxifaceRequestHelper.getAddUserServicerequest(userName, code, nxtxnToken);
            if (isMerchant) {
                oxiRequest.getServiceRequest().getParams().setParam3("sms");
            }
            String oxifaceJsonRequest = mapper.writeValueAsString(oxiRequest);

            ResponseEntity<String> oxifaceJsonResponse = restCalling.postOxiface(oxifaceJsonRequest);
            String oxifaceStringResponse = oxifaceJsonResponse.getBody();

            com.oxigenwallet.userservice.common.json.response.oxiface.Response oxifaceResponse
                    = mapper.readValue(oxifaceStringResponse, com.oxigenwallet.userservice.common.json.response.oxiface.Response.class);
            oxifaceToken = oxifaceResponse.getServiceResponse().getResponseInfo().getKey() == null ? "" : oxifaceResponse.getServiceResponse().getResponseInfo().getKey();
        } catch (Exception ex) {

        }
        return oxifaceToken;
    }

    private String walletType(String username) throws ParserConfigurationException, SAXException, IOException {
        ResponseEntity<String> walletResponse = restCalling.postNxtxn(xmlGenerator.getWalletInfoRequest(username), createUserUrl);
        String walletresponseBody = walletResponse.getBody();

        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder;
        InputSource is;
        Document doc = null;
        System.out.println(walletResponse);
        System.out.println(walletresponseBody);
        builder = factory.newDocumentBuilder();
        is = new InputSource(new StringReader(walletresponseBody));

        doc = builder.parse(is);

        Node accountInfo = doc.getElementsByTagName("Account_info").item(0);
        String accountType = accountInfo.getAttributes().getNamedItem("type").getNodeValue();
        return accountType;
    }

    public JsonResponseBody createUserData(String requestBody, String code, String clientId)
            throws IOException, ParserConfigurationException, SAXException, TransformerException, ParseException, org.json.simple.parser.ParseException, SignatureException, NoSuchAlgorithmException, InvalidKeyException {
        Map<String, String> clientResult = redisStorage.getClient(clientId);

        JsonRequestBody jsonRequestBody = mapper.readValue(requestBody, JsonRequestBody.class);

        if (encryptionSwitch == 1) {
            String clientPassword = clientResult.get("secret");
            if (!Encryption.validateHash(Encryption.HMAC_SHA256_ALGORITHM, code, requestBody, clientId + ":" + clientPassword)) {
                return new JsonResponseBody(null, Constants.FAILURE, ErrorCodes.INVALID_CLIENT_OR_HASH.getCode(), ErrorCodes.INVALID_CLIENT_OR_HASH.getDescription());
            }
        }
        String scope = clientResult.get("scope");
        String expiresIn = clientResult.get("expire_in");
        String tokenStatus = clientResult.get("token_status") == null ? "true" : clientResult.get("token_status");

        Codes validationCode = CreateUserValidation.validate(jsonRequestBody);
        if (validationCode.getCode() != ErrorCodes.VALID.getCode()) {
            return new JsonResponseBody(null, Constants.FAILURE, validationCode.getCode(), validationCode.getDescription());
        }

        //RETRIEVING OTP AND PASSWORD
        String otp = jsonRequestBody.getRequest().getUser().getOtp();
        String password = jsonRequestBody.getRequest().getUser().getPassword();
        String username = jsonRequestBody.getRequest().getUser().getUsername();

        if (password == null) {
            password = Util.getPassword(6) + "";
        }
        String ip = "";
        String deviceId = "";
        String deviceOs = "";
        String deviceOsVersion = "";
        String modelName = "";

        if (jsonRequestBody.getRequest().getDeviceInfo() != null) {
            ip = jsonRequestBody.getRequest().getDeviceInfo().getIpAddress() == null ? "" : jsonRequestBody.getRequest().getDeviceInfo().getIpAddress();
            deviceId = jsonRequestBody.getRequest().getDeviceInfo().getDeviceId() == null ? "" : jsonRequestBody.getRequest().getDeviceInfo().getDeviceId();
            deviceOs = jsonRequestBody.getRequest().getDeviceInfo().getDeviceOs() == null ? "" : jsonRequestBody.getRequest().getDeviceInfo().getDeviceOs();
            deviceOsVersion = jsonRequestBody.getRequest().getDeviceInfo().getDeviceOsVersion() == null ? "" : jsonRequestBody.getRequest().getDeviceInfo().getDeviceOsVersion();
            modelName = jsonRequestBody.getRequest().getDeviceInfo().getModelName() == null ? "" : jsonRequestBody.getRequest().getDeviceInfo().getModelName();
        }


        String type_id = "";
//        //Validation for document type id
        if (jsonRequestBody.getRequest().getDocument_info() == null) {
            return new JsonResponseBody(null, Constants.FAILURE, ErrorCodes.TYPE_ID_RESPONSE_ERROR.getCode(), ErrorCodes.TYPE_ID_RESPONSE_ERROR.getDescription());
        }
        if (jsonRequestBody.getRequest().getDocument_info() != null) {
            type_id = jsonRequestBody.getRequest().getDocument_info().get(0).getType_id();
            if (type_id == null || type_id.equals("")) {
                return new JsonResponseBody(null, Constants.FAILURE, ErrorCodes.TYPE_ID_RESPONSE_ERROR.getCode(), ErrorCodes.TYPE_ID_RESPONSE_ERROR.getDescription());
            }
        }

//        com.oxigenwallet.userservice.common.json.request.User user = jsonRequestBody.getRequest().getUser();
//        com.oxigenwallet.userservice.model.User userDatabase = userDao.getByMdn(user.getUsername());
//        if (userDatabase != null) {
//            return new JsonResponseBody(null, Constants.FAILURE, ErrorCodes., ErrorCodes.NXTXN_RESPONSE_ERROR.getDescription());
//        }

        //GETTING XML REQUEST STRING
        String xmlRequestBody = xmlGenerator.getCreateUserRequestXml(jsonRequestBody);

        ResponseEntity<String> response = restCalling.postNxtxn(xmlRequestBody, createUserUrl);
        if (response.getBody() == null) {
            return new JsonResponseBody(null, Constants.FAILURE, ErrorCodes.NXTXN_RESPONSE_ERROR.getCode(), ErrorCodes.NXTXN_RESPONSE_ERROR.getDescription());
        }

        //UNMARSHALING RESPONSE FROM TO XML OBJECTS
        com.oxigenwallet.userservice.common.xml.model.responseXml.NxtxnInterface responseXmlObject = xmlGenerator.getCreateUserResponseXml(response.getBody());
        //GETTING SUCCESS / FAILURE RESPONSE
        int status = responseXmlObject.getB2C_Service_Interface().getService_Response().getResponse_Info().getHostCode();
        //CREATING JSON BODY , SAVING TO MYSQL AND REDIS IF SUCCESS REQUEST
        JsonResponseBody createUserJsonResponse;
        System.out.println("Status in Nxtxn : " + status);
        if (status == 0) {
            createUserJsonResponse = jsonGenerator.getSuccessResponseJson(responseXmlObject);
            userDao.saveCreateUserRequest(jsonRequestBody, userStatusDisabled);
            //redisStorage.saveUserDetails( jsonRequestBody, userStatusDisabled );
            System.out.println("jsonRequestBody : " + jsonRequestBody);
            //CREATING NEW UPDATE USER REQUEST
            String xmlUpdateUserRequestBody = xmlGenerator.getUpdateUserRequestXml(jsonRequestBody, otp, password);
            //MAKING UPDATE REQUEST
            ResponseEntity<String> responseUpdateUser = restCalling.postNxtxn(xmlUpdateUserRequestBody, updateUserUrl);
            if (response.getBody() == null) {
                return new JsonResponseBody(null, Constants.FAILURE, ErrorCodes.NXTXN_RESPONSE_ERROR.getCode(), ErrorCodes.NXTXN_RESPONSE_ERROR.getDescription());
            }
            //UNMARSHALING RESPONSE FROM TO XML OBJECTS
            com.oxigenwallet.userservice.common.xml.model.responseXml.NxtxnInterface responseXmlObjectUpdateUser = xmlGenerator.getUpdateUserResponseXml(responseUpdateUser.getBody());
            //GETTING SUCCESS / FAILURE RESPONSE
            int statusUpdateUser = responseXmlObjectUpdateUser.getB2C_Service_Interface().getService_Response().getResponse_Info().getHostCode();

            System.out.println("statusUpdateUser : " + statusUpdateUser);

            //SAVING TO MYSQL AND CACHE
            if (statusUpdateUser == 0) {

                System.out.println("Saving in DB..");

                userDao.updateUpdateUserRequest(jsonRequestBody, userStatusEnabled);
                //redisStorage.saveUserDetails( jsonRequestBody, userStatusEnabled );
                /* token manager create token */
                String xmlRequest = tokenXml.tokenManagerXml(Constants.CREATE_USER_OPERATOR, username, Constants.TOKEN_NXTXN_OPERATION[1]);
                ResponseEntity<String> xmlResponse = restCalling.postNxtxn(xmlRequest, tokenManagerNxtxnUrl);
                if (response.getBody() == null) {
                    return new JsonResponseBody(null, Constants.FAILURE, ErrorCodes.NXTXN_RESPONSE_ERROR.getCode(), ErrorCodes.NXTXN_RESPONSE_ERROR.getDescription());
                }

                String tokenManagerresponseBody = xmlResponse.getBody();
                DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
                DocumentBuilder builder;
                InputSource is;
                Document doc = null;

                builder = factory.newDocumentBuilder();
                is = new InputSource(new StringReader(tokenManagerresponseBody));
                doc = builder.parse(is);
                Node HostCode = doc.getElementsByTagName("HostCode").item(0);
                String hostCode = HostCode.getTextContent();
                String nxtxnToken = "";
                if (hostCode.equals(Constants.NXTXN_SUCCESS_CODE)) {
                    Node Data = doc.getElementsByTagName("Data").item(0);
                    nxtxnToken = Data.getTextContent();
                }

//                LoginOtp loginEvent = new LoginOtp(new Data(username), new Event(Constants.NOTIFICATION_EVENT_ID[2], Constants.NOTIFICATION_EVENT_NAME[2], Constants.NOTIFICATION_BUSINESS, Constants.NOTIFICATION_CHANNEL));
//                try {
//                    RabbitTemplate rabbitTemplate = messagingQueueFactory.getMessagingTemplate();
//                    rabbitTemplate.setExchange(createUserExchange);
//                    rabbitTemplate.setRoutingKey(createUserKey);
//                    Object rabbitMqResult = rabbitTemplate.convertSendAndReceive(mapper.writeValueAsString(loginEvent));
//                } catch (Exception ex) {
//                    ex.printStackTrace();
//                }

                if (!sendSmsURL.equals("")) {
                    try {
                        String content = "Congratulations!!! You have successfully Registered at Oxigen Wallet.";
                        Timestamp timestamp = new Timestamp(System.currentTimeMillis());
                        String postData = "{\"RequestId\":\"" + timestamp.getTime() + "\",\"Mobile\":\"" + username + "\",\"Content\":\"" + content + "\"}";
                        RestCalling restCalling = new RestCalling();
                        restCalling.postSMS(postData, sendSmsURL);
                    } catch (Exception ex) {
                        System.out.println(ex.getMessage());
                    }
                }

                if (tokenStatus.equalsIgnoreCase("true")) {
//                    String accessToken = generateToken.getToken();
                    String accessToken = generateToken.getToken(username);
                    String userName = jsonRequestBody.getRequest().getUser().getUsername();
                    String referralCode = jsonRequestBody.getRequest().getUser().getReferralCode();
                    boolean isMerchant = jsonRequestBody.getRequest().getUser().getIsMerchant();

                    RedisToken redisToken = new RedisToken(accessToken, clientId, expiresIn, scope, ip, deviceId, deviceOs, deviceOsVersion, modelName);
                    return getTokenResponse(userName, redisToken, referralCode, isMerchant, nxtxnToken);
                }
            } else {
                return jsonGenerator.getFailureResponseJson(responseXmlObjectUpdateUser);
            }
        } else {
            createUserJsonResponse = jsonGenerator.getFailureResponseJson(responseXmlObject);
        }
        return createUserJsonResponse;
    }

    // making nil-kyc to semi-kyc for only display purpose
    public JsonResponseBody set_Semikyc_fn(JsonResponseBody jsonResponseBody, String infoType) {

        if ((jsonResponseBody.getResponse().getUser().getProfile_type() == null) || (jsonResponseBody.getResponse().getUser().getDocument() == null)) {

            return jsonResponseBody;
        }

        String profileType = jsonResponseBody.getResponse().getUser().getProfile_type();
        String document = jsonResponseBody.getResponse().getUser().getDocument();

        if (infoType.equals("lite") || infoType.equals("full")) {

            if (profileType.equalsIgnoreCase("nil_kyc") && document.equalsIgnoreCase("1")) {
                jsonResponseBody.getResponse().getUser().setProfile_type("semi_kyc");
            } else {
                jsonResponseBody.getResponse().getUser().getProfile_type();
            }
        }

        return jsonResponseBody;
    }

    // dateformat setting here
    public JsonResponseBody set_CreatedDate_fn(JsonResponseBody jsonResponseBody) {

        com.oxigenwallet.userservice.model.User userM = new User();
        com.oxigenwallet.userservice.common.json.response.User userJ = new com.oxigenwallet.userservice.common.json.response.User();
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");

        if (jsonResponseBody.getResponse().getUser().getDateCreated() == null) {
            return jsonResponseBody;
        }

        String inDate = jsonResponseBody.getResponse().getUser().getDateCreated();
        if (Pattern.matches(Constants.DATE_PATTERN, inDate)) {
            Date createdDate = null;
            try {
                createdDate = dateFormat.parse(inDate);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            jsonResponseBody.getResponse().getUser().setDateCreated(String.valueOf(new Timestamp(createdDate.getTime())));

        } else {
            jsonResponseBody.getResponse().getUser().setDateCreated(inDate);

        }

        return jsonResponseBody;
    }

    // updateuserinfo commonly created because of optional header parameters
    public JsonResponseBody udpateUserInfo(String requestBody, String clientId, String code, String authorization)
            throws IOException, ParserConfigurationException, SAXException, TransformerException, ParseException, org.json.simple.parser.ParseException, SignatureException, NoSuchAlgorithmException, InvalidKeyException {

        JsonRequestBody jsonRequestBody = mapper.readValue(requestBody, JsonRequestBody.class);

        if (!code.equalsIgnoreCase("") && !clientId.equalsIgnoreCase("")) {
            if (encryptionSwitch == 1) {
                Map<String, String> clientResult = redisStorage.getClient(clientId);
                String clientPassword = clientResult.get("secret");
                //String clientPassword = redisStorage.getClientPassword(clientId);
                if (!Encryption.validateHash(Encryption.HMAC_SHA256_ALGORITHM, code, requestBody, clientId + ":" + clientPassword)) {
                    return new JsonResponseBody(null, Constants.FAILURE, ErrorCodes.INVALID_CLIENT_OR_HASH.getCode(), ErrorCodes.INVALID_CLIENT_OR_HASH.getDescription());
                }
            }
        }

        Codes validationCode = UpdateUserValidation.validate(jsonRequestBody);
        if (validationCode.getCode() != ErrorCodes.VALID.getCode()) {
            return new JsonResponseBody(null, Constants.FAILURE, validationCode.getCode(), validationCode.getDescription());
        }

        String username = jsonRequestBody.getRequest().getUser().getUsername();
        if (!authorization.equalsIgnoreCase("")) {
            if (authorizationSwitch == 1) {
                // finding token from storage
                boolean tokenRedisResult = redisStorage.isAccessTokenExist(username, util.encrypt(authorization));
                // reponse code if token in not found in redis and sql
                if (tokenRedisResult && !privilageUpdateUser.equals(authorization)) {
                    return new JsonResponseBody(null, Constants.FAILURE, ErrorCodes.INVALID_TOKEN.getCode(), ErrorCodes.INVALID_TOKEN.getDescription());
                }
            }
        }

        //String kycStatus = Constants.USER_PROFILE_INFO[0];
        com.oxigenwallet.userservice.common.json.request.User userDetails = jsonRequestBody.getRequest().getUser();
        String kycStatus = userDetails.getProfile_type();

        if (userDetails.getName() == null || userDetails.getName().equals("")
                || userDetails.getLastName() == null || userDetails.getLastName().equals("")
                || userDetails.getDob() == null || userDetails.getDob().equals("")
                || userDetails.getAddress() == null || userDetails.getAddress().equals("")
                || userDetails.getCity() == null || userDetails.getCity().equals("")
                || userDetails.getDistrict() == null || userDetails.getDistrict().equals("")
                || userDetails.getState() == null || userDetails.getState().equals("")
                || userDetails.getCountry() == null || userDetails.getCountry().equals("")) {

            Map<String, String> redisResult = redisStorage.getUserDetails(userDetails.getUsername());
            if (redisResult.isEmpty() || redisResult.get("profile_type") == null) {

                com.oxigenwallet.userservice.model.User userDatabase = userDao.getByMdn(userDetails.getUsername());
                if (userDatabase != null && userDatabase.getProfile_type() != null) {
                    redisStorage.saveUserDetails(userDatabase);
                    userDetails.setName(userDetails.getName() == null || userDetails.getName().equals("") ? userDatabase.getFirst_name() : userDetails.getName());
                    userDetails.setLastName(userDetails.getLastName() == null || userDetails.getLastName().equals("") ? userDatabase.getLast_name() : userDetails.getLastName());
                    userDetails.setDob(userDetails.getDob() == null || userDetails.getDob().equals("") ? userDatabase.getDob() : userDetails.getDob());

                    userDetails.setAddress(userDetails.getAddress() == null || userDetails.getAddress().equals("") ? userDatabase.getAddress() : userDetails.getAddress());
                    userDetails.setCity(userDetails.getCity() == null || userDetails.getCity().equals("") ? userDatabase.getCity() : userDetails.getCity());
                    userDetails.setDistrict(userDetails.getDistrict() == null || userDetails.getDistrict().equals("") ? userDatabase.getDistrict() : userDetails.getDistrict());
                    userDetails.setState(userDetails.getState() == null || userDetails.getState().equals("") ? userDatabase.getState() : userDetails.getState());
                    userDetails.setCountry(userDetails.getCountry() == null || userDetails.getCountry().equals("") ? userDatabase.getCountry() : userDetails.getCountry());

                    kycStatus = userDatabase.getProfile_type();
                } else {
                    String XmlRequestUserInfo = xmlGenerator.getGetUserInfoRequest(username, Constants.USER_INFO_API_TYPE[1]);

                    ResponseEntity<String> userInfoResponse = null;
                    JsonResponseBody getUserInfoJsonResponse = null;
                    userInfoResponse = restCalling.postNxtxn(XmlRequestUserInfo, getUserInfoUrl);
                    System.out.println("userInfoResponse===..." + userInfoResponse.getBody());

                    if (userInfoResponse.getBody() == null) {
                        return new JsonResponseBody(null, Constants.FAILURE, ErrorCodes.NXTXN_RESPONSE_ERROR.getCode(), ErrorCodes.NXTXN_RESPONSE_ERROR.getDescription());
                    }
                    com.oxigenwallet.userservice.common.xml.model.responseXml.NxtxnInterface userResponseXmlObject = xmlGenerator.getGetUserInfoResponseXml(userInfoResponse.getBody(), Constants.USER_INFO_API_TYPE[1]);

                    int status = userResponseXmlObject.getB2C_Service_Interface().getService_Response().getResponse_Info().getHostCode();
                    String userStatus = userResponseXmlObject.getB2C_Service_Interface().getService_Response().getUser().getUser_Info().getStatus();
                    if (status == 0) {
                        getUserInfoJsonResponse = jsonGenerator.getGetUserInfoSuccessResponseJson(userResponseXmlObject, Constants.USER_INFO_API_TYPE[1]);
                        kycStatus = getUserInfoJsonResponse.getResponse().getUser().getProfile_type();
                        if (userDatabase == null) {
                            userDao.saveGetUserInfoResponse(getUserInfoJsonResponse, userStatus, Constants.USER_INFO_API_TYPE[1]);
                        } else {
                            userDao.updateGetUserInfoRequest(userDatabase, getUserInfoJsonResponse, userStatus, Constants.USER_INFO_API_TYPE[1]);
                        }
                        redisStorage.saveGetUserInfoResponse(getUserInfoJsonResponse, userStatus, Constants.USER_INFO_API_TYPE[1]);

                        String firstName = userResponseXmlObject.getB2C_Service_Interface().getService_Response().getUser().getUser_Info().getUser_Profile().getUser_PI().getFirst_Name();
                        String lastName = userResponseXmlObject.getB2C_Service_Interface().getService_Response().getUser().getUser_Info().getUser_Profile().getUser_PI().getLast_Name();
                        String dob = userResponseXmlObject.getB2C_Service_Interface().getService_Response().getUser().getUser_Info().getUser_Profile().getUser_PI().getDob();

                        String address = userResponseXmlObject.getB2C_Service_Interface().getService_Response().getUser().getUser_Info().getUser_Profile().getUser_PA().getAddress();
                        String city = userResponseXmlObject.getB2C_Service_Interface().getService_Response().getUser().getUser_Info().getUser_Profile().getUser_PA().getCity();
                        String district = userResponseXmlObject.getB2C_Service_Interface().getService_Response().getUser().getUser_Info().getUser_Profile().getUser_PA().getDistrict();
                        String state = userResponseXmlObject.getB2C_Service_Interface().getService_Response().getUser().getUser_Info().getUser_Profile().getUser_PA().getState();
                        String country = userResponseXmlObject.getB2C_Service_Interface().getService_Response().getUser().getUser_Info().getUser_Profile().getUser_PA().getCountry();

                        userDetails.setName(userDetails.getName() == null || userDetails.getName().equals("") ? firstName : userDetails.getName());
                        userDetails.setLastName(userDetails.getLastName() == null || userDetails.getLastName().equals("") ? lastName : userDetails.getLastName());
                        userDetails.setDob(userDetails.getDob() == null || userDetails.getDob().equals("") ? dob : userDetails.getDob());

                        userDetails.setAddress(userDetails.getAddress() == null || userDetails.getAddress().equals("") ? address : userDetails.getAddress());
                        userDetails.setCity(userDetails.getCity() == null || userDetails.getCity().equals("") ? city : userDetails.getCity());
                        userDetails.setDistrict(userDetails.getDistrict() == null || userDetails.getDistrict().equals("") ? district : userDetails.getDistrict());
                        userDetails.setState(userDetails.getState() == null || userDetails.getState().equals("") ? state : userDetails.getState());
                        userDetails.setCountry(userDetails.getCountry() == null || userDetails.getCountry().equals("") ? country : userDetails.getCountry());

                    } else {
                        return new JsonResponseBody(null, Constants.FAILURE, ErrorCodes.INVALID_DETAILS.getCode(), ErrorCodes.INVALID_DETAILS.getDescription());
                    }
                }
            } else {
                userDetails.setName(userDetails.getName() == null || userDetails.getName().equals("") ? redisResult.get("first_name") : userDetails.getName());
                userDetails.setLastName(userDetails.getLastName() == null || userDetails.getLastName().equals("") ? redisResult.get("last_name") : userDetails.getLastName());
                userDetails.setDob(userDetails.getDob() == null || userDetails.getDob().equals("") ? redisResult.get("dob") : userDetails.getDob());

                userDetails.setAddress(userDetails.getAddress() == null || userDetails.getAddress().equals("") ? redisResult.get("address") : userDetails.getAddress());
                userDetails.setCity(userDetails.getCity() == null || userDetails.getCity().equals("") ? redisResult.get("city") : userDetails.getCity());
                userDetails.setDistrict(userDetails.getDistrict() == null || userDetails.getDistrict().equals("") ? redisResult.get("district") : userDetails.getDistrict());
                userDetails.setState(userDetails.getState() == null || userDetails.getState().equals("") ? redisResult.get("state") : userDetails.getState());
                userDetails.setCountry(userDetails.getCountry() == null || userDetails.getCountry().equals("") ? redisResult.get("country") : userDetails.getCountry());

                kycStatus = redisResult.get("profile_type");
            }
        }
        System.out.println(username);
        System.out.println(walletType(username));
        //GETTING XML REQUEST STRING
        if (walletType(username).equals(Constants.USER_PROFILE_INFO[2])) {
            kycStatus = Constants.USER_PROFILE_INFO[2];
        }
        // added for update user new

        ResponseEntity<String> userInfoResponse1 = null;

        if (Constants.USER_INFO_API_TYPE[1] == "full") {
            String XmlRequestUserInfo1 = xmlGenerator.getGetUserInfoRequest(username, Constants.USER_INFO_API_TYPE[1]);
            JsonResponseBody getUserInfoJsonResponse1 = null;
            userInfoResponse1 = restCalling.postNxtxn(XmlRequestUserInfo1, getUserInfoUrl);
            System.out.println("userInfoResponse===..." + userInfoResponse1.getBody());
        } else if (Constants.USER_INFO_API_TYPE[0] == "lite") {
            String XmlRequestUserInfo1 = xmlGenerator.getGetUserInfoRequest(username, Constants.USER_INFO_API_TYPE[0]);
            JsonResponseBody getUserInfoJsonResponse1 = null;
            userInfoResponse1 = restCalling.postNxtxn(XmlRequestUserInfo1, getUserInfoLiteUrl);
            System.out.println("userInfoResponse===..." + userInfoResponse1.getBody());

        }
        if (userInfoResponse1.getBody() == null) {
            return new JsonResponseBody(null, Constants.FAILURE, ErrorCodes.NXTXN_RESPONSE_ERROR.getCode(), ErrorCodes.NXTXN_RESPONSE_ERROR.getDescription());
        }

        com.oxigenwallet.userservice.common.xml.model.responseXml.NxtxnInterface userResponseXmlObject1 = xmlGenerator.getGetUserInfoResponseXml(userInfoResponse1.getBody(), Constants.USER_INFO_API_TYPE[1]);

        String req_firstname = jsonRequestBody.getRequest().getUser().getName();
        String req_middlename = jsonRequestBody.getRequest().getUser().getMiddleName();

        String req_lastname = jsonRequestBody.getRequest().getUser().getLastName();

        System.out.println("userResponseXmlObject..." + userResponseXmlObject1);
        String pan_verified = userResponseXmlObject1.getB2C_Service_Interface().getService_Response().getUser().getUser_Info().getUser_Profile().getUser_PC().getPan_Verified();
        String first_nameold = userResponseXmlObject1.getB2C_Service_Interface().getService_Response().getUser().getUser_Info().getUser_Profile().getUser_PI().getFirst_Name();
        String middle_nameold = userResponseXmlObject1.getB2C_Service_Interface().getService_Response().getUser().getUser_Info().getUser_Profile().getUser_PI().getMiddle_Name();
        String last_nameold = userResponseXmlObject1.getB2C_Service_Interface().getService_Response().getUser().getUser_Info().getUser_Profile().getUser_PI().getLast_Name();

        System.out.println("pan_verified_test....====" + pan_verified);
        JsonResponseBody updateUserJsonResponse = null;

        if (pan_verified != null) {
            if (pan_verified.equals("1")) {
                if ((req_firstname != first_nameold) || (req_middlename != middle_nameold) || (req_lastname != last_nameold)) {
                    return new JsonResponseBody(null, Constants.FAILURE, ErrorCodes.USER_PAN_PRESENT_RESPONSE_ERROR.getCode(), ErrorCodes.USER_PAN_PRESENT_RESPONSE_ERROR.getDescription());
                }
            }
        }

        String xmlRequestBody = xmlGenerator.getUpdateUserRequestXml(jsonRequestBody, kycStatus);
        //MAKING REQUEST
        ResponseEntity<String> response = restCalling.postNxtxn(xmlRequestBody, updateUserUrl);
        if (response.getBody() == null) {
            return new JsonResponseBody(null, Constants.FAILURE, ErrorCodes.NXTXN_RESPONSE_ERROR.getCode(), ErrorCodes.NXTXN_RESPONSE_ERROR.getDescription());
        }
        //UNMARSHALING RESPONSE FROM TO XML OBJECTS
        com.oxigenwallet.userservice.common.xml.model.responseXml.NxtxnInterface responseXmlObject = xmlGenerator.getUpdateUserResponseXml(response.getBody());

        //GETTING SUCCESS / FAILURE RESPONSE
        int status = responseXmlObject.getB2C_Service_Interface().getService_Response().getResponse_Info().getHostCode();

        //JsonResponseBody updateUserJsonResponse;
        if (status == 0) {

            System.out.println(username);
            System.out.println("==== value==" + userEventLogDao);
            User user = userDao.getByMdn(username);
            if (user != null) {
                userEventLogDao.SaveUserAuditLogs(user);
            }

            //userEventLogDao.SaveUserAuditLogs( userDao.getByMdn( username ) );
            userDao.updateUpdateUserRequest(jsonRequestBody, userStatusEnabled);
//            redisStorage.saveUserDetails(jsonRequestBody, userStatusEnabled);

            updateUserJsonResponse = jsonGenerator.getSuccessResponseJson(responseXmlObject);
            updateUserJsonResponse.setResponseDescription(ResponseCodes.ACCOUNT_CREATED_UPDATED);
        } else {
            updateUserJsonResponse = jsonGenerator.getFailureResponseJson(responseXmlObject);
        }

        return updateUserJsonResponse;
    }

    // getuserinfo commonly created because of optional header parameters
    public JsonResponseBody getUserRecords(String username, String info, String code, String clientId, String authorization)
            throws IOException, ParserConfigurationException, SAXException, TransformerException, ParseException, org.json.simple.parser.ParseException, SignatureException, NoSuchAlgorithmException, InvalidKeyException {

        if (!code.equalsIgnoreCase("") && !clientId.equalsIgnoreCase("")) {
            if (encryptionSwitch == 1) {
                Map<String, String> clientResult = redisStorage.getClient(clientId);
                String clientPassword = clientResult.get("secret");
                //String clientPassword = redisStorage.getClientPassword(clientId);
                if (!Encryption.validateHash(Encryption.HMAC_SHA256_ALGORITHM, code, username + ":" + info, clientId + ":" + clientPassword)) {
                    return new JsonResponseBody(null, Constants.FAILURE, ErrorCodes.INVALID_CLIENT_OR_HASH.getCode(), ErrorCodes.INVALID_CLIENT_OR_HASH.getDescription());
                }
            }
        }

        if (!authorization.equalsIgnoreCase("")) {
            if (authorizationSwitch == 1) {
                // finding token from storage
                boolean tokenRedisResult = redisStorage.isAccessTokenExist(username, util.encrypt(authorization));
                // reponse code if token in not found in redis and sql
                if (tokenRedisResult && !privilageGetInfo.equals(authorization)) {
                    return new JsonResponseBody(null, Constants.FAILURE, ErrorCodes.INVALID_TOKEN.getCode(), ErrorCodes.INVALID_TOKEN.getDescription());
                }
            }
        }

        System.out.println("Step1 : " + username);
        Codes validationCode = validateUsername(username);
        if (validationCode.getCode() != ErrorCodes.VALID.getCode()) {
            JsonResponseBody validationResponse = new JsonResponseBody(null, Constants.FAILURE, validationCode.getCode(), validationCode.getDescription());
            return validationResponse;
        }
        JsonResponseBody jsonResponseBody = null;
        ObjectMapper objectMapper = new ObjectMapper();

        //checking in redis
//        Jedis jedisObject = redisCacheResource.getCacheResource().getResource();
//        String key = redisStorage.GenerateKey( username );
//        System.out.println("STEP2: "+ "Going to redis.");
//        if (jedisObject.exists( key )) {
//            if (info.equals( jedisObject.hget( key, "info_type" ) ) || info.equals( "lite" )) {
//                jedisObject.close();
//               // jsonResponseBody = this.set_Semikyc_fn(jsonGenerator.getJsonFromRedis( username, info ), info );
//                jsonResponseBody = jsonGenerator.getJsonFromRedis( username, info );
//                jsonResponseBody = this.set_Semikyc_fn(jsonResponseBody, info );
//                System.out.println("STEP3: "+ "Key Exists in Redis.");
//                System.out.println("STEP3: "+ mapper.writeValueAsString(jsonResponseBody));
//                return jsonResponseBody;
//            }
//        }
//        jedisObject.close();
        //checking in mysql
        com.oxigenwallet.userservice.model.User userM = userDao.getByMdn(username);
        System.out.println("STEP4: " + "Going to DB.");
        if (userM != null) {
            if (info.equals(userM.getInfo_type()) || info.equals("lite")) {
                //redisStorage.saveUserDetails( userM );
                //jsonResponseBody = this.set_Semikyc_fn( jsonGenerator.getJsonFromDbObject( userM, info ), info );
                jsonResponseBody = jsonGenerator.getJsonFromDbObject(userM, info);
                jsonResponseBody = this.set_Semikyc_fn(jsonResponseBody, info);
                System.out.println("STEP5: " + "Record exists in DB.");
                System.out.println("STEP5: " + objectMapper.writeValueAsString(jsonResponseBody));
                return jsonResponseBody;
            }
        }
        String xmlRequestBody = xmlGenerator.getGetUserInfoRequest(username, info);

        ResponseEntity<String> response = null;
        JsonResponseBody getUserInfoJsonResponse = null;

        System.out.println("STEP 6: " + "API Calling.");
        if (info.equals(Constants.USER_INFO_API_TYPE[1])) {
            response = restCalling.postNxtxn(xmlRequestBody, getUserInfoUrl);
        } else if (info.equals(Constants.USER_INFO_API_TYPE[0])) {
            response = restCalling.postNxtxn(xmlRequestBody, getUserInfoLiteUrl);
        }
        if (response.getBody() == null) {
            return new JsonResponseBody(null, Constants.FAILURE, ErrorCodes.NXTXN_RESPONSE_ERROR.getCode(), ErrorCodes.NXTXN_RESPONSE_ERROR.getDescription());
        }
        com.oxigenwallet.userservice.common.xml.model.responseXml.NxtxnInterface responseXmlObject = xmlGenerator.getGetUserInfoResponseXml(response.getBody(), info);
        int status = responseXmlObject.getB2C_Service_Interface().getService_Response().getResponse_Info().getHostCode();
        String userStatus = responseXmlObject.getB2C_Service_Interface().getService_Response().getUser().getUser_Info().getStatus();
        if (status == 0) {
            getUserInfoJsonResponse = jsonGenerator.getGetUserInfoSuccessResponseJson(responseXmlObject, info);
            if (userM != null) {
                userDao.updateGetUserInfoRequest(userM, getUserInfoJsonResponse, userStatus, info);
            } else {
                userDao.saveGetUserInfoResponse(getUserInfoJsonResponse, userStatus, info);
            }
            //redisStorage.saveGetUserInfoResponse( getUserInfoJsonResponse, userStatus, info );
            getUserInfoJsonResponse = this.set_Semikyc_fn(getUserInfoJsonResponse, info);
            getUserInfoJsonResponse = this.set_CreatedDate_fn(getUserInfoJsonResponse);

        } else {
            getUserInfoJsonResponse = jsonGenerator.getGetUserInfoFailureResponseJson(responseXmlObject, 0);  // 0 failure for getuserinfo
        }

        System.out.println("STEP 6: " + objectMapper.writeValueAsString(getUserInfoJsonResponse));
        return getUserInfoJsonResponse;
    }

    // upgrade-kyc where convert the nil-kyc user to kyc and commonly created because of optional parameters
    public JsonResponseBody upgradeKyc(String requestBody, String clientId, String code, String authorization)
            throws IOException, ParserConfigurationException, SAXException, TransformerException, ParseException, org.json.simple.parser.ParseException, SignatureException, NoSuchAlgorithmException, InvalidKeyException {

        JsonRequestBody jsonRequestBody = mapper.readValue(requestBody, JsonRequestBody.class);

        if (!code.equalsIgnoreCase("") && !clientId.equalsIgnoreCase("")) {
            if (encryptionSwitch == 1) {
                Map<String, String> clientResult = redisStorage.getClient(clientId);
                String clientPassword = clientResult.get("secret");
                //String clientPassword = redisStorage.getClientPassword(clientId);
                if (!Encryption.validateHash(Encryption.HMAC_SHA256_ALGORITHM, code, requestBody, clientId + ":" + clientPassword)) {
                    System.out.println("ErrorCodes.INVALID_TOKEN.getDescription() : " + ErrorCodes.INVALID_CLIENT_OR_HASH.getDescription());
                    return new JsonResponseBody(null, Constants.FAILURE, ErrorCodes.INVALID_CLIENT_OR_HASH.getCode(), ErrorCodes.INVALID_CLIENT_OR_HASH.getDescription());
                }
            }
        }

//        Codes validationCode = UpdateUserValidation.validate(jsonRequestBody);
//        if (validationCode.getCode() != ErrorCodes.VALID.getCode()) {
//            return new JsonResponseBody(null, Constants.FAILURE, validationCode.getCode(), validationCode.getDescription());
//        }
        String username = jsonRequestBody.getRequest().getUser().getUsername();
        if (!authorization.equalsIgnoreCase("")) {
            if (authorizationSwitch == 1) {
                // finding token from storage
                boolean tokenRedisResult = redisStorage.isAccessTokenExist(username, util.encrypt(authorization));
                // reponse code if token in not found in redis and sql
                if (tokenRedisResult) {
                    System.out.println("ErrorCodes.INVALID_TOKEN.getDescription() : " + ErrorCodes.INVALID_TOKEN.getDescription());
                    return new JsonResponseBody(null, Constants.FAILURE, ErrorCodes.INVALID_TOKEN.getCode(), ErrorCodes.INVALID_TOKEN.getDescription());
                }
            }
        }

        String xmlRequestBody = xmlGenerator.getUpgradeKycRequestXml(jsonRequestBody);
        //MAKING REQUEST
        ResponseEntity<String> response = restCalling.postNxtxn(xmlRequestBody, updateUserUrl);

        com.oxigenwallet.userservice.common.xml.model.responseXml.NxtxnInterface responseXmlObject = xmlGenerator.getUpdateUserResponseXml(response.getBody());

        //GETTING SUCCESS / FAILURE RESPONSE
        int status = responseXmlObject.getB2C_Service_Interface().getService_Response().getResponse_Info().getHostCode();
        JsonResponseBody updateUserJsonResponse;
        if (status == 0) {
            System.out.println("==== value==" + userEventLogDao);
            User user = userDao.getByMdn(username);
            if (user != null) {
                userEventLogDao.SaveUserAuditLogs(user);
                // return new JsonResponseBody(null, Constants.FAILURE, ErrorCodes.USER_RESPONSE_ERROR.getCode(), ErrorCodes.USER_RESPONSE_ERROR.getDescription());
            }
            //  userEventLogDao.SaveUserAuditLogs( user);
            //userEventLogDao.SaveUserAuditLogs( userDao.getByMdn( username ) );
            // userEventLogDao.SaveUserAuditLogs( userDao.getByMdn( username ) );
            userDao.updateUpdateUserRequestKyc(jsonRequestBody, userStatusEnabled);
//            redisStorage.saveUserDetailsKyc(jsonRequestBody, userStatusEnabled);
//            LoginOtp loginEvent = new LoginOtp(new Data(username), new Event(Constants.NOTIFICATION_EVENT_ID[1], Constants.NOTIFICATION_EVENT_NAME[1], Constants.NOTIFICATION_BUSINESS, Constants.NOTIFICATION_CHANNEL));
//            //if (rabbitmqSwitch == 1) {
//            try {
//                RabbitTemplate rabbitTemplate = messagingQueueFactory.getMessagingTemplate();
//                rabbitTemplate.setExchange(updateUserExchange);
//                rabbitTemplate.setRoutingKey(updateUserKey);
//                Object rabbitMqResult = rabbitTemplate.convertSendAndReceive(mapper.writeValueAsString(loginEvent));
//            } catch (Exception ex) {
//                ex.printStackTrace();
//            }
            //}

            if (!sendSmsURL.equals("")) {
                try {
                    String content = "Congratulations!!! You have successfully Registered at Oxigen Wallet.";
                    Timestamp timestamp = new Timestamp(System.currentTimeMillis());
                    String postData = "{\"RequestId\":\"" + timestamp.getTime() + "\",\"Mobile\":\"" + username + "\",\"Content\":\"" + content + "\"}";
                    RestCalling restCalling = new RestCalling();
                    restCalling.postSMS(postData, sendSmsURL);
                } catch (Exception ex) {
                    System.out.println(ex.getMessage());
                }
            }

            updateUserJsonResponse = jsonGenerator.getSuccessResponseJson(responseXmlObject);
        } else {
            updateUserJsonResponse = jsonGenerator.getFailureResponseJson(responseXmlObject);
        }
        return updateUserJsonResponse;
    }
}
