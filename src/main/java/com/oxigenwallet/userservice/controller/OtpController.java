/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.oxigenwallet.userservice.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.oxigenwallet.userservice.common.CustomException;
import com.oxigenwallet.userservice.common.Encryption;
import com.oxigenwallet.userservice.common.ExceptionHandling;
import com.oxigenwallet.userservice.common.GenerateToken;
import com.oxigenwallet.userservice.common.MessagingQueueFactory;
import com.oxigenwallet.userservice.common.RedisStorage;
import com.oxigenwallet.userservice.common.RestCalling;
import com.oxigenwallet.userservice.common.constants.Codes;
import com.oxigenwallet.userservice.common.json.JsonGenerator;
import com.oxigenwallet.userservice.common.json.request.JsonRequestBody;
import com.oxigenwallet.userservice.common.json.response.JsonResponseBody;
import com.oxigenwallet.userservice.common.xml.model.XmlGenerator;
import com.oxigenwallet.userservice.common.constants.Constants;
import com.oxigenwallet.userservice.common.constants.ErrorCodes;
import com.oxigenwallet.userservice.common.constants.ResponseCodes;
import com.oxigenwallet.userservice.common.json.request.notificationService.Data;
import com.oxigenwallet.userservice.common.json.request.notificationService.Event;
import com.oxigenwallet.userservice.common.json.request.notificationService.LoginOtp;
import com.oxigenwallet.userservice.common.json.response.Response;
import com.oxigenwallet.userservice.common.notification.Vediocon;
import com.oxigenwallet.userservice.common.validation.GenerateOtpValidation;
import com.oxigenwallet.userservice.dao.OtpAuthorizationDao;
import com.oxigenwallet.userservice.dao.TokenDao;
import com.oxigenwallet.userservice.model.OtpAuthorization;
import com.oxigenwallet.userservice.utils.Util;
import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.SignatureException;
import java.sql.Timestamp;
import java.text.ParseException;
import java.util.HashMap;
import java.util.Map;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.xml.sax.SAXException;

/**
 *
 * @author mitz
 */
@RestController
public class OtpController extends ExceptionHandling {

    @Value("${nxtxnUrl.createAndSendOtp}")
    private String createAndSendOtpUrl;
    @Value("${nxtxnUrl.resetCustomerLpin}")
    private String resetCustomerLpinUrl;
    @Value("${nxtxnUrl.getUserInfoLite}")
    private String getUserInfoLiteUrl;
    @Value("#{ @environment['nxtxnUrl.verifyUser']}")
    String verifUseryNxtxnUrl;
    @Value("#{ @environment['notificationService.switch']}")
    int notificationServiceSwitch;

    @Autowired
    private XmlGenerator xmlGenerator;
    @Autowired
    private JsonGenerator jsonGenerator;
    @Autowired
    private RestCalling restCalling;
    @Autowired
    private RedisStorage redisStorage;
    @Autowired
    private ObjectMapper mapper;
    @Autowired
    private Vediocon vediocon;
    @Autowired
    GenerateToken generateToken;
    @Autowired
    TokenDao tokenDao;
    @Autowired
    OtpAuthorizationDao otpAuthorizationDao;
    @Autowired
    private MessagingQueueFactory messagingQueueFactory;
    @Autowired
    Util util;

    @Value("#{ @environment['encryption.switch']}")
    int encryptionSwitch;
    //@Value("#{ @environment['rabbitmq.switch']}")
    //int rabbitmqSwitch;
    @Value("#{ @environment['notificationService.exchange']}")
    String otpExchange;

    @Value("#{ @environment['key.login.otp']}")
    String otpKey;

    @Value("#{ @environment['accessTokenSalt']}")
    String accessTokenSalt;

    /**
     *
     * @param requestBody
     * @param code
     * @param clientId
     * @return
     * @throws IOException
     * @throws ParserConfigurationException
     * @throws SAXException
     * @throws TransformerException
     * @throws ParseException
     * @throws org.json.simple.parser.ParseException
     * @throws SignatureException
     * @throws NoSuchAlgorithmException
     * @throws InvalidKeyException
     */
    @RequestMapping(value = "/generate-otp", method = RequestMethod.POST, consumes = "application/json", produces = "application/json")
    public @ResponseBody
    JsonResponseBody generateOtp(
            @RequestBody String requestBody,
            @RequestHeader("code") String code,
            @RequestHeader("client_id") String clientId)
            throws IOException, ParserConfigurationException, SAXException, TransformerException, ParseException, org.json.simple.parser.ParseException, SignatureException, NoSuchAlgorithmException, InvalidKeyException, CustomException {

        JsonRequestBody jsonRequestBody = mapper.readValue(requestBody, JsonRequestBody.class);
        if (encryptionSwitch == 1) {
            Map<String, String> clientResult = redisStorage.getClient(clientId);
            String clientPassword = clientResult.get("secret");
            //String clientPassword = redisStorage.getClientPassword(clientId);
            if (!Encryption.validateHash(Encryption.HMAC_SHA256_ALGORITHM, code, requestBody, clientId + ":" + clientPassword)) {
                return new JsonResponseBody(null, Constants.FAILURE, ErrorCodes.INVALID_CLIENT_OR_HASH.getCode(), ErrorCodes.INVALID_CLIENT_OR_HASH.getDescription());
            }
        }

        Codes validationCode = GenerateOtpValidation.validate(jsonRequestBody);
        if (validationCode.getCode() != ErrorCodes.VALID.getCode()) {
            return new JsonResponseBody(null, Constants.FAILURE, validationCode.getCode(), validationCode.getDescription());
        }
        String username = jsonRequestBody.getRequest().getUser().getUsername();

        String type = jsonRequestBody.getRequest().getUser().getType();
        String xmlRequestBody = null;
        int status;
        JsonResponseBody generateOtpResponse = null;
        ResponseEntity<String> response = null;
        ResponseEntity<String> responseGetUserInfo = null;
        com.oxigenwallet.userservice.common.xml.model.responseXml.NxtxnInterface responseXmlObject = null;
        com.oxigenwallet.userservice.common.xml.model.responseXml.NxtxnInterface responseXmlObjectGetUserInfo = null;

        //CALLING create and send otp
        if (type.equals(Constants.GENERATEOTP_API[0])) {
            String userName = jsonRequestBody.getRequest().getUser().getUsername();
            //checking if user already exists? calling getuserinfo lite api
            String xmlGetUserInfoRequest = xmlGenerator.getGetUserInfoRequest(userName, Constants.USER_INFO_API_TYPE[0]);
            responseGetUserInfo = restCalling.postNxtxn(xmlGetUserInfoRequest, getUserInfoLiteUrl);
            responseXmlObjectGetUserInfo = xmlGenerator.getGetUserInfoResponseXml(responseGetUserInfo.getBody(), Constants.USER_INFO_API_TYPE[0]);
            int getUserInfoStatus = responseXmlObjectGetUserInfo.getB2C_Service_Interface().getService_Response().getResponse_Info().getHostCode();
            //display error message if error code doesn't correspond to "no record found"
            //30 => no record found
            //38 => user already exists
            //00 => success
            if (getUserInfoStatus != 30) {
                return jsonGenerator.getGetUserInfoFailureResponseJson(responseXmlObjectGetUserInfo, 1); // 1 => generate otp for registration failure
            }
            xmlRequestBody = xmlGenerator.getCreateAndSendRequestXml(jsonRequestBody);
            response = restCalling.postNxtxn(xmlRequestBody, createAndSendOtpUrl);
            responseXmlObject = xmlGenerator.getCreateAndSendResponseXml(response.getBody());
            status = responseXmlObject.getB2C_Service_Interface().getService_Response().getResponse_Info().getHostCode();
            if (status == 0) {
                generateOtpResponse = jsonGenerator.getSuccessResponseJson(responseXmlObject);
                generateOtpResponse.setResponseDescription(ResponseCodes.OTP_SENTSUCCESSFULLY + username);
            } else {
                generateOtpResponse = jsonGenerator.getFailureResponseJson(responseXmlObject);
            }
        } else if (type.equals(Constants.GENERATEOTP_API[1])) {
            String userName = jsonRequestBody.getRequest().getUser().getUsername();
            xmlRequestBody = xmlGenerator.getResetCustomerLpinRequestXml(jsonRequestBody);
            response = restCalling.postNxtxn(xmlRequestBody, resetCustomerLpinUrl);
            responseXmlObject = xmlGenerator.getResetCustomerLpinResponseXml(response.getBody());
            status = responseXmlObject.getNxTxN_System_Interface().getSystemService_Response().getResponse_Info().getHostCode();
            if (status == 0) {
                generateOtpResponse = jsonGenerator.getResetCustomerLpinSuccessResponseJson(responseXmlObject, userName);
                generateOtpResponse.setResponseDescription(ResponseCodes.OTP_SENTSUCCESSFULLY + username);
            } else {
                generateOtpResponse = jsonGenerator.getResetCustomerLpinFailureResponseJson(responseXmlObject);
            }
        } else if (type.equals(Constants.GENERATEOTP_API[2])) {

            HashMap<String, String> otpResult = (HashMap<String, String>) redisStorage.getOtp(username);
            if (otpResult != null && Integer.parseInt(otpResult.get("count")) >= 3) {
                return new JsonResponseBody(null, Constants.FAILURE, ErrorCodes.TOO_MANY_OTP_ATTEMPTS.getCode(), ErrorCodes.TOO_MANY_OTP_ATTEMPTS.getDescription());
            }
            int otp = 0;
            int count = 0;
            if (otpResult == null) {
                otp = vediocon.createOtp(Constants.LOGIN_OTP_LENGTH);
            } else {
                otp = Integer.parseInt(otpResult.get("otp"));
                count = Integer.parseInt(otpResult.get("count")) + 1;
            }

            OtpAuthorization otpAuth = new OtpAuthorization(username, Constants.OTP_TYPE[2], otp, clientId, true, new Timestamp(new java.util.Date().getTime()), null);
            otpAuthorizationDao.save(otpAuth);

            LoginOtp loginEvent = new LoginOtp(new Data(username, otp), new Event(Constants.NOTIFICATION_EVENT_ID[0], Constants.NOTIFICATION_EVENT_NAME[0], Constants.NOTIFICATION_BUSINESS, Constants.NOTIFICATION_CHANNEL));
            try {
                RabbitTemplate rabbitTemplate = messagingQueueFactory.getMessagingTemplate();
                rabbitTemplate.setExchange(otpExchange);
                rabbitTemplate.setRoutingKey(otpKey);

                Object rabbitMqResult = rabbitTemplate.convertSendAndReceive(mapper.writeValueAsString(loginEvent));
            } catch (Exception ex) {
                ex.printStackTrace();
            }

//            redisStorage.delPreviousOtp(username);
            redisStorage.saveOtp(username, otp, count);
            if (notificationServiceSwitch == 0) {
                vediocon.sendOtp(otp, username);
            }

            com.oxigenwallet.userservice.common.json.response.User userInfo
                    = new com.oxigenwallet.userservice.common.json.response.User();
            userInfo.setUsername(username);

            Response responseInfo = new Response(userInfo, null, null, null, null, null);

            generateOtpResponse = new JsonResponseBody();
            generateOtpResponse.setResponseCode(Constants.SUCCESSCODE);
            generateOtpResponse.setResponseDescription(ResponseCodes.OTP_SENTSUCCESSFULLY + username);
            generateOtpResponse.setStatus(Constants.SUCCESS);
            generateOtpResponse.setResponse(responseInfo);

        } else {
            generateOtpResponse = new JsonResponseBody(null, Constants.FAILURE, ErrorCodes.INVALID_OTP_TYPE.getCode(), ErrorCodes.INVALID_OTP_TYPE.getDescription());
        }

        return generateOtpResponse;
    }
}
