/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.oxigenwallet.userservice.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.oxigenwallet.userservice.common.CustomException;
import com.oxigenwallet.userservice.common.GenerateToken;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import com.oxigenwallet.userservice.common.Encryption;
import com.oxigenwallet.userservice.common.ExceptionHandling;
import com.oxigenwallet.userservice.common.MessagingQueueFactory;
import com.oxigenwallet.userservice.common.RedisStorage;
import com.oxigenwallet.userservice.common.RestCalling;
import com.oxigenwallet.userservice.common.constants.Codes;
import com.oxigenwallet.userservice.common.constants.Constants;
import com.oxigenwallet.userservice.common.constants.ErrorCodes;
import com.oxigenwallet.userservice.common.constants.ResponseCodes;
import com.oxigenwallet.userservice.common.json.oxifaceJsonHelper;
import com.oxigenwallet.userservice.dao.TokenDao;
import com.oxigenwallet.userservice.model.Token;

import java.io.StringReader;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.xml.sax.InputSource;
import com.oxigenwallet.userservice.common.json.request.*;
import com.oxigenwallet.userservice.common.json.request.notificationService.Data;
import com.oxigenwallet.userservice.common.json.request.notificationService.Event;
import com.oxigenwallet.userservice.common.json.request.notificationService.LoginOtp;
import com.oxigenwallet.userservice.common.json.response.*;
import com.oxigenwallet.userservice.common.notification.Vediocon;
import com.oxigenwallet.userservice.common.validation.TokenValidation;
import com.oxigenwallet.userservice.common.validation.Validation;
import com.oxigenwallet.userservice.common.xml.model.TokenXml;
import com.oxigenwallet.userservice.common.xml.model.XmlGenerator;
import com.oxigenwallet.userservice.dao.ClientDao;
import com.oxigenwallet.userservice.dao.NxtxnTokenDao;
import com.oxigenwallet.userservice.dao.OtpAuthorizationDao;
import com.oxigenwallet.userservice.dao.UserDao;
import com.oxigenwallet.userservice.model.NxtxnToken;
import com.oxigenwallet.userservice.model.OtpAuthorization;
import com.oxigenwallet.userservice.utils.RedisToken;
import com.oxigenwallet.userservice.utils.Util;

import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.SignatureException;
import java.sql.Timestamp;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import javax.xml.parsers.ParserConfigurationException;

import org.h2.engine.DbObject;
import org.springframework.beans.factory.annotation.Value;
import org.xml.sax.SAXException;

import org.springframework.amqp.rabbit.core.RabbitTemplate;

/**
 * @author sagarsharma
 */
@RestController
public class TokenController extends ExceptionHandling {

    @Autowired
    RedisStorage redisStorage;

    @Autowired
    GenerateToken generateToken;

    @Autowired
    TokenDao tokenDao;

    @Autowired
    NxtxnTokenDao nxtxnTokenDao;

    @Autowired
    ObjectMapper mapper;

    @Autowired
    Encryption encryption;

    @Autowired
    TokenXml tokenXml;

    @Autowired
    private XmlGenerator xmlGenerator;

    @Autowired
    private RestCalling restCalling;

    @Autowired
    private oxifaceJsonHelper oxifaceRequestHelper;

    @Autowired
    private Vediocon vediocon;

    @Autowired
    private MessagingQueueFactory messagingQueueFactory;

    @Autowired
    OtpAuthorizationDao otpAuthorizationDao;

    @Autowired
    ClientDao clientDao;

    @Autowired
    Util util;

    @Autowired
    UserDao userDao;

    @Value("#{ @environment['nxtxnUrl.verifyUser']}")
    String verifUseryNxtxnUrl;

    @Value("#{ @environment['nxtxnUrl.updatefraudstatus']}")
    String updateFraudStatusUrl;

    @Value("#{ @environment['nxtxnUrl.tokenManager']}")
    String tokenManagerNxtxnUrl;

    //@Value("#{ @environment['rabbitmq.switch']}")
    //int rabbitmqSwitch;

    @Value("#{ @environment['encryption.switch']}")
    int encryptionSwitch;

    @Value("#{ @environment['notificationService.switch']}")
    int notificationServiceSwitch;

    @Value("#{ @environment['notificationService.exchange']}")
    String otpExchange;

    @Value("#{ @environment['key.login.otp']}")
    String otpKey;

    @Value("#{ @environment['deleteall.token']}")
    String deleteAllToken;

    @Value("#{ @environment['accessTokenSalt']}")
    String accessTokenSalt;

    @Value("${nxtxnUrl.getUserInfoLite}")
    private String getUserInfoLiteUrl;

    @Value("${token.deviceBinding}")
    private int isDeviceBinding;

    @Value("${sms.sendsmsurl}")
    private String sendSmsURL;
    /*
    @Autowired
    OAuth2Authentication oath;
     */

    /**
     * @param tokenRequest tokenRequest
     * @param code         code
     * @param clientId     clientId
     * @return json
     * @throws IOException                  Exception
     * @throws SignatureException           Exception
     * @throws NoSuchAlgorithmException     Exception
     * @throws InvalidKeyException          Exception
     * @throws SAXException                 Exception
     * @throws ParserConfigurationException Exception
     * @throws CustomException              Exception
     */
    @RequestMapping(
            value = {"/token"},
            method = RequestMethod.POST,
            produces = "application/json")
    public @ResponseBody
    JsonResponseBody token(
            @RequestBody String tokenRequest,
            @RequestHeader(value = "code", required = true) String code,
            @RequestHeader(value = "client_id", required = true) String clientId
    ) throws IOException, SignatureException, NoSuchAlgorithmException, InvalidKeyException, SAXException, ParserConfigurationException, CustomException {

        Map<String, String> clientResult = redisStorage.getClient(clientId);
//        System.out.println("clientId " + clientId);
//        System.out.println("clientResult " + clientResult);

        System.out.println("Code: " + code);
        System.out.println("ClientId: " + clientId);
        JsonRequestBody request
                = mapper.readValue(tokenRequest, JsonRequestBody.class);
        if (encryptionSwitch == 1) {
            String clientPassword = clientResult.get("secret");
            if (!Encryption.validateHash(Encryption.HMAC_SHA256_ALGORITHM, code, tokenRequest, clientId + ":" + clientPassword)) {
                return new JsonResponseBody(null, Constants.FAILURE, ErrorCodes.INVALID_CLIENT_OR_HASH.getCode(), ErrorCodes.INVALID_CLIENT_OR_HASH.getDescription());
            }
        }

        Codes error = TokenValidation.validate(request);
        // request validation
        if (error.getCode() != ErrorCodes.VALID.getCode()) {
            return new JsonResponseBody(null, Constants.FAILURE, error.getCode(), error.getDescription());
        }
        String grantType = request.getRequest().getTokeninfo().getGrantType();
        String userName = request.getRequest().getTokeninfo().getUsername();
        String scope = request.getRequest().getTokeninfo().getScope();
        String expiresIn = clientResult.get("expire_in");
        String ip = "";
        String deviceId = "";
        String deviceOs = "";
        String deviceOsVersion = "";
        String modelName = "";
        DeviceInfo deviceInfo = request.getRequest().getDeviceInfo();

        if (request.getRequest().getDeviceInfo() != null) {
            ip = request.getRequest().getDeviceInfo().getIpAddress() == null ? "" : request.getRequest().getDeviceInfo().getIpAddress();
            deviceId = request.getRequest().getDeviceInfo().getDeviceId() == null ? "" : request.getRequest().getDeviceInfo().getDeviceId();
            deviceOs = request.getRequest().getDeviceInfo().getDeviceOs() == null ? "" : request.getRequest().getDeviceInfo().getDeviceOs();
            deviceOsVersion = request.getRequest().getDeviceInfo().getDeviceOsVersion() == null ? "" : request.getRequest().getDeviceInfo().getDeviceOsVersion();
            modelName = request.getRequest().getDeviceInfo().getModelName() == null ? "" : request.getRequest().getDeviceInfo().getModelName();
        }
        System.out.println("STEP1: OTP");
        if (grantType.equalsIgnoreCase(Constants.GRANT_TYPE[0])) {

            String authCode = request.getRequest().getTokeninfo().getAuthCode();
            String otp = request.getRequest().getTokeninfo().getOtp();

//            if (!redisStorage.getAuthCode(userName, util.encrypt(authCode), clientId) || !redisStorage.getOtp(userName, otp)) {
//                return new JsonResponseBody(null, Constants.FAILURE, ErrorCodes.INVALID_AUTH_TOKEN_OR_OTP.getCode(), ErrorCodes.INVALID_AUTH_TOKEN_OR_OTP.getDescription());
//            }
            System.out.println("AuthCode: " + authCode);
            if (redisStorage.getAuthCode(userName, util.encrypt(authCode), clientId, deviceInfo)) {

                if (!redisStorage.getOtp(userName, otp)) {
                    Token authCodeDb = tokenDao.getDeviceToken(util.encrypt(authCode), deviceInfo);
                    if (authCodeDb == null) {
                        return new JsonResponseBody(null, Constants.FAILURE, ErrorCodes.INVALID_AUTH_TOKEN_OR_OTP.getCode(), ErrorCodes.INVALID_AUTH_TOKEN_OR_OTP.getDescription());
                    }

                    if (authCodeDb.getFail_count() >= Constants.MAX_OTP_ATTEMPTS) {
                        // block user code
                        String updateFraudStatusRequest = xmlGenerator.getUpdateFraudStatusRequestXml(request);
                        System.out.println("UpdateFraudStatus :" + updateFraudStatusUrl);
                        System.out.println("UpdateFraudStatus Request :" + updateFraudStatusRequest);
                        try {
                            ResponseEntity<String> response = restCalling.postNxtxn(updateFraudStatusRequest, updateFraudStatusUrl);
                            if (response.getBody() == null) {
                                return new JsonResponseBody(null, Constants.FAILURE, ErrorCodes.NXTXN_RESPONSE_ERROR.getCode(), ErrorCodes.NXTXN_RESPONSE_ERROR.getDescription());
                            }
                            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
                            DocumentBuilder builder = factory.newDocumentBuilder();
                            InputSource is = new InputSource(new StringReader(response.getBody()));
                            Document doc = builder.parse(is);
                            Node HostCode = doc.getElementsByTagName("HostCode").item(0);
                            String hostCode = HostCode.getTextContent();
                            if (hostCode.equals(Constants.NXTXN_SUCCESS_CODE)) {
                                //Remove Key
                                redisStorage.delAuthCode(userName, util.encrypt(authCode), clientId);
                                return new JsonResponseBody(null, Constants.FAILURE, ErrorCodes.ACCOUNT_LOCKED_INVALID_TOKEN.getCode(), ErrorCodes.ACCOUNT_LOCKED_INVALID_TOKEN.getDescription());
                            }
                        } catch (Exception ex) {
                            //Log Exception
                            Logger logger = Logger.getLogger("debugLogger");
                            logger.info(ex.getStackTrace());
                        }
                        //Remove OTP & AuthCode From Redis
                        redisStorage.delAuthCode(userName, util.encrypt(authCode), clientId);
                        //Return Invalid OTP Response if failed to Locked User
                        return new JsonResponseBody(null, Constants.FAILURE, ErrorCodes.INVALID_AUTH_TOKEN_OR_OTP.getCode(), ErrorCodes.INVALID_AUTH_TOKEN_OR_OTP.getDescription());
                    }

                    authCodeDb.setFail_count(authCodeDb.getFail_count() + 1);
                    tokenDao.update(authCodeDb);
                    return new JsonResponseBody(null, Constants.FAILURE, ErrorCodes.INVALID_AUTH_TOKEN_OR_OTP.getCode(), ErrorCodes.INVALID_AUTH_TOKEN_OR_OTP.getDescription());
                }

            } else {
                com.oxigenwallet.userservice.common.xml.model.responseXml.NxtxnInterface responseXmlObjectGetUserInfo = null;
//                //Check isAccount Locked in Nxtxn
//                //IF Yes, Return Account Locked Exception
                String xmlGetUserInfoRequest = xmlGenerator.getGetUserInfoRequest(userName, Constants.USER_INFO_API_TYPE[0]);
                ResponseEntity<String> responseGetUserInfo = restCalling.postNxtxn(xmlGetUserInfoRequest, getUserInfoLiteUrl);
                if (responseGetUserInfo.getBody() == null) {
                    return new JsonResponseBody(null, Constants.FAILURE, ErrorCodes.NXTXN_RESPONSE_ERROR.getCode(), ErrorCodes.NXTXN_RESPONSE_ERROR.getDescription());
                }
                responseXmlObjectGetUserInfo = xmlGenerator.getGetUserInfoResponseXml(responseGetUserInfo.getBody(), Constants.USER_INFO_API_TYPE[0]);
                String UserLockStatus = responseXmlObjectGetUserInfo.getB2C_Service_Interface().getService_Response().getUser().getUser_Info().getStatus();

                if (Constants.STATUS[0].equalsIgnoreCase(UserLockStatus)) {
                    return new JsonResponseBody(null, Constants.FAILURE, ErrorCodes.ACCOUNT_LOCKED_INVALID_TOKEN.getCode(), ErrorCodes.ACCOUNT_LOCKED_INVALID_TOKEN.getDescription());
                }
                //ELSE Return Invalid OTP response
                return new JsonResponseBody(null, Constants.FAILURE, ErrorCodes.INVALID_AUTH_TOKEN_OR_OTP.getCode(), ErrorCodes.INVALID_AUTH_TOKEN_OR_OTP.getDescription());
            }
            // delete auth code and otp after use
            redisStorage.delAuthCode(userName, util.encrypt(authCode), clientId);
            redisStorage.delOtp(userName, otp);

//            String accessToken = generateToken.getToken();
            String accessToken = generateToken.getToken(userName);
            RedisToken redisToken = new RedisToken(accessToken, clientId, expiresIn, scope, ip, deviceId, deviceOs, deviceOsVersion, modelName);
            return getTokenResponse(userName, redisToken, Constants.US_TOKEN_TYPE[1]);

        } else if (grantType.equalsIgnoreCase(Constants.GRANT_TYPE[1])) {

            String xmlRequestBody = xmlGenerator.getVerifyUserRequestXml(request);

            ResponseEntity<String> xmlResponse = restCalling.postNxtxn(xmlRequestBody, verifUseryNxtxnUrl);
            if (xmlResponse.getBody() == null) {
                return new JsonResponseBody(null, Constants.FAILURE, ErrorCodes.NXTXN_RESPONSE_ERROR.getCode(), ErrorCodes.NXTXN_RESPONSE_ERROR.getDescription());
            }

            String responseBody = xmlResponse.getBody();

            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder;
            InputSource is;
            Document doc = null;

            builder = factory.newDocumentBuilder();
            is = new InputSource(new StringReader(responseBody));

            doc = builder.parse(is);

            Node HostCode = doc.getElementsByTagName("HostCode").item(0);
            String hostCode = HostCode.getTextContent();
            if (!hostCode.equals(Constants.NXTXN_SUCCESS_CODE)) {

                Node SystemDescription = doc.getElementsByTagName("SystemDescription").item(0);
                String systemDescription = SystemDescription.getTextContent();
                return new JsonResponseBody(null, Constants.FAILURE, Integer.parseInt(hostCode), systemDescription);
            }

            if (util.checkOtpStatus(userName)) {
                int otp = vediocon.createOtp(Constants.LOGIN_OTP_LENGTH);
//                String authCode = generateToken.getToken();
                String authCode = generateToken.getToken(userName);
                redisStorage.saveAuthCode(userName, util.encrypt(authCode), clientId, deviceInfo);

                Long currentTime = System.currentTimeMillis();

//                Token authToken = new Token(userName, clientId, null, Constants.US_TOKEN_TYPE[0], true, new Timestamp(currentTime), new Timestamp(new Long(Constants.AUTH_CODE_VALIDITY) * 1000 + currentTime), 0);
                Token authToken = new Token(userName, clientId, null, Constants.US_TOKEN_TYPE[0], true, 0, new Timestamp(new Long(Constants.AUTH_CODE_VALIDITY) * 1000 + currentTime), new Timestamp(currentTime), new Timestamp(currentTime), deviceInfo.getDeviceId(), deviceInfo.getDeviceOs(), deviceInfo.getDeviceOsVersion(), deviceInfo.getModelName(), deviceInfo.getImei(), deviceInfo.getIpAddress());
                authToken.setAuth_code(util.encrypt(authCode));
                tokenDao.save(authToken);

                OtpAuthorization otpAuth = new OtpAuthorization(userName, Constants.OTP_TYPE[2], otp, clientId, true, new Timestamp(new java.util.Date().getTime()), null);
                otpAuthorizationDao.save(otpAuth);

                redisStorage.saveOtp(userName, otp, 0);
                com.oxigenwallet.userservice.model.User userDetails = userDao.getByMdn(userName);
                String name = "";
                if (userDetails == null) {
                    name = userName;
                } else {
                    name = userDetails.getFirst_name();
                }
//                LoginOtp loginEvent = new LoginOtp(new Data(userName, otp, name), new Event(Constants.NOTIFICATION_EVENT_ID[0], Constants.NOTIFICATION_EVENT_NAME[0], Constants.NOTIFICATION_BUSINESS, Constants.NOTIFICATION_CHANNEL));
//                try {
//                    RabbitTemplate rabbitTemplate = messagingQueueFactory.getMessagingTemplate();
//                    rabbitTemplate.setExchange(otpExchange);
//                    rabbitTemplate.setRoutingKey(otpKey);
////                    System.out.println(mapper.writeValueAsString(loginEvent));
//                    Object rabbitMqResult = rabbitTemplate.convertSendAndReceive(mapper.writeValueAsString(loginEvent));
//                } catch (Exception ex) {
//                    ex.printStackTrace();
//                }
//                if (notificationServiceSwitch == 0) {
//                    vediocon.sendOtp(otp, userName);
//                }
//                Map<String, String> gg= new HashMap<>("", "");

                if (!sendSmsURL.equals("")) {
                    try {
                        String otpContent = "Your OTP for login is " + Integer.toString(otp);
                        Timestamp timestamp = new Timestamp(System.currentTimeMillis());
                        String postData = "{\"RequestId\":\"" + timestamp.getTime() + "\",\"Mobile\":\"" + userName + "\",\"Content\":\"" + otpContent + "\"}";
                        RestCalling restCalling = new RestCalling();
                        restCalling.postSMS(postData, sendSmsURL);
                    } catch (Exception ex) {
                        System.out.println(ex.getMessage());
                    }
                }

                com.oxigenwallet.userservice.common.json.response.TokenInfo tokenInfo
                        = new com.oxigenwallet.userservice.common.json.response.TokenInfo(userName, authCode, Constants.AUTH_CODE_VALIDITY + "", scope, Constants.US_TOKEN_TYPE[0], null);
                com.oxigenwallet.userservice.common.json.response.Response responseInfo
                        = new com.oxigenwallet.userservice.common.json.response.Response(null, tokenInfo, null, null, null, null);
                return new JsonResponseBody(responseInfo, Constants.SUCCESS, Constants.SUCCESSCODE, ResponseCodes.TOKEN_GENERATED);

            } else {
//                String accessToken = generateToken.getToken();
                String accessToken = generateToken.getToken(userName);
                RedisToken redisToken = new RedisToken(accessToken, clientId, expiresIn, scope, ip, deviceId, deviceOs, deviceOsVersion, modelName);
                return getTokenResponse(userName, redisToken, Constants.US_TOKEN_TYPE[1]);
            }
        }
        return new JsonResponseBody(null, Constants.FAILURE, ErrorCodes.INVALID_GRANT_TYPE.getCode(), ErrorCodes.INVALID_GRANT_TYPE.getDescription());
    }

    /**
     * @param userName   userName
     * @param redisToken redisToken
     * @param tokenType  tokenType
     * @return json
     */
    public JsonResponseBody getTokenResponse(String userName, RedisToken redisToken, String tokenType) {
        Map<String, String> tokenMap = new HashMap<String, String>();
        tokenMap.put("token", util.encrypt(redisToken.getToken()));
        tokenMap.put("scope", redisToken.getScope());
        tokenMap.put("time_stamp", Util.getTimeStamp());
        tokenMap.put("expires_in", redisToken.getExpireIn());
        tokenMap.put("client_id", redisToken.getClientId());
        tokenMap.put("ip", redisToken.getIp());
        tokenMap.put("device_id", redisToken.getDeviceId());
        tokenMap.put("device_os", redisToken.getDeviceOs());
        tokenMap.put("device_version", redisToken.getDeviceVersion());
        tokenMap.put("model_name", redisToken.getModelName());
//        System.out.println("Username : " + userName);
//        System.out.println("tokenMap : " + tokenMap);
//        System.out.println("redisToken.getClientId() : " + redisToken.getClientId());
//        System.out.println("redisToken.getExpireIn() : "  + (redisToken.getExpireIn()));
//        System.out.println("Expiry : "  + Integer.parseInt(redisToken.getExpireIn()));
        //redisStorage.saveAccessToken(userName, tokenMap, redisToken.getClientId(), Integer.parseInt(redisToken.getExpireIn()));
        Long currentTime = System.currentTimeMillis();
        Token token = new Token(userName, redisToken.getClientId(), util.encrypt(redisToken.getToken()), tokenType, true, new Timestamp(currentTime), new Timestamp(Long.parseLong(redisToken.getExpireIn()) * 1000 + currentTime));
        token.setIp_address(redisToken.getIp());
        token.setDevice_id(redisToken.getDeviceId());
        token.setDevice_os(redisToken.getDeviceOs());
        token.setDevice_os_version(redisToken.getDeviceVersion());
        token.setModel_name(redisToken.getModelName());
        tokenDao.save(token);

        com.oxigenwallet.userservice.common.json.response.TokenInfo tokenInfo = new com.oxigenwallet.userservice.common.json.response.TokenInfo(userName, redisToken.getToken(), redisToken.getExpireIn(), redisToken.getScope(), tokenType, getOxifaceToken(userName));
        Response responseInfo = new Response(null, tokenInfo, null, null, null, null);

        JsonResponseBody tokenResponse = new JsonResponseBody(responseInfo, Constants.SUCCESS, Constants.SUCCESSCODE, ResponseCodes.TOKEN_GENERATED);
        tokenResponse.setResponse(responseInfo);
        return tokenResponse;
    }

    /**
     * @param userName userName
     * @return string
     */
    public String getOxifaceToken(String userName) {
        String oxifaceToken = "";
        try {
            String oxifaceJsonRequest = mapper.writeValueAsString(oxifaceRequestHelper.getVerifyUserServicerequest(userName));

            ResponseEntity<String> oxifaceJsonResponse = restCalling.postOxiface(oxifaceJsonRequest);
            String oxifaceStringResponse = oxifaceJsonResponse.getBody();

            com.oxigenwallet.userservice.common.json.response.oxiface.Response oxifaceResponse
                    = mapper.readValue(oxifaceStringResponse, com.oxigenwallet.userservice.common.json.response.oxiface.Response.class);
            oxifaceToken = oxifaceResponse.getServiceResponse().getResponseInfo().getKey() == null ? "" : oxifaceResponse.getServiceResponse().getResponseInfo().getKey();
        } catch (Exception ex) {

        }
        return oxifaceToken;
    }

    /**
     * @param code        code
     * @param clientId    clientId
     * @param accessToken toke
     * @return json
     */
    @RequestMapping(
            value = {"/token"},
            method = RequestMethod.DELETE,
            produces = "application/json")
    public @ResponseBody
    JsonResponseBody tokenDelete(
            @RequestHeader(value = "code", required = true) String code,
            @RequestHeader(value = "client_id", required = true) String clientId,
            @RequestHeader(value = "Authorisation", required = true) String accessToken
    ) {

        //boolean deleteTokenResult = redisStorage.deleteAccessToken(util.encrypt(accessToken));
        try {
            tokenDao.updateOnDelete(util.encrypt(accessToken));
        } catch (Exception e) {
            e.printStackTrace();
        }
//        if (!deleteTokenResult) {
//            return new JsonResponseBody(null, Constants.FAILURE, ErrorCodes.INVALID_TOKEN.getCode(), ErrorCodes.INVALID_TOKEN.getDescription());
//        }
        return new JsonResponseBody(null, Constants.SUCCESS, Constants.SUCCESSCODE, ResponseCodes.TOKEN_DELETED);

    }

    /**
     * @param validateTokenRequest token
     * @param accessToken          token
     * @return json
     * @throws IOException                  Exception
     * @throws SAXException                 Exception
     * @throws ParserConfigurationException Exception
     */
    @RequestMapping(
            value = {"/validate-token"},
            method = RequestMethod.POST,
            consumes = {"application/json", "text/plain"},
            produces = "application/json")
    public @ResponseBody
    JsonResponseBody validateToken(
            @RequestBody String validateTokenRequest,
            @RequestHeader(value = "Authorisation", required = true) String accessToken
    ) throws IOException, SAXException, ParserConfigurationException {
        // parsing request in object
        JsonRequestBody request
                = mapper.readValue(validateTokenRequest, JsonRequestBody.class);

        // finding value of field required
        Codes validation = Validation.validateUsername(request.getRequest().getTokeninfo().getUsername());
        if (validation.getCode() != ErrorCodes.VALID.getCode()) {
            return new JsonResponseBody(null, Constants.FAILURE, validation.getCode(), validation.getDescription());
        }
        String userName = request.getRequest().getTokeninfo().getUsername();
        String type = request.getRequest().getTokeninfo().getType() == null ? "" : request.getRequest().getTokeninfo().getType();
        String merchantName = request.getRequest().getTokeninfo().getMerchantName() == null ? "" : request.getRequest().getTokeninfo().getMerchantName();
        DeviceInfo deviceInfo = request.getRequest().getDeviceInfo();

        String responseBody = "";
        //if not ekyc and not debit wallet then, merchantname is needed
        if (!type.equalsIgnoreCase(Constants.TOKEN_TYPE[3]) && (!type.equalsIgnoreCase(Constants.TOKEN_TYPE[0]) && merchantName != "")) {
            merchantName = "";
        }
        // cases handling
        // if token type is debit wallet and merchant not present 
        if (type.equalsIgnoreCase(Constants.TOKEN_TYPE[0]) && merchantName.equals("")) {
            return new JsonResponseBody(null, Constants.FAILURE, ErrorCodes.MERCHANTNAME_REQUIRED.getCode(), ErrorCodes.MERCHANTNAME_REQUIRED.getDescription());
        }
        // if token type is p2p and p2m
        if (Arrays.asList(Constants.TRUSTED_OPERATOR).contains(merchantName)) {
            return new JsonResponseBody(null, Constants.FAILURE, ErrorCodes.INVALID_OPERATOR_TYPE.getCode(), ErrorCodes.INVALID_OPERATOR_TYPE.getDescription());
        }

        System.out.println("AccessToken:" + accessToken);
//            boolean tokenRedisResult = redisStorage.isAccessTokenExist(userName, util.encrypt(accessToken));
        boolean tokenRedisResult = redisStorage.isAccessTokenExist(userName, accessToken);
        // reponse code if token in not found in redis and sql
        System.out.println("Token Result: " + tokenRedisResult);
        if (!tokenRedisResult) {
            return new JsonResponseBody(null, Constants.FAILURE, ErrorCodes.INVALID_TOKEN.getCode(), ErrorCodes.INVALID_TOKEN.getDescription());
        }


        if (type.equals("") && merchantName.equals("")) {
            return new JsonResponseBody(null, Constants.SUCCESS, Constants.SUCCESSCODE, ResponseCodes.TOKEN_VALIDATED);
        }

        String nxtxnRedisResult = redisStorage.getNxtxnToken(userName, merchantName);
//        System.out.println("Redis: " + nxtxnRedisResult.toString() );
        if (nxtxnRedisResult != null) {
            Response response = new Response(null, null, userName, nxtxnRedisResult, type, merchantName);
//            System.out.println("Response: " + response.toString());
            return new JsonResponseBody(response, Constants.SUCCESS, Constants.SUCCESSCODE, ResponseCodes.TOKEN_VALIDATED);
        }
        NxtxnToken nxtxnTokenDBResult = nxtxnTokenDao.getByUsernameTypeMerchantName(userName, type, merchantName);
        if (nxtxnTokenDBResult != null) {
            redisStorage.saveNxtxnToken(userName, nxtxnTokenDBResult.getToken(), nxtxnTokenDBResult.getToken_for());
            Response response = new Response(null, null, userName, nxtxnTokenDBResult.getToken(), nxtxnTokenDBResult.getToken_type(), nxtxnTokenDBResult.getToken_for());

//            System.out.println("Response2: " + response.toString());            
            return new JsonResponseBody(response, Constants.SUCCESS, Constants.SUCCESSCODE, ResponseCodes.TOKEN_VALIDATED);
        }

        String xmlRequest = tokenXml.tokenManagerXml(merchantName, userName, Constants.TOKEN_NXTXN_OPERATION[0]);
        ResponseEntity<String> xmlResponse = restCalling.postNxtxn(xmlRequest, tokenManagerNxtxnUrl);
        if (xmlResponse.getBody() == null) {
            return new JsonResponseBody(null, Constants.FAILURE, ErrorCodes.NXTXN_RESPONSE_ERROR.getCode(), ErrorCodes.NXTXN_RESPONSE_ERROR.getDescription());
        }
        responseBody = xmlResponse.getBody();
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder;
        InputSource is;
        Document doc = null;

        builder = factory.newDocumentBuilder();
        is = new InputSource(new StringReader(responseBody));

        doc = builder.parse(is);
//        System.out.println("Doc: " + doc.toString());
        Node HostCode = doc.getElementsByTagName("HostCode").item(0);
        String hostCode = HostCode.getTextContent();
//        System.out.println("hostcode: " + hostCode);
        if (hostCode.equals(Constants.NXTXN_SUCCESS_CODE)) {
            Node Data = doc.getElementsByTagName("Data").item(0);
            String nxtxnToken = Data.getTextContent();
//            System.out.println("Username : " + userName);
//            System.out.println("nxtxnToken : " + nxtxnToken);
//            System.out.println("type : " + type);
            //System.out.println("merchantName : " + merchantName);
            NxtxnToken nxtxnTokenData = nxtxnTokenDao.getNxtxnTokenByToken(nxtxnToken);
            //System.out.println("nxtxnTokenData : " + nxtxnTokenData.toString());
            if (nxtxnTokenData == null) {
                System.out.println("nxtxnToken data not found!! Saving token.");
                NxtxnToken nxtxnTokenObj = new NxtxnToken(userName, nxtxnToken, type, merchantName, true, new Timestamp(System.currentTimeMillis()));
                nxtxnTokenDao.save(nxtxnTokenObj);
                redisStorage.saveNxtxnToken(userName, nxtxnToken, merchantName);
            }
            Response response = new Response(null, null, userName, nxtxnToken, type, merchantName);

            return new JsonResponseBody(response, Constants.SUCCESS, Constants.SUCCESSCODE, ResponseCodes.TOKEN_VALIDATED);
        }
        Node SystemDescription = doc.getElementsByTagName("SystemDescription").item(0);
        String systemDescription = SystemDescription.getTextContent();
        return new JsonResponseBody(null, Constants.FAILURE, Integer.parseInt(hostCode), systemDescription);
    }
}