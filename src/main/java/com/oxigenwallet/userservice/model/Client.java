/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.oxigenwallet.userservice.model;

import java.sql.Timestamp;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

/**
 *
 * @author Asce
 */
@Entity
@Table(name = "client")
public class Client {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String display_name;
    private String client_id;
    private String client_secret;
    private String reference_email;
    private long reference_mobile;
    private String client_name;
    private boolean is_active;
    private Timestamp date_created;
    private Timestamp date_updated;

    @OneToMany(mappedBy = "client", cascade = CascadeType.ALL)
    @Fetch(FetchMode.JOIN)
    private Set<ClientAuthSpan> client_auth_span = new HashSet<ClientAuthSpan>(0);

    public Client() {
    }

    public Client(String display_name, String client_id, String client_secret, String reference_email, long reference_mobile, String client_name, boolean is_active, Timestamp date_created, Timestamp date_updated) {
        this.display_name = display_name;
        this.client_id = client_id;
        this.client_secret = client_secret;
        this.reference_email = reference_email;
        this.reference_mobile = reference_mobile;
        this.client_name = client_name;
        this.is_active = is_active;
        this.date_created = date_created;
        this.date_updated = date_updated;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDisplay_name() {
        return display_name;
    }

    public void setDisplay_name(String display_name) {
        this.display_name = display_name;
    }

    public String getClient_id() {
        return client_id;
    }

    public void setClient_id(String client_id) {
        this.client_id = client_id;
    }

    public String getClient_secret() {
        return client_secret;
    }

    public void setClient_secret(String client_secret) {
        this.client_secret = client_secret;
    }

    public String getReference_email() {
        return reference_email;
    }

    public void setReference_email(String reference_email) {
        this.reference_email = reference_email;
    }

    public long getReference_mobile() {
        return reference_mobile;
    }

    public void setReference_mobile(long reference_mobile) {
        this.reference_mobile = reference_mobile;
    }

    public String getClient_name() {
        return client_name;
    }

    public void setClient_name(String client_name) {
        this.client_name = client_name;
    }

    public boolean isIs_active() {
        return is_active;
    }

    public void setIs_active(boolean is_active) {
        this.is_active = is_active;
    }

    public Timestamp getDate_created() {
        return date_created;
    }

    public void setDate_created(Timestamp date_created) {
        this.date_created = date_created;
    }

    public Timestamp getDate_updated() {
        return date_updated;
    }

    public void setDate_updated(Timestamp date_updated) {
        this.date_updated = date_updated;
    }

    public Set<ClientAuthSpan> getClientAuthSpan() {
        return client_auth_span;
    }

    public void setClientAuthSpan(Set<ClientAuthSpan> client_auth_span) {
        this.client_auth_span = client_auth_span;
    }

}
