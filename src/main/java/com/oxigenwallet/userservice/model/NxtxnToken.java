/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.oxigenwallet.userservice.model;

import java.sql.Timestamp;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author Asce
 */
@Entity
@Table(name = "nxtxn_token")
public class NxtxnToken {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String username;
    private String token;
    private String token_type;
    private String token_for;
    private boolean is_active;
    private Timestamp date_created;

    /**
     *
     */
    public NxtxnToken() {
    }

    /**
     *
     * @param username
     * @param token
     * @param token_type
     * @param token_for
     * @param is_active
     * @param date_created
     */
    public NxtxnToken(String username, String token, String token_type, String token_for, boolean is_active, Timestamp date_created) {
        this.username = username;
        this.token = token;
        this.token_type = token_type;
        this.token_for = token_for;
        this.is_active = is_active;
        this.date_created = date_created;
    }

    /**
     *
     * @return
     */
    public Long getId() {
        return id;
    }

    /**
     *
     * @param id
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     *
     * @return
     */
    public String getUsername() {
        return username;
    }

    /**
     *
     * @param username
     */
    public void setUsername(String username) {
        this.username = username;
    }

    /**
     *
     * @return
     */
    public String getToken() {
        return token;
    }

    /**
     *
     * @param token
     */
    public void setToken(String token) {
        this.token = token;
    }

    /**
     *
     * @return
     */
    public String getToken_type() {
        return token_type;
    }

    /**
     *
     * @param token_type
     */
    public void setToken_type(String token_type) {
        this.token_type = token_type;
    }

    /**
     *
     * @return
     */
    public String getToken_for() {
        return token_for;
    }

    /**
     *
     * @param token_for
     */
    public void setToken_for(String token_for) {
        this.token_for = token_for;
    }

    /**
     *
     * @return
     */
    public boolean isIs_active() {
        return is_active;
    }

    /**
     *
     * @param is_active
     */
    public void setIs_active(boolean is_active) {
        this.is_active = is_active;
    }

    /**
     *
     * @return
     */
    public Timestamp getDate_created() {
        return date_created;
    }

    /**
     *
     * @param date_created
     */
    public void setDate_created(Timestamp date_created) {
        this.date_created = date_created;
    }

   
    
}
