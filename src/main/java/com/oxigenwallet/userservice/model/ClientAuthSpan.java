/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.oxigenwallet.userservice.model;

import java.sql.Timestamp;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 *
 * @author Asce
 */
@Entity
@Table(name = "client_auth_span")
public class ClientAuthSpan {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
//    private String client_id;
    private String access_type;
    private Timestamp date_created;
    private long expired_at;
    private boolean token_status;

    @ManyToOne
    @JoinColumn(name = "client_id")
    private Client client;

    public ClientAuthSpan() {
    }

    public ClientAuthSpan(String access_type, Timestamp date_created, long expired_at, boolean token_status) {
        
        this.access_type = access_type;
        this.date_created = date_created;
        this.expired_at = expired_at;
        this.token_status = token_status;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

//    public String getClient_id() {
//        return client_id;
//    }
//
//    public void setClient_id(String client_id) {
//        this.client_id = client_id;
//    }

    public String getAccess_type() {
        return access_type;
    }

    public void setAccess_type(String access_type) {
        this.access_type = access_type;
    }

    public Timestamp getDate_created() {
        return date_created;
    }

    public void setDate_created(Timestamp date_created) {
        this.date_created = date_created;
    }

    public long getExpired_at() {
        return expired_at;
    }

    public void setExpired_at(long expired_at) {
        this.expired_at = expired_at;
    }

    public boolean isToken_status() {
        return token_status;
    }

    public void setToken_status(boolean token_status) {
        this.token_status = token_status;
    }

    public Client getClient() {
        return client;
    }

    public void setClient(Client client) {
        this.client = client;
    }

}
