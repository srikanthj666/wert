/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.oxigenwallet.userservice.model;

import java.sql.Timestamp;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author sagarsharma
 */
@Entity
@Table(name = "token")
public class Token {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String username;
    private String client_id;
    private String auth_code;
    private String token;
    private String token_type;
    private boolean is_active;
    private int fail_count;
    private Timestamp expired_at;
    private Timestamp date_created;
    private Timestamp date_updated;

    private String device_id;
    private String device_os;
    private String device_os_version;
    private String model_name;
    private String imei;
    private String ip_address;

    public Token(String username, String client_id, String token, String token_type, boolean is_active, int fail_count, Timestamp expired_at, Timestamp date_created, Timestamp date_updated, String device_id, String device_os, String device_os_version, String model_name, String imei, String ip_address) {
        this.username = username;
        this.client_id = client_id;
        this.token = token;
        this.token_type = token_type;
        this.is_active = is_active;
        this.fail_count = fail_count;
        this.expired_at = expired_at;
        this.date_created = date_created;
        this.date_updated = date_updated;
        this.device_id = device_id;
        this.device_os = device_os;
        this.device_os_version = device_os_version;
        this.model_name = model_name;
        this.imei = imei;
        this.ip_address = ip_address;
    }

    /**
     *
     * @param userName
     * @param clientId
     * @param accessToken
     * @param tokenType
     * @param isActive
     * @param date
     */
    public Token(String userName, String clientId, String accessToken, String tokenType, boolean isActive, Timestamp date, Timestamp expired_at) {
        this.username = userName;
        this.client_id = clientId;
        this.token = accessToken;
        this.token_type = tokenType;
        this.is_active = isActive;
        this.date_created = date;
        this.expired_at = expired_at;
    }


    /**
     *
     * @param userName
     * @param clientId
     * @param accessToken
     * @param tokenType
     * @param isActive
     * @param date
     */
    public Token(String userName, String clientId, String accessToken, String tokenType, boolean isActive, Timestamp date, Timestamp expired_at, int fail_count) {
        this.username = userName;
        this.client_id = clientId;
        this.token = accessToken;
        this.token_type = tokenType;
        this.is_active = isActive;
        this.date_created = date;
        this.expired_at = expired_at;
        this.fail_count = fail_count;
    }    
    /**
     *
     */
    public Token() {

    }

    /**
     *
     * @return
     */
    public Long getId() {
        return id;
    }

    /**
     *
     * @return
     */
    public String getUsername() {
        return username;
    }

    /**
     *
     * @param username
     */
    public void setUsername(String username) {
        this.username = username;
    }

    /**
     *
     * @param id
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     *
     * @return
     */
    public String getClient_id() {
        return client_id;
    }

    /**
     *
     * @param client_id
     */
    public void setClient_id(String client_id) {
        this.client_id = client_id;
    }

    /**
     *
     * @return
     */
    public String getAuth_code() {
        return auth_code;
    }

    /**
     *
     * @param auth_code
     */
    public void setAuth_code(String auth_code) {
        this.auth_code = auth_code;
    }

    /**
     *
     * @return
     */
    public String getToken() {
        return token;
    }

    /**
     *
     * @param token
     */
    public void setToken(String token) {
        this.token = token;
    }

    /**
     *
     * @return
     */
    public String getToken_type() {
        return token_type;
    }

    /**
     *
     * @param token_type
     */
    public void setToken_type(String token_type) {
        this.token_type = token_type;
    }

    /**
     *
     * @return
     */
    public boolean isIs_active() {
        return is_active;
    }

    /**
     *
     * @param is_active
     */
    public void setIs_active(boolean is_active) {
        this.is_active = is_active;
    }

    public int getFail_count() {
        return fail_count;
    }

    public void setFail_count(int fail_count) {
        this.fail_count = fail_count;
    }

    /**
     *
     * @return
     */
    public Timestamp getExpired_at() {
        return expired_at;
    }

    /**
     *
     * @param expired_at
     */
    public void setExpired_at(Timestamp expired_at) {
        this.expired_at = expired_at;
    }

    /**
     *
     * @return
     */
    public Timestamp getDate_created() {
        return date_created;
    }

    /**
     *
     * @param date_created
     */
    public void setDate_created(Timestamp date_created) {
        this.date_created = date_created;
    }

    /**
     *
     * @return
     */
    public Timestamp getDate_updated() {
        return date_updated;
    }

    /**
     *
     * @param date_updated
     */
    public void setDate_updated(Timestamp date_updated) {
        this.date_updated = date_updated;
    }


    public String getDevice_id() {
        return device_id;
    }

    public void setDevice_id(String device_id) {
        this.device_id = device_id;
    }

    public String getDevice_os() {
        return device_os;
    }

    public void setDevice_os(String device_os) {
        this.device_os = device_os;
    }

    public String getModel_name() {
        return model_name;
    }

    public void setModel_name(String model_name) {
        this.model_name = model_name;
    }

    public String getImei() {
        return imei;
    }

    public void setImei(String imei) {
        this.imei = imei;
    }

    public String getIp_address() {
        return ip_address;
    }

    public void setIp_address(String ip_address) {
        this.ip_address = ip_address;
    }

    public String getDevice_os_version() {
        return device_os_version;
    }

    public void setDevice_os_version(String device_os_version) {
        this.device_os_version = device_os_version;
    }
}
