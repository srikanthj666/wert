/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.oxigenwallet.userservice.model;

import java.sql.Timestamp;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author mitz
 */
@Entity
@Table(name = "users")
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String username;
    private String first_name;
    private String middle_name;
    private String last_name;
    private String email;
    private String dob;
    private char gender;
    private String address;
    private String city;
    private String district;
    private Integer zip;
    private String state;
    private String country;
    private Integer status;
    private String landline;
    private Timestamp date_created;
    private Timestamp date_updated;
    private String profile_type;
    private String info_type;
    private String created_by;

    public String getPan_Verified() {
        return Pan_Verified;
    }

    public void setPan_Verified(String pan_Verified) {
        Pan_Verified = pan_Verified;
    }

    private  String Pan_Verified;

    public String getDocument() {
        return document;
    }

    public void setDocument(String document) {
        this.document = document;
    }

    private  String document;

    /**
     *
     * @return
     */
    public String getInfo_type() {
        return info_type;
    }

    /**
     *
     * @param info_type
     */
    public void setInfo_type(String info_type) {
        this.info_type = info_type;
    }

    /**
     *
     * @return
     */
    public String getProfile_type() {
        return profile_type;
    }

    /**
     *
     * @param profile_type
     */
    public void setProfile_type(String profile_type) {
        this.profile_type = profile_type;
    }

    /**
     *
     * @return
     */
    public Long getId() {
        return id;
    }

    /**
     *
     * @param id
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     *
     * @return
     */
    public String getUsername() {
        return username;
    }

    /**
     *
     * @param username
     */
    public void setUsername(String username) {
        this.username = username;
    }

    /**
     *
     * @return
     */
    public String getFirst_name() {
        return first_name;
    }

    /**
     *
     * @param first_name
     */
    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    /**
     *
     * @return
     */
    public String getMiddle_name() {
        return middle_name;
    }

    /**
     *
     * @param middle_name
     */
    public void setMiddle_name(String middle_name) {
        this.middle_name = middle_name;
    }

    /**
     *
     * @return
     */
    public String getLast_name() {
        return last_name;
    }

    /**
     *
     * @param last_name
     */
    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    /**
     *
     * @return
     */
    public String getEmail() {
        return email;
    }

    /**
     *
     * @param email
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     *
     * @return
     */
    public String getDob() {
        return dob;
    }

    /**
     *
     * @param dob
     */
    public void setDob(String dob) {
        this.dob = dob;
    }

    /**
     *
     * @return
     */
    public char getGender() {
        return gender;
    }

    /**
     *
     * @param gender
     */
    public void setGender(char gender) {
        this.gender = gender;
    }

    /**
     *
     * @return
     */
    public String getAddress() {
        return address;
    }

    /**
     *
     * @param address
     */
    public void setAddress(String address) {
        this.address = address;
    }

    /**
     *
     * @return
     */
    public String getCity() {
        return city;
    }

    /**
     *
     * @param city
     */
    public void setCity(String city) {
        this.city = city;
    }

    /**
     *
     * @return
     */
    public String getDistrict() {
        return district;
    }

    /**
     *
     * @param district
     */
    public void setDistrict(String district) {
        this.district = district;
    }

    /**
     *
     * @return
     */
    public Integer getZip() {
        return zip;
    }

    /**
     *
     * @param zip
     */
    public void setZip(Integer zip) {
        this.zip = zip;
    }

    /**
     *
     * @return
     */
    public String getState() {
        return state;
    }

    /**
     *
     * @param state
     */
    public void setState(String state) {
        this.state = state;
    }

    /**
     *
     * @return
     */
    public String getCountry() {
        return country;
    }

    /**
     *
     * @param country
     */
    public void setCountry(String country) {
        this.country = country;
    }

    /**
     *
     * @return
     */
    public Integer getStatus() {
        return status;
    }

    /**
     *
     * @param status
     */
    public void setStatus(Integer status) {
        this.status = status;
    }

    /**
     *
     * @return
     */
    public String getLandline() {
        return landline;
    }

    /**
     *
     * @param landline
     */
    public void setLandline(String landline) {
        this.landline = landline;
    }

    /**
     *
     * @return
     */
    public Timestamp getDate_created() {
        return date_created;
    }

    /**
     *
     * @param date_created
     */
    public void setDate_created(Timestamp date_created) {
        this.date_created = date_created;
    }

    /**
     *
     * @return
     */
    public Timestamp getDate_updated() {
        return date_updated;
    }

    /**
     *
     * @param date_updated
     */
    public void setDate_updated(Timestamp date_updated) {
        this.date_updated = date_updated;
    }

    /**
     *
     */
    public User() {
    }

    public String getCreated_by() {
        return created_by;
    }

    public void setCreated_by(String created_by) {
        this.created_by = created_by;
    }

}
