
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.oxigenwallet.userservice.common.json.request.JsonRequestBody;
import com.oxigenwallet.userservice.common.json.request.Request;
import com.oxigenwallet.userservice.common.json.request.User;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author Asce
 */
public class UserTest {
    //**************values to be changed*******************

    /**
     *
     * @throws JsonProcessingException
     */

    public void createUserTest() throws JsonProcessingException {
        String username = "919999635340";
        String code = "1234";
        String clientId = "12345";
        String name = "Muneer";
        String otp = "123456";
        String password = "123123";
        //*****************************************************

        JsonRequestBody request = new JsonRequestBody();
        Request requestInfo = new Request();
        User userInfo = new User();

        userInfo.setUsername(username);
        userInfo.setName(name);
        userInfo.setDob("01/01/1991");
        userInfo.setPassword(password);
        userInfo.setOtp(otp);

        requestInfo.setUser(userInfo);
        request.setRequest(requestInfo);

        ObjectMapper map = new ObjectMapper();
        String jsonRequest = map.writeValueAsString(request);

        final RestTemplate restTemplate = new RestTemplate();
        MultiValueMap<String, String> header = new LinkedMultiValueMap<String, String>();

        //requestHeaders.setAuthorization(httpAuthentication);
        header.add("Content-Type", "application/json");
        header.add("client_id", clientId);
        header.add("code", code);

        System.out.println(jsonRequest);
        final HttpEntity<String> requestEntity = new HttpEntity<String>(jsonRequest, header);
        ResponseEntity<String> result = restTemplate.exchange("http://localhost:8080/user", HttpMethod.PUT, requestEntity, String.class);
        System.out.println(result);
    }

    /**
     *
     * @throws JsonProcessingException
     */
    public void updateUserTest() throws JsonProcessingException {
        String username = "919999635340";
        String code = "1234";
        String clientId = "12345";
        String name = "Muneer";
        String accessToken = "qwerty";

        //*****************************************************
        JsonRequestBody request = new JsonRequestBody();
        Request requestInfo = new Request();
        User userInfo = new User();

        userInfo.setUsername(username);
        userInfo.setName(name);
        userInfo.setDob("01/01/1991");

        requestInfo.setUser(userInfo);
        request.setRequest(requestInfo);

        ObjectMapper map = new ObjectMapper();
        String jsonRequest = map.writeValueAsString(request);

        final RestTemplate restTemplate = new RestTemplate();
        MultiValueMap<String, String> header = new LinkedMultiValueMap<String, String>();

        //requestHeaders.setAuthorization(httpAuthentication);
        header.add("Content-Type", "application/json");
        header.add("client_id", clientId);
        header.add("code", code);
        header.add("Authorisation", accessToken);

        System.out.println(jsonRequest);
        final HttpEntity<String> requestEntity = new HttpEntity<String>(jsonRequest, header);
        ResponseEntity<String> result = restTemplate.exchange("http://localhost:8080/user", HttpMethod.POST, requestEntity, String.class);
        System.out.println(result);
    }

    /**
     *
     * @throws JsonProcessingException
     */
    public void getUserLiteInfoTest() throws JsonProcessingException {
        String username = "919999635340";
        String code = "1234";
        String clientId = "12345";
        String info = "lite";
        String accessToken = "qwerty";

        //*****************************************************
        final RestTemplate restTemplate = new RestTemplate();
        MultiValueMap<String, String> header = new LinkedMultiValueMap<String, String>();

        //requestHeaders.setAuthorization(httpAuthentication);
        header.add("Content-Type", "application/json");
        header.add("client_id", clientId);
        header.add("code", code);
        header.add("Authorisation", accessToken);

        UriComponentsBuilder params = UriComponentsBuilder.fromHttpUrl("http://localhost:8080/user")
                .queryParam("username", username)
                .queryParam("info", info);

        final HttpEntity<String> requestEntity = new HttpEntity<String>(header);
        ResponseEntity<String> result = restTemplate.exchange(
                params.build().encode().toUri(),
                HttpMethod.GET,
                requestEntity,
                String.class);
        System.out.println(result);
    }

//    @Test

    /**
     *
     * @throws JsonProcessingException
     */
    public void getUserFullInfoTest() throws JsonProcessingException {
        String username = "919999635340";
        String code = "1234";
        String clientId = "1234";
        String info = "full";
        String accessToken = "qwerty";

        //*****************************************************
        final RestTemplate restTemplate = new RestTemplate();
        MultiValueMap<String, String> header = new LinkedMultiValueMap<String, String>();

        //requestHeaders.setAuthorization(httpAuthentication);
        header.add("Content-Type", "application/json");
        header.add("client_id", clientId);
        header.add("code", code);
        header.add("Authorisation", accessToken);

        UriComponentsBuilder params = UriComponentsBuilder.fromHttpUrl("http://localhost:8080/user")
                .queryParam("username", username)
                .queryParam("info", info);

        final HttpEntity<String> requestEntity = new HttpEntity<String>(header);
        ResponseEntity<String> result = restTemplate.exchange(
                params.build().encode().toUri(),
                HttpMethod.GET,
                requestEntity,
                String.class);
        System.out.println(result);
    }
}
