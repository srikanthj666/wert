
import com.fasterxml.jackson.databind.ObjectMapper;
import com.oxigenwallet.userservice.common.CustomException;
import com.oxigenwallet.userservice.common.json.request.JsonRequestBody;
import com.oxigenwallet.userservice.common.json.request.Request;
import com.oxigenwallet.userservice.common.json.request.TokenInfo;
import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.SignatureException;
import javax.xml.parsers.ParserConfigurationException;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import org.xml.sax.SAXException;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author Asce
 */
public class TokenTest {

    /**
     *
     * @throws IOException
     * @throws SignatureException
     * @throws NoSuchAlgorithmException
     * @throws InvalidKeyException
     * @throws SAXException
     * @throws ParserConfigurationException
     * @throws CustomException
     */
//    @Test
    public void tokenPostTest1() throws IOException, SignatureException, NoSuchAlgorithmException, InvalidKeyException, SAXException, ParserConfigurationException, CustomException {

        //**************values to be changed*******************
       
        String username = "919999635340";
        String password = "123123";
        String code = "12312334";
        String clientId = "1234";
        String grantType = "password";
        String scope = "OW";
        String redirectUri = "";
        //*****************************************************
        
        
        JsonRequestBody request = new JsonRequestBody();
        Request requestInfo = new Request();
        TokenInfo tokenInfo = new TokenInfo();

        tokenInfo.setGrantType(grantType);
        tokenInfo.setPassword(password);
        tokenInfo.setUsername(username);
        tokenInfo.setScope(scope);
        tokenInfo.setRedirectUri(redirectUri);

        requestInfo.setTokeninfo(tokenInfo);
        request.setRequest(requestInfo);

        ObjectMapper map = new ObjectMapper();
        String jsonRequest = map.writeValueAsString(request);

        final RestTemplate restTemplate = new RestTemplate();
        MultiValueMap<String, String> header = new LinkedMultiValueMap<String, String>();

        //requestHeaders.setAuthorization(httpAuthentication);
        header.add("Content-Type", "application/json");
        header.add("client_id",clientId );
        header.add("code", code);

        System.out.println(jsonRequest);
        final HttpEntity<String> requestEntity = new HttpEntity<String>(jsonRequest, header);
        ResponseEntity<String> result = restTemplate.exchange("http://localhost:8080/token", HttpMethod.POST, requestEntity, String.class);
        System.out.println(result);
    }

//    @Test

    /**
     *
     * @throws IOException
     * @throws SignatureException
     * @throws NoSuchAlgorithmException
     * @throws InvalidKeyException
     * @throws SAXException
     * @throws ParserConfigurationException
     * @throws CustomException
     */
    public void tokenDeleteTest1() throws IOException, SignatureException, NoSuchAlgorithmException, InvalidKeyException, SAXException, ParserConfigurationException, CustomException {

        //**************values to be changed*******************
       
        String code = "12312334";
        String clientId = "1234";
        String accessToken = "ceb3b378-d091-3e05-919c-fb5b313cf12f";
        //*****************************************************
        final RestTemplate restTemplate = new RestTemplate();
        MultiValueMap<String, String> header = new LinkedMultiValueMap<String, String>();

        //requestHeaders.setAuthorization(httpAuthentication);
        header.add("Content-Type", "application/json");
        header.add("client_id", clientId);
        header.add("code", code);
        header.add("Authorisation", accessToken);

        final HttpEntity<String> requestEntity = new HttpEntity<String>(null, header);
        ResponseEntity<String> result = restTemplate.exchange("http://localhost:8080/token", HttpMethod.DELETE, requestEntity, String.class);
        System.out.println(result);
    }
    
    
//     @Test

    /**
     *
     * @throws IOException
     * @throws SignatureException
     * @throws NoSuchAlgorithmException
     * @throws InvalidKeyException
     * @throws SAXException
     * @throws ParserConfigurationException
     * @throws CustomException
     */
    public void validateTokenPostTest1() throws IOException, SignatureException, NoSuchAlgorithmException, InvalidKeyException, SAXException, ParserConfigurationException, CustomException {
        
        //**************values to be changed*******************
        String username = "919999635340"; 
        String accessToken = "6d95ae4e-e339-3046-9d34-cbd628a9a34a";
        String operator = "oxigen";
        //*****************************************************
        
        JsonRequestBody request = new JsonRequestBody();
        Request requestInfo = new Request();
        TokenInfo tokenInfo = new TokenInfo();

        tokenInfo.setType("");
        tokenInfo.setMerchantName(operator);
        tokenInfo.setUsername(username);


        requestInfo.setTokeninfo(tokenInfo);
        request.setRequest(requestInfo);

        ObjectMapper map = new ObjectMapper();
        String jsonRequest = map.writeValueAsString(request);

        final RestTemplate restTemplate = new RestTemplate();
        MultiValueMap<String, String> header = new LinkedMultiValueMap<String, String>();

        //requestHeaders.setAuthorization(httpAuthentication);
        header.add("Content-Type", "application/json");
        header.add("Authorisation",accessToken );

        System.out.println(jsonRequest);
        final HttpEntity<String> requestEntity = new HttpEntity<String>(jsonRequest, header);
        ResponseEntity<String> result = restTemplate.exchange("http://localhost:8080/validate-token", HttpMethod.POST, requestEntity, String.class);
        System.out.println(result);
    }
}
