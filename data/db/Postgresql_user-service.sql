CREATE TABLE token (
   id BIGSERIAL  NOT NULL,
   username varchar(12) NOT NULL,
   client_id varchar(100) NOT NULL,
   auth_code varchar(100) DEFAULT NULL,
   token varchar(100)  NULL,
   token_type varchar(50) NOT NULL, 
   is_active boolean DEFAULT true,
   expired_at timestamp NOT NULL,
   date_created timestamp DEFAULT now(),
   date_updated timestamp  NULL ,
   PRIMARY KEY(id)
); 


CREATE TABLE nxtxn_token (
  id BIGSERIAL  NOT NULL,
  username varchar(12) NOT NULL,
  token varchar(50) NOT NULL,
  token_type varchar(50) NOT NULL DEFAULT '',
  token_for varchar(50) NOT NULL DEFAULT '',
  is_active boolean DEFAULT true,
  date_created timestamp DEFAULT now(),
  unique (username, token_type,token_for),
  PRIMARY KEY(id)
);



CREATE TABLE client (
  id BIGSERIAL  NOT NULL,
  display_name varchar(100) DEFAULT NULL,
  client_id varchar(100) NOT NULL,
  client_secret varchar(100) NOT NULL,
  reference_email varchar(100) DEFAULT NULL,
  reference_mobile bigint  NOT NULL,
  client_name varchar(100) DEFAULT NULL,
  is_active boolean DEFAULT true,
  date_created timestamp DEFAULT now(),
  date_updated timestamp  NULL ,
  UNIQUE(client_id),
  PRIMARY KEY(id)
);

CREATE TABLE users (
   id BIGSERIAL  NOT NULL,
   username varchar(12) NOT NULL,
   first_name varchar(100) DEFAULT NULL,
   middle_name varchar(100) DEFAULT NULL,
   last_name varchar(100) DEFAULT NULL,
   email varchar(100) DEFAULT NULL,
   dob varchar(20) DEFAULT NULL,
   gender character DEFAULT NULL,
   address varchar(200) DEFAULT NULL,
   city varchar(50) DEFAULT NULL,
   district varchar(50) DEFAULT NULL,
   zip bigint DEFAULT NULL,
   state varchar(50) DEFAULT NULL,
   country varchar(50) DEFAULT NULL,
   status smallint DEFAULT 0,
   landline varchar(12) DEFAULT NULL,
   profile_type varchar(20)  DEFAULT NULL,
   info_type varchar(20) DEFAULT NULL,
   created_by varchar(100) NULL, 
   date_created timestamp DEFAULT now(),
   date_updated timestamp  NULL ,
   PRIMARY KEY(id)
);


CREATE TABLE client_auth_span (
   id BIGSERIAL  NOT NULL,
   client_id bigint NOT NULL REFERENCES client,
   access_type varchar(100) NOT NULL,
   date_created  timestamp DEFAULT CURRENT_TIMESTAMP,
   expired_at bigint NOT NULL,
   token_status boolean NOT NULL DEFAULT TRUE,
   unique (client_id, access_type),
   PRIMARY KEY(id)
);

CREATE TABLE login_event_logs (
   id BIGSERIAL  NOT NULL,
   username varchar(12) NOT NULL,
   type smallint NOT NULL, 
   imei varchar(50) DEFAULT NULL,
   device_os varchar(20),
   device_os_version varchar(50) DEFAULT NULL,
   device_id varchar(100) DEFAULT NULL,
   lat varchar(100) DEFAULT NULL,
   lng varchar(100) DEFAULT NULL,
   model_name varchar(100) DEFAULT NULL,
   ip_address inet DEFAULT NULL,
   status smallint NOT NULL,
   date_created timestamp DEFAULT now(),
   PRIMARY KEY(id)
);

CREATE TABLE otp_authorization (
   id BIGSERIAL  NOT NULL,
   username varchar(12) NOT NULL,
   type varchar(20) NOT NULL,
   code integer NOT NULL,
   client_id varchar(100) NOT NULL,
   is_active boolean DEFAULT true,
   date_created timestamp DEFAULT now(),
   date_updated timestamp  NULL,
   PRIMARY KEY(id)
);

CREATE TABLE otp_event_logs (
   id BIGSERIAL  NOT NULL,
   username varchar(12) NOT NULL,
   type varchar(20) NOT NULL,	
   code integer NOT NULL,
   expired_at bigint DEFAULT NULL,
   device_os varchar(20)  NULL,
   imei varchar(50)  NULL,
   lat varchar(100)  NULL,
   lng varchar(100)  NULL,
   model_name varchar(100)  NULL,
   ip_address inet DEFAULT NULL,
   device_os_version varchar(50)  NULL,
   device_id varchar(20)  NULL,
   date_created timestamp DEFAULT now(),
   PRIMARY KEY(id)
);

CREATE TABLE user_authorization (
   id BIGSERIAL  NOT NULL,
   username varchar(12) NOT NULL,
   auth_code varchar(50) DEFAULT NULL,
   redirect_uris varchar(256) DEFAULT NULL,
   current_state varchar(256) DEFAULT NULL,
   client_id varchar(100) NOT NULL,
   is_processed boolean DEFAULT false,
   token_type varchar(50) NOT NULL,
   date_created timestamp DEFAULT now(),
   date_updated timestamp  NULL,
   PRIMARY KEY(id)
);

CREATE TABLE user_event_logs (
   id BIGSERIAL  NOT NULL,
   username varchar(12) NOT NULL,
   type boolean NOT NULL,
   token varchar(50) NOT NULL,
   first_name varchar(100) DEFAULT NULL,
   middle_name varchar(100) DEFAULT NULL,
   last_name varchar(100) DEFAULT NULL,
   email varchar(100) DEFAULT NULL,
   dob varchar(20) DEFAULT NULL,
   gender character DEFAULT NULL,
   address varchar(200) DEFAULT NULL,
   city varchar(50) DEFAULT NULL,
   district varchar(50) DEFAULT NULL,
   zip integer DEFAULT NULL,
   state varchar(50) DEFAULT NULL,
   country varchar(50) DEFAULT NULL,
   landline bigint DEFAULT NULL,
   device_os varchar(20)  NULL,
   imei varchar(50)  NULL,
   lat varchar(100)  NULL,
   lng varchar(100)  NULL,
   model_name varchar(100)  NULL,
   ip_address inet DEFAULT NULL,
   device_os_version varchar(50)  NULL,
   device_id varchar(20)  NULL,
   date_created timestamp DEFAULT now(),
   PRIMARY KEY(id)
);

-- CREATE OR REPLACE FUNCTION log_last_name_changes()
--   RETURNS trigger AS
-- $BODY$
-- BEGIN
--  IF NEW.last_name <> OLD.last_name THEN
--  INSERT INTO employee_audits(employee_id,last_name,changed_on)
--  VALUES(OLD.id,OLD.last_name,now());
--  END IF;
--  
--  RETURN NEW;
-- END;
-- $BODY$
-- 
-- 
-- 
-- CREATE TRIGGER last_name_changes
--   BEFORE UPDATE
--   ON employees
--   FOR EACH ROW
--   EXECUTE PROCEDURE log_last_name_changes();
--  ********************** Alter Scripts ************************************ 

ALTER TABLE users ADD COLUMN created_by varchar(100) NULL;
ALTER TABLE token ALTER COLUMN token DROP NOT NULL;
ALTER TABLE otp_authorization ALTER COLUMN date_created SET NOT NULL;
ALTER TABLE otp_authorization ALTER COLUMN date_updated DROP NOT NULL;
ALTER TABLE client_auth_span ADD COLUMN token_status boolean NOT NULL DEFAULT TRUE;
ALTER TABLE token ALTER COLUMN token TYPE varchar(100);
ALTER TABLE token ALTER COLUMN auth_code TYPE varchar(100);
ALTER TABLE token ADD COLUMN fail_count smallint NOT NULL DEFAULT 0;


-- INSERT INTO client_auth_span (client_id,access_type,expired_at,token_status) VALUES(1,'OW',40000000,true);
-- INSERT INTO client (display_name,client_id,client_secret,reference_email,reference_mobile,client_name,is_active) 
-- VALUES('OW_ANDROID_7.75','OWAPPANDROID775','aabbccdd','ritesh@gmail.com',919999635340,'OW',true);